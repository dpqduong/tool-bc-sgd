var pref_url="/phuong";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenDuong',title:'Tên phường'},
			{name:'tenKhuVuc',title:'Tên khu vực'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-1" rid="'+obj.idDuong+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idDuong+'" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'70px'}}
		]
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#cmb-donvi-filter').change(function(){
		layDanhSachKv(1);
	});
	
	$('#cmb-donvi').change(function(){
		layDanhSachKv(2);
	});
	
	$('#cmb-khuvuc-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	initForm();
});

function check(){
	if($('#txt-tenDuong').val()===''){
		showError('Thông báo','Vui lòng nhập tên phường!');
		return false;
	}
	
	if($('#cmb-donvi').val()==null){
		showError('Thông báo','Vui lòng chọn đơn vị!');
		return false;
	}
	
	if($('#cmb-khuvuc').val()==null){
		showError('Thông báo','Vui lòng chọn khu vực!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
			$('#box-ds').showLoading();
		},
		success:function(data){
			var donvi=$.map(data,function(obj){
				obj.id=obj.idDonVi;
				obj.text=obj.tenDonVi;
				return obj;
			});
			
			$('#cmb-donvi,#cmb-donvi-filter').select2({
				data: donvi
			});
			
			if($('#cmb-donvi-filter').val()!=null)
				layDanhSachKv(0);
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachKv(m){
	var id=m==0||m==1?$('#cmb-donvi-filter').val():$('#cmb-donvi').val();
	$.ajax({
		url:pref_url+'/lay-ds-khu-vuc',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(m==0){
				var khuvuc=$.map(data,function(obj){
					obj.id=obj.idKhuVuc;
					obj.text=obj.tenKhuVuc;
					return obj;
				});
				
				$('#cmb-khuvuc-filter,#cmb-khuvuc').select2({
					data: khuvuc
				});
				
				if($('#cmb-khuvuc-filter').val()!=null)
					layDanhSach(0);
				
			}else if(m==1){
				var khuvuc=$.map(data,function(obj){
					obj.id=obj.idKhuVuc;
					obj.text=obj.tenKhuVuc;
					return obj;
				});
				$('#cmb-khuvuc-filter option').remove();
				$('#cmb-khuvuc-filter').select2({
					data: khuvuc
				});
				if($('#cmb-khuvuc-filter').val()!=null)
					layDanhSach(0);
				
			}else{
				var khuvuc=$.map(data,function(obj){
					obj.id=obj.idKhuVuc;
					obj.text=obj.tenKhuVuc;
					return obj;
				});
				$('#cmb-khuvuc option').remove();
				$('#cmb-khuvuc').select2({
					data: khuvuc
				});
				
				if(clgt!==undefined){
					$('#cmb-khuvuc').val(clgt).trigger('change');
					clgt=undefined;
				}
			}
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idKhuVuc:$('#cmb-khuvuc-filter').val(),thongTinCanTim:$('#txt-keyword').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	$('#txt-idDuong').val('');
	//$('#cmb-donvi').val($("#cmb-donvi option:first").val()).trigger('change');
	$('#txt-tenDuong').val('').focus();
}

var clgt=undefined;
function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
			$('#box-ds').showLoading();
		},
		success:function(data){
			clgt=data.idKhuVuc;
			$('#txt-idDuong').val(data.idDuong);
			$('#cmb-donvi').val(data.idDonVi).trigger('change');
			$('#txt-tenDuong').val(data.tenDuong);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
			$('#box-ds').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}