var pref_url="/khach-hang";
var giatri;

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'thuTu',title:'Thứ tự'},
			{name:'maDongHo',title:'Mã đồng hồ'},
			{name:'maKhachHang',title:'Mã KH'},
			{name:'tenKhachHang',title:'Tên KH'},
			{name:'diaChiSuDung',title:'Địa chỉ'},
			{name:'soDienThoai',title:'Điện thoại'},
			{name:'tenDoiTuong',title:'Đối tượng'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a data-toggle="modal" data-target="#modForm" href="#" class="row-clock-1" rid="'+obj.idKhachHang+'" title="Quản lý đồng hồ nước"><i class="fas fa-clock"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modPrint" href="#" class="row-print-1" rid="'+obj.idThongTinKhachHang+'" title="In hóa đơn nước"><i class="fa fa-print"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="row-edit-1" rid="'+obj.idThongTinKhachHang+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idThongTinKhachHang+'" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'110px'}}
		],
		height:'645px'
	});
	
	$('#grid-detail').jdGrid({
		columns:[
			{name:'maDongHo',title:'Mã đồng hồ'},
			{name:'chungLoai',title:'Chủng loại'},
			{name:'soSeri',title:'Số seri'},
			{name:'kichCo',title:'Kích cỡ'},
			{name:'heSoNhan',title:'Hệ số nhân'},
			{name:'chiSoCucDai',title:'Chỉ số cực đại'},
			{name:'nhaSanXuat',title:'Nhà sản xuất'},
			{name:'ngaySanXuat',title:'Ngày sản xuất',type:'interval'},
			{name:'ngayDuaVaoSuDung',title:'Ngày đưa vào sử dụng',type:'interval'},
			{name:'ngayHetHanBaoHanh',title:'Ngày hết hạn bảo hành',type:'interval'},
			{name:'ngayKiemDinh',title:'Ngày kiểm định',type:'interval'},
			{name:'ngaySuaChuaLanCuoi',title:'Ngày sửa chữa lần cuối',type:'interval'},
			{name:'ghiChuSuaChuaLanCuoi',title:'Ghi chú sửa chữa lần cuối'},
			{name:'tenTrangThaiDongHo',title:'Trạng thái'},
			{name:'thangSuDung',title:'Tháng sử dụng'}
		],
		height:'120px'
	});
	
	$('.input-mask').inputmask();
	
	$('.chon-ngay').datepicker({
		autoclose: true,
		todayHighlight:true,
		format:'dd/mm/yyyy',
		zIndexOffset:10000
    });//.datepicker('setDate', new Date());
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#page-detail').jdPage({
		onPageChanged:function(p){
			layDanhSachDetail(p-1);
		}
	});
	
	$('#modPrint').on('shown.bs.modal',function(e){
		var urlp = pref_url+'/xem-bao-cao?id='+$(e.relatedTarget).attr('rid');
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-modPrint').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-modPrint').hideLoading();
			}
	    });
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#cmb-phuong-filter').change(function(){
		layDsTo();
	});
	
	$('#cmb-phuong').change(function(){
		layDsToTT();
	});
	
	$('#cmb-to-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#txt-keyword').keypress(function(e){
        if(e.which == 13){
            $('#btn-search').click();
        }
    });
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#txt-ho, #txt-ten').change(function(){
		$('#txt-tenkhachhang').val($('#txt-ho').val()+' '+$('#txt-ten').val());
	});
	
	$('#txt-diachisudung').change(function(){
		if($.trim($('#txt-diachithanhtoan').val()) == '')
			$('#txt-diachithanhtoan').val($('#txt-diachisudung').val());
	});
	
	$('#saoChep').click(function(){
		$('#txt-diachithanhtoan').val($('#txt-diachisudung').val());
	});
	
	$('#modForm').on('shown.bs.modal',function(e){
		layChiTietDongHo($(e.relatedTarget).attr('rid'));
		layDsTrangThaiDongHo();
		// layDanhSachDetail($(e.relatedTarget).attr('rid'),0);
	});
	
	$('#kiemTra').click(function(){
		if($.trim($('#txt-madongho').val())==='')
			showError('Thông báo','Vui lòng nhập mã đồng hồ!');
		else{
			kiemTraDongHo($('#txt-madongho').val(),$('#txt-idkhachhang-dh').val());
		}
	});
	
	$('#txt-madongho').keypress(function(e){
        if(e.which == 13){
            $('#kiemTra').click();
        }
    });
	
	$('#btn-cancel-2').click(function(){
		if($.trim($('#txt-iddongho').val())==='')
			showError('Thông báo','Không có đồng hồ để xóa!');
		else{
			showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoaDongHo('+$('#txt-iddongho').val()+','+$('#txt-idkhachhang-dh').val()+')');
		}
	});
	
	$('#btn-ok-2').click(function(){
		if($.trim($('#txt-madongho').val())==='')
			showError('Thông báo','Vui lòng nhập mã đồng hồ!');
		else{
			luuDongHo();
		}
	});
	
	layDsPhuong();
	layDsPhuongTT();
	layDsNganHang();
	layDsDoiTuong();
});

function check(){
	if($('#txt-ho').val()===''){
		showError('Thông báo','Vui lòng nhập họ khách hàng!');
		return false;
	}
	if($('#txt-ten').val()===''){
		showError('Thông báo','Vui lòng nhập tên khách hàng!');
		return false;
	}
	if($('#txt-diachisudung').val()===''){
		showError('Thông báo','Vui lòng nhập địa chỉ sử dụng!');
		return false;
	}
	if($('#txt-diachithanhtoan').val()===''){
		showError('Thông báo','Vui lòng nhập địa chỉ thanh toán!');
		return false;
	}
	return true;
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{idDuong:$('#cmb-phuong-filter').val(),idSoGhi:$('#cmb-to-filter').val(),thongTinCanTim:$.trim($('#txt-keyword').val()),page:p},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachDetail(id,p){
	$.ajax({
		url:pref_url+'/lay-ds-detail',
		method:'get',
		dataType:'json',
		data:{id:id,page:p},
		beforeSend:function(){
			$('#box-form-2').showLoading();
		},
		success:function(data){
			$('#grid-detail').data('jdgrid').fillData(data.content);
			$('#page-detail').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form-2').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	$('#frm-1')[0].reset();
	$('#txt-idthongtinkhachhang').val('');
	layDsPhuongTT();
	layDsNganHang();
	layDsDoiTuong();
	// $('.chon-ngay').datepicker('setDate', new Date());
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#txt-idthongtinkhachhang').val(data.idThongTinKhachHang);
			$('#txt-makhachhang').val(data.maKhachHang);
			$('#txt-thutu').val(data.thuTu);
			$('#txt-ho').val(data.ho);
			$('#txt-ten').val(data.ten);
			$('#txt-tenkhachhang').val(data.tenKhachHang);
			$('#txt-diachisudung').val(data.diaChiSuDung);
			$('#txt-diachithanhtoan').val(data.diaChiThanhToan);
			$('#txt-sodienthoai').val(data.soDienThoai);
			$('#txt-socmnd').val(data.soCmnd);
			$('#txt-masothue').val(data.maSoThue);
			$('#txt-sotaikhoan').val(data.soTaiKhoan);
			$('#txt-tentaikhoan').val(data.tenTaiKhoan);
			$('#cmb-nganhang').val(data.idNganHang).trigger('change');
			giatri = data.idSoGhi;
			$('#cmb-phuong').val(data.idDuong).trigger('change');
			// $('#cmb-to').val(data.idSoGhi).trigger('change');
			$('#txt-sohopdong').val(data.soHopDong);
			$('#txt-ngaylaphopdong').datepicker('setDate', data.ngayLapHopDong==null?'':new Date(data.ngayLapHopDong));
			$('#txt-ngaydangky').datepicker('setDate', data.ngayDangKy==null?'':new Date(data.ngayDangKy));
			$('#txt-ngaylapdat').datepicker('setDate', data.ngayLapDat==null?'':new Date(data.ngayLapDat));
			$('#cmb-doituong').val(data.idDoiTuong).trigger('change');
			$('#chk-thupbvmt').prop('checked', data.thuPbvmt);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDsPhuong(){
	$.ajax({
		url:pref_url+'/lay-ds-phuong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var phuong=$.map(data,function(obj){
				obj.id=obj.idDuong;
				obj.text=obj.tenDuong;
				return obj;
			});
			
			$('#cmb-phuong-filter').select2({
				data: phuong
			});
			
			layDsTo();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsTo(){
	$.ajax({
		url:pref_url+'/lay-ds-to',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-phuong-filter').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-to-filter').html('<option value="0">-- Tất cả các tổ --</option>');
			
			var to=$.map(data,function(obj){
				obj.id=obj.idSoGhi;
				obj.text=obj.tenSoGhi;
				return obj;
			});
			
			$('#cmb-to-filter').select2({
				data: to
			});
			
			layDanhSach(0);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tổ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsNganHang(){
	$.ajax({
		url:pref_url+'/lay-ds-ngan-hang',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var nganhang=$.map(data,function(obj){
				obj.id=obj.idNganHang;
				obj.text=obj.tenNganHang;
				return obj;
			});
			
			$('#cmb-nganhang').select2({
				data: nganhang
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách ngân hàng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsDoiTuong(){
	$.ajax({
		url:pref_url+'/lay-ds-doi-tuong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var doituong=$.map(data,function(obj){
				obj.id=obj.idDoiTuong;
				obj.text=obj.tenDoiTuong;
				return obj;
			});
			
			$('#cmb-doituong').select2({
				data: doituong
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách đối tượng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsPhuongTT(){
	$.ajax({
		url:pref_url+'/lay-ds-phuong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var phuong=$.map(data,function(obj){
				obj.id=obj.idDuong;
				obj.text=obj.tenDuong;
				return obj;
			});
			
			$('#cmb-phuong').select2({
				data: phuong
			});
			
			layDsToTT();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsToTT(){
	$.ajax({
		url:pref_url+'/lay-ds-to',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-phuong').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-to option').remove();
			
			var to=$.map(data,function(obj){
				obj.id=obj.idSoGhi;
				obj.text=obj.tenSoGhi;
				return obj;
			});
			
			$('#cmb-to').select2({
				data: to
			});
			
			if($('#cmb-to').val()!=null && giatri!==undefined){
				$('#cmb-to').val(giatri).trigger('change');
				giatri=undefined;
			}
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tổ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsTrangThaiDongHo(){
	$.ajax({
		url:pref_url+'/lay-ds-trang-thai-dong-ho',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form-2').showLoading();
		},
		success:function(data){
			var trangthaidongho=$.map(data,function(obj){
				obj.id=obj.idTrangThaiDongHo;
				obj.text=obj.tenTrangThaiDongHo;
				return obj;
			});
			
			$('#cmb-trangthaidongho').select2({
				data: trangthaidongho
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách trạng thái đồng hồ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form-2').hideLoading();
		}
	});
}

function layChiTietDongHo(idkh){
	$.ajax({
		url:pref_url+'/lay-ct-dong-ho',
		method:'get',
		dataType:'json',
		data:{id:idkh},
		beforeSend:function(){
			$('#box-frm-2').showLoading();
		},
		success:function(data){
			if(data.maDongHo!=null){
				$('#txt-idkhachhang-dh').val(idkh);
				$('#txt-iddongho').val(data.idDongHo);
				$('#txt-madongho').val(data.maDongHo);
				$('#txt-chungloai').val(data.chungLoai);
				$('#txt-soseri').val(data.soSeri);
				$('#txt-kichco').val(data.kichCo);
				$('#txt-hesonhan').val(data.heSoNhan);
				$('#txt-chisocucdai').val(data.chiSoCucDai);
				$('#txt-nhasanxuat').val(data.nhaSanXuat);
				$('#txt-ngaysanxuat').datepicker('setDate', data.ngaySanXuat==null?'':new Date(data.ngaySanXuat));
				$('#txt-ngayduavaosudung').datepicker('setDate', data.ngayDuaVaoSuDung==null?'':new Date(data.ngayDuaVaoSuDung));
				$('#txt-ngayhethanbaohanh').datepicker('setDate', data.ngayHetHanBaoHanh==null?'':new Date(data.ngayHetHanBaoHanh));
				$('#txt-ngaykiemdinh').datepicker('setDate', data.ngayKiemDinh==null?'':new Date(data.ngayKiemDinh));
				$('#txt-ngaysuachualancuoi').datepicker('setDate', data.ngaySuaChuaLanCuoi==null?'':new Date(data.ngaySuaChuaLanCuoi));
				$('#txt-ghichusuachualancuoi').val(data.ghiChuSuaChuaLanCuoi);
				$('#cmb-trangthaidongho').val(data.idTrangThaiDongHo).trigger('change');
			}else{
				$('#frm-2')[0].reset();
				$('#txt-idkhachhang-dh').val(idkh);
				$('#txt-iddongho').val('');
				layDsTrangThaiDongHo();
			}
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm-2').hideLoading();
		}
	});
}

function kiemTraDongHo(madongho,idkh){
	$.ajax({
		url:pref_url+'/kiem-tra-dong-ho',
		method:'get',
		dataType:'json',
		data:{maDongHo:madongho,idKhachHang:idkh},
		beforeSend:function(){
			$('#box-frm-2').showLoading();
		},
		success:function(data){
			if(data.resCode === undefined){
				toastInfo('Có thể sử dụng mã đồng hồ này');
				if(data.maDongHo!=null){
					$('#txt-idkhachhang-dh').val(idkh);
					$('#txt-iddongho').val(data.idDongHo);
					$('#txt-madongho').val(data.maDongHo);
					$('#txt-chungloai').val(data.chungLoai);
					$('#txt-soseri').val(data.soSeri);
					$('#txt-kichco').val(data.kichCo);
					$('#txt-hesonhan').val(data.heSoNhan);
					$('#txt-chisocucdai').val(data.chiSoCucDai);
					$('#txt-nhasanxuat').val(data.nhaSanXuat);
					$('#txt-ngaysanxuat').datepicker('setDate', data.ngaySanXuat==null?'':new Date(data.ngaySanXuat));
					$('#txt-ngayduavaosudung').datepicker('setDate', data.ngayDuaVaoSuDung==null?'':new Date(data.ngayDuaVaoSuDung));
					$('#txt-ngayhethanbaohanh').datepicker('setDate', data.ngayHetHanBaoHanh==null?'':new Date(data.ngayHetHanBaoHanh));
					$('#txt-ngaykiemdinh').datepicker('setDate', data.ngayKiemDinh==null?'':new Date(data.ngayKiemDinh));
					$('#txt-ngaysuachualancuoi').datepicker('setDate', data.ngaySuaChuaLanCuoi==null?'':new Date(data.ngaySuaChuaLanCuoi));
					$('#txt-ghichusuachualancuoi').val(data.ghiChuSuaChuaLanCuoi);
					$('#cmb-trangthaidongho').val(data.idTrangThaiDongHo).trigger('change');
				}else{
					$('#frm-2')[0].reset();
					$('#txt-idkhachhang-dh').val(idkh);
					$('#txt-madongho').val(madongho);
					$('#txt-iddongho').val('');
					layDsTrangThaiDongHo();
				}
			}else{
				$('#frm-2')[0].reset();
				$('#txt-idkhachhang-dh').val(idkh);
				$('#txt-madongho').val(madongho);
				$('#txt-iddongho').val('');
				layDsTrangThaiDongHo();
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm-2').hideLoading();
		}
	});
}

function xoaDongHo(idDongHo,idKhachHang){
	$.ajax({
		url:pref_url+'/xoa-dong-ho',
		method:'post',
		dataType:'json',
		data:{idDongHo:idDongHo,idKhachHang:idKhachHang},
		beforeSend:function(){
			$('#box-form-2').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				$('#frm-2')[0].reset();
				$('#txt-idkhachhang-dh').val(idKhachHang);
				$('#txt-iddongho').val('');
				layDsTrangThaiDongHo();
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form-2').hideLoading();
		}
	});
}

function luuDongHo(){
	$.ajax({
		url:pref_url+'/luu-dong-ho',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-2')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm-2').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Lưu dữ liệu thành công');
				layChiTietDongHo($('#txt-idkhachhang-dh').val());
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm-2').hideLoading();
		}
	});
}
