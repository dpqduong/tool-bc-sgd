var pref_url="";
var first = true;

$(document).ready(function(){
	$('#btn-ok').click(function(){
		luu();
	});
	
	$('#cmb-donvi').change(function(){
		if(!first)
			layDsKhuVuc();
	});
	
	$('#cmb-khuvuc').change(function(){
		if(!first)
			layDsThang();
	});
	
	$('#cmb-donvi').select2({});
	$('#cmb-khuvuc').select2({});
	$('#cmb-thang').select2({});
	
	layDsDonVi();
});



function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:{idDonViLamViec:$('#cmb-donvi').val(),idKhuVucLamViec:$('#cmb-khuvuc').val(),idThangLamViec:$('#cmb-thang').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				$('#span-thang').text($('#cmb-thang option:selected').text());
				$('#span-khuvuc').text($('#cmb-khuvuc option:selected').text());
				toastInfo('Thiết lập thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Thiết lập không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDsDonVi(){
	$.ajax({
		url:pref_url+'/lay-ds-don-vi',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-donvi option').remove();
			
//			var donvi=$.map(data,function(obj){
//				obj.id=obj.idDonVi;
//				obj.text=obj.tenDonVi;
//				return obj;
//			});
//			
//			$('#cmb-donvi').select2({
//				data: donvi
//			});
			
			$.each(data, function(i, item) {
			    $('#cmb-donvi').append($('<option>', { 
			        value: item.idDonVi,
			        text : item.tenDonVi 
			    }));
			});
			
			if(first)
				layDonViHienTai();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách đơn vị không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsKhuVuc(){
	$.ajax({
		url:pref_url+'/lay-ds-khu-vuc',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-donvi').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-khuvuc option').remove();
			
//			var khuvuc=$.map(data,function(obj){
//				obj.id=obj.idKhuVuc;
//				obj.text=obj.tenKhuVuc;
//				return obj;
//			});
//			
//			$('#cmb-khuvuc').select2({
//				data: khuvuc
//			});
			
			$.each(data, function(i, item) {
			    $('#cmb-khuvuc').append($('<option>', { 
			        value: item.idKhuVuc,
			        text : item.tenKhuVuc 
			    }));
			});
			
			if(first)
				layKhuVucHienTai();
			else
				$('#cmb-khuvuc').trigger('change');
		},
		error:function(){
			showError('Thông báo','Lấy danh sách khu vực không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsThang(){
	$.ajax({
		url:pref_url+'/lay-ds-thang',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-khuvuc').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-thang option').remove();
			
//			var thang=$.map(data,function(obj){
//				obj.id=obj.idThang;
//				obj.text=obj.tenThang;
//				return obj;
//			});
//			
//			$('#cmb-thang').select2({
//				data: thang
//			});
			
			$.each(data, function(i, item) {
			    $('#cmb-thang').append($('<option>', { 
			        value: item.idThang,
			        text : item.tenThang 
			    }));
			});
			
			if(first)
				layThangHienTai();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tháng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDonViHienTai(){
	$.ajax({
		url:pref_url+'/lay-don-vi-hien-tai',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-donvi').val(data).trigger('change');
			layDsKhuVuc();
		},
		error:function(){
			showError('Thông báo','Lấy đơn vị hiện tại không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layKhuVucHienTai(){
	$.ajax({
		url:pref_url+'/lay-khu-vuc-hien-tai',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-khuvuc').val(data).trigger('change');
			layDsThang();
		},
		error:function(){
			showError('Thông báo','Lấy khu vực hiện tại không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layThangHienTai(){
	$.ajax({
		url:pref_url+'/lay-thang-hien-tai',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-thang').val(data).trigger('change');
			first = false;
		},
		error:function(){
			showError('Thông báo','Lấy tháng hiện tại không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}
