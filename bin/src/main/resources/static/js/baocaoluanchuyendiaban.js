var pref_url="/bao-cao-luan-chuyen-dia-ban";
var selectedNhanVien=[];

$('#view').click(function() {
	if(check()){
		var urlp = pref_url+"/xem-bao-cao?nhanvien="+$('#cmb-nhanvien').val();
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-ds').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-ds').hideLoading();
			}
	    });
	}
 });
 
$(document).ready(function(){
	$('#cmb-nhanvien').multiselect({
		includeSelectAllOption: true,
        selectAllText: 'Chọn tất cả',
        maxHeight: 200,
        buttonWidth: '100%',
        nonSelectedText: 'Chọn nhân viên',
        nSelectedText: ' nhân viên được chọn',
        allSelectedText: 'Đã chọn tất cả nhân viên',
        buttonClass: 'btn btn-default btn-flat'
	});
	layDsNhanVien();
	
	$('#pdf').click(function() {
		if(check()){
			var urlp = pref_url+"/xuat-file-pdf?nhanvien="+$('#cmb-nhanvien').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
		}
    });
	
    $('#xls').click(function() {
    	if(check()){
	    	var urlp = pref_url+"/xuat-file-xls?nhanvien="+$('#cmb-nhanvien').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
    
    $('#xlsx').click(function() {
    	if(check()){
	    	var urlp = pref_url+"/xuat-file-xlsx?nhanvien="+$('#cmb-nhanvien').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
    $('#rtf').click(function() {
    	if(check()){
	    	var urlp = pref_url+"/xuat-file-rtf?nhanvien="+$('#cmb-nhanvien').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
});

function check(){
	if($('#cmb-nhanvien').val()==''){
		showError('Thông báo','Vui lòng chọn nhân viên!');
		return false;
	}
	return true;
}

function layDsNhanVien(){
	$.ajax({
		url:pref_url+'/lay-ds-nhan-vien',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$.each(data, function(j, item) {
			    var selected=false;
			    for(var i=0;i<selectedNhanVien.length;i++){
			    	if(selectedNhanVien[i]==item.idNguoiDung){
			    		selected=true;
			    		break;
			    	}
			    }
				$('#cmb-nhanvien').append($('<option>', { 
			        value: item.idNguoiDung,
			        text : item.hoTen,
			        selected:selected
			    }));
			});
			$('#cmb-nhanvien').multiselect('rebuild');
		},
		error:function(){
			showError('Thông báo','Lấy danh sách nhân viên không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

