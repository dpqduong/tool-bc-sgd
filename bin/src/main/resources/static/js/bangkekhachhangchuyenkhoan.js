var pref_url="/bang-ke-khach-hang-chuyen-khoan";

$('#view').click(function() {
	var urlp = pref_url+"/xem-bao-cao?idNganHang="+$('#cmb-nganhang').val()+'&tenNganHang='+$('#cmb-nganhang option:selected').text();
	$.ajax({
        url: urlp,
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
		},
        success: function(response){
			PDFObject.embed(response, "#pdfRenderer");
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
		}
    });
 });
 
$(document).ready(function(){
	$('#cmb-nganhang').select2({});
	layDsNganHang();
	
	$('#pdf').click(function() {
		var urlp = pref_url+"/xuat-file-pdf?idNganHang="+$('#cmb-nganhang').val()+'&tenNganHang='+$('#cmb-nganhang option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
	
    $('#xls').click(function() {
    	var urlp = pref_url+"/xuat-file-xls?idNganHang="+$('#cmb-nganhang').val()+'&tenNganHang='+$('#cmb-nganhang option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
    $('#xlsx').click(function() {
    	var urlp = pref_url+"/xuat-file-xlsx?idNganHang="+$('#cmb-nganhang').val()+'&tenNganHang='+$('#cmb-nganhang option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    $('#rtf').click(function() {
    	var urlp = pref_url+"/xuat-file-rtf?idNganHang="+$('#cmb-nganhang').val()+'&tenNganHang='+$('#cmb-nganhang option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
});

function layDsNganHang(){
	$.ajax({
		url:pref_url+'/lay-ds-ngan-hang',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var nganhang=$.map(data,function(obj){
				obj.id=obj.idNganHang;
				obj.text=obj.tenNganHang;
				return obj;
			});
			
			$('#cmb-nganhang').select2({
				data: nganhang
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách ngân hàng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

