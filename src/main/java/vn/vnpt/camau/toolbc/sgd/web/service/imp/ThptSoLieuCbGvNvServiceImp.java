package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThptSoLieuCbGvNvConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThptSoLieuCbGvNvRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptSoLieuCbGvNvService;

@Service
public class ThptSoLieuCbGvNvServiceImp implements ThptSoLieuCbGvNvService {

	@Autowired
	private ThptSoLieuCbGvNvConverter converter;
	@Autowired
	private ThptSoLieuCbGvNvRepository repository;
	
	@Override
	public void themSoLieuCbGvNvFileExcel(ThptSoLieuCbGvNvDto soLieuCbGvNv) throws Exception {
		ThptSoLieuCbGvNv soLieuCbGvNvCanLuu= converter.convertToEntity(soLieuCbGvNv);
		try {
			repository.save(soLieuCbGvNvCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThptSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThptSoLieuCbGvNv soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThptSoLieuCbGvNvDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
