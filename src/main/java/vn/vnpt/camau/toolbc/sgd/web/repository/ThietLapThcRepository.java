package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.ThietLapThc;

public interface ThietLapThcRepository extends CrudRepository<ThietLapThc, Integer> {

	Page<ThietLapThc> findAll(Pageable pageable);
	
	ThietLapThc findById(int id);
	
//	Page<ThietLapThc> findByHienThiIsTrueAndTenThietLapThcContains(String tenThietLapThc, Pageable pageable);
//
//	List<ThietLapThc> findByIdThietLapThcInAndHienThiIsTrueOrderByIdThietLapThc(List<Long> dsIdThietLapThc);
	
}
