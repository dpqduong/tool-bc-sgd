package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuLop;

@Component
public class ThcsSoLieuLopConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThcsSoLieuLopDto convertToDto(ThcsSoLieuLop entity) {
		if (entity == null) {
			return new ThcsSoLieuLopDto();
		}
		ThcsSoLieuLopDto dto = mapper.map(entity, ThcsSoLieuLopDto.class);
		return dto;
	}

	public ThcsSoLieuLop convertToEntity(ThcsSoLieuLopDto dto) {
		if (dto == null) {
			return new ThcsSoLieuLop();
		}
		ThcsSoLieuLop entity = mapper.map(dto, ThcsSoLieuLop.class);
		return entity;
	}

}
