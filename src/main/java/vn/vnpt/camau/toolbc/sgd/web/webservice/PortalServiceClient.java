package vn.vnpt.camau.toolbc.sgd.web.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.service.ThamSoService;
import vn.vnpt.camau.toolbc.sgd.web.wsdl.ConvertForStoreFkey;
import vn.vnpt.camau.toolbc.sgd.web.wsdl.ConvertForStoreFkeyResponse;

public class PortalServiceClient extends WebServiceGatewaySupport {

	@Autowired
	private ThamSoService thamSoService;

	public ConvertForStoreFkeyResponse convertForStoreFkey(String fkey) {
		ConvertForStoreFkey request = new ConvertForStoreFkey();
		
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setFkey(fkey);
		
		String soapAction = Utilities.NAMESPACE + "convertForStoreFkey";
		ConvertForStoreFkeyResponse response = (ConvertForStoreFkeyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPortalService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	
}
