package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.SgdThpt;

public interface SgdThptRepository extends CrudRepository<SgdThpt, Integer> {

	List<SgdThpt> findByIdTruongAndNamOrderByThangAsc(int idDonVi, int namHoc);
	List<SgdThpt> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt);
	SgdThpt findByThangAndNamAndIdTruong(int thang, int nam, int idDonVi);
	SgdThpt findById(int id);
	SgdThpt findByIdAndGuiBcSgd(int id, int flag);
}
