package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;

public class DmNamHocDto implements Comparable<DmNamHocDto> {

	@Getter
	@Setter
	private Integer id;
	@Getter
	@Setter
	private String namHoc;
	@Getter
	@Setter
	private int giaTri;
	
	@Override
	public int compareTo(DmNamHocDto object) {
		return this.namHoc.compareTo(object.getNamHoc());
	}
	
}
