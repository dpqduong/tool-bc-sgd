package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.converter.ThietLapThcConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThietLapThcDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThietLapThc;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThietLapThcRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThietLapThcService;

@Service
public class ThietLapThcServiceImp implements ThietLapThcService {

	@Autowired
	private ThietLapThcConverter converter;
	@Autowired
	private ThietLapThcRepository repository;
	
	@Override
	public Page<ThietLapThcDto> layDsThietLapThc( Pageable pageable) {
		Page<ThietLapThc> dsThietLapThcCanLay;
		dsThietLapThcCanLay = repository.findAll(pageable);
		Page<ThietLapThcDto> dsThietLapThcDaChuyenDoi = dsThietLapThcCanLay.map(converter::convertToDto);
		return dsThietLapThcDaChuyenDoi;
	}

	@Override
	public ThietLapThcDto layThietLapThc(int id) {
		ThietLapThc ThietLapThcCanLay = repository.findById(id);
		ThietLapThcDto ThietLapThcDaChuyenDoi = converter.convertToDto(ThietLapThcCanLay);
		return ThietLapThcDaChuyenDoi;
	}

	@Override
	public ThietLapThcDto luuThietLapThc(ThietLapThcDto dto) throws Exception {
		kiemTraThietLapThc(dto.getId());
		ThietLapThc ThietLapThcCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(ThietLapThcCanLuu);
			ThietLapThcDto ThietLapThcDaChuyenDoi = converter.convertToDto(ThietLapThcCanLuu);
			return ThietLapThcDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu tham số.");
		}
	}

	private void kiemTraThietLapThc(int id) throws Exception {
		if (id == 0) {
			throw new Exception("Không tìm thấy tham số.");
		}
		ThietLapThc ThietLapThcCanKiemTra = repository.findById(id);
		if (ThietLapThcCanKiemTra == null) {
			throw new Exception("Không tìm thấy tham số.");
		}
	}

}
