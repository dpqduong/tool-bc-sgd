package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuCbGvNvDto;

public interface ThcsSoLieuCbGvNvService {
	void themSoLieuCbGvNvFileExcel(ThcsSoLieuCbGvNvDto soLieuCbGvNv) throws Exception;
	ThcsSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
