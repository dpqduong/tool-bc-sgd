package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrekt;

@Component
public class SgdTrektConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public SgdTrektDto convertToDto(SgdTrekt entity) {
		if (entity == null) {
			return new SgdTrektDto();
		}
		SgdTrektDto dto = mapper.map(entity, SgdTrektDto.class);
		return dto;
	}

	public SgdTrekt convertToEntity(SgdTrektDto dto) {
		if (dto == null) {
			return new SgdTrekt();
		}
		SgdTrekt entity = mapper.map(dto, SgdTrekt.class);
		return entity;
	}

}