package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;

public interface MnSoLieuCbGvNvRepository extends CrudRepository<MnSoLieuCbGvNv, Long> {
	MnSoLieuCbGvNv findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
