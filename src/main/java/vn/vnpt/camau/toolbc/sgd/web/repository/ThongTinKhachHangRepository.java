package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.ThongTinKhachHang;

public interface ThongTinKhachHangRepository extends CrudRepository<ThongTinKhachHang, Long> {

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht "
				+ "join tt.soGhi sg join sg.duong d "
			+ "where d.khuVuc.idKhuVuc = ?1 and (0L = ?2 or d.idDuong = ?2) and (0L = ?3 or sg.idSoGhi = ?3) and th.idThang = ?4 and kh.huy = 0 and (dhkht.thang.idThang = ?4 or dhkht.thang.idThang = null) "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHang(Long idKhuVuc, Long idDuong, Long idSoGhi, Long idThang, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join tt.soGhi sg join sg.duong d "
			+ "where d.khuVuc.idKhuVuc = ?1 and (0L = ?2 or d.idDuong = ?2) and (0L = ?3 or sg.idSoGhi = ?3) and th.idThang = ?4 and kh.huy = 0 and (dhkht.thang.idThang = ?4 or dhkht.thang.idThang = null) "
				+ "and (tt.maKhachHang like %?5% or tt.tenKhachHang like %?5% or tt.diaChiSuDung like %?5% or tt.diaChiThanhToan like %?5% or tt.maSoThue like %?5% or tt.soCmnd like %?5% or tt.soDienThoai like %?5% or dh.maDongHo like %?5%) "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHang(Long idKhuVuc, Long idDuong, Long idSoGhi, Long idThang, String thongTinCanTim, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht "
			+ "where tt.soGhi.idSoGhi = ?1 and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
			+ "order by tt.thuTu")
	List<ThongTinKhachHang> layDsThongTinKhachHangTheoSoGhi(Long idSoGhi, Long idThang);

	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
			+ "where tt.soGhi.idSoGhi = ?1 "
				+ "and (tt.maKhachHang like %?2% or tt.tenKhachHang like %?2% or tt.diaChiSuDung like %?2% or tt.diaChiThanhToan like %?2% or tt.maSoThue like %?2% or tt.soCmnd like %?2% or tt.soDienThoai like %?2% or dh.maDongHo like %?2%) "
				+ "and th.idThang = ?3 and kh.huy = 0 and (dhkht.thang.idThang = ?3 or dhkht.thang.idThang = null) "
			+ "order by tt.thuTu")
	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	List<ThongTinKhachHang> layDsThongTinKhachHangTheoSoGhiVaThongTinCanTim(Long idSoGhi, String thongTinCanTim,
			Long idThang);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi.duong", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht "
			+ "where tt.idThongTinKhachHang = ?1 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null)")
	ThongTinKhachHang layThongTinKhachHang(Long idThongTinKhachHang, Long idThang);

	Long countByMaKhachHang(String maKhachHang);
	
	Long countByMaKhachHangAndIdThongTinKhachHangNot(String maKhachHang, Long idThongTinKhachHang);
	
	Long countByMaKhachHangAndKhachHang_IdKhachHangNot(String maKhachHang, Long idKhachHang);

//	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
//			+ "where (tt.maKhachHang = ?1 or dh.maDongHo = ?1) and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) and tt.nganHang.idNganHang = null")
//	@EntityGraph(attributePaths = { "doiTuong", "khachHang.dongHoKhachHangThangs.dongHo" })
//	ThongTinKhachHang layThongTinKhachHangTheoMaKhachHang(String maKhachHang, Long idThang);

	@EntityGraph(attributePaths = { "soGhi.duong.khuVuc", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh join kh.hoaDons hd left join kh.dongHoKhachHangThangs dhkht "
			+ "where hd.idHoaDon = ?1 and th.idThang = ?2 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null)")
	ThongTinKhachHang layThongTinKhachHangTheoHoaDon(Long idHoaDon, Long idThang);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and sg.idSoGhi = ?1 and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
				+ "and lgtt.idThang = ?2 and nd.idNguoiDung = ?3 and tt.nganHang.idNganHang = null "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHangTheoSoGhiVaNhanVienThanhToanGiaoDich(Long idSoGhi, Long idThang, Long idNguoiDuocPhanCong, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and sg.idSoGhi = ?1 and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
				+ "and lgtt.idThang = ?2 and nd.idNguoiDung = ?3 and tt.nganHang.idNganHang = null "
				+ "and (tt.maKhachHang like %?4% or tt.tenKhachHang like %?4% or tt.diaChiSuDung like %?4% or tt.diaChiThanhToan like %?4% or tt.maSoThue like %?4% or tt.soCmnd like %?4% or tt.soDienThoai like %?4% or dh.maDongHo like %?4%) "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHangTheoSoGhiVaNhanVienThanhToanGiaoDich(Long idSoGhi, Long idThang, Long idNguoiDuocPhanCong, String thongTinCanTim, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and sg.duong.idDuong = ?1 and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
				+ "and lgtt.idThang = ?2 and nd.idNguoiDung = ?3 and tt.nganHang.idNganHang = null "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHangTheoDuongVaNhanVienThanhToanGiaoDich(Long idDuong, Long idThang, Long idNguoiDuocPhanCong, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and sg.duong.idDuong = ?1 and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
				+ "and lgtt.idThang = ?2 and nd.idNguoiDung = ?3 and tt.nganHang.idNganHang = null "
				+ "and (tt.maKhachHang like %?4% or tt.tenKhachHang like %?4% or tt.diaChiSuDung like %?4% or tt.diaChiThanhToan like %?4% or tt.maSoThue like %?4% or tt.soCmnd like %?4% or tt.soDienThoai like %?4% or dh.maDongHo like %?4%) "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHangTheoDuongVaNhanVienThanhToanGiaoDich(Long idDuong, Long idThang, Long idNguoiDuocPhanCong, String thongTinCanTim, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and th.idThang = ?1 and kh.huy = 0 and (dhkht.thang.idThang = ?1 or dhkht.thang.idThang = null) "
				+ "and lgtt.idThang = ?1 and nd.idNguoiDung = ?2 and tt.nganHang.idNganHang = null "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHangTheoNhanVienThanhToanGiaoDich(Long idThang, Long idNguoiDuocPhanCong, Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "nganHang", "nguoiDung", "soGhi", "khachHang.dongHoKhachHangThangs.dongHo" })
	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and th.idThang = ?1 and kh.huy = 0 and (dhkht.thang.idThang = ?1 or dhkht.thang.idThang = null) "
				+ "and lgtt.idThang = ?1 and nd.idNguoiDung = ?2 and tt.nganHang.idNganHang = null "
				+ "and (tt.maKhachHang like %?3% or tt.tenKhachHang like %?3% or tt.diaChiSuDung like %?3% or tt.diaChiThanhToan like %?3% or tt.maSoThue like %?3% or tt.soCmnd like %?3% or tt.soDienThoai like %?3% or dh.maDongHo like %?3%) "
			+ "order by tt.thuTu")
	Page<ThongTinKhachHang> layDsThongTinKhachHangTheoNhanVienThanhToanGiaoDich(Long idThang, Long idNguoiDuocPhanCong, String thongTinCanTim, Pageable pageable);

	@Query("select tt from ThongTinKhachHang tt join tt.thangs th join tt.khachHang kh left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join tt.soGhi sg join sg.lichGhiThus lgt join lgt.doiTuongs dt join lgt.thangs lgtt join lgt.nguoiDuocPhanCong nd "
			+ "where tt.doiTuong.idDoiTuong = dt.idDoiTuong and (tt.maKhachHang = ?1 or dh.maDongHo = ?1) and th.idThang = ?2 and kh.huy = 0 and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
			+ "and lgtt.idThang = ?2 and nd.idNguoiDung = ?3 and tt.nganHang.idNganHang = null")
	@EntityGraph(attributePaths = { "doiTuong", "khachHang.dongHoKhachHangThangs.dongHo" })
	ThongTinKhachHang layThongTinKhachHangTheoMaKhachHang(String maKhachHang, Long idThang, Long idNguoiDuocPhanCong);

}
