package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.Response;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuTreDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.FileBean;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.service.DmTruongService;
import vn.vnpt.camau.toolbc.sgd.web.service.ImportSoLieuService;
import vn.vnpt.camau.toolbc.sgd.web.service.MnSoLieuCbGvNvService;
import vn.vnpt.camau.toolbc.sgd.web.service.MnSoLieuLopService;
import vn.vnpt.camau.toolbc.sgd.web.service.MnSoLieuTreService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThSoLieuCbGvNvService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThSoLieuHocSinhService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThSoLieuLopService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsKqhtService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsSoLieuCbGvNvService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsSoLieuHocSinhService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsSoLieuLopService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptKqhtService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptSoLieuCbGvNvService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptSoLieuHocSinhService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptSoLieuLopService;

@Service
public class ImportSoLieuServiceImp implements ImportSoLieuService {

	@Autowired
	private MnSoLieuLopService mNSoLieuLopServ;
	@Autowired
	private MnSoLieuTreService mNSoLieuTreService;
	@Autowired
	private MnSoLieuCbGvNvService mNSoLieuCbGvNvService;
	@Autowired
	private ThSoLieuLopService tHSoLieuLopService;
	@Autowired
	private ThSoLieuHocSinhService tHSoLieuHocSinhService;
	@Autowired
	private ThSoLieuCbGvNvService tHSoLieuCbGvNvService;
	@Autowired
	private ThcsSoLieuLopService tHCSSoLieuLopService;
	@Autowired
	private ThcsSoLieuHocSinhService tHCSSoLieuHocSinhService;
	@Autowired
	private ThcsSoLieuCbGvNvService tHCSSoLieuCbGvNvService;
	@Autowired
	private ThcsKqhtService tHCSKqhtService;
	@Autowired
	private ThptSoLieuLopService tHPTSoLieuLopService;
	@Autowired
	private ThptSoLieuHocSinhService tHPTSoLieuHocSinhService;
	@Autowired
	private ThptSoLieuCbGvNvService tHPTSoLieuCbGvNvService;
	@Autowired
	private ThptKqhtService tHPTKqhtService;
	@Autowired
	private DmTruongService dmTruongService;

	@SuppressWarnings("resource")
	@Override
	public String importSoLieuLopMN(FileBean fileBean, int idTruong, int namHoc, int thoiGian, int action, int loaiTruong) {

		ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
		ByteArrayInputStream bisHS = new ByteArrayInputStream(fileBean.getFileData1().getBytes());
		ByteArrayInputStream bisGV = new ByteArrayInputStream(fileBean.getFileData2().getBytes());
		ByteArrayInputStream bisKQHT = new ByteArrayInputStream(fileBean.getFileData3().getBytes());
		Workbook workbook = null;
		Workbook workbookHS = null;
		Workbook workbookGV = null;
		Workbook workbookKQHT = null;
		DmTruongDto ttTruong = dmTruongService.layTTTruong(idTruong);
//		int loaiTruong = ttTruong.getLoaiTruong();
//		System.out.println(loaiTruong);

		try {
			if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
				workbook = new HSSFWorkbook(bis);
			} else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(bis);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel số liệu lớp !").toString();
			}

			if (fileBean.getFileData1().getOriginalFilename().endsWith("xls")) {
				workbookHS = new HSSFWorkbook(bisHS);
			} else if (fileBean.getFileData1().getOriginalFilename().endsWith("xlsx")) {
				workbookHS = new XSSFWorkbook(bisHS);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel số liệu học sinh!").toString();
			}

			if (fileBean.getFileData2().getOriginalFilename().endsWith("xls")) {
				workbookGV = new HSSFWorkbook(bisGV);
			} else if (fileBean.getFileData2().getOriginalFilename().endsWith("xlsx")) {
				workbookGV = new XSSFWorkbook(bisGV);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel số liệu giáo viên!").toString();
			}

			if (loaiTruong == 3 || loaiTruong == 4) {
				if (fileBean.getFileData3().getOriginalFilename().endsWith("xls")) {
					workbookKQHT = new HSSFWorkbook(bisKQHT);
				} else if (fileBean.getFileData3().getOriginalFilename().endsWith("xlsx")) {
					workbookKQHT = new XSSFWorkbook(bisKQHT);
				}
//				else {
//					return new Response(-1, "Vui lòng chọn file Excel kết quả học tập!").toString();
//				}
			}

			// Xử lý số liệu lớp
			if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 1) {
				int soCot = workbook.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
				// Xử lý số liệu lớp
				int nhomTre = 0;
				int mauGiao = 0;
				int namTuoi = 0;
				int tongSo = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(2);
						Cell cell2 = row.getCell(3);
						if (cell1 != null) {
							if (cell1.getStringCellValue().equals("Nhóm trẻ")) {
								nhomTre++;
							}
							if (cell1.getStringCellValue().equals("Lớp mẫu giáo")) {
								mauGiao++;
							}
						}
						if (cell2 != null) {
							if (cell2.getStringCellValue().equals("5-6 Tuổi")) {
								namTuoi++;
							}
						}
					}
				}

				tongSo = nhomTre + mauGiao;
				if (soCot != 3) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu lớp Mầm non !").toString();
				} else {
					MnSoLieuLopDto MnSoLieuLopDto;
					if (action == 0) {
						MnSoLieuLopDto = new MnSoLieuLopDto();
					} else {
						MnSoLieuLopDto = mNSoLieuLopServ.laySoLieu(idTruong, namHoc, thoiGian);
					}
					MnSoLieuLopDto.setNamHoc(namHoc);
					MnSoLieuLopDto.setThoiGianImport(thoiGian);
					MnSoLieuLopDto.setIdTruong(idTruong);
					MnSoLieuLopDto.setNt(nhomTre);
					MnSoLieuLopDto.setMg(mauGiao);
					MnSoLieuLopDto.setTongSo(tongSo);
					MnSoLieuLopDto.setNamTuoi(namTuoi);
					mNSoLieuLopServ.themSoLieuLopFileExcel(MnSoLieuLopDto);
				}

			} else if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 2) {
				int soCot = workbook.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu lớp
				int k1 = 0;
				int k2 = 0;
				int k3 = 0;
				int k4 = 0;
				int k5 = 0;
				int tongSo = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(1);
//						System.out.println(val);
						if (cell1 != null) {
							String val = cell1.getStringCellValue();
							if (val.equals("Khối 1")) {
								k1++;
							}
							if (val.equals("Khối 2")) {
								k2++;
							}
							if (val.equals("Khối 3")) {
								k3++;
							}
							if (val.equals("Khối 4")) {
								k4++;
							}
							if (val.equals("Khối 5")) {
								k5++;
							}
						}
					}
				}
				tongSo = k1 + k2 + k3 + k4 + k5;
				if (soCot != 11) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu lớp Tiểu học !").toString();
				} else {
				ThSoLieuLopDto ThSoLieuLopDto;
				if (action == 0) {
					ThSoLieuLopDto = new ThSoLieuLopDto();
				} else {
					ThSoLieuLopDto = tHSoLieuLopService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThSoLieuLopDto.setNamHoc(namHoc);
				ThSoLieuLopDto.setThoiGianImport(thoiGian);
				ThSoLieuLopDto.setIdTruong(idTruong);
				ThSoLieuLopDto.setK1(k1);
				ThSoLieuLopDto.setK2(k2);
				ThSoLieuLopDto.setK3(k3);
				ThSoLieuLopDto.setK4(k4);
				ThSoLieuLopDto.setK5(k5);
				ThSoLieuLopDto.setTongSo(tongSo);
				tHSoLieuLopService.themSoLieuLopFileExcel(ThSoLieuLopDto);
				}
			} else if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 3) {
				int soCot = workbook.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu lớp
				int k6 = 0;
				int k7 = 0;
				int k8 = 0;
				int k9 = 0;
				int tongSo = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(2);
						if (cell1 != null) {
							String val = cell1.getStringCellValue();
							if (val.equals("Khối 6")) {
								k6++;
							}
							if (val.equals("Khối 7")) {
								k7++;
							}
							if (val.equals("Khối 8")) {
								k8++;
							}
							if (val.equals("Khối 9")) {
								k9++;
							}
						}
					}
				}
				tongSo = k6 + k7 + k8 + k9;
				if (soCot != 3) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu lớp THCS !").toString();
				} else {
				ThcsSoLieuLopDto ThcsSoLieuLopDto;
				if (action == 0) {
					ThcsSoLieuLopDto = new ThcsSoLieuLopDto();
				} else {
					ThcsSoLieuLopDto = tHCSSoLieuLopService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThcsSoLieuLopDto.setNamHoc(namHoc);
				ThcsSoLieuLopDto.setThoiGianImport(thoiGian);
				ThcsSoLieuLopDto.setIdTruong(idTruong);
				ThcsSoLieuLopDto.setK6(k6);
				ThcsSoLieuLopDto.setK7(k7);
				ThcsSoLieuLopDto.setK8(k8);
				ThcsSoLieuLopDto.setK9(k9);
				ThcsSoLieuLopDto.setTongSo(tongSo);
				tHCSSoLieuLopService.themSoLieuLopFileExcel(ThcsSoLieuLopDto);
				}
			} else if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 4) {
				int soCot = workbook.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu lớp
				int k10 = 0;
				int k11 = 0;
				int k12 = 0;
				int tongSo = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(2);
						if (cell1 != null) {
							String val = cell1.getStringCellValue();
							if (val.equals("Khối 10")) {
								k10++;
							}
							if (val.equals("Khối 11")) {
								k11++;
							}
							if (val.equals("Khối 12")) {
								k12++;
							}
						}
					}
				}
				tongSo = k10 + k11 + k12;
				
				if (soCot != 3) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu lớp THPT !").toString();
				} else {
				ThptSoLieuLopDto ThptSoLieuLopDto;
				if (action == 0) {
					ThptSoLieuLopDto = new ThptSoLieuLopDto();
				} else {
					ThptSoLieuLopDto = tHPTSoLieuLopService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThptSoLieuLopDto.setNamHoc(namHoc);
				ThptSoLieuLopDto.setThoiGianImport(thoiGian);
				ThptSoLieuLopDto.setIdTruong(idTruong);
				ThptSoLieuLopDto.setK10(k10);
				ThptSoLieuLopDto.setK11(k11);
				ThptSoLieuLopDto.setK12(k12);
				ThptSoLieuLopDto.setTongSo(tongSo);
				tHPTSoLieuLopService.themSoLieuLopFileExcel(ThptSoLieuLopDto);
				}
			} else {
				return new Response(-1, "Import lỗi dữ liệu lớp học").toString();
			}

			// Xử lý số liệu học sinh
			if (fileBean.getFileData1().getOriginalFilename() != "" && loaiTruong == 1) {
				int soCot = workbookHS.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
				// Xử lý số liệu học sinh
				int hs = 0;
				int namTuoi = 0;
//				int check = 0;
				for (Row rowHS : workbookHS.getSheetAt(0)) {
					if (rowHS.getRowNum() > 0) {
						Cell cell1 = rowHS.getCell(1);
//						Cell cell4 = rowHS.getCell(4);
						if (cell1 != null) {
							hs++;
							String val = cell1.getStringCellValue();
							if (val.indexOf("Lá") != -1) {
								namTuoi++;
							}
						}
						
//						if (cell4 != null) {
//							String val = cell4.getStringCellValue();
//							if (val.equals("Nam") || val.equals("Nữ") ) {
//								check++;
//							}
//						}
					}
				}
//				System.out.println(check);
				if (soCot != 49 ) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu trẻ Mầm non !").toString();
				} else {
				MnSoLieuTreDto mnSoLieuTreDto;
				if (action == 0) {
					mnSoLieuTreDto = new MnSoLieuTreDto();
				} else {
					mnSoLieuTreDto = mNSoLieuTreService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				mnSoLieuTreDto.setIdTruong(idTruong);
				mnSoLieuTreDto.setTongSo(hs);
				mnSoLieuTreDto.setNamTuoi(namTuoi);
				mnSoLieuTreDto.setNamHoc(namHoc);
				mnSoLieuTreDto.setThoiGianImport(thoiGian);

				mNSoLieuTreService.themSoLieuTreFileExcel(mnSoLieuTreDto);
				}
			} else if (fileBean.getFileData1().getOriginalFilename() != "" && loaiTruong == 2) {
				int soCot = workbookHS.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
				System.out.println(soCot);
				// Xử lý số liệu học sinh tiểu học
				int k1 = 0;
				int k2 = 0;
				int k3 = 0;
				int k4 = 0;
				int k5 = 0;
				int tongSo = 0;
//				int check = 0;
				for (Row row : workbookHS.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(1);
//						Cell cell4 = row.getCell(4);
//						System.out.println(val);
						if (cell1 != null) {
							String val = cell1.getStringCellValue();
							if (val.indexOf("1") != -1) {
								k1++;
							}
							if (val.indexOf("2") != -1) {
								k2++;
							}
							if (val.indexOf("3") != -1) {
								k3++;
							}
							if (val.indexOf("4") != -1) {
								k4++;
							}
							if (val.indexOf("5") != -1) {
								k5++;
							}
						}
//						if (cell4 != null) {
//							String val = cell4.getStringCellValue();
//							if (val.equals("Nam") || val.equals("Nữ") ) {
//								check++;
//							}
//						}
					}
				}
				tongSo = k1 + k2 + k3 + k4 + k5;
				if (soCot != 61) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu học sinh Tiểu học !").toString();
				} else {
				ThSoLieuHocSinhDto ThSoLieuHocSinhDto;
				if (action == 0) {
					ThSoLieuHocSinhDto = new ThSoLieuHocSinhDto();
				} else {
					ThSoLieuHocSinhDto = tHSoLieuHocSinhService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThSoLieuHocSinhDto.setNamHoc(namHoc);
				ThSoLieuHocSinhDto.setThoiGianImport(thoiGian);
				ThSoLieuHocSinhDto.setIdTruong(idTruong);
				ThSoLieuHocSinhDto.setK1(k1);
				ThSoLieuHocSinhDto.setK2(k2);
				ThSoLieuHocSinhDto.setK3(k3);
				ThSoLieuHocSinhDto.setK4(k4);
				ThSoLieuHocSinhDto.setK5(k5);
				ThSoLieuHocSinhDto.setTongSo(tongSo);
				tHSoLieuHocSinhService.themSoLieuHocSinhFileExcel(ThSoLieuHocSinhDto);
				}
			} else if (fileBean.getFileData1().getOriginalFilename() != "" && loaiTruong == 3) {
				int soCot = workbookHS.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu học sinh tiểu học
				int k6 = 0;
				int k7 = 0;
				int k8 = 0;
				int k9 = 0;
				int tongSo = 0;
//				int check = 0;
				for (Row row : workbookHS.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(1);
//						Cell cell4 = row.getCell(4);
						if (cell1 != null) {
							String val = cell1.getStringCellValue();
							if (val.indexOf("6") != -1) {
								k6++;
							}
							if (val.indexOf("7") != -1) {
								k7++;
							}
							if (val.indexOf("8") != -1) {
								k8++;
							}
							if (val.indexOf("9") != -1) {
								k9++;
							}
						}
						
//						if (cell4 != null) {
//							String val = cell4.getStringCellValue();
//							if (val.equals("Nam") || val.equals("Nữ") ) {
//								check++;
//							}
//						}
					}
				}
				tongSo = k6 + k7 + k8 + k9;
				if (soCot != 63) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu học sinh THCS !").toString();
				} else {
				ThcsSoLieuHocSinhDto ThcsSoLieuHocSinhDto;
				if (action == 0) {
					ThcsSoLieuHocSinhDto = new ThcsSoLieuHocSinhDto();
				} else {
					ThcsSoLieuHocSinhDto = tHCSSoLieuHocSinhService.laySoLieu(idTruong, namHoc, thoiGian);
				}
				
				ThcsSoLieuHocSinhDto.setNamHoc(namHoc);
				ThcsSoLieuHocSinhDto.setThoiGianImport(thoiGian);
				ThcsSoLieuHocSinhDto.setIdTruong(idTruong);
				ThcsSoLieuHocSinhDto.setK6(k6);
				ThcsSoLieuHocSinhDto.setK7(k7);
				ThcsSoLieuHocSinhDto.setK8(k8);
				ThcsSoLieuHocSinhDto.setK9(k9);
				ThcsSoLieuHocSinhDto.setTongSo(tongSo);
				tHCSSoLieuHocSinhService.themSoLieuHocSinhFileExcel(ThcsSoLieuHocSinhDto);
				}
			} else if (fileBean.getFileData1().getOriginalFilename() != "" && loaiTruong == 4) {
				int soCot = workbookHS.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu học sinh tiểu học
				int k10 = 0;
				int k11 = 0;
				int k12 = 0;
				int tongSo = 0;
//				int check = 0;
				for (Row row : workbookHS.getSheetAt(0)) {
					if (row.getRowNum() > 0) {
						Cell cell1 = row.getCell(1);
//						Cell cell4 = row.getCell(4);
//						System.out.println(val);
						if (cell1 != null) {
							String val = cell1.getStringCellValue();
							if (val.indexOf("10") != -1) {
								k10++;
							}
							if (val.indexOf("11") != -1) {
								k11++;
							}
							if (val.indexOf("12") != -1) {
								k12++;
							}
						}
						
//						if (cell4 != null) {
//							String val = cell4.getStringCellValue();
//							if (val.equals("Nam") || val.equals("Nữ") ) {
//								check++;
//							}
//						}
					}
				}
				tongSo = k10 + k11 + k12;
				if (soCot != 60) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu học sinh THPT !").toString();
				} else {
				ThptSoLieuHocSinhDto ThptSoLieuHocSinhDto;
				if (action == 0) {
					ThptSoLieuHocSinhDto = new ThptSoLieuHocSinhDto();
				} else {
					ThptSoLieuHocSinhDto = tHPTSoLieuHocSinhService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThptSoLieuHocSinhDto.setNamHoc(namHoc);
				ThptSoLieuHocSinhDto.setThoiGianImport(thoiGian);
				ThptSoLieuHocSinhDto.setIdTruong(idTruong);
				ThptSoLieuHocSinhDto.setK10(k10);
				ThptSoLieuHocSinhDto.setK11(k11);
				ThptSoLieuHocSinhDto.setK12(k12);
				ThptSoLieuHocSinhDto.setTongSo(tongSo);
				tHPTSoLieuHocSinhService.themSoLieuHocSinhFileExcel(ThptSoLieuHocSinhDto);
				}
			} else {
				return new Response(-1, "Import lỗi dữ liệu học sinh").toString();
			}

			// Xử lý số liệu cbql, gv, nv
			if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 1) {
				int soCot = workbookGV.getSheetAt(0).getRow(1).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý dữ liệu giáo viên
				int cbql = 0;
				int gv = 0;
				int nv = 0;
				for (Row rowGV : workbookGV.getSheetAt(0)) {
					if (rowGV.getRowNum() > 0) {
						Cell cell1 = rowGV.getCell(17);
						if (cell1 != null) {
							String ten = cell1.getStringCellValue();
//						System.out.println(ten);
							if (ten.equals("Cán bộ quản lý")) {
								cbql++;
							}
							if (ten.equals("Giáo viên")) {
								gv++;
							}
							if (ten.equals("Nhân viên")) {
								nv++;
							}
						}

					}
				}
				
				if (soCot != 55) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV Mầm non !").toString();
				} else {
				MnSoLieuCbGvNvDto MnSoLieuCbGvNvDto;
				if (action == 0) {
					MnSoLieuCbGvNvDto = new MnSoLieuCbGvNvDto();
				} else {
					MnSoLieuCbGvNvDto = mNSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				MnSoLieuCbGvNvDto.setIdTruong(idTruong);
				MnSoLieuCbGvNvDto.setCbql(cbql);
				MnSoLieuCbGvNvDto.setGv(gv);
				MnSoLieuCbGvNvDto.setNv(nv);
				MnSoLieuCbGvNvDto.setNamHoc(namHoc);
				MnSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
				mNSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(MnSoLieuCbGvNvDto);
				}
			} else if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 2) {
				int soCot = workbookGV.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu cán bộ ql, gv, nv tiểu học
				int cbql = 0;
				int gv = 0;
				int nv = 0;
				int tongSo = 0;
				for (Row rowGV : workbookGV.getSheetAt(0)) {
					if (rowGV.getRowNum() > 0) {
						Cell cell1 = rowGV.getCell(17);
						if (cell1 != null) {
							String ten = cell1.getStringCellValue();
//							System.out.println(ten);
							if (ten.equals("Cán bộ quản lý")) {
								cbql++;
							}
							if (ten.equals("Giáo viên")) {
								gv++;
							}
							if (ten.equals("Nhân viên")) {
								nv++;
							}
						}

					}
				}
				tongSo = cbql + gv + nv;
				if (soCot != 63) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV Tiểu học !").toString();
				} else {
				ThSoLieuCbGvNvDto ThSoLieuCbGvNvDto;
				if (action == 0) {
					ThSoLieuCbGvNvDto = new ThSoLieuCbGvNvDto();
				} else {
					ThSoLieuCbGvNvDto = tHSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThSoLieuCbGvNvDto.setNamHoc(namHoc);
				ThSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
				ThSoLieuCbGvNvDto.setIdTruong(idTruong);
				ThSoLieuCbGvNvDto.setCbql(cbql);
				ThSoLieuCbGvNvDto.setGv(gv);
				ThSoLieuCbGvNvDto.setNv(nv);
				ThSoLieuCbGvNvDto.setTongSo(tongSo);
				tHSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(ThSoLieuCbGvNvDto);
				}
			} else if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 3) {
				int soCot = workbookGV.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu cán bộ ql, gv, nv tiểu học
				int cbql = 0;
				int gv = 0;
				int nv = 0;
				int tongSo = 0;
				for (Row rowGV : workbookGV.getSheetAt(0)) {
					if (rowGV.getRowNum() > 0) {
						Cell cell1 = rowGV.getCell(17);
						if (cell1 != null) {
							String ten = cell1.getStringCellValue();
//							System.out.println(ten);
							if (ten.equals("Cán bộ quản lý")) {
								cbql++;
							}
							if (ten.equals("Giáo viên")) {
								gv++;
							}
							if (ten.equals("Nhân viên")) {
								nv++;
							}
						}

					}
				}
				tongSo = cbql + gv + nv;
				if (soCot != 63) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV THCS !").toString();
				} else {
				ThcsSoLieuCbGvNvDto ThcsSoLieuCbGvNvDto;
				if (action == 0) {
					ThcsSoLieuCbGvNvDto = new ThcsSoLieuCbGvNvDto();
				} else {
					ThcsSoLieuCbGvNvDto = tHCSSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThcsSoLieuCbGvNvDto.setNamHoc(namHoc);
				ThcsSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
				ThcsSoLieuCbGvNvDto.setIdTruong(idTruong);
				ThcsSoLieuCbGvNvDto.setCbql(cbql);
				ThcsSoLieuCbGvNvDto.setGv(gv);
				ThcsSoLieuCbGvNvDto.setNv(nv);
				ThcsSoLieuCbGvNvDto.setTongSo(tongSo);
				tHCSSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(ThcsSoLieuCbGvNvDto);
				}
			} else if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 4) {
				int soCot = workbookGV.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				// Xử lý số liệu cán bộ ql, gv, nv tiểu học
				int cbql = 0;
				int gv = 0;
				int nv = 0;
				int tongSo = 0;
				for (Row rowGV : workbookGV.getSheetAt(0)) {
					if (rowGV.getRowNum() > 0) {
						Cell cell1 = rowGV.getCell(17);
						if (cell1 != null) {
							String ten = cell1.getStringCellValue();
//							System.out.println(ten);
							if (ten.equals("Cán bộ quản lý")) {
								cbql++;
							}
							if (ten.equals("Giáo viên")) {
								gv++;
							}
							if (ten.equals("Nhân viên")) {
								nv++;
							}
						}

					}
				}
				tongSo = cbql + gv + nv;
				if (soCot != 62) {
					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV THPT !").toString();
				} else {
				ThptSoLieuCbGvNvDto ThptSoLieuCbGvNvDto;
				if (action == 0) {
					ThptSoLieuCbGvNvDto = new ThptSoLieuCbGvNvDto();
				} else {
					ThptSoLieuCbGvNvDto = tHPTSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
				}

				ThptSoLieuCbGvNvDto.setNamHoc(namHoc);
				ThptSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
				ThptSoLieuCbGvNvDto.setIdTruong(idTruong);
				ThptSoLieuCbGvNvDto.setCbql(cbql);
				ThptSoLieuCbGvNvDto.setGv(gv);
				ThptSoLieuCbGvNvDto.setNv(nv);
				ThptSoLieuCbGvNvDto.setTongSo(tongSo);
				tHPTSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(ThptSoLieuCbGvNvDto);
				}
			} else {
				return new Response(-1, "Import lỗi dữ liệu CBQL, GV, NV").toString();
			}
			if (fileBean.getFileData3().getOriginalFilename() != "") {
				int soCot = workbookKQHT.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
//				System.out.println(soCot);
				if (loaiTruong == 3) {
					int hlGioi = 0;
					int hlKha = 0;
					int hlTrungBinh = 0;
					int hlYeu = 0;
					int hlKem = 0;
					int hkTot = 0;
					int hkKha = 0;
					int hkTrungBinh = 0;
					int hkYeu = 0;
					for (Row row : workbookKQHT.getSheetAt(0)) {
						if (row.getRowNum() > 0) {
							Cell cellHL = row.getCell(22);
							Cell cellHK = row.getCell(23);
							if (cellHL != null) {
								String val = cellHL.getStringCellValue();
//								System.out.println(ten);
								if (val.equals("Giỏi")) {
									hlGioi++;
								}
								if (val.equals("Khá")) {
									hlKha++;
								}
								if (val.equals("Trung bình")) {
									hlTrungBinh++;
								}
								if (val.equals("Yếu")) {
									hlYeu++;
								}
								if (val.equals("Kém")) {
									hlKem++;
								}
							}
							if (cellHK != null) {
								String val = cellHK.getStringCellValue();
								if (val.equals("Tốt")) {
									hkTot++;
								}
								if (val.equals("Khá")) {
									hkKha++;
								}
								if (val.equals("Trung bình")) {
									hkTrungBinh++;
								}
								if (val.equals("Yếu")) {
									hkYeu++;
								}
							}

						}
					}
					if (soCot != 25) {
						return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu KQHT THCS !").toString();
					} else {
					ThcsKqhtDto ThcsKqhtDto;
					if (action == 0) {
						ThcsKqhtDto = new ThcsKqhtDto();
					} else {
						ThcsKqhtDto = tHCSKqhtService.laySoLieu(idTruong, namHoc, thoiGian);
					}

					ThcsKqhtDto.setNamHoc(namHoc);
					ThcsKqhtDto.setIdTruong(idTruong);
					ThcsKqhtDto.setSlHlGioi(hlGioi);
					ThcsKqhtDto.setSlHlKha(hlKha);
					ThcsKqhtDto.setSlHlTrungBinh(hlTrungBinh);
					ThcsKqhtDto.setSlHlYeu(hlYeu);
					ThcsKqhtDto.setSlHlKem(hlKem);
					ThcsKqhtDto.setSlHkTot(hkTot);
					ThcsKqhtDto.setSlHkKha(hkKha);
					ThcsKqhtDto.setSlHkTrungBinh(hkTrungBinh);
					ThcsKqhtDto.setSlHkYeu(hkYeu);
					ThcsKqhtDto.setThoiGianImport(thoiGian);
					tHCSKqhtService.themSoLieuLopFileExcel(ThcsKqhtDto);
					}
				} else if (loaiTruong == 4) {
					int hlGioi = 0;
					int hlKha = 0;
					int hlTrungBinh = 0;
					int hlYeu = 0;
					int hlKem = 0;
					int hkTot = 0;
					int hkKha = 0;
					int hkTrungBinh = 0;
					int hkYeu = 0;
					for (Row row : workbookKQHT.getSheetAt(0)) {
						if (row.getRowNum() > 0) {
							Cell cellHL = row.getCell(21);
							Cell cellHK = row.getCell(22);
							if (cellHL != null) {
								String val = cellHL.getStringCellValue();
//								System.out.println(ten);
								if (val.equals("Giỏi")) {
									hlGioi++;
								}
								if (val.equals("Khá")) {
									hlKha++;
								}
								if (val.equals("Trung bình")) {
									hlTrungBinh++;
								}
								if (val.equals("Yếu")) {
									hlYeu++;
								}
								if (val.equals("Kém")) {
									hlKem++;
								}
							}
							if (cellHK != null) {
								String val = cellHK.getStringCellValue();
								if (val.equals("Tốt")) {
									hkTot++;
								}
								if (val.equals("Khá")) {
									hkKha++;
								}
								if (val.equals("Trung bình")) {
									hkTrungBinh++;
								}
								if (val.equals("Yếu")) {
									hkYeu++;
								}
							}

						}
					}
					if (soCot != 24) {
						return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu KQHT THPT !").toString();
					} else {
					ThptKqhtDto ThptKqhtDto;
					if (action == 0) {
						ThptKqhtDto = new ThptKqhtDto();
					} else {
						ThptKqhtDto = tHPTKqhtService.laySoLieu(idTruong, namHoc, thoiGian);
					}

					ThptKqhtDto.setNamHoc(namHoc);
					ThptKqhtDto.setIdTruong(idTruong);
					ThptKqhtDto.setSlHlGioi(hlGioi);
					ThptKqhtDto.setSlHlKha(hlKha);
					ThptKqhtDto.setSlHlTrungBinh(hlTrungBinh);
					ThptKqhtDto.setSlHlYeu(hlYeu);
					ThptKqhtDto.setSlHlKem(hlKem);
					ThptKqhtDto.setSlHkTot(hkTot);
					ThptKqhtDto.setSlHkKha(hkKha);
					ThptKqhtDto.setSlHkTrungBinh(hkTrungBinh);
					ThptKqhtDto.setSlHkYeu(hkYeu);
					ThptKqhtDto.setThoiGianImport(thoiGian);
					tHPTKqhtService.themSoLieuLopFileExcel(ThptKqhtDto);
					}
				} else {
					return new Response(-1, "Import lỗi dữ liệu KQHT").toString();
				}
			}

			return new Response(1, "Success").toString();

		} catch (IOException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@Override
	public String kiemTra(int idTruong, int namHoc, int thoiGian, int loaiTruong) {
//		DmTruongDto ttTruong = dmTruongService.layTTTruong(idTruong);
////		int loaiTruong = ttTruong.getLoaiTruong();
//		if (loaiTruong == 1) {
//			boolean kt = mNSoLieuLopServ.existsByIdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
//			if (kt) {
//				return new Response(-1, "Dữ liệu tháng "+thoiGian+"/"+namHoc+" đã tồn tại, nhấn Có để cập nhật lại dữ liệu").toString();
//			} else {
//				return new Response(1, "OK").toString();
//			}
//
//		} else if (loaiTruong == 2) {
//			boolean kt = tHSoLieuLopService.existsByIdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
//			if (kt) {
//				return new Response(-1, "Dữ liệu tháng "+thoiGian+"/"+namHoc+" đã tồn tại, nhấn Có để cập nhật lại dữ liệu").toString();
//			} else {
//				return new Response(1, "OK").toString();
//			}
//		} else if (loaiTruong == 3) {
//			boolean kt = tHCSSoLieuLopService.existsByIdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
//			if (kt) {
//				return new Response(-1, "Dữ liệu tháng "+thoiGian+"/"+namHoc+" đã tồn tại, nhấn Có để cập nhật lại dữ liệu").toString();
//			} else {
//				return new Response(1, "OK").toString();
//			}
//		} else {
//			boolean kt = tHPTSoLieuLopService.existsByIdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
//			if (kt) {
//				return new Response(-1, "Dữ liệu tháng "+thoiGian+"/"+namHoc+" đã tồn tại, nhấn Có để cập nhật lại dữ liệu").toString();
//			} else {
//				return new Response(1, "OK").toString();
//			}
//		}
		return new Response(1, "OK").toString();
	}

	@Override
	public String importSoLieuVnEdu(FileBean fileBean, int idPhongGd, int namHoc, int thoiGian, int action,
			int loaiTruong) {
		ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
		ByteArrayInputStream bisHS = new ByteArrayInputStream(fileBean.getFileData1().getBytes());
		ByteArrayInputStream bisGV = new ByteArrayInputStream(fileBean.getFileData2().getBytes());
		ByteArrayInputStream bisKQHT = new ByteArrayInputStream(fileBean.getFileData3().getBytes());
		Workbook workbook = null;
		Workbook workbookHS = null;
		Workbook workbookGV = null;
		Workbook workbookKQHT = null;

		try {
			if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
				workbook = new HSSFWorkbook(bis);
			} else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(bis);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel số liệu lớp !").toString();
			}

//			if (fileBean.getFileData1().getOriginalFilename().endsWith("xls")) {
//				workbookHS = new HSSFWorkbook(bisHS);
//			} else if (fileBean.getFileData1().getOriginalFilename().endsWith("xlsx")) {
//				workbookHS = new XSSFWorkbook(bisHS);
//			} else {
//				return new Response(-1, "Vui lòng chọn file Excel số liệu học sinh!").toString();
//			}

			if (fileBean.getFileData2().getOriginalFilename().endsWith("xls")) {
				workbookGV = new HSSFWorkbook(bisGV);
			} else if (fileBean.getFileData2().getOriginalFilename().endsWith("xlsx")) {
				workbookGV = new XSSFWorkbook(bisGV);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel số liệu giáo viên!").toString();
			}

			if (loaiTruong == 3 || loaiTruong == 4) {
				if (fileBean.getFileData3().getOriginalFilename().endsWith("xls")) {
					workbookKQHT = new HSSFWorkbook(bisKQHT);
				} else if (fileBean.getFileData3().getOriginalFilename().endsWith("xlsx")) {
					workbookKQHT = new XSSFWorkbook(bisKQHT);
				}
			}

			// Xử lý số liệu lớp
			if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 2) {
				// Xử lý số liệu lớp
				int k1 = 0;
				int k2 = 0;
				int k3 = 0;
				int k4 = 0;
				int k5 = 0;
				int tongSo = 0;
				int idTruong = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 7) {
						Cell cell2 = row.getCell(2);
						Cell cell4 = row.getCell(4);
						Cell cell5 = row.getCell(5);
						Cell cell6 = row.getCell(6);
						Cell cell7 = row.getCell(7);
						Cell cell8 = row.getCell(8);
						if(cell2 != null) {
							String tenTruong = cell2.getStringCellValue();
							System.out.println(tenTruong);
							System.out.println(idPhongGd);
							if(tenTruong != null && !tenTruong.isEmpty()) {
								DmTruongDto dmTruong = dmTruongService.layIdTruong(tenTruong, idPhongGd);
								idTruong = dmTruong.getIdTruong();
							}
						}
						if (cell4 != null) {
							k1 =(int) cell4.getNumericCellValue();
						}
						if (cell5 != null) {
							k2 =(int) cell5.getNumericCellValue();
						}
						if (cell6 != null) {
							k3 =(int) cell6.getNumericCellValue();
						}
						if (cell7 != null) {
							k4 =(int) cell7.getNumericCellValue();
						}
						if (cell8 != null) {
							k5 =(int) cell8.getNumericCellValue();
						}
						
						tongSo = k1 + k2 + k3 + k4 + k5;
						if(k1 != 0 && k2 != 0 && k3 != 0 && k4 != 0 && k5 != 0) {
							ThSoLieuLopDto ThSoLieuLopDto;
							if (action == 0) {
								ThSoLieuLopDto = new ThSoLieuLopDto();
							} else {
								ThSoLieuLopDto = tHSoLieuLopService.laySoLieu(idTruong, namHoc, thoiGian);
							}

							ThSoLieuLopDto.setNamHoc(namHoc);
							ThSoLieuLopDto.setThoiGianImport(thoiGian);
							ThSoLieuLopDto.setIdTruong(idTruong);
							ThSoLieuLopDto.setK1(k1);
							ThSoLieuLopDto.setK2(k2);
							ThSoLieuLopDto.setK3(k3);
							ThSoLieuLopDto.setK4(k4);
							ThSoLieuLopDto.setK5(k5);
							ThSoLieuLopDto.setTongSo(tongSo);
							tHSoLieuLopService.themSoLieuLopFileExcel(ThSoLieuLopDto);
						}
					}
				}
				
			}
			else if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 3) {
				// Xử lý số liệu lớp
				int k6 = 0;
				int k7 = 0;
				int k8 = 0;
				int k9 = 0;
				int tongSo = 0;
				int idTruong = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 7) {
						Cell cell2 = row.getCell(2);
						Cell cell4 = row.getCell(4);
						Cell cell5 = row.getCell(5);
						Cell cell6 = row.getCell(6);
						Cell cell7 = row.getCell(7);

						if(cell2 != null) {
							String tenTruong = cell2.getStringCellValue();
							System.out.println(tenTruong);
							System.out.println(idPhongGd);
							if(tenTruong != null && !tenTruong.isEmpty()) {
								DmTruongDto dmTruong = dmTruongService.layIdTruong(tenTruong, idPhongGd);
								idTruong = dmTruong.getIdTruong();
							}
						}
						if (cell4 != null) {
							k6 =(int) cell4.getNumericCellValue();
						}
						if (cell5 != null) {
							k7 =(int) cell5.getNumericCellValue();
						}
						if (cell6 != null) {
							k8 =(int) cell6.getNumericCellValue();
						}
						if (cell7 != null) {
							k9 =(int) cell7.getNumericCellValue();
						}
						
						tongSo = k6 + k7 + k8 + k9;
						
						ThcsSoLieuLopDto ThcsSoLieuLopDto;
						if (action == 0) {
							ThcsSoLieuLopDto = new ThcsSoLieuLopDto();
						} else {
							ThcsSoLieuLopDto = tHCSSoLieuLopService.laySoLieu(idTruong, namHoc, thoiGian);
						}
						

						ThcsSoLieuLopDto.setNamHoc(namHoc);
						ThcsSoLieuLopDto.setThoiGianImport(thoiGian);
						ThcsSoLieuLopDto.setIdTruong(idTruong);
						ThcsSoLieuLopDto.setK6(k6);
						ThcsSoLieuLopDto.setK7(k7);
						ThcsSoLieuLopDto.setK8(k8);
						ThcsSoLieuLopDto.setK9(k9);
						ThcsSoLieuLopDto.setTongSo(tongSo);
						tHCSSoLieuLopService.themSoLieuLopFileExcel(ThcsSoLieuLopDto);
					}
				}
			}
			 else if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 4) {
				// Xử lý số liệu lớp
				int k10 = 0;
				int k11 = 0;
				int k12 = 0;
				int tongSo = 0;
				int idTruong = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 7) {
						
						Cell cell2 = row.getCell(2);
						Cell cell4 = row.getCell(4);
						Cell cell5 = row.getCell(5);
						Cell cell6 = row.getCell(6);

						if(cell2 != null) {
							String tenTruong = cell2.getStringCellValue();
							System.out.println(tenTruong);
							System.out.println(idPhongGd);
							if(tenTruong != null && !tenTruong.isEmpty()) {
								DmTruongDto dmTruong = dmTruongService.layIdTruong(tenTruong, idPhongGd);
								idTruong = dmTruong.getIdTruong();
							}
						}
						if (cell4 != null) {
							k10 =(int) cell4.getNumericCellValue();
						}
						if (cell5 != null) {
							k11 =(int) cell5.getNumericCellValue();
						}
						if (cell6 != null) {
							k12 =(int) cell6.getNumericCellValue();
						}
						
						tongSo = k10 + k11 + k12;
						ThptSoLieuLopDto ThptSoLieuLopDto;
						if (action == 0) {
							ThptSoLieuLopDto = new ThptSoLieuLopDto();
						} else {
							ThptSoLieuLopDto = tHPTSoLieuLopService.laySoLieu(idTruong, namHoc, thoiGian);
						}

						ThptSoLieuLopDto.setNamHoc(namHoc);
						ThptSoLieuLopDto.setThoiGianImport(thoiGian);
						ThptSoLieuLopDto.setIdTruong(idTruong);
						ThptSoLieuLopDto.setK10(k10);
						ThptSoLieuLopDto.setK11(k11);
						ThptSoLieuLopDto.setK12(k12);
						ThptSoLieuLopDto.setTongSo(tongSo);
						tHPTSoLieuLopService.themSoLieuLopFileExcel(ThptSoLieuLopDto);
					}
				}
			} 
			else {
				return new Response(-1, "Import lỗi dữ liệu lớp học").toString();
			}

			// Xử lý số liệu học sinh
			if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 2) {
				// Xử lý số liệu học sinh tiểu học
				int k1 = 0;
				int k2 = 0;
				int k3 = 0;
				int k4 = 0;
				int k5 = 0;
				int tongSo = 0;
				int idTruong = 0;
				for (Row row : workbook.getSheetAt(1)) {
					if (row.getRowNum() > 7) {
						Cell cell2 = row.getCell(2);
						Cell cell4 = row.getCell(4);
						Cell cell5 = row.getCell(5);
						Cell cell6 = row.getCell(6);
						Cell cell7 = row.getCell(7);
						Cell cell8 = row.getCell(8);
						if(cell2 != null) {
							String tenTruong = cell2.getStringCellValue();
							System.out.println(tenTruong);
							System.out.println(idPhongGd);
							if(tenTruong != null && !tenTruong.isEmpty()) {
								DmTruongDto dmTruong = dmTruongService.layIdTruong(tenTruong, idPhongGd);
								idTruong = dmTruong.getIdTruong();
							}
						}
						if (cell4 != null) {
							k1 =(int) cell4.getNumericCellValue();
						}
						if (cell5 != null) {
							k2 =(int) cell5.getNumericCellValue();
						}
						if (cell6 != null) {
							k3 =(int) cell6.getNumericCellValue();
						}
						if (cell7 != null) {
							k4 =(int) cell7.getNumericCellValue();
						}
						if (cell8 != null) {
							k5 =(int) cell8.getNumericCellValue();
						}
						
						tongSo = k1 + k2 + k3 + k4 + k5;
						ThSoLieuHocSinhDto ThSoLieuHocSinhDto;
						if (action == 0) {
							ThSoLieuHocSinhDto = new ThSoLieuHocSinhDto();
						} else {
							ThSoLieuHocSinhDto = tHSoLieuHocSinhService.laySoLieu(idTruong, namHoc, thoiGian);
						}

						ThSoLieuHocSinhDto.setNamHoc(namHoc);
						ThSoLieuHocSinhDto.setThoiGianImport(thoiGian);
						ThSoLieuHocSinhDto.setIdTruong(idTruong);
						ThSoLieuHocSinhDto.setK1(k1);
						ThSoLieuHocSinhDto.setK2(k2);
						ThSoLieuHocSinhDto.setK3(k3);
						ThSoLieuHocSinhDto.setK4(k4);
						ThSoLieuHocSinhDto.setK5(k5);
						ThSoLieuHocSinhDto.setTongSo(tongSo);
						tHSoLieuHocSinhService.themSoLieuHocSinhFileExcel(ThSoLieuHocSinhDto);
					}
				}
			} else if (fileBean.getFileData().getOriginalFilename() != "" && loaiTruong == 3) {
				// Xử lý số liệu học sinh tiểu học
				int k6 = 0;
				int k7 = 0;
				int k8 = 0;
				int k9 = 0;
				int tongSo = 0;
				int idTruong = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 7) {
						
						Cell cell2 = row.getCell(2);
						Cell cell4 = row.getCell(20);
						Cell cell5 = row.getCell(21);
						Cell cell6 = row.getCell(22);
						Cell cell7 = row.getCell(23);

						if(cell2 != null) {
							String tenTruong = cell2.getStringCellValue();
							System.out.println(tenTruong);
							System.out.println(idPhongGd);
							if(tenTruong != null && !tenTruong.isEmpty()) {
								DmTruongDto dmTruong = dmTruongService.layIdTruong(tenTruong, idPhongGd);
								idTruong = dmTruong.getIdTruong();
							}
						}
						if (cell4 != null) {
							k6 =(int) cell4.getNumericCellValue();
						}
						if (cell5 != null) {
							k7 =(int) cell5.getNumericCellValue();
						}
						if (cell6 != null) {
							k8 =(int) cell6.getNumericCellValue();
						}
						if (cell7 != null) {
							k9 =(int) cell7.getNumericCellValue();
						}
						
						tongSo = k6 + k7 + k8 + k9;
						ThcsSoLieuHocSinhDto ThcsSoLieuHocSinhDto;
						if (action == 0) {
							ThcsSoLieuHocSinhDto = new ThcsSoLieuHocSinhDto();
						} else {
							ThcsSoLieuHocSinhDto = tHCSSoLieuHocSinhService.laySoLieu(idTruong, namHoc, thoiGian);
						}
						
						ThcsSoLieuHocSinhDto.setNamHoc(namHoc);
						ThcsSoLieuHocSinhDto.setThoiGianImport(thoiGian);
						ThcsSoLieuHocSinhDto.setIdTruong(idTruong);
						ThcsSoLieuHocSinhDto.setK6(k6);
						ThcsSoLieuHocSinhDto.setK7(k7);
						ThcsSoLieuHocSinhDto.setK8(k8);
						ThcsSoLieuHocSinhDto.setK9(k9);
						ThcsSoLieuHocSinhDto.setTongSo(tongSo);
						tHCSSoLieuHocSinhService.themSoLieuHocSinhFileExcel(ThcsSoLieuHocSinhDto);
					}
				}
			} else if (fileBean.getFileData1().getOriginalFilename() != "" && loaiTruong == 4) {
				
				// Xử lý số liệu học sinh
				int k10 = 0;
				int k11 = 0;
				int k12 = 0;
				int tongSo = 0;
				int idTruong = 0;
				for (Row row : workbook.getSheetAt(0)) {
					if (row.getRowNum() > 7) {
						Cell cell2 = row.getCell(2);
						Cell cell4 = row.getCell(8);
						Cell cell5 = row.getCell(9);
						Cell cell6 = row.getCell(10);

						if(cell2 != null) {
							String tenTruong = cell2.getStringCellValue();
							System.out.println(tenTruong);
							System.out.println(idPhongGd);
							if(tenTruong != null && !tenTruong.isEmpty()) {
								DmTruongDto dmTruong = dmTruongService.layIdTruong(tenTruong, idPhongGd);
								idTruong = dmTruong.getIdTruong();
							}
						}
						if (cell4 != null) {
							k10 =(int) cell4.getNumericCellValue();
						}
						if (cell5 != null) {
							k11 =(int) cell5.getNumericCellValue();
						}
						if (cell6 != null) {
							k12 =(int) cell6.getNumericCellValue();
						}
						tongSo = k10 + k11 + k12;
						
						ThptSoLieuHocSinhDto ThptSoLieuHocSinhDto;
						if (action == 0) {
							ThptSoLieuHocSinhDto = new ThptSoLieuHocSinhDto();
						} else {
							ThptSoLieuHocSinhDto = tHPTSoLieuHocSinhService.laySoLieu(idTruong, namHoc, thoiGian);
						}

						ThptSoLieuHocSinhDto.setNamHoc(namHoc);
						ThptSoLieuHocSinhDto.setThoiGianImport(thoiGian);
						ThptSoLieuHocSinhDto.setIdTruong(idTruong);
						ThptSoLieuHocSinhDto.setK10(k10);
						ThptSoLieuHocSinhDto.setK11(k11);
						ThptSoLieuHocSinhDto.setK12(k12);
						ThptSoLieuHocSinhDto.setTongSo(tongSo);
						tHPTSoLieuHocSinhService.themSoLieuHocSinhFileExcel(ThptSoLieuHocSinhDto);
						
					}
				}
			} else {
				return new Response(-1, "Import lỗi dữ liệu học sinh").toString();
			}
//
//			// Xử lý số liệu cbql, gv, nv
//			if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 1) {
//				int soCot = workbookGV.getSheetAt(0).getRow(1).getPhysicalNumberOfCells();
////				System.out.println(soCot);
//				// Xử lý dữ liệu giáo viên
//				int cbql = 0;
//				int gv = 0;
//				int nv = 0;
//				for (Row rowGV : workbookGV.getSheetAt(0)) {
//					if (rowGV.getRowNum() > 0) {
//						Cell cell1 = rowGV.getCell(17);
//						if (cell1 != null) {
//							String ten = cell1.getStringCellValue();
////						System.out.println(ten);
//							if (ten.equals("Cán bộ quản lý")) {
//								cbql++;
//							}
//							if (ten.equals("Giáo viên")) {
//								gv++;
//							}
//							if (ten.equals("Nhân viên")) {
//								nv++;
//							}
//						}
//
//					}
//				}
//				
//				if (soCot != 55) {
//					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV Mầm non !").toString();
//				} else {
//				MnSoLieuCbGvNvDto MnSoLieuCbGvNvDto;
//				if (action == 0) {
//					MnSoLieuCbGvNvDto = new MnSoLieuCbGvNvDto();
//				} else {
//					MnSoLieuCbGvNvDto = mNSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
//				}
//
//				MnSoLieuCbGvNvDto.setIdTruong(idTruong);
//				MnSoLieuCbGvNvDto.setCbql(cbql);
//				MnSoLieuCbGvNvDto.setGv(gv);
//				MnSoLieuCbGvNvDto.setNv(nv);
//				MnSoLieuCbGvNvDto.setNamHoc(namHoc);
//				MnSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
//				mNSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(MnSoLieuCbGvNvDto);
//				}
//			} else if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 2) {
//				int soCot = workbookGV.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
////				System.out.println(soCot);
//				// Xử lý số liệu cán bộ ql, gv, nv tiểu học
//				int cbql = 0;
//				int gv = 0;
//				int nv = 0;
//				int tongSo = 0;
//				for (Row rowGV : workbookGV.getSheetAt(0)) {
//					if (rowGV.getRowNum() > 0) {
//						Cell cell1 = rowGV.getCell(17);
//						if (cell1 != null) {
//							String ten = cell1.getStringCellValue();
////							System.out.println(ten);
//							if (ten.equals("Cán bộ quản lý")) {
//								cbql++;
//							}
//							if (ten.equals("Giáo viên")) {
//								gv++;
//							}
//							if (ten.equals("Nhân viên")) {
//								nv++;
//							}
//						}
//
//					}
//				}
//				tongSo = cbql + gv + nv;
//				if (soCot != 63) {
//					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV Tiểu học !").toString();
//				} else {
//				ThSoLieuCbGvNvDto ThSoLieuCbGvNvDto;
//				if (action == 0) {
//					ThSoLieuCbGvNvDto = new ThSoLieuCbGvNvDto();
//				} else {
//					ThSoLieuCbGvNvDto = tHSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
//				}
//
//				ThSoLieuCbGvNvDto.setNamHoc(namHoc);
//				ThSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
//				ThSoLieuCbGvNvDto.setIdTruong(idTruong);
//				ThSoLieuCbGvNvDto.setCbql(cbql);
//				ThSoLieuCbGvNvDto.setGv(gv);
//				ThSoLieuCbGvNvDto.setNv(nv);
//				ThSoLieuCbGvNvDto.setTongSo(tongSo);
//				tHSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(ThSoLieuCbGvNvDto);
//				}
//			} else if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 3) {
//				int soCot = workbookGV.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
////				System.out.println(soCot);
//				// Xử lý số liệu cán bộ ql, gv, nv tiểu học
//				int cbql = 0;
//				int gv = 0;
//				int nv = 0;
//				int tongSo = 0;
//				for (Row rowGV : workbookGV.getSheetAt(0)) {
//					if (rowGV.getRowNum() > 0) {
//						Cell cell1 = rowGV.getCell(17);
//						if (cell1 != null) {
//							String ten = cell1.getStringCellValue();
////							System.out.println(ten);
//							if (ten.equals("Cán bộ quản lý")) {
//								cbql++;
//							}
//							if (ten.equals("Giáo viên")) {
//								gv++;
//							}
//							if (ten.equals("Nhân viên")) {
//								nv++;
//							}
//						}
//
//					}
//				}
//				tongSo = cbql + gv + nv;
//				if (soCot != 63) {
//					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV THCS !").toString();
//				} else {
//				ThcsSoLieuCbGvNvDto ThcsSoLieuCbGvNvDto;
//				if (action == 0) {
//					ThcsSoLieuCbGvNvDto = new ThcsSoLieuCbGvNvDto();
//				} else {
//					ThcsSoLieuCbGvNvDto = tHCSSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
//				}
//
//				ThcsSoLieuCbGvNvDto.setNamHoc(namHoc);
//				ThcsSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
//				ThcsSoLieuCbGvNvDto.setIdTruong(idTruong);
//				ThcsSoLieuCbGvNvDto.setCbql(cbql);
//				ThcsSoLieuCbGvNvDto.setGv(gv);
//				ThcsSoLieuCbGvNvDto.setNv(nv);
//				ThcsSoLieuCbGvNvDto.setTongSo(tongSo);
//				tHCSSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(ThcsSoLieuCbGvNvDto);
//				}
//			} else if (fileBean.getFileData2().getOriginalFilename() != "" && loaiTruong == 4) {
//				int soCot = workbookGV.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
////				System.out.println(soCot);
//				// Xử lý số liệu cán bộ ql, gv, nv tiểu học
//				int cbql = 0;
//				int gv = 0;
//				int nv = 0;
//				int tongSo = 0;
//				for (Row rowGV : workbookGV.getSheetAt(0)) {
//					if (rowGV.getRowNum() > 0) {
//						Cell cell1 = rowGV.getCell(17);
//						if (cell1 != null) {
//							String ten = cell1.getStringCellValue();
////							System.out.println(ten);
//							if (ten.equals("Cán bộ quản lý")) {
//								cbql++;
//							}
//							if (ten.equals("Giáo viên")) {
//								gv++;
//							}
//							if (ten.equals("Nhân viên")) {
//								nv++;
//							}
//						}
//
//					}
//				}
//				tongSo = cbql + gv + nv;
//				if (soCot != 62) {
//					return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu CBQL, GV, NV THPT !").toString();
//				} else {
//				ThptSoLieuCbGvNvDto ThptSoLieuCbGvNvDto;
//				if (action == 0) {
//					ThptSoLieuCbGvNvDto = new ThptSoLieuCbGvNvDto();
//				} else {
//					ThptSoLieuCbGvNvDto = tHPTSoLieuCbGvNvService.laySoLieu(idTruong, namHoc, thoiGian);
//				}
//
//				ThptSoLieuCbGvNvDto.setNamHoc(namHoc);
//				ThptSoLieuCbGvNvDto.setThoiGianImport(thoiGian);
//				ThptSoLieuCbGvNvDto.setIdTruong(idTruong);
//				ThptSoLieuCbGvNvDto.setCbql(cbql);
//				ThptSoLieuCbGvNvDto.setGv(gv);
//				ThptSoLieuCbGvNvDto.setNv(nv);
//				ThptSoLieuCbGvNvDto.setTongSo(tongSo);
//				tHPTSoLieuCbGvNvService.themSoLieuCbGvNvFileExcel(ThptSoLieuCbGvNvDto);
//				}
//			} else {
//				return new Response(-1, "Import lỗi dữ liệu CBQL, GV, NV").toString();
//			}
//			
//			if (fileBean.getFileData3().getOriginalFilename() != "") {
//				int soCot = workbookKQHT.getSheetAt(0).getRow(0).getPhysicalNumberOfCells();
////				System.out.println(soCot);
//				if (loaiTruong == 3) {
//					int hlGioi = 0;
//					int hlKha = 0;
//					int hlTrungBinh = 0;
//					int hlYeu = 0;
//					int hlKem = 0;
//					int hkTot = 0;
//					int hkKha = 0;
//					int hkTrungBinh = 0;
//					int hkYeu = 0;
//					for (Row row : workbookKQHT.getSheetAt(0)) {
//						if (row.getRowNum() > 0) {
//							Cell cellHL = row.getCell(22);
//							Cell cellHK = row.getCell(23);
//							if (cellHL != null) {
//								String val = cellHL.getStringCellValue();
////								System.out.println(ten);
//								if (val.equals("Giỏi")) {
//									hlGioi++;
//								}
//								if (val.equals("Khá")) {
//									hlKha++;
//								}
//								if (val.equals("Trung bình")) {
//									hlTrungBinh++;
//								}
//								if (val.equals("Yếu")) {
//									hlYeu++;
//								}
//								if (val.equals("Kém")) {
//									hlKem++;
//								}
//							}
//							if (cellHK != null) {
//								String val = cellHK.getStringCellValue();
//								if (val.equals("Tốt")) {
//									hkTot++;
//								}
//								if (val.equals("Khá")) {
//									hkKha++;
//								}
//								if (val.equals("Trung bình")) {
//									hkTrungBinh++;
//								}
//								if (val.equals("Yếu")) {
//									hkYeu++;
//								}
//							}
//
//						}
//					}
//					if (soCot != 25) {
//						return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu KQHT THCS !").toString();
//					} else {
//					ThcsKqhtDto ThcsKqhtDto;
//					if (action == 0) {
//						ThcsKqhtDto = new ThcsKqhtDto();
//					} else {
//						ThcsKqhtDto = tHCSKqhtService.laySoLieu(idTruong, namHoc, thoiGian);
//					}
//
//					ThcsKqhtDto.setNamHoc(namHoc);
//					ThcsKqhtDto.setIdTruong(idTruong);
//					ThcsKqhtDto.setSlHlGioi(hlGioi);
//					ThcsKqhtDto.setSlHlKha(hlKha);
//					ThcsKqhtDto.setSlHlTrungBinh(hlTrungBinh);
//					ThcsKqhtDto.setSlHlYeu(hlYeu);
//					ThcsKqhtDto.setSlHlKem(hlKem);
//					ThcsKqhtDto.setSlHkTot(hkTot);
//					ThcsKqhtDto.setSlHkKha(hkKha);
//					ThcsKqhtDto.setSlHkTrungBinh(hkTrungBinh);
//					ThcsKqhtDto.setSlHkYeu(hkYeu);
//					ThcsKqhtDto.setThoiGianImport(thoiGian);
//					tHCSKqhtService.themSoLieuLopFileExcel(ThcsKqhtDto);
//					}
//				} else if (loaiTruong == 4) {
//					int hlGioi = 0;
//					int hlKha = 0;
//					int hlTrungBinh = 0;
//					int hlYeu = 0;
//					int hlKem = 0;
//					int hkTot = 0;
//					int hkKha = 0;
//					int hkTrungBinh = 0;
//					int hkYeu = 0;
//					for (Row row : workbookKQHT.getSheetAt(0)) {
//						if (row.getRowNum() > 0) {
//							Cell cellHL = row.getCell(21);
//							Cell cellHK = row.getCell(22);
//							if (cellHL != null) {
//								String val = cellHL.getStringCellValue();
////								System.out.println(ten);
//								if (val.equals("Giỏi")) {
//									hlGioi++;
//								}
//								if (val.equals("Khá")) {
//									hlKha++;
//								}
//								if (val.equals("Trung bình")) {
//									hlTrungBinh++;
//								}
//								if (val.equals("Yếu")) {
//									hlYeu++;
//								}
//								if (val.equals("Kém")) {
//									hlKem++;
//								}
//							}
//							if (cellHK != null) {
//								String val = cellHK.getStringCellValue();
//								if (val.equals("Tốt")) {
//									hkTot++;
//								}
//								if (val.equals("Khá")) {
//									hkKha++;
//								}
//								if (val.equals("Trung bình")) {
//									hkTrungBinh++;
//								}
//								if (val.equals("Yếu")) {
//									hkYeu++;
//								}
//							}
//
//						}
//					}
//					if (soCot != 24) {
//						return new Response(-1, "Import dữ liệu không thành công. Vui lòng kiểm tra lại file số liệu KQHT THPT !").toString();
//					} else {
//					ThptKqhtDto ThptKqhtDto;
//					if (action == 0) {
//						ThptKqhtDto = new ThptKqhtDto();
//					} else {
//						ThptKqhtDto = tHPTKqhtService.laySoLieu(idTruong, namHoc, thoiGian);
//					}
//
//					ThptKqhtDto.setNamHoc(namHoc);
//					ThptKqhtDto.setIdTruong(idTruong);
//					ThptKqhtDto.setSlHlGioi(hlGioi);
//					ThptKqhtDto.setSlHlKha(hlKha);
//					ThptKqhtDto.setSlHlTrungBinh(hlTrungBinh);
//					ThptKqhtDto.setSlHlYeu(hlYeu);
//					ThptKqhtDto.setSlHlKem(hlKem);
//					ThptKqhtDto.setSlHkTot(hkTot);
//					ThptKqhtDto.setSlHkKha(hkKha);
//					ThptKqhtDto.setSlHkTrungBinh(hkTrungBinh);
//					ThptKqhtDto.setSlHkYeu(hkYeu);
//					ThptKqhtDto.setThoiGianImport(thoiGian);
//					tHPTKqhtService.themSoLieuLopFileExcel(ThptKqhtDto);
//					}
//				} else {
//					return new Response(-1, "Import lỗi dữ liệu KQHT").toString();
//				}
//			}

			return new Response(1, "Success").toString();

		} catch (IOException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

}
