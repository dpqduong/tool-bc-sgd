package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmNamHoc;

public interface DmNamHocRepository extends CrudRepository<DmNamHoc, Integer> {
	Page<DmNamHoc> findByOrderByIdDesc(Pageable pageable);
	
}
