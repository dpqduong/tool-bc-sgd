package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.DmTruongConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;
import vn.vnpt.camau.toolbc.sgd.web.entity.VaiTro;
import vn.vnpt.camau.toolbc.sgd.web.repository.DmTruongRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.DmTruongService;

@Service
public class DmTruongServiceImp implements DmTruongService {

	@Autowired
	private DmTruongConverter converter;
	@Autowired
	private DmTruongRepository repository;

	@Override
	public Page<DmTruongDto> layDsDmTruong(Pageable pageable) {
		Page<DmTruong> dsDmTruongCanLay = repository.layDsTruong(pageable);
		Page<DmTruongDto> dsDmTruongDaChuyenDoi = dsDmTruongCanLay.map(converter::convertToDto);
		return dsDmTruongDaChuyenDoi;
	}
	
	@Override
	public List<DmTruongDto> layDsDmTruong() {
		Pageable pageable = null;
		List<DmTruongDto> dsDmTruongCanLay = layDsDmTruong(pageable).getContent().stream()
				.collect(Collectors.toList());
		return dsDmTruongCanLay;
	}

	@Override
	public DmTruongDto layTTTruong(int id) {
		DmTruong DmTruongCanLay = repository.findOne(id);
		DmTruongDto DmTruongDaChuyenDoi = converter.convertToDto(DmTruongCanLay);
		return DmTruongDaChuyenDoi;
	}

	@Override
	public DmTruongDto layIdTruong(String tenTruong, int idPhongGd) {
		DmTruong DmTruongCanLay = repository.layIdTruong(tenTruong, idPhongGd);
		DmTruongDto DmTruongDaChuyenDoi = converter.convertToDto(DmTruongCanLay);
		return DmTruongDaChuyenDoi;
	}

	@Override
	public List<DmTruongDto> layDsDmTruongByIdDonVi(int idDonVi) {
		List<DmTruong> DmTruongCanLay = repository.findByDmPhongGd_IdPhongGd(idDonVi);
		List<DmTruongDto> DmTruongDaChuyenDoi = DmTruongCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return DmTruongDaChuyenDoi;
	}

	@Override
	public List<DmTruongDto> layDsDmTruongTheoCap(int cap) {
		List<DmTruong> DmTruongCanLay = repository.findByLoaiTruongOrderByLoaiTruongAsc(cap);
		List<DmTruongDto> DmTruongDaChuyenDoi = DmTruongCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return DmTruongDaChuyenDoi;
	}

	@Override
	public List<DmTruongDto> layDsDmTruongByIdDonViAndTheoCap(int idDonVi, int cap) {
		List<DmTruong> DmTruongCanLay = repository.findByDmPhongGd_IdPhongGdAndLoaiTruongOrderByLoaiTruongAsc(idDonVi,cap);
		List<DmTruongDto> DmTruongDaChuyenDoi = DmTruongCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return DmTruongDaChuyenDoi;
	}

}
