package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuLopDto;

public interface ThSoLieuHocSinhService {
	void themSoLieuHocSinhFileExcel(ThSoLieuHocSinhDto soLieuHocSinh) throws Exception;
	ThSoLieuHocSinhDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
