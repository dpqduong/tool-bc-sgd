package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.SgdTrektConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrekt;
import vn.vnpt.camau.toolbc.sgd.web.repository.SgdTrektRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektService;

@Service
public class SgdTrektServiceImp implements SgdTrektService {

	@Autowired
	private SgdTrektConverter converter;
	@Autowired
	private SgdTrektRepository repository;

	@Override
	public Page<SgdTrektDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<SgdTrekt> soLieuCanLay;
		if(idDonVi == 999) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdOrderByThangAsc(namHoc, thang, 1,pageable);
		}else {
			soLieuCanLay = repository.findByNamAndThangOrderByThangAsc(namHoc, thang, pageable);
		}
		Page<SgdTrektDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public SgdTrektDto luuSoLieu(SgdTrektDto dto) throws Exception {
		SgdTrekt soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		SgdTrektDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private SgdTrekt themSoLieu(SgdTrektDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam(), dto.getNoiDung())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		SgdTrekt soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam, String noiDung) {
		SgdTrekt canKiemTra = repository.findByThangAndNamAndNoiDung(thang, nam, noiDung);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private SgdTrekt capNhatSoLieu(SgdTrektDto dto) throws Exception {
		SgdTrekt canKiemTra = kiemTraSoLieu(dto.getId());
		SgdTrekt soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private SgdTrekt kiemTraSoLieu(int id) throws Exception {
		SgdTrekt canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private SgdTrekt kiemTraSoLieuDaGui(int id) throws Exception {
		SgdTrekt canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public SgdTrektDto laySoLieu(int id) {
		SgdTrekt soLieuCanLay = repository.findById(id);
		SgdTrektDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		SgdTrekt soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
