package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;

public interface MnSoLieuLopRepository extends CrudRepository<MnSoLieuLop, Integer> {
	public boolean existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	MnSoLieuLop findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
