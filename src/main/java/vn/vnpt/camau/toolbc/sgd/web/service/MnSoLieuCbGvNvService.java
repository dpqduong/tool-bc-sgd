package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuTreDto;

public interface MnSoLieuCbGvNvService {
	void themSoLieuCbGvNvFileExcel(MnSoLieuCbGvNvDto soLieuCbGvNv) throws Exception;
	MnSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
