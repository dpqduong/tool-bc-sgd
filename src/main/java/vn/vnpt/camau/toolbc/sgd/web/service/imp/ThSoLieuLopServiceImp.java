package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThSoLieuLopConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThSoLieuLopRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThSoLieuLopService;

@Service
public class ThSoLieuLopServiceImp implements ThSoLieuLopService {

	@Autowired
	private ThSoLieuLopConverter converter;
	@Autowired
	private ThSoLieuLopRepository repository;
	
	@Override
	public void themSoLieuLopFileExcel(ThSoLieuLopDto soLieuLop) throws Exception {
		ThSoLieuLop soLieuLopCanLuu= converter.convertToEntity(soLieuLop);
		try {
			repository.save(soLieuLopCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian) {
		return repository.existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
	}

	@Override
	public ThSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThSoLieuLop soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThSoLieuLopDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
