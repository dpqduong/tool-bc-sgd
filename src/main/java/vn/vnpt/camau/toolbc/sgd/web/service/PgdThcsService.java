package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThcsDto;

public interface PgdThcsService {
	public Page<PgdThcsDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	PgdThcsDto luuSoLieu(PgdThcsDto dto) throws Exception;
	PgdThcsDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
