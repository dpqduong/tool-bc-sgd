package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuLop;

public interface ThptSoLieuLopRepository extends CrudRepository<ThptSoLieuLop, Long> {
	public boolean existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	ThptSoLieuLop findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
