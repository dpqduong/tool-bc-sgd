package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuHocSinh;

@Component
public class ThcsSoLieuHocSinhConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThcsSoLieuHocSinhDto convertToDto(ThcsSoLieuHocSinh entity) {
		if (entity == null) {
			return new ThcsSoLieuHocSinhDto();
		}
		ThcsSoLieuHocSinhDto dto = mapper.map(entity, ThcsSoLieuHocSinhDto.class);
		return dto;
	}

	public ThcsSoLieuHocSinh convertToEntity(ThcsSoLieuHocSinhDto dto) {
		if (dto == null) {
			return new ThcsSoLieuHocSinh();
		}
		ThcsSoLieuHocSinh entity = mapper.map(dto, ThcsSoLieuHocSinh.class);
		return entity;
	}

}
