package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektCbGvNv;

@Component
public class SgdTrektCbGvNvConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public SgdTrektCbGvNvDto convertToDto(SgdTrektCbGvNv entity) {
		if (entity == null) {
			return new SgdTrektCbGvNvDto();
		}
		SgdTrektCbGvNvDto dto = mapper.map(entity, SgdTrektCbGvNvDto.class);
		return dto;
	}

	public SgdTrektCbGvNv convertToEntity(SgdTrektCbGvNvDto dto) {
		if (dto == null) {
			return new SgdTrektCbGvNv();
		}
		SgdTrektCbGvNv entity = mapper.map(dto, SgdTrektCbGvNv.class);
		return entity;
	}

}