package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class SgdTrektCsvcDto {
	private Integer id;
	private Integer idTruong;
	private String tenTruong;
	private Integer tsPhong;
	private Integer tsPhongHoc;
	private Integer phKienCo;
	private Integer phBanKienCo;
	private Integer phTam;
	private Integer tsPhongLamViec;
	private Integer plvKienCo;
	private Integer plvBanKienCo;
	private Integer plvTam;
	private Integer plvNhaCongVu;
	private Integer pthTinHoc;
	private Integer pthNgoaiNgu;
	private Integer thang;
	private Integer nam;
	private Date ngayGioCapNhat;
	private Integer guiBcSgd;
}
