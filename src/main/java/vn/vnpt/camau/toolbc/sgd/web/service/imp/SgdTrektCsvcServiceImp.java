package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.SgdTrektCsvcConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCsvcDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektCsvc;
import vn.vnpt.camau.toolbc.sgd.web.repository.SgdTrektCsvcRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektCsvcService;

@Service
public class SgdTrektCsvcServiceImp implements SgdTrektCsvcService {

	@Autowired
	private SgdTrektCsvcConverter converter;
	@Autowired
	private SgdTrektCsvcRepository repository;

	@Override
	public Page<SgdTrektCsvcDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<SgdTrektCsvc> soLieuCanLay;
		if(idDonVi ==  999) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdOrderByThangAsc(namHoc, thang, 1,pageable);
		}else {
			soLieuCanLay = repository.findByNamAndThangOrderByThangAsc(namHoc, thang, pageable);
		}
		Page<SgdTrektCsvcDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public SgdTrektCsvcDto luuSoLieu(SgdTrektCsvcDto dto) throws Exception {
		SgdTrektCsvc soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		SgdTrektCsvcDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private SgdTrektCsvc themSoLieu(SgdTrektCsvcDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		SgdTrektCsvc soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam) {
		SgdTrektCsvc canKiemTra = repository.findByThangAndNam(thang, nam);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private SgdTrektCsvc capNhatSoLieu(SgdTrektCsvcDto dto) throws Exception {
		SgdTrektCsvc canKiemTra = kiemTraSoLieu(dto.getId());
		SgdTrektCsvc soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private SgdTrektCsvc kiemTraSoLieu(int id) throws Exception {
		SgdTrektCsvc canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private SgdTrektCsvc kiemTraSoLieuDaGui(int id) throws Exception {
		SgdTrektCsvc canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public SgdTrektCsvcDto laySoLieu(int id) {
		SgdTrektCsvc soLieuCanLay = repository.findById(id);
		SgdTrektCsvcDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		SgdTrektCsvc soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
