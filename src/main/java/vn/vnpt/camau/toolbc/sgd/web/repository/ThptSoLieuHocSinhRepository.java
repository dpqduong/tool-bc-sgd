package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuHocSinh;

public interface ThptSoLieuHocSinhRepository extends CrudRepository<ThptSoLieuHocSinh, Long> {
	ThptSoLieuHocSinh findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
