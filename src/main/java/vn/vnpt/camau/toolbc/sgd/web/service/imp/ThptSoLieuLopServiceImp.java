package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThptSoLieuLopConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThptSoLieuLopRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptSoLieuLopService;

@Service
public class ThptSoLieuLopServiceImp implements ThptSoLieuLopService {

	@Autowired
	private ThptSoLieuLopConverter converter;
	@Autowired
	private ThptSoLieuLopRepository repository;
	
	@Override
	public void themSoLieuLopFileExcel(ThptSoLieuLopDto soLieuLop) throws Exception {
		ThptSoLieuLop soLieuLopCanLuu= converter.convertToEntity(soLieuLop);
		try {
			repository.save(soLieuLopCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian) {
		return repository.existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
	}

	@Override
	public ThptSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThptSoLieuLop soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThptSoLieuLopDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
