package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.PgdThConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdTh;
import vn.vnpt.camau.toolbc.sgd.web.repository.PgdThRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.PgdThService;

@Service
public class PgdThServiceImp implements PgdThService {

	@Autowired
	private PgdThConverter converter;
	@Autowired
	private PgdThRepository repository;

	@Override
	public Page<PgdThDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<PgdTh> soLieuCanLay;
		if(idDonVi == 10 || idDonVi == 11) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdOrderByThangAsc(namHoc, thang, 1,pageable);
		}else {
			soLieuCanLay = repository.findByIdPhongGdAndNamOrderByThangAsc(idDonVi, namHoc, pageable);
		}
		Page<PgdThDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public PgdThDto luuSoLieu(PgdThDto dto) throws Exception {
		PgdTh soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		PgdThDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private PgdTh themSoLieu(PgdThDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam(), dto.getIdPhongGd())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		PgdTh soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam, int idDonVi) {
		PgdTh canKiemTra = repository.findByThangAndNamAndIdPhongGd(thang, nam, idDonVi);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private PgdTh capNhatSoLieu(PgdThDto dto) throws Exception {
		PgdTh canKiemTra = kiemTraSoLieu(dto.getId());
		PgdTh soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private PgdTh kiemTraSoLieu(int id) throws Exception {
		PgdTh canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private PgdTh kiemTraSoLieuDaGui(int id) throws Exception {
		PgdTh canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public PgdThDto laySoLieu(int id) {
		PgdTh soLieuCanLay = repository.findById(id);
		PgdThDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		PgdTh soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
