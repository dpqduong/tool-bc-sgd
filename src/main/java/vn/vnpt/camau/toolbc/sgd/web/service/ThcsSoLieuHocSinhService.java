package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;

public interface ThcsSoLieuHocSinhService {
	void themSoLieuHocSinhFileExcel(ThcsSoLieuHocSinhDto soLieuHocSinh) throws Exception;
	ThcsSoLieuHocSinhDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
