package vn.vnpt.camau.toolbc.sgd.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.toolbc.sgd.web.ModelAttr;
import vn.vnpt.camau.toolbc.sgd.web.Response;
import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThDto;
import vn.vnpt.camau.toolbc.sgd.web.service.CallReportService;
import vn.vnpt.camau.toolbc.sgd.web.service.DmNamHocService;
import vn.vnpt.camau.toolbc.sgd.web.service.PgdThService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThamSoService;

@Controller
@RequestMapping("/nhap-so-lieu-th")
public class NhapSoLieuThController {
	private ModelAttr modelAttr = new ModelAttr("Nhập số liệu tiểu học", "nhapsolieuth", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","js/nhapsolieuth.js"},
			new String[]{"bower_components/select2/dist/css/select2.min.css","jdgrid/jdgrid.css"});
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private PgdThService pgdThServ;
	@Autowired
	private ThamSoService thamSoServ;
	@Autowired
	private DmNamHocService dmNHServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody String initForm(){
		ObjectMapper mapper =new ObjectMapper();
		try {
			return "{\"namhoc\":"+mapper.writeValueAsString(dmNHServ.layDsDmNamHoc())+"}";
		} catch (JsonProcessingException e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ds-nam-hoc")
	public @ResponseBody List<DmNamHocDto> layDsNamHoc() {
		return dmNHServ.layDsDmNamHoc();
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<PgdThDto> layDanhSach(int idDonVi, int nam, int thang, Pageable page){
		return pgdThServ.layDanhSach(idDonVi, nam, thang, page);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(PgdThDto dto){
		try {
			pgdThServ.luuSoLieu(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody PgdThDto layChiTiet(int  id){
		return pgdThServ.laySoLieu(id);
	}

	@PostMapping("/xoa")
	public @ResponseBody String xoa(int id){
		try {
			pgdThServ.xoaSoLieu(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/bao-cao")
	public @ResponseBody String baoCao(int id, int tt){
		try {
			pgdThServ.baoCaoSgd(id,tt);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
