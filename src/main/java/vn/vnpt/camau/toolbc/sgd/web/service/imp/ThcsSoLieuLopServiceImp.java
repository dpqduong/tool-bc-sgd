package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThcsSoLieuLopConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThcsSoLieuLopRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsSoLieuLopService;

@Service
public class ThcsSoLieuLopServiceImp implements ThcsSoLieuLopService {

	@Autowired
	private ThcsSoLieuLopConverter converter;
	@Autowired
	private ThcsSoLieuLopRepository repository;
	
	@Override
	public void themSoLieuLopFileExcel(ThcsSoLieuLopDto soLieuLop) throws Exception {
		ThcsSoLieuLop soLieuLopCanLuu= converter.convertToEntity(soLieuLop);
		try {
			repository.save(soLieuLopCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian) {
		return repository.existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
	}

	@Override
	public ThcsSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThcsSoLieuLop soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThcsSoLieuLopDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
