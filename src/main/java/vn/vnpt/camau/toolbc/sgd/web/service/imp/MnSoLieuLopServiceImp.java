package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.MnSoLieuLopConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.repository.MnSoLieuLopRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.MnSoLieuLopService;

@Service
public class MnSoLieuLopServiceImp implements MnSoLieuLopService {

	@Autowired
	private MnSoLieuLopConverter converter;
	@Autowired
	private MnSoLieuLopRepository repository;
	
	@Override
	public void themSoLieuLopFileExcel(MnSoLieuLopDto soLieuLop) throws Exception {
		MnSoLieuLop soLieuLopCanLuu= converter.convertToEntity(soLieuLop);
		try {
			repository.save(soLieuLopCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian) {
		return repository.existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
	}

	@Override
	public MnSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		MnSoLieuLop soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		MnSoLieuLopDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	
}
