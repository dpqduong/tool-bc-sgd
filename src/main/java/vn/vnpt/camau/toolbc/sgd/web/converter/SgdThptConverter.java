package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdThptDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdThpt;

@Component
public class SgdThptConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public SgdThptDto convertToDto(SgdThpt entity) {
		if (entity == null) {
			return new SgdThptDto();
		}
		SgdThptDto dto = mapper.map(entity, SgdThptDto.class);
		return dto;
	}

	public SgdThpt convertToEntity(SgdThptDto dto) {
		if (dto == null) {
			return new SgdThpt();
		}
		SgdThpt entity = mapper.map(dto, SgdThpt.class);
		return entity;
	}

}
