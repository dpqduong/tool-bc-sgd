package vn.vnpt.camau.toolbc.sgd.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.Huyen;

public interface HuyenRepository extends CrudRepository<Huyen, Long> {

	Page<Huyen> findByOrderByIdHuyenDesc(Pageable pageable);
	
}
