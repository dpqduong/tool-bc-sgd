package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

public class ThSoLieuLopDto {

	@Getter
	@Setter
	private Integer idSlLop;
	@Getter
	@Setter
	@Mapping("dmTruong.idTruong")
	private Integer idTruong;
	@Getter
	@Setter
	private Integer tongSo;
	@Getter
	@Setter
	private Integer k1;
	@Getter
	@Setter
	private Integer k2;
	@Getter
	@Setter
	private Integer k3;
	@Getter
	@Setter
	private Integer k4;
	@Getter
	@Setter
	private Integer k5;
	@Getter
	@Setter
	private Integer namHoc;
	@Getter
	@Setter
	private Integer thoiGianImport;
	@Getter
	@Setter
	private Date ngayGioCapNhat;

	
}
