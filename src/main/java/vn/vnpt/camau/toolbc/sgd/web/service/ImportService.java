package vn.vnpt.camau.toolbc.sgd.web.service;

import vn.vnpt.camau.toolbc.sgd.web.entity.FileBean;

public interface ImportService {

	String importChiSo(FileBean fileBean);

}
