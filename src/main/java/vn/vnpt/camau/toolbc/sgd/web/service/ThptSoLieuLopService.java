package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuLopDto;

public interface ThptSoLieuLopService {
	void themSoLieuLopFileExcel(ThptSoLieuLopDto soLieuLop) throws Exception;
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	ThptSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
