package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

public class ThSoLieuCbGvNvDto {

	@Getter
	@Setter
	private Integer idSlCbGvNv;
	@Getter
	@Setter
	@Mapping("dmTruong.idTruong")
	private Integer idTruong;
	@Getter
	@Setter
	private Integer tongSo;
	@Getter
	@Setter
	private Integer cbql;
	@Getter
	@Setter
	private Integer gv;
	@Getter
	@Setter
	private Integer nv;
	@Getter
	@Setter
	private Integer namHoc;
	@Getter
	@Setter
	private Integer thoiGianImport;
	@Getter
	@Setter
	private Date ngayGioCapNhat;

	
}
