package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCsvcDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektCsvc;

@Component
public class SgdTrektCsvcConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public SgdTrektCsvcDto convertToDto(SgdTrektCsvc entity) {
		if (entity == null) {
			return new SgdTrektCsvcDto();
		}
		SgdTrektCsvcDto dto = mapper.map(entity, SgdTrektCsvcDto.class);
		return dto;
	}

	public SgdTrektCsvc convertToEntity(SgdTrektCsvcDto dto) {
		if (dto == null) {
			return new SgdTrektCsvc();
		}
		SgdTrektCsvc entity = mapper.map(dto, SgdTrektCsvc.class);
		return entity;
	}

}