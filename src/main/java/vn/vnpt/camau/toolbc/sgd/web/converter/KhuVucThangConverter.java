package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.KhuVucThangDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVucThang;

@Component
public class KhuVucThangConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public KhuVucThangDto convertToDto(KhuVucThang entity) {
		if (entity == null) {
			return new KhuVucThangDto();
		}
		KhuVucThangDto dto = mapper.map(entity, KhuVucThangDto.class);
		return dto;
	}

}
