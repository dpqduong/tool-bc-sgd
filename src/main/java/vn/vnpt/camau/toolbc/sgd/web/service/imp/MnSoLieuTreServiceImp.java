package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.MnSoLieuTreConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuTreDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuTre;
import vn.vnpt.camau.toolbc.sgd.web.repository.MnSoLieuTreRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.MnSoLieuTreService;

@Service
public class MnSoLieuTreServiceImp implements MnSoLieuTreService {

	@Autowired
	private MnSoLieuTreConverter converter;
	@Autowired
	private MnSoLieuTreRepository repository;
	
	@Override
	public void themSoLieuTreFileExcel(MnSoLieuTreDto soLieuTre) throws Exception {
		MnSoLieuTre soLieuTreCanLuu= converter.convertToEntity(soLieuTre);
		try {
			repository.save(soLieuTreCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public MnSoLieuTreDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		MnSoLieuTre soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		MnSoLieuTreDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
