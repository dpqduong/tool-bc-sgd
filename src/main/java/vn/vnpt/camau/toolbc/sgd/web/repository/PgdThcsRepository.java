package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcs;

public interface PgdThcsRepository extends CrudRepository<PgdThcs, Integer> {

	Page<PgdThcs> findByIdPhongGdAndNamOrderByThangAsc(int idDonVi, int namHoc, Pageable pageable);
	Page<PgdThcs> findByNamAndThangAndGuiBcSgdAndIdPhongGdLessThanOrderByThangAsc(int namHoc, int thang, int tt, int idPhongGd,Pageable pageable);
	Page<PgdThcs> findByNamAndThangAndGuiBcSgdAndIdPhongGdGreaterThanOrderByThangAsc(int namHoc, int thang, int tt, int idPhongGd, Pageable pageable);
	PgdThcs findByThangAndNamAndIdPhongGd(int thang, int nam, int idDonVi);
	PgdThcs findById(int id);
	PgdThcs findByIdAndGuiBcSgd(int id, int flag);
}
