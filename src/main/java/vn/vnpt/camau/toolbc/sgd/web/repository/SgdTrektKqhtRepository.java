package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektKqht;

public interface SgdTrektKqhtRepository extends CrudRepository<SgdTrektKqht, Integer> {

	Page<SgdTrektKqht> findByNamAndThangOrderByThangAsc(int namHoc, int thang, Pageable pageable);
	Page<SgdTrektKqht> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt, Pageable pageable);
	SgdTrektKqht findByThangAndNam(int thang, int nam);
	SgdTrektKqht findById(int id);
	SgdTrektKqht findByIdAndGuiBcSgd(int id, int flag);
}
