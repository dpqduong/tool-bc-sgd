package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

@Data
public class PgdMnDto {

	private Integer id;
	private int idPhongGd;
	private Integer truongClMn;
	private Integer truongClMg;
	private Integer tsTruongCl;
	private Integer truongTtMn;
	private Integer truongTtMg;
	private Integer tsTruongTt;
	private Integer tsChungTruong;
	private Integer lopClNt;
	private Integer lopClMg;
	private Integer tsLopCl;
	private Integer lopTtNt;
	private Integer lopTtMg;
	private Integer tsLopTt;
	private Integer tsChungLop;
	private Integer namTuoi;
	private Integer treClNt;
	private Integer treClMg;
	private Integer tsTreCl;
	private Integer treTtNt;
	private Integer treTtMg;
	private Integer tsTreTt;
	private Integer tsChungTre;
	private Integer treNamTuoi;
	private Integer cbqlCl;
	private Integer cbqlTt;
	private Integer tsCbql;
	private Integer gvclNt;
	private Integer gvclMg;
	private Integer tsGvcl;
	private Integer gvttNt;
	private Integer gvttMg;
	private Integer tsGvtt;
	private Integer nvCl;
	private Integer nvTt;
	private Integer tsNv;
	private Integer tsChungCbGvNv;
	private Integer thang;
	private Integer nam;
	private Date ngayGioCapNhat;
	private Integer guiBcSgd;
	private Integer tsPhong;
	private Integer tsPhongHoc;
	private Integer phKienCo;
	private Integer phBanKienCo;
	private Integer phTam;
	private Integer tsPhongLamViec;
	private Integer plvKienCo;
	private Integer plvBanKienCo;
	private Integer plvTam;
	private Integer plvNhaCongVu;
	private Integer diemLe;
	private Integer hocNho;
	private Integer coSoHai;
	private Integer ttNhomLop;
	private Integer lopNhomLop;
	private Integer lopNamTuoiCl;
	private Integer lopNamTuoiTt;
	private Integer treNhomLop;
	private Integer treNamTuoiCl;
	private Integer treNamTuoiTt;
	private Integer gvNhomLop;
	private Integer phKienCoTt;
	private Integer phBanKienCoTt;
	private Integer phTamTt;
	private Integer plvKienCoTt;
	private Integer plvBanKienCoTt;
	private Integer plvTamTt;
}
