package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThcsDtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcsDt;

@Component
public class PgdThcsDtConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PgdThcsDtDto convertToDto(PgdThcsDt entity) {
		if (entity == null) {
			return new PgdThcsDtDto();
		}
		PgdThcsDtDto dto = mapper.map(entity, PgdThcsDtDto.class);
		return dto;
	}

	public PgdThcsDt convertToEntity(PgdThcsDtDto dto) {
		if (dto == null) {
			return new PgdThcsDt();
		}
		PgdThcsDt entity = mapper.map(dto, PgdThcsDt.class);
		return entity;
	}

}
