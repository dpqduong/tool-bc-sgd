package vn.vnpt.camau.toolbc.sgd.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.toolbc.sgd.web.ModelAttr;
import vn.vnpt.camau.toolbc.sgd.web.Response;
import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.DuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NganHangDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.FileBean;
import vn.vnpt.camau.toolbc.sgd.web.service.DmNamHocService;
import vn.vnpt.camau.toolbc.sgd.web.service.DmTruongService;
import vn.vnpt.camau.toolbc.sgd.web.service.ImportSoLieuService;

@Controller
@RequestMapping("/import-so-lieu")
public class ImportSoLieuController {
	private ModelAttr modelAttr = new ModelAttr("Import số liệu trường, lớp, học sinh và CBQL, GV, NV", "importsolieu",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/importsolieu.js", "bower_components/select2/dist/js/select2.min.js" },
			new String[] { "jdgrid/jdgrid.css","bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	private ImportSoLieuService importServ;
	@Autowired
	private DmTruongService dmServ;
	@Autowired
	private DmNamHocService dmNHServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@PostMapping("/xu-ly")
	public @ResponseBody String upload(FileBean uploadItem, BindingResult result,@RequestParam int idTruong,@RequestParam int namHoc,@RequestParam int thoiGian,@RequestParam int action,@RequestParam int cap) {
		return importServ.importSoLieuLopMN(uploadItem, idTruong, namHoc, thoiGian, action, cap);
	}
	
	@PostMapping("/kiem-tra")
	public @ResponseBody String kiemTra(FileBean uploadItem, BindingResult result,@RequestParam int idTruong,@RequestParam int namHoc,@RequestParam int thoiGian,@RequestParam int cap) {
		return importServ.kiemTra(idTruong, namHoc, thoiGian, cap);
	}
	
	@GetMapping("/lay-ds-nam-hoc")
	public @ResponseBody List<DmNamHocDto> layDsNamHoc() {
		return dmNHServ.layDsDmNamHoc();
	}
	
//	@GetMapping("/lay-ds-truong")
//	public @ResponseBody List<DmTruongDto> layDsTruong() {
//		return dmServ.layDsDmTruong();
//	}
	
	@PostMapping("/lay-ds-truong")
	public @ResponseBody String layDsTruong(int id){
		ObjectMapper mapper =new ObjectMapper();
		if(id==10) {
			try {
				return "{\"truongs\":"+mapper.writeValueAsString(dmServ.layDsDmTruong())+"}";
			} catch (JsonProcessingException e) {
				return new Response(-1, e.getMessage()).toString();
			}
		}
		else {
			try {
				return "{\"data\":"+mapper.writeValueAsString(dmServ.layDsDmTruongByIdDonVi(id))+"}";
			} catch (JsonProcessingException e) {
				return new Response(-1, e.getMessage()).toString();
			}
		}
		
	}
	
	@PostMapping("/lay-ds-truong-theo-cap")
	public @ResponseBody String layDsTruongTheoCap(int id, int cap){
		ObjectMapper mapper =new ObjectMapper();
		if(id==10) {
			try {
				return "{\"truongs\":"+mapper.writeValueAsString(dmServ.layDsDmTruongTheoCap(cap))+"}";
			} catch (JsonProcessingException e) {
				return new Response(-1, e.getMessage()).toString();
			}
		}
		else {
			try {
				return "{\"data\":"+mapper.writeValueAsString(dmServ.layDsDmTruongByIdDonViAndTheoCap(id,cap))+"}";
			} catch (JsonProcessingException e) {
				return new Response(-1, e.getMessage()).toString();
			}
		}
		
	}
}
