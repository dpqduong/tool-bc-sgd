package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThDto;

public interface PgdThService {
	public Page<PgdThDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	PgdThDto luuSoLieu(PgdThDto dto) throws Exception;
	PgdThDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
