package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThamSoDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThamSo;

@Component
public class ThamSoConverter {

	@Autowired
	private DozerBeanMapper mapper;
	
	public ThamSoDto convertToDto(ThamSo entity) {
		if (entity == null) {
			return new ThamSoDto();
		}
		ThamSoDto dto = mapper.map(entity, ThamSoDto.class);
		return dto;
	}
	
	public ThamSo convertToEntity(ThamSoDto dto) {
		if (dto == null) {
			return new ThamSo();
		}
		ThamSo entity = mapper.map(dto, ThamSo.class);
		setNguoiDung(dto, entity);
		return entity;
	}

	private void setNguoiDung(ThamSoDto dto, ThamSo entity) {
		if (dto.getIdNguoiCapNhat() != null) {
			NguoiDung nguoiDung = new NguoiDung();
			nguoiDung.setIdNguoiDung(dto.getIdNguoiCapNhat());
			entity.setNguoiDung(nguoiDung);
		}
	}

}
