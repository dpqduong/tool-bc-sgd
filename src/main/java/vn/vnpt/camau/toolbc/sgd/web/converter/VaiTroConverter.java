package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.VaiTroDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.VaiTro;

@Component
public class VaiTroConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public VaiTroDto convertToDto(VaiTro entity) {
		if (entity == null) {
			return new VaiTroDto();
		}
		VaiTroDto dto = mapper.map(entity, VaiTroDto.class);
		return dto;
	}
	
	public VaiTro convertToEntity(VaiTroDto dto) {
		if (dto == null) {
			return new VaiTro();
		}
		VaiTro entity = mapper.map(dto, VaiTro.class);
		return entity;
	}

}
