package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;

public interface MnSoLieuLopService {
	void themSoLieuLopFileExcel(MnSoLieuLopDto soLieuLop) throws Exception;
	boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	MnSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
