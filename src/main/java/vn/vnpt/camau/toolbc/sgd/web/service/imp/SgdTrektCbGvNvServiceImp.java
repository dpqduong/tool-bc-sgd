package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.SgdTrektCbGvNvConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.repository.SgdTrektCbGvNvRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektCbGvNvService;

@Service
public class SgdTrektCbGvNvServiceImp implements SgdTrektCbGvNvService {

	@Autowired
	private SgdTrektCbGvNvConverter converter;
	@Autowired
	private SgdTrektCbGvNvRepository repository;

	@Override
	public Page<SgdTrektCbGvNvDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<SgdTrektCbGvNv> soLieuCanLay;
		if(idDonVi ==  999) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdOrderByThangAsc(namHoc, thang, 1,pageable);
		}else {
			soLieuCanLay = repository.findByNamAndThangOrderByThangAsc(namHoc, thang, pageable);
		}
		Page<SgdTrektCbGvNvDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public SgdTrektCbGvNvDto luuSoLieu(SgdTrektCbGvNvDto dto) throws Exception {
		SgdTrektCbGvNv soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		SgdTrektCbGvNvDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private SgdTrektCbGvNv themSoLieu(SgdTrektCbGvNvDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		SgdTrektCbGvNv soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam) {
		SgdTrektCbGvNv canKiemTra = repository.findByThangAndNam(thang, nam);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private SgdTrektCbGvNv capNhatSoLieu(SgdTrektCbGvNvDto dto) throws Exception {
		SgdTrektCbGvNv canKiemTra = kiemTraSoLieu(dto.getId());
		SgdTrektCbGvNv soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private SgdTrektCbGvNv kiemTraSoLieu(int id) throws Exception {
		SgdTrektCbGvNv canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private SgdTrektCbGvNv kiemTraSoLieuDaGui(int id) throws Exception {
		SgdTrektCbGvNv canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public SgdTrektCbGvNvDto laySoLieu(int id) {
		SgdTrektCbGvNv soLieuCanLay = repository.findById(id);
		SgdTrektCbGvNvDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		SgdTrektCbGvNv soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
