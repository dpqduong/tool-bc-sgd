package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmNamHoc;

@Component
public class DmNamHocConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public DmNamHocDto convertToDto(DmNamHoc entity) {
		if (entity == null) {
			return new DmNamHocDto();
		}
		DmNamHocDto dto = mapper.map(entity, DmNamHocDto.class);
		return dto;
	}

	public DmNamHoc convertToEntity(DmNamHocDto dto) {
		if (dto == null) {
			return new DmNamHoc();
		}
		DmNamHoc entity = mapper.map(dto, DmNamHoc.class);
		return entity;
	}

}
