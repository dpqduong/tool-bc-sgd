package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.PgdTh;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcs;

public interface PgdThRepository extends CrudRepository<PgdTh, Integer> {

	Page<PgdTh> findByIdPhongGdAndNamOrderByThangAsc(int idDonVi, int namHoc, Pageable pageable);
	Page<PgdTh> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt, Pageable pageable);
	PgdTh findByThangAndNamAndIdPhongGd(int thang, int nam, int idDonVi);
	PgdTh findById(int id);
	PgdTh findByIdAndGuiBcSgd(int id, int flag);
}
