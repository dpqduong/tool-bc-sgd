package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThSoLieuCbGvNvConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThSoLieuCbGvNvRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThSoLieuCbGvNvService;

@Service
public class ThSoLieuCbGvNvServiceImp implements ThSoLieuCbGvNvService {

	@Autowired
	private ThSoLieuCbGvNvConverter converter;
	@Autowired
	private ThSoLieuCbGvNvRepository repository;
	
	@Override
	public void themSoLieuCbGvNvFileExcel(ThSoLieuCbGvNvDto soLieuCbGvNv) throws Exception {
		ThSoLieuCbGvNv soLieuCbGvNvCanLuu= converter.convertToEntity(soLieuCbGvNv);
		try {
			repository.save(soLieuCbGvNvCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThSoLieuCbGvNv soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThSoLieuCbGvNvDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
