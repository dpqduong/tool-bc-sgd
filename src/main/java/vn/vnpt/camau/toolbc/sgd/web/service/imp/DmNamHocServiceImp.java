package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.DmNamHocConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmNamHoc;
import vn.vnpt.camau.toolbc.sgd.web.repository.DmNamHocRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.DmNamHocService;

@Service
public class DmNamHocServiceImp implements DmNamHocService {

	@Autowired
	private DmNamHocConverter converter;
	@Autowired
	private DmNamHocRepository repository;

	@Override
	public Page<DmNamHocDto> layDsDmNamHoc(Pageable pageable) {
		Page<DmNamHoc> dsDmNamHocCanLay = repository.findByOrderByIdDesc(pageable);
		Page<DmNamHocDto> dsDmNamHocDaChuyenDoi = dsDmNamHocCanLay.map(converter::convertToDto);
		return dsDmNamHocDaChuyenDoi;
	}
	
	@Override
	public List<DmNamHocDto> layDsDmNamHoc() {
		Pageable pageable = null;
		List<DmNamHocDto> dsDmNamHocCanLay = layDsDmNamHoc(pageable).getContent().stream()
				.collect(Collectors.toList());
		return dsDmNamHocCanLay;
	}


}
