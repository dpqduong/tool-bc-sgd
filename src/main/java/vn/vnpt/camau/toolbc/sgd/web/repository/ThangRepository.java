package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.Thang;

public interface ThangRepository extends CrudRepository<Thang, Long> {

	Thang findTopByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(Long idKhuVuc);
	
	List<Thang> findByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(Long idKhuVuc);
	
	Long countByThongTinKhachHangThangs_IdThongTinKhachHang(Long idThongTinKhachHang);
	
	Thang findOne(Long id);
	
	Long countByLichGhiThuThangs_IdLichGhiThu(Long idLichGhiThu);
	
	Thang findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(Long idNguoiDung);
	
	Thang findTopByIdThangLessThanOrderByIdThangDesc(Long idThang);
	
	Thang findTopByOrderByIdThangDesc();
}
