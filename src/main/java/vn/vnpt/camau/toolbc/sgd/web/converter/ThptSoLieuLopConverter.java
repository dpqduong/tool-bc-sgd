package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuLop;

@Component
public class ThptSoLieuLopConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThptSoLieuLopDto convertToDto(ThptSoLieuLop entity) {
		if (entity == null) {
			return new ThptSoLieuLopDto();
		}
		ThptSoLieuLopDto dto = mapper.map(entity, ThptSoLieuLopDto.class);
		return dto;
	}

	public ThptSoLieuLop convertToEntity(ThptSoLieuLopDto dto) {
		if (dto == null) {
			return new ThptSoLieuLop();
		}
		ThptSoLieuLop entity = mapper.map(dto, ThptSoLieuLop.class);
		return entity;
	}

}
