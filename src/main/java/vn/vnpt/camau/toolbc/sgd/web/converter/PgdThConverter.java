package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdTh;

@Component
public class PgdThConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PgdThDto convertToDto(PgdTh entity) {
		if (entity == null) {
			return new PgdThDto();
		}
		PgdThDto dto = mapper.map(entity, PgdThDto.class);
		return dto;
	}

	public PgdTh convertToEntity(PgdThDto dto) {
		if (dto == null) {
			return new PgdTh();
		}
		PgdTh entity = mapper.map(dto, PgdTh.class);
		return entity;
	}

}
