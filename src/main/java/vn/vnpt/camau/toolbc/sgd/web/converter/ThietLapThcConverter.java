package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThietLapThcDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThietLapThc;

@Component
public class ThietLapThcConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThietLapThcDto convertToDto(ThietLapThc entity) {
		if (entity == null) {
			return new ThietLapThcDto();
		}
		ThietLapThcDto dto = mapper.map(entity, ThietLapThcDto.class);
		return dto;
	}

	public ThietLapThc convertToEntity(ThietLapThcDto dto) {
		if (dto == null) {
			return new ThietLapThc();
		}
		ThietLapThc entity = mapper.map(dto, ThietLapThc.class);
		return entity;
	}

}
