package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuLopDto;

public interface ThcsSoLieuLopService {
	void themSoLieuLopFileExcel(ThcsSoLieuLopDto soLieuLop) throws Exception;
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	ThcsSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
