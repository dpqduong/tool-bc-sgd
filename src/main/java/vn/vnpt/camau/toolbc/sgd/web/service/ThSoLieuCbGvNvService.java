package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuHocSinhDto;

public interface ThSoLieuCbGvNvService {
	void themSoLieuCbGvNvFileExcel(ThSoLieuCbGvNvDto soLieuCbGvNv) throws Exception;
	ThSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
