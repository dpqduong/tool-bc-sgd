package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.PgdMnConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdMnDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdMn;
import vn.vnpt.camau.toolbc.sgd.web.repository.PgdMnRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.PgdMnService;

@Service
public class PgdMnServiceImp implements PgdMnService {

	@Autowired
	private PgdMnConverter converter;
	@Autowired
	private PgdMnRepository repository;

	@Override
	public Page<PgdMnDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<PgdMn> soLieuCanLay;
		if(idDonVi == 10 || idDonVi == 11) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdOrderByThangAsc(namHoc, thang, 1,pageable);
		}else {
			soLieuCanLay = repository.findByIdPhongGdAndNamOrderByThangAsc(idDonVi, namHoc, pageable);
		}
		Page<PgdMnDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public PgdMnDto luuSoLieu(PgdMnDto dto) throws Exception {
		PgdMn soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		PgdMnDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private PgdMn themSoLieu(PgdMnDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam(), dto.getIdPhongGd())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		PgdMn soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam, int idDonVi) {
		PgdMn canKiemTra = repository.findByThangAndNamAndIdPhongGd(thang, nam, idDonVi);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private PgdMn capNhatSoLieu(PgdMnDto dto) throws Exception {
		PgdMn canKiemTra = kiemTraSoLieu(dto.getId());
		PgdMn soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private PgdMn kiemTraSoLieu(int id) throws Exception {
		PgdMn canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private PgdMn kiemTraSoLieuDaGui(int id) throws Exception {
		PgdMn canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public PgdMnDto laySoLieu(int id) {
		PgdMn soLieuCanLay = repository.findById(id);
		PgdMnDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		PgdMn soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
