package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuHocSinh;

public interface ThcsSoLieuHocSinhRepository extends CrudRepository<ThcsSoLieuHocSinh, Long> {
	ThcsSoLieuHocSinh findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
