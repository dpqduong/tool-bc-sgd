package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuTreDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuTre;

@Component
public class MnSoLieuTreConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public MnSoLieuTreDto convertToDto(MnSoLieuTre entity) {
		if (entity == null) {
			return new MnSoLieuTreDto();
		}
		MnSoLieuTreDto dto = mapper.map(entity, MnSoLieuTreDto.class);
		return dto;
	}

	public MnSoLieuTre convertToEntity(MnSoLieuTreDto dto) {
		if (dto == null) {
			return new MnSoLieuTre();
		}
		MnSoLieuTre entity = mapper.map(dto, MnSoLieuTre.class);
		return entity;
	}

}
