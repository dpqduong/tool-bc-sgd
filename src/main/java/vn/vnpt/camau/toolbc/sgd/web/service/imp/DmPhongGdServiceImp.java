package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.converter.PgdMnConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmPhongGdDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdMnDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdMn;
import vn.vnpt.camau.toolbc.sgd.web.repository.PgdMnRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.DmPhongGdService;
import vn.vnpt.camau.toolbc.sgd.web.service.NguoiDungService;

@Service
public class DmPhongGdServiceImp implements DmPhongGdService {

	@Autowired
	private PgdMnConverter converter;
	@Autowired
	private PgdMnRepository repository;
	@Autowired
	private NguoiDungService nguoiDungService;


	@Override
	public List<PgdMnDto> layDsPhongGdBaoCao(int thang, int nam, int tt) {
		List<PgdMn> dsDmPhongGdCanLay = new ArrayList<PgdMn>(0);
		dsDmPhongGdCanLay = repository.findByThangAndNamAndGuiBcSgdOrderByIdPhongGdAsc(thang, nam, tt);
		List<PgdMnDto> dsDmPhongGdDaChuyenDoi = dsDmPhongGdCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsDmPhongGdDaChuyenDoi;
	}


}
