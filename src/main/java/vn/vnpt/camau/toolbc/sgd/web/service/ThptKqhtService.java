package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptKqhtDto;

public interface ThptKqhtService {
	void themSoLieuLopFileExcel(ThptKqhtDto soLieuLop) throws Exception;
	ThptKqhtDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
