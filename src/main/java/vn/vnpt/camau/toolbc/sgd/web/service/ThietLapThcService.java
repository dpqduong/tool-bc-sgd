package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThietLapThcDto;

public interface ThietLapThcService {

	Page<ThietLapThcDto> layDsThietLapThc(Pageable pageable);

	ThietLapThcDto layThietLapThc(int id);

	ThietLapThcDto luuThietLapThc(ThietLapThcDto dto) throws Exception;
	
}
