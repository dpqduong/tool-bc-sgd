package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuTreDto;

public interface MnSoLieuTreService {
	void themSoLieuTreFileExcel(MnSoLieuTreDto soLieuTre) throws Exception;
	MnSoLieuTreDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
