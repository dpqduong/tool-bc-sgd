package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

@Component
public class DmTruongConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public DmTruongDto convertToDto(DmTruong entity) {
		if (entity == null) {
			return new DmTruongDto();
		}
		DmTruongDto dto = mapper.map(entity, DmTruongDto.class);
		return dto;
	}

	public DmTruong convertToEntity(DmTruongDto dto) {
		if (dto == null) {
			return new DmTruong();
		}
		DmTruong entity = mapper.map(dto, DmTruong.class);
		return entity;
	}

}
