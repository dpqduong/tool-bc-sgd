package vn.vnpt.camau.toolbc.sgd.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.toolbc.sgd.web.ModelAttr;
import vn.vnpt.camau.toolbc.sgd.web.Response;
import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCsvcDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.service.CallReportService;
import vn.vnpt.camau.toolbc.sgd.web.service.DmNamHocService;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektCbGvNvService;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektCsvcService;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektKqhtService;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdTrektService;

@Controller
@RequestMapping("/nhap-so-lieu-trekt")
public class NhapSoLieuTreKtController {
	private ModelAttr modelAttr = new ModelAttr("Nhập số liệu Trung tâm HTPTGDHN ", "nhapsolieutrekt", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","js/nhapsolieutrekt.js"},
			new String[]{"bower_components/select2/dist/css/select2.min.css","jdgrid/jdgrid.css"});
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private SgdTrektService SgdTrektServ;
	@Autowired
	private SgdTrektCsvcService SgdTrektCsvcServ;
	@Autowired
	private SgdTrektKqhtService SgdTrektKqhtServ;
	@Autowired
	private SgdTrektCbGvNvService SgdTrektCbGvNvServ;
	@Autowired
	private DmNamHocService dmNHServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody String initForm(){
		ObjectMapper mapper =new ObjectMapper();
		try {
			return "{\"namhoc\":"+mapper.writeValueAsString(dmNHServ.layDsDmNamHoc())+"}";
		} catch (JsonProcessingException e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ds-nam-hoc")
	public @ResponseBody List<DmNamHocDto> layDsNamHoc() {
		return dmNHServ.layDsDmNamHoc();
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<SgdTrektDto> layDanhSach(int idDonVi, int nam, int thang, Pageable page){
		return SgdTrektServ.layDanhSach(idDonVi, nam, thang, page);
	}
	
	@GetMapping("/lay-ds-csvc")
	public @ResponseBody Page<SgdTrektCsvcDto> layDanhSachCsvc(int idDonVi, int nam, int thang, Pageable page){
		return SgdTrektCsvcServ.layDanhSach(idDonVi, nam, thang, page);
	}
	
	@GetMapping("/lay-ds-kqht")
	public @ResponseBody Page<SgdTrektKqhtDto> layDanhSachKqht(int idDonVi, int nam, int thang, Pageable page){
		return SgdTrektKqhtServ.layDanhSach(idDonVi, nam, thang, page);
	}
	
	@GetMapping("/lay-ds-cbgvnv")
	public @ResponseBody Page<SgdTrektCbGvNvDto> layDanhSachCbGvNv(int idDonVi, int nam, int thang, Pageable page){
		return SgdTrektCbGvNvServ.layDanhSach(idDonVi, nam, thang, page);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(SgdTrektDto dto){
		try {
			SgdTrektServ.luuSoLieu(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/luu-csvc")
	public @ResponseBody String luuCsvc(SgdTrektCsvcDto dto){
		try {
			SgdTrektCsvcServ.luuSoLieu(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody SgdTrektDto layChiTiet(int  id){
		return SgdTrektServ.laySoLieu(id);
	}

	@GetMapping("/lay-ct-csvc")
	public @ResponseBody SgdTrektCsvcDto layChiTietCsvc(int  id){
		return SgdTrektCsvcServ.laySoLieu(id);
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(int id){
		try {
			SgdTrektServ.xoaSoLieu(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa-csvc")
	public @ResponseBody String xoaCsvc(int id){
		try {
			SgdTrektCsvcServ.xoaSoLieu(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/bao-cao")
	public @ResponseBody String baoCao(int id, int tt){
		try {
			SgdTrektServ.baoCaoSgd(id,tt);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/bao-cao-csvc")
	public @ResponseBody String baoCaoCsvc(int id, int tt){
		try {
			SgdTrektCsvcServ.baoCaoSgd(id,tt);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/luu-kqht")
	public @ResponseBody String luuKqht(SgdTrektKqhtDto dto){
		try {
			SgdTrektKqhtServ.luuSoLieu(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ct-kqht")
	public @ResponseBody SgdTrektKqhtDto layChiTietKqht(int  id){
		return SgdTrektKqhtServ.laySoLieu(id);
	}

	@PostMapping("/xoa-kqht")
	public @ResponseBody String xoaKqht(int id){
		try {
			SgdTrektKqhtServ.xoaSoLieu(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/bao-cao-kqht")
	public @ResponseBody String baoCaoKqht(int id, int tt){
		try {
			SgdTrektKqhtServ.baoCaoSgd(id,tt);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/luu-cbgvnv")
	public @ResponseBody String luuCbGvNv(SgdTrektCbGvNvDto dto){
		try {
			SgdTrektCbGvNvServ.luuSoLieu(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ct-cbgvnv")
	public @ResponseBody SgdTrektCbGvNvDto layChiTietCbGvNv(int  id){
		return SgdTrektCbGvNvServ.laySoLieu(id);
	}

	@PostMapping("/xoa-cbgvnv")
	public @ResponseBody String xoaCbGvNv(int id){
		try {
			SgdTrektCbGvNvServ.xoaSoLieu(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/bao-cao-cbgvnv")
	public @ResponseBody String baoCaoCbGvNv(int id, int tt){
		try {
			SgdTrektCbGvNvServ.baoCaoSgd(id,tt);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
