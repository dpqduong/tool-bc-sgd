package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsKqht;

@Component
public class ThcsKqhtConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThcsKqhtDto convertToDto(ThcsKqht entity) {
		if (entity == null) {
			return new ThcsKqhtDto();
		}
		ThcsKqhtDto dto = mapper.map(entity, ThcsKqhtDto.class);
		return dto;
	}

	public ThcsKqht convertToEntity(ThcsKqhtDto dto) {
		if (dto == null) {
			return new ThcsKqht();
		}
		ThcsKqht entity = mapper.map(dto, ThcsKqht.class);
		return entity;
	}

}
