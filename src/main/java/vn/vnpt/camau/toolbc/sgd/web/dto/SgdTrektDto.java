package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class SgdTrektDto {
	private Integer id;
	private String noiDung;
	private Integer tsLop;
	private Integer tsHs;
	private Integer lopTk;
	private Integer hsTk;
	private Integer lopDb;
	private Integer hsDb;
	private Integer lopK1;
	private Integer hsK1;
	private Integer lopK2;
	private Integer hsK2;
	private Integer lopK3;
	private Integer hsK3;
	private Integer lopK4;
	private Integer hsK4;
	private Integer lopK5;
	private Integer hsK5;
	private Integer lopK6;
	private Integer hsK6;
	private Integer lopK7;
	private Integer hsK7;
	private Integer lopK8;
	private Integer hsK8;
	private Integer lopK9;
	private Integer hsK9;
	private Integer lopK10;
	private Integer hsK10;
	private Integer lopK11;
	private Integer hsK11;
	private Integer lopK12;
	private Integer hsK12;
	private Integer thang;
	private Integer nam;
	private Date ngayGioCapNhat;
	private Integer guiBcSgd;
}
