package vn.vnpt.camau.toolbc.sgd.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.toolbc.sgd.web.dto.MenuDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThamSoDto;
import vn.vnpt.camau.toolbc.sgd.web.service.MenuService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThamSoService;

@Controller
@RequestMapping("authen")
public class AuthenController {
	@Autowired
	private MenuService menuServ;
	@Autowired
	private ThamSoService thamSoServ;
	
	@GetMapping("/main-menu")
	public @ResponseBody List<MenuDto> layDsMenu2Cap() {
		return menuServ.layDsMenu2Cap();
	}
	
	@GetMapping("/lay-tt-cty")
	public @ResponseBody List<ThamSoDto> layTTCty() {
		return thamSoServ.layDsThamSoCongTy();
	}
}
