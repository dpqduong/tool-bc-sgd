package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmPhongGdDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdMnDto;

public interface DmPhongGdService {
	List<PgdMnDto> layDsPhongGdBaoCao(int thang, int nam, int tt);
}
