package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektKqht;

@Component
public class SgdTrektKqhtConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public SgdTrektKqhtDto convertToDto(SgdTrektKqht entity) {
		if (entity == null) {
			return new SgdTrektKqhtDto();
		}
		SgdTrektKqhtDto dto = mapper.map(entity, SgdTrektKqhtDto.class);
		return dto;
	}

	public SgdTrektKqht convertToEntity(SgdTrektKqhtDto dto) {
		if (dto == null) {
			return new SgdTrektKqht();
		}
		SgdTrektKqht entity = mapper.map(dto, SgdTrektKqht.class);
		return entity;
	}

}