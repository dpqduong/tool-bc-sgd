package vn.vnpt.camau.toolbc.sgd.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.KhuVucThangDto;

public interface KhuVucThangService {

	Page<KhuVucThangDto> layDsKhuVucThangTheoKhuVucLamViec(Pageable pageable);
	
	void ketChuyenKhuVucThang(long idThang) throws Exception;
	
	void khoiPhucKhuVucThangDangLamViec(long idThang) throws Exception;
	
	KhuVucThangDto layKhuVucThangLamViec();
}
