package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThptKqhtConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptKqht;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThptKqhtRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptKqhtService;

@Service
public class ThptKqhtServiceImp implements ThptKqhtService {

	@Autowired
	private ThptKqhtConverter converter;
	@Autowired
	private ThptKqhtRepository repository;
	
	@Override
	public void themSoLieuLopFileExcel(ThptKqhtDto soLieuLop) throws Exception {
		ThptKqht soLieuLopCanLuu= converter.convertToEntity(soLieuLop);
		try {
			repository.save(soLieuLopCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThptKqhtDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThptKqht soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThptKqhtDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
