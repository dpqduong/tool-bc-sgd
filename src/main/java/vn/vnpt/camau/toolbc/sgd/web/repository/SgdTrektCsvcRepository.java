package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektCsvc;

public interface SgdTrektCsvcRepository extends CrudRepository<SgdTrektCsvc, Integer> {

	Page<SgdTrektCsvc> findByNamAndThangOrderByThangAsc(int namHoc, int thang, Pageable pageable);
	Page<SgdTrektCsvc> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt, Pageable pageable);
	SgdTrektCsvc findByThangAndNam(int thang, int nam);
	SgdTrektCsvc findById(int id);
	SgdTrektCsvc findByIdAndGuiBcSgd(int id, int flag);
}
