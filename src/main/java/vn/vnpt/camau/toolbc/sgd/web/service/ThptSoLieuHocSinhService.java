package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuHocSinhDto;

public interface ThptSoLieuHocSinhService {
	void themSoLieuHocSinhFileExcel(ThptSoLieuHocSinhDto soLieuHocSinh) throws Exception;
	ThptSoLieuHocSinhDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
