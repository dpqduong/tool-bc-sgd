package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;

public interface DoiTuongRepository extends CrudRepository<DoiTuong, Long> {

	DoiTuong findByMaDoiTuong(String maDoiTuong);

	Page<DoiTuong> findByOrderByIdDoiTuongDesc(Pageable pageable);

	List<DoiTuong> findByLichGhiThus_IdLichGhiThuOrderByTenDoiTuong(Long idLichGhiThu);

}
