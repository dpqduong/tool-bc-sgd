package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuCbGvNv;

@Component
public class ThSoLieuCbGvNvConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThSoLieuCbGvNvDto convertToDto(ThSoLieuCbGvNv entity) {
		if (entity == null) {
			return new ThSoLieuCbGvNvDto();
		}
		ThSoLieuCbGvNvDto dto = mapper.map(entity, ThSoLieuCbGvNvDto.class);
		return dto;
	}

	public ThSoLieuCbGvNv convertToEntity(ThSoLieuCbGvNvDto dto) {
		if (dto == null) {
			return new ThSoLieuCbGvNv();
		}
		ThSoLieuCbGvNv entity = mapper.map(dto, ThSoLieuCbGvNv.class);
		return entity;
	}

}
