package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.PgdThcsDtConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThcsDtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcsDt;
import vn.vnpt.camau.toolbc.sgd.web.repository.PgdThcsDtRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.PgdThcsDtService;

@Service
public class PgdThcsDtServiceImp implements PgdThcsDtService {

	@Autowired
	private PgdThcsDtConverter converter;
	@Autowired
	private PgdThcsDtRepository repository;

	@Override
	public Page<PgdThcsDtDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<PgdThcsDt> soLieuCanLay;
		if(idDonVi == 10 || idDonVi == 11 ) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdAndIdPhongGdLessThanOrderByThangAsc(namHoc, thang, 1, 10,pageable);
		}else if(idDonVi == 999) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdAndIdPhongGdGreaterThanOrderByThangAsc(namHoc, thang, 1, 10,pageable);
		}else {
			soLieuCanLay = repository.findByIdPhongGdAndNamOrderByThangAsc(idDonVi, namHoc, pageable);
		}
		Page<PgdThcsDtDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public PgdThcsDtDto luuSoLieu(PgdThcsDtDto dto) throws Exception {
		PgdThcsDt soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		PgdThcsDtDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private PgdThcsDt themSoLieu(PgdThcsDtDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam(), dto.getIdPhongGd())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		PgdThcsDt soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam, int idDonVi) {
		PgdThcsDt canKiemTra = repository.findByThangAndNamAndIdPhongGd(thang, nam, idDonVi);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private PgdThcsDt capNhatSoLieu(PgdThcsDtDto dto) throws Exception {
		PgdThcsDt canKiemTra = kiemTraSoLieu(dto.getId());
		PgdThcsDt soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private PgdThcsDt kiemTraSoLieu(int id) throws Exception {
		PgdThcsDt canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private PgdThcsDt kiemTraSoLieuDaGui(int id) throws Exception {
		PgdThcsDt canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public PgdThcsDtDto laySoLieu(int id) {
		PgdThcsDt soLieuCanLay = repository.findById(id);
		PgdThcsDtDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		PgdThcsDt soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
