package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsKqht;

public interface ThcsKqhtRepository extends CrudRepository<ThcsKqht, Long> {
	ThcsKqht findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
