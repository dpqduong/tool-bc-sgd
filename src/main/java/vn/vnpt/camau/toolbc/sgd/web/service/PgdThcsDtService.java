package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThcsDtDto;

public interface PgdThcsDtService {
	public Page<PgdThcsDtDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	PgdThcsDtDto luuSoLieu(PgdThcsDtDto dto) throws Exception;
	PgdThcsDtDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
