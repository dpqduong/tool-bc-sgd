package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class SgdTrektCbGvNvDto {
	private Integer id;
	private Integer idTruong;
	private String tenTruong;
	private Integer thang;
	private Integer nam;
	private Date ngayGioCapNhat;
	private Integer guiBcSgd;
	private Integer cbql;
	private Integer gv2;
	private Integer gv3;
	private Integer nv;
	private Integer tsCbGvNv;
}
