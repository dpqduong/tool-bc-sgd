package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

public class ThcsKqhtDto {

	@Getter
	@Setter
	private Integer idKqht;
	@Getter
	@Setter
	@Mapping("dmTruong.idTruong")
	private Integer idTruong;
	@Getter
	@Setter
	private Integer slHlGioi;
	@Getter
	@Setter
	private Integer slHlKha;
	@Getter
	@Setter
	private Integer slHlTrungBinh;
	@Getter
	@Setter
	private Integer slHlYeu;
	@Getter
	@Setter
	private Integer slHlKem;
	@Getter
	@Setter
	private Integer slHkTot;
	@Getter
	@Setter
	private Integer slHkKha;
	@Getter
	@Setter
	private Integer slHkTrungBinh;
	@Getter
	@Setter
	private Integer slHkYeu;
	@Getter
	@Setter
	private Integer namHoc;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private Integer thoiGianImport;
	
}
