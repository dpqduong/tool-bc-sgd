package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThcsSoLieuCbGvNvConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThcsSoLieuCbGvNvRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsSoLieuCbGvNvService;

@Service
public class ThcsSoLieuCbGvNvServiceImp implements ThcsSoLieuCbGvNvService {

	@Autowired
	private ThcsSoLieuCbGvNvConverter converter;
	@Autowired
	private ThcsSoLieuCbGvNvRepository repository;
	
	@Override
	public void themSoLieuCbGvNvFileExcel(ThcsSoLieuCbGvNvDto soLieuCbGvNv) throws Exception {
		ThcsSoLieuCbGvNv soLieuCbGvNvCanLuu= converter.convertToEntity(soLieuCbGvNv);
		try {
			repository.save(soLieuCbGvNvCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThcsSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThcsSoLieuCbGvNv soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThcsSoLieuCbGvNvDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
