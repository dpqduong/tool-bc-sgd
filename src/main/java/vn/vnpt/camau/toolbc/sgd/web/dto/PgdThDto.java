package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;
@Data
public class PgdThDto {

	private Integer id;
	private int idPhongGd;
	private Integer truong;
	private Integer lopK1;
	private Integer lopK2;
	private Integer lopK3;
	private Integer lopK4;
	private Integer lopK5;
	private Integer tsLop;
	private Integer hsK1;
	private Integer hsK2;
	private Integer hsK3;
	private Integer hsK4;
	private Integer hsK5;
	private Integer tsHs;
	private Integer cbql;
	private Integer gv;
	private Integer nv;
	private Integer tsCbGvNv;
	private Integer thang;
	private Integer nam;
	private Date ngayGioCapNhat;
	private Integer guiBcSgd;
	private Integer tsPhong;
	private Integer tsPhongHoc;
	private Integer phKienCo;
	private Integer phBanKienCo;
	private Integer phTam;
	private Integer tsPhongLamViec;
	private Integer plvKienCo;
	private Integer plvBanKienCo;
	private Integer plvTam;
	private Integer plvNhaCongVu;
	private Integer pthTinHoc;
	private Integer pthNgoaiNgu;
	private Integer diemLe;
	private Integer truongHaiBuoi;
	private Integer truongBanTru;
	private Integer lopHaiBuoi;
	private Integer lopBanTru;
	private Integer lopHaiBuoiK1;
	private Integer lopHaiBuoiK2;
	private Integer lopHaiBuoiK3;
	private Integer lopHaiBuoiK4;
	private Integer lopHaiBuoiK5;
	private Integer lopBanTruK1;
	private Integer lopBanTruK2;
	private Integer lopBanTruK3;
	private Integer lopBanTruK4;
	private Integer lopBanTruK5;
	private Integer hsHaiBuoi;
	private Integer hsBanTru;
	private Integer hsHaiBuoiK1;
	private Integer hsHaiBuoiK2;
	private Integer hsHaiBuoiK3;
	private Integer hsHaiBuoiK4;
	private Integer hsHaiBuoiK5;
	private Integer hsBanTruK1;
	private Integer hsBanTruK2;
	private Integer hsBanTruK3;
	private Integer hsBanTruK4;
	private Integer hsBanTruK5;
}
