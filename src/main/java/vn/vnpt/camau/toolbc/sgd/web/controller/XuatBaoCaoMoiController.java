package vn.vnpt.camau.toolbc.sgd.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import vn.vnpt.camau.toolbc.sgd.web.ModelAttr;
import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.KhuVucDto;
import vn.vnpt.camau.toolbc.sgd.web.service.CallReportService;
import vn.vnpt.camau.toolbc.sgd.web.service.DmNamHocService;
import vn.vnpt.camau.toolbc.sgd.web.service.NguoiDungService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThamSoService;

@Controller
@RequestMapping("/xuat-bao-cao-moi")
public class XuatBaoCaoMoiController {
	private ModelAttr modelAttr = new ModelAttr("Xuất báo cáo", "xuatbaocaomoi",
			new String[] { "js/xuatbaocaomoi.js", "js/pdfobject.js","bower_components/select2/dist/js/select2.min.js" }
			, new String[] { "bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private ThamSoService thamSoServ;
	@Autowired
	private DmNamHocService dmNHServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds-nam-hoc")
	public @ResponseBody List<DmNamHocDto> layDsNamHoc() {
		return dmNHServ.layDsDmNamHoc();
	}
	
	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession, int nam, int maubc, int thang, int donvi, int thang_2, int nam_2) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		Map<String, Object> parameters = new HashMap<String, Object>();
		File reportFile;
		if(maubc == 0) {
			classPathResource = new ClassPathResource("/static/report/MN-NEW.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_MN", ".jasper");
		}else if(maubc == 1) {
			
	    classPathResource = new ClassPathResource("/static/report/TH-NEW.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("BC_TH", ".jasper");
		}else if(maubc == 2) {
		classPathResource = new ClassPathResource("/static/report/THCS-CHI-TIET.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("BC_THCS", ".jasper");
		}else if(maubc == 3){
			classPathResource = new ClassPathResource("/static/report/THPT-NEW.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THPT", ".jasper");
		}else if(maubc == 5){
			classPathResource = new ClassPathResource("/static/report/GD-HOA-NHAP.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_HTPTGDHN", ".jasper");
		}else if(maubc == 6){
			classPathResource = new ClassPathResource("/static/report/THCS-PGD.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THCS", ".jasper");
		}else if(maubc == 8){
			classPathResource = new ClassPathResource("/static/report/THCS-DT.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THCS_DT", ".jasper");
		}
		else
			{
			classPathResource = new ClassPathResource("/static/report/SO-LIEU-TONG-HOP.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("SO-LIEU-TONG-HOP", ".jasper");
			}

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
		String SUBREPORT_DIR = "static/report/";
		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		
		parameters.put("nam", nam);
		parameters.put("thang", thang);
		parameters.put("nam_2", nam_2);
		parameters.put("thang_2", thang_2);
		parameters.put("donvi", donvi);
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		parameters.put("SUBREPORT_DIR", SUBREPORT_DIR);
		return callReportServ.viewReport(reportFile, parameters, dataSource, httpSession);
	}
	
	@RequestMapping(value = "/xuat-file-xls", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLS(HttpServletResponse response, HttpSession httpSession, int nam, int maubc, int thang, int donvi, int thang_2, int nam_2)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		if(maubc == 0) {
			classPathResource = new ClassPathResource("/static/report/MN-NEW.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_MN", ".jasper");
		}else if(maubc == 1) {
			
	    classPathResource = new ClassPathResource("/static/report/TH-NEW.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("BC_TH", ".jasper");
		}else if(maubc == 2) {
		classPathResource = new ClassPathResource("/static/report/THCS-CHI-TIET.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("BC_THCS", ".jasper");
		}else if(maubc == 3){
			classPathResource = new ClassPathResource("/static/report/THPT-NEW.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THPT", ".jasper");
		}else if(maubc == 5){
			classPathResource = new ClassPathResource("/static/report/GD-HOA-NHAP.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_HTPTGDHN", ".jasper");
		}else if(maubc == 6){
			classPathResource = new ClassPathResource("/static/report/THCS-PGD.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THCS", ".jasper");
		}else if(maubc == 8){
			classPathResource = new ClassPathResource("/static/report/THCS-DT.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THCS_DT", ".jasper");
		}
		else
			{
			classPathResource = new ClassPathResource("/static/report/SO-LIEU-TONG-HOP.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("SO-LIEU-TONG-HOP", ".jasper");
			}
		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String SUBREPORT_DIR = "static/report/";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("nam", nam);
		parameters.put("thang", thang);
		parameters.put("nam_2", nam_2);
		parameters.put("thang_2", thang_2);
		parameters.put("donvi", donvi);
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		parameters.put("SUBREPORT_DIR", SUBREPORT_DIR);
		callReportServ.printReport("xls", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession, int nam, int maubc, int thang, int donvi, int thang_2, int nam_2)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		if(maubc == 0) {
			classPathResource = new ClassPathResource("/static/report/MN-NEW.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_MN", ".jasper");
		}else if(maubc == 1) {
			
	    classPathResource = new ClassPathResource("/static/report/TH-NEW.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("BC_TH", ".jasper");
		}else if(maubc == 2) {
		classPathResource = new ClassPathResource("/static/report/THCS-CHI-TIET.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("BC_THCS", ".jasper");
		}else if(maubc == 3){
			classPathResource = new ClassPathResource("/static/report/THPT-NEW.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THPT", ".jasper");
		}else if(maubc == 5){
			classPathResource = new ClassPathResource("/static/report/GD-HOA-NHAP.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_HTPTGDHN", ".jasper");
		}else if(maubc == 6){
			classPathResource = new ClassPathResource("/static/report/THCS-PGD.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THCS", ".jasper");
		}else if(maubc == 8){
			classPathResource = new ClassPathResource("/static/report/THCS-DT.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("BC_THCS_DT", ".jasper");
		}
		else
			{
			classPathResource = new ClassPathResource("/static/report/SO-LIEU-TONG-HOP.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("SO-LIEU-TONG-HOP", ".jasper");
			}

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
		String SUBREPORT_DIR = "static/report/";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("nam", nam);
		parameters.put("thang", thang);
		parameters.put("nam_2", nam_2);
		parameters.put("thang_2", thang_2);
		parameters.put("donvi", donvi);
		parameters.put("SUBREPORT_DIR", SUBREPORT_DIR);
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xlsx", reportFile, parameters, dataSource, httpSession, response);
	}

}
