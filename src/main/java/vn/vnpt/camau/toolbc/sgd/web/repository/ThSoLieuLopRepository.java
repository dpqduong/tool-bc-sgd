package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuLop;

public interface ThSoLieuLopRepository extends CrudRepository<ThSoLieuLop, Long> {
	public boolean existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	ThSoLieuLop findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
