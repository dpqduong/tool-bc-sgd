package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuCbGvNv;

@Component
public class ThcsSoLieuCbGvNvConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThcsSoLieuCbGvNvDto convertToDto(ThcsSoLieuCbGvNv entity) {
		if (entity == null) {
			return new ThcsSoLieuCbGvNvDto();
		}
		ThcsSoLieuCbGvNvDto dto = mapper.map(entity, ThcsSoLieuCbGvNvDto.class);
		return dto;
	}

	public ThcsSoLieuCbGvNv convertToEntity(ThcsSoLieuCbGvNvDto dto) {
		if (dto == null) {
			return new ThcsSoLieuCbGvNv();
		}
		ThcsSoLieuCbGvNv entity = mapper.map(dto, ThcsSoLieuCbGvNv.class);
		return entity;
	}

}
