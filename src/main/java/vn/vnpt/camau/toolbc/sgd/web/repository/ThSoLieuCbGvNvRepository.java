package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuHocSinh;

public interface ThSoLieuCbGvNvRepository extends CrudRepository<ThSoLieuCbGvNv, Long> {
	ThSoLieuCbGvNv findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
