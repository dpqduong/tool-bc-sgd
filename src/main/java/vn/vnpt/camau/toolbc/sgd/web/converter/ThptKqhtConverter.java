package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptKqht;

@Component
public class ThptKqhtConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThptKqhtDto convertToDto(ThptKqht entity) {
		if (entity == null) {
			return new ThptKqhtDto();
		}
		ThptKqhtDto dto = mapper.map(entity, ThptKqhtDto.class);
		return dto;
	}

	public ThptKqht convertToEntity(ThptKqhtDto dto) {
		if (dto == null) {
			return new ThptKqht();
		}
		ThptKqht entity = mapper.map(dto, ThptKqht.class);
		return entity;
	}

}
