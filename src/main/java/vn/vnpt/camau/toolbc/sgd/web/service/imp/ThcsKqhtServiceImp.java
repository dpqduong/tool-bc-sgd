package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThcsKqhtConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsKqht;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThcsKqhtRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsKqhtService;

@Service
public class ThcsKqhtServiceImp implements ThcsKqhtService {

	@Autowired
	private ThcsKqhtConverter converter;
	@Autowired
	private ThcsKqhtRepository repository;
	
	@Override
	public void themSoLieuLopFileExcel(ThcsKqhtDto soLieuLop) throws Exception {
		ThcsKqht soLieuLopCanLuu= converter.convertToEntity(soLieuLop);
		try {
			repository.save(soLieuLopCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThcsKqhtDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThcsKqht soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThcsKqhtDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
