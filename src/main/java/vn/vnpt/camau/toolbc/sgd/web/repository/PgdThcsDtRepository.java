package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcsDt;

public interface PgdThcsDtRepository extends CrudRepository<PgdThcsDt, Integer> {

	Page<PgdThcsDt> findByIdPhongGdAndNamOrderByThangAsc(int idDonVi, int namHoc, Pageable pageable);
	Page<PgdThcsDt> findByNamAndThangAndGuiBcSgdAndIdPhongGdLessThanOrderByThangAsc(int namHoc, int thang, int tt, int idPhongGd,Pageable pageable);
	Page<PgdThcsDt> findByNamAndThangAndGuiBcSgdAndIdPhongGdGreaterThanOrderByThangAsc(int namHoc, int thang, int tt, int idPhongGd, Pageable pageable);
	PgdThcsDt findByThangAndNamAndIdPhongGd(int thang, int nam, int idDonVi);
	PgdThcsDt findById(int id);
	PgdThcsDt findByIdAndGuiBcSgd(int id, int flag);
}
