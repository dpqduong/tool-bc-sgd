package vn.vnpt.camau.toolbc.sgd.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.toolbc.sgd.web.ModelAttr;
import vn.vnpt.camau.toolbc.sgd.web.Response;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThietLapThcDto;
import vn.vnpt.camau.toolbc.sgd.web.service.ThietLapThcService;


@Controller
@RequestMapping("/thiet-lap-thc")
public class ThietLapTongHopChungController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục thiết lập tổng hợp chung", "thietlapthc", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","js/thietlapthc.js"},
			new String[]{"jdgrid/jdgrid.css"});
	@Autowired
	private ThietLapThcService ThietLapThcServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<ThietLapThcDto> layDanhSach(Pageable page){
		return ThietLapThcServ.layDsThietLapThc(page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody ThietLapThcDto layChiTiet(int id){
		return ThietLapThcServ.layThietLapThc(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(ThietLapThcDto dto){
		try {
			ThietLapThcServ.luuThietLapThc(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
