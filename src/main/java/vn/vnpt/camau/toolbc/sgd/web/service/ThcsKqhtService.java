package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsKqhtDto;

public interface ThcsKqhtService {
	void themSoLieuLopFileExcel(ThcsKqhtDto soLieuLop) throws Exception;
	ThcsKqhtDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
