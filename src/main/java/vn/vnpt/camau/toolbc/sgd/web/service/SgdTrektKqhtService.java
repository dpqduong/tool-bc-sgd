package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektKqhtDto;

public interface SgdTrektKqhtService {
	public Page<SgdTrektKqhtDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	SgdTrektKqhtDto luuSoLieu(SgdTrektKqhtDto dto) throws Exception;
	SgdTrektKqhtDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
