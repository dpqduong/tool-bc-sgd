package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThangConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThangDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.Thang;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThangRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThangService;

@Service
public class ThangServiceImp implements ThangService {

	@Autowired
	private ThangConverter converter;
	@Autowired
	private ThangRepository repository;

	@Override
	public ThangDto layThangLamViec(long idKhuVuc) {
		Thang thangCanLay = repository.findTopByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(idKhuVuc);
		ThangDto thangDaChuyenDoi = converter.convertToDto(thangCanLay);
		return thangDaChuyenDoi;
	}

	@Override
	public List<ThangDto> layDsThangLamViec(long idKhuVuc) {
		List<Thang> dsThangCanLay = repository.findByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(idKhuVuc);
		List<ThangDto> dsThangDaChuyenDoi = dsThangCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsThangDaChuyenDoi;
	}

	@Override
	public ThangDto layThang(long idThang) {
		Thang thangCanLay = repository.findOne(idThang);
		ThangDto thangDaChuyenDoi = converter.convertToDto(thangCanLay);
		return thangDaChuyenDoi;
	}

	@Override
	public Thang layThangPhanCongGhiThuMoiNhat(long idNguoiDung) throws Exception {
		// TODO: refactor
		Thang thangPhanCongGhiThuMoiNhat = repository
				.findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			throw new Exception("Không thể lấy tháng thanh toán hóa đơn.");
		}
		return thangPhanCongGhiThuMoiNhat;
	}

}
