package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.PgdThcsConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThcsDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcs;
import vn.vnpt.camau.toolbc.sgd.web.repository.PgdThcsRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.PgdThcsService;

@Service
public class PgdThcsServiceImp implements PgdThcsService {

	@Autowired
	private PgdThcsConverter converter;
	@Autowired
	private PgdThcsRepository repository;

	@Override
	public Page<PgdThcsDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable) {
		Page<PgdThcs> soLieuCanLay;
		if(idDonVi == 10 || idDonVi == 11 ) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdAndIdPhongGdLessThanOrderByThangAsc(namHoc, thang, 1, 10,pageable);
		}else if(idDonVi == 999) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdAndIdPhongGdGreaterThanOrderByThangAsc(namHoc, thang, 1, 10,pageable);
		}else {
			soLieuCanLay = repository.findByIdPhongGdAndNamOrderByThangAsc(idDonVi, namHoc, pageable);
		}
		Page<PgdThcsDto> soLieuDaChuyenDoi = soLieuCanLay.map(converter::convertToDto);
		return soLieuDaChuyenDoi;
	}

	@Override
	public PgdThcsDto luuSoLieu(PgdThcsDto dto) throws Exception {
		PgdThcs soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		PgdThcsDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private PgdThcs themSoLieu(PgdThcsDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam(), dto.getIdPhongGd())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		PgdThcs soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam, int idDonVi) {
		PgdThcs canKiemTra = repository.findByThangAndNamAndIdPhongGd(thang, nam, idDonVi);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private PgdThcs capNhatSoLieu(PgdThcsDto dto) throws Exception {
		PgdThcs canKiemTra = kiemTraSoLieu(dto.getId());
		PgdThcs soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private PgdThcs kiemTraSoLieu(int id) throws Exception {
		PgdThcs canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private PgdThcs kiemTraSoLieuDaGui(int id) throws Exception {
		PgdThcs canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public PgdThcsDto laySoLieu(int id) {
		PgdThcs soLieuCanLay = repository.findById(id);
		PgdThcsDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		PgdThcs soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
