package vn.vnpt.camau.toolbc.sgd.web.entity;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class FileBean {

	private CommonsMultipartFile fileData;
	private CommonsMultipartFile fileData1;
	private CommonsMultipartFile fileData2;
	private CommonsMultipartFile fileData3;

	public CommonsMultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}
	
	public CommonsMultipartFile getFileData1() {
		return fileData1;
	}

	public void setFileData1(CommonsMultipartFile fileData1) {
		this.fileData1 = fileData1;
	}
	
	public CommonsMultipartFile getFileData2() {
		return fileData2;
	}

	public void setFileData2(CommonsMultipartFile fileData2) {
		this.fileData2 = fileData2;
	}
	
	public CommonsMultipartFile getFileData3() {
		return fileData3;
	}

	public void setFileData3(CommonsMultipartFile fileData3) {
		this.fileData3 = fileData3;
	}
}
