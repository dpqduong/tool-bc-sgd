package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuCbGvNvDto;

public interface ThptSoLieuCbGvNvService {
	void themSoLieuCbGvNvFileExcel(ThptSoLieuCbGvNvDto soLieuCbGvNv) throws Exception;
	ThptSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
