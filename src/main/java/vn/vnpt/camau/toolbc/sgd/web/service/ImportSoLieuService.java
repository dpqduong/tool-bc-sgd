package vn.vnpt.camau.toolbc.sgd.web.service;

import vn.vnpt.camau.toolbc.sgd.web.entity.FileBean;

public interface ImportSoLieuService {

	String importSoLieuLopMN(FileBean fileBean, int idTruong, int namHoc, int thoiGian, int action, int loaiTruong);
	String importSoLieuVnEdu(FileBean fileBean, int idTruong, int namHoc, int thoiGian, int action, int loaiTruong);
	String kiemTra(int idTruong, int namHoc, int thoiGian, int loaiTruong);
}
