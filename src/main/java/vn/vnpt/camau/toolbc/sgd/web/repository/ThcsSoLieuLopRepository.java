package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuLop;

public interface ThcsSoLieuLopRepository extends CrudRepository<ThcsSoLieuLop, Long> {
	public boolean existsBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	ThcsSoLieuLop findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
