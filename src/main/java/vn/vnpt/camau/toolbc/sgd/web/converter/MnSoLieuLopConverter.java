package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;

@Component
public class MnSoLieuLopConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public MnSoLieuLopDto convertToDto(MnSoLieuLop entity) {
		if (entity == null) {
			return new MnSoLieuLopDto();
		}
		MnSoLieuLopDto dto = mapper.map(entity, MnSoLieuLopDto.class);
		return dto;
	}

	public MnSoLieuLop convertToEntity(MnSoLieuLopDto dto) {
		if (dto == null) {
			return new MnSoLieuLop();
		}
		MnSoLieuLop entity = mapper.map(dto, MnSoLieuLop.class);
		return entity;
	}

}
