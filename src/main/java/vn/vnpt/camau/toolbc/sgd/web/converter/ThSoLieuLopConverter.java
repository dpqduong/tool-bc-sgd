package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuLop;

@Component
public class ThSoLieuLopConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThSoLieuLopDto convertToDto(ThSoLieuLop entity) {
		if (entity == null) {
			return new ThSoLieuLopDto();
		}
		ThSoLieuLopDto dto = mapper.map(entity, ThSoLieuLopDto.class);
		return dto;
	}

	public ThSoLieuLop convertToEntity(ThSoLieuLopDto dto) {
		if (dto == null) {
			return new ThSoLieuLop();
		}
		ThSoLieuLop entity = mapper.map(dto, ThSoLieuLop.class);
		return entity;
	}

}
