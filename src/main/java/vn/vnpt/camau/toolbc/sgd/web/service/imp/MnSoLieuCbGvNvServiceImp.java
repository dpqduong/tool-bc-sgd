package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.MnSoLieuCbGvNvConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuLopDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuLop;
import vn.vnpt.camau.toolbc.sgd.web.repository.MnSoLieuCbGvNvRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.MnSoLieuCbGvNvService;

@Service
public class MnSoLieuCbGvNvServiceImp implements MnSoLieuCbGvNvService {

	@Autowired
	private MnSoLieuCbGvNvConverter converter;
	@Autowired
	private MnSoLieuCbGvNvRepository repository;
	
	@Override
	public void themSoLieuCbGvNvFileExcel(MnSoLieuCbGvNvDto soLieuCbGvNv) throws Exception {
		MnSoLieuCbGvNv soLieuCbGvNvCanLuu= converter.convertToEntity(soLieuCbGvNv);
		try {
			repository.save(soLieuCbGvNvCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public MnSoLieuCbGvNvDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		MnSoLieuCbGvNv soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		MnSoLieuCbGvNvDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
