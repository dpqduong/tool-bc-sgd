package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;

@Component
public class DoiTuongConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public DoiTuongDto convertToDto(DoiTuong entity) {
		if (entity == null) {
			return new DoiTuongDto();
		}
		DoiTuongDto dto = mapper.map(entity, DoiTuongDto.class);
		return dto;
	}

	public DoiTuong convertToEntity(DoiTuongDto dto) {
		if (dto == null) {
			return new DoiTuong();
		}
		DoiTuong entity = mapper.map(dto, DoiTuong.class);
		return entity;
	}

}
