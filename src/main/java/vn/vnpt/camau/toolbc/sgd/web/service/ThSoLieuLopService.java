package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuLopDto;

public interface ThSoLieuLopService {
	void themSoLieuLopFileExcel(ThSoLieuLopDto soLieuLop) throws Exception;
	public boolean existsByIdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
	ThSoLieuLopDto laySoLieu(int idTruong, int namHoc, int thoiGian);
}
