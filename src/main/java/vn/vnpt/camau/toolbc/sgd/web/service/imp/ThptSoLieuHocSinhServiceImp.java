package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThptSoLieuHocSinhConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThptSoLieuHocSinhRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThptSoLieuHocSinhService;

@Service
public class ThptSoLieuHocSinhServiceImp implements ThptSoLieuHocSinhService {

	@Autowired
	private ThptSoLieuHocSinhConverter converter;
	@Autowired
	private ThptSoLieuHocSinhRepository repository;
	
	@Override
	public void themSoLieuHocSinhFileExcel(ThptSoLieuHocSinhDto soLieuHocSinh) throws Exception {
		ThptSoLieuHocSinh soLieuHocSinhCanLuu= converter.convertToEntity(soLieuHocSinh);
		try {
			repository.save(soLieuHocSinhCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThptSoLieuHocSinhDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThptSoLieuHocSinh soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThptSoLieuHocSinhDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
