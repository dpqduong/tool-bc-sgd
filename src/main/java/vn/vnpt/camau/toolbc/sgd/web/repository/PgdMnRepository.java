package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.PgdMn;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcs;

public interface PgdMnRepository extends CrudRepository<PgdMn, Integer> {

	Page<PgdMn> findByIdPhongGdAndNamOrderByThangAsc(int idDonVi, int namHoc, Pageable pageable);
	Page<PgdMn> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt, Pageable pageable);
	PgdMn findByThangAndNamAndIdPhongGd(int thang, int nam, int idDonVi);
	PgdMn findById(int id);
	PgdMn findByIdAndGuiBcSgd(int id, int flag);
	List<PgdMn> findByThangAndNamAndGuiBcSgdOrderByIdPhongGdAsc(int thang, int nam, int tt);
}
