package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;

public interface DmNamHocService {

	Page<DmNamHocDto> layDsDmNamHoc(Pageable pageable);

	List<DmNamHocDto> layDsDmNamHoc();
	
}
