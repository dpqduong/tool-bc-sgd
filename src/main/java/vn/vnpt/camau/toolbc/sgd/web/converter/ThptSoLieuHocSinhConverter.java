package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuHocSinh;

@Component
public class ThptSoLieuHocSinhConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThptSoLieuHocSinhDto convertToDto(ThptSoLieuHocSinh entity) {
		if (entity == null) {
			return new ThptSoLieuHocSinhDto();
		}
		ThptSoLieuHocSinhDto dto = mapper.map(entity, ThptSoLieuHocSinhDto.class);
		return dto;
	}

	public ThptSoLieuHocSinh convertToEntity(ThptSoLieuHocSinhDto dto) {
		if (dto == null) {
			return new ThptSoLieuHocSinh();
		}
		ThptSoLieuHocSinh entity = mapper.map(dto, ThptSoLieuHocSinh.class);
		return entity;
	}

}
