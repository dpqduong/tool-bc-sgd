package vn.vnpt.camau.toolbc.sgd.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.toolbc.sgd.web.ModelAttr;
import vn.vnpt.camau.toolbc.sgd.web.Utilities;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmNamHocDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.DuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NganHangDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.FileBean;
import vn.vnpt.camau.toolbc.sgd.web.service.DmNamHocService;
import vn.vnpt.camau.toolbc.sgd.web.service.DmTruongService;
import vn.vnpt.camau.toolbc.sgd.web.service.ImportSoLieuService;

@Controller
@RequestMapping("/import-so-lieu-vnedu")
public class ImportSoLieuVnEduController {
	private ModelAttr modelAttr = new ModelAttr("Import số liệu trường, lớp, học sinh và CBQL, GV, NV (VnEdu)", "importsolieuvnedu",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/importsolieuvnedu.js", "bower_components/select2/dist/js/select2.min.js" },
			new String[] { "jdgrid/jdgrid.css","bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	private ImportSoLieuService importServ;
	@Autowired
	private DmTruongService dmServ;
	@Autowired
	private DmNamHocService dmNHServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@PostMapping("/xu-ly")
	public @ResponseBody String upload(FileBean uploadItem, BindingResult result,@RequestParam int idTruong,@RequestParam int namHoc,@RequestParam int thoiGian,@RequestParam int action,@RequestParam int cap) {
		return importServ.importSoLieuVnEdu(uploadItem, idTruong, namHoc, thoiGian, action, cap);
	}
	
	@PostMapping("/kiem-tra")
	public @ResponseBody String kiemTra(FileBean uploadItem, BindingResult result,@RequestParam int idTruong,@RequestParam int namHoc,@RequestParam int thoiGian,@RequestParam int cap) {
		return importServ.kiemTra(idTruong, namHoc, thoiGian, cap);
	}
	
	@GetMapping("/lay-ds-nam-hoc")
	public @ResponseBody List<DmNamHocDto> layDsNamHoc() {
		return dmNHServ.layDsDmNamHoc();
	}
	
	@GetMapping("/lay-ds-truong")
	public @ResponseBody List<DmTruongDto> layDsTruong() {
		return dmServ.layDsDmTruong();
	}
}
