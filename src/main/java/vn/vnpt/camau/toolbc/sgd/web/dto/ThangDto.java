package vn.vnpt.camau.toolbc.sgd.web.dto;

import lombok.Getter;
import lombok.Setter;

public class ThangDto {

	@Getter
	@Setter
	private long idThang;
	@Getter
	@Setter
	private String tenThang;

}
