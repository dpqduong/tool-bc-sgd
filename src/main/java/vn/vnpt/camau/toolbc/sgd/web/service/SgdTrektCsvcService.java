package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCsvcDto;

public interface SgdTrektCsvcService {
	public Page<SgdTrektCsvcDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	SgdTrektCsvcDto luuSoLieu(SgdTrektCsvcDto dto) throws Exception;
	SgdTrektCsvcDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
