package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuHocSinh;

@Component
public class ThSoLieuHocSinhConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThSoLieuHocSinhDto convertToDto(ThSoLieuHocSinh entity) {
		if (entity == null) {
			return new ThSoLieuHocSinhDto();
		}
		ThSoLieuHocSinhDto dto = mapper.map(entity, ThSoLieuHocSinhDto.class);
		return dto;
	}

	public ThSoLieuHocSinh convertToEntity(ThSoLieuHocSinhDto dto) {
		if (dto == null) {
			return new ThSoLieuHocSinh();
		}
		ThSoLieuHocSinh entity = mapper.map(dto, ThSoLieuHocSinh.class);
		return entity;
	}

}
