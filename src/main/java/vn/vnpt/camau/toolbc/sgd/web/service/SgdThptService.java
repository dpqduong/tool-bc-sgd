package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdThptDto;

public interface SgdThptService {
	public List<SgdThptDto> layDanhSach(int idDonVi, int namHoc, int thang);
	SgdThptDto luuSoLieu(SgdThptDto dto) throws Exception;
	SgdThptDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
