package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThcsSoLieuHocSinhConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThcsSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThcsSoLieuHocSinhRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThcsSoLieuHocSinhService;

@Service
public class ThcsSoLieuHocSinhServiceImp implements ThcsSoLieuHocSinhService {

	@Autowired
	private ThcsSoLieuHocSinhConverter converter;
	@Autowired
	private ThcsSoLieuHocSinhRepository repository;
	
	@Override
	public void themSoLieuHocSinhFileExcel(ThcsSoLieuHocSinhDto soLieuHocSinh) throws Exception {
		ThcsSoLieuHocSinh soLieuHocSinhCanLuu= converter.convertToEntity(soLieuHocSinh);
		try {
			repository.save(soLieuHocSinhCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThcsSoLieuHocSinhDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThcsSoLieuHocSinh soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThcsSoLieuHocSinhDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
