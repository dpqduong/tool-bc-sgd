package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptKqhtDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThcsKqht;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptKqht;

public interface ThptKqhtRepository extends CrudRepository<ThptKqht, Long> {
	ThptKqht findBydmTruong_IdTruongAndNamHocAndThoiGianImport(int idTruong, int namHoc, int thoiGian);
}
