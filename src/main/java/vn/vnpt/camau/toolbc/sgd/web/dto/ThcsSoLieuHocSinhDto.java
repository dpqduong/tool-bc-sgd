package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

public class ThcsSoLieuHocSinhDto {

	@Getter
	@Setter
	private Integer idSlHs;
	@Getter
	@Setter
	@Mapping("dmTruong.idTruong")
	private Integer idTruong;
	@Getter
	@Setter
	private Integer tongSo;
	@Getter
	@Setter
	private Integer k6;
	@Getter
	@Setter
	private Integer k7;
	@Getter
	@Setter
	private Integer k8;
	@Getter
	@Setter
	private Integer k9;
	@Getter
	@Setter
	private Integer namHoc;
	@Getter
	@Setter
	private Integer thoiGianImport;
	@Getter
	@Setter
	private Date ngayGioCapNhat;

	
}
