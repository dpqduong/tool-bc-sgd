package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.PgdThcsDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdThcs;

@Component
public class PgdThcsConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PgdThcsDto convertToDto(PgdThcs entity) {
		if (entity == null) {
			return new PgdThcsDto();
		}
		PgdThcsDto dto = mapper.map(entity, PgdThcsDto.class);
		return dto;
	}

	public PgdThcs convertToEntity(PgdThcsDto dto) {
		if (dto == null) {
			return new PgdThcs();
		}
		PgdThcs entity = mapper.map(dto, PgdThcs.class);
		return entity;
	}

}
