package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmPhongGdDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;
import vn.vnpt.camau.toolbc.sgd.web.entity.Huyen;

@Component
public class DmPhongGdConverter {

	@Autowired
	private DozerBeanMapper mapper;
	
	public DmPhongGdDto convertToDto(DmPhongGd entity) {
		if (entity == null) {
			return new DmPhongGdDto();
		}
		DmPhongGdDto dto = mapper.map(entity, DmPhongGdDto.class);
		return dto;
	}
	
	public DmPhongGd convertToEntity(DmPhongGdDto dto) {
		if (dto == null) {
			return new DmPhongGd();
		}
		DmPhongGd entity = mapper.map(dto, DmPhongGd.class);
		return entity;
	}

}
