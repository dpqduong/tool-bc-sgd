package vn.vnpt.camau.toolbc.sgd.web.entity;
// Generated May 27, 2020 2:41:51 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ThietLapThc generated by hbm2java
 */
@Entity
@Table(name = "THIET_LAP_THC")
public class ThietLapThc implements java.io.Serializable {

	private Integer id;
	private Integer nam;
	private Integer thangDauNam;
	private Integer thangCuoiNam;

	public ThietLapThc() {
	}

	public ThietLapThc(Integer nam, Integer thangDauNam, Integer thangCuoiNam) {
		this.nam = nam;
		this.thangDauNam = thangDauNam;
		this.thangCuoiNam = thangCuoiNam;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAM")
	public Integer getNam() {
		return this.nam;
	}

	public void setNam(Integer nam) {
		this.nam = nam;
	}

	@Column(name = "THANG_DAU_NAM")
	public Integer getthangDauNam() {
		return this.thangDauNam;
	}

	public void setthangDauNam(Integer thangDauNam) {
		this.thangDauNam = thangDauNam;
	}

	@Column(name = "THANG_CUOI_NAM")
	public Integer getthangCuoiNam() {
		return this.thangCuoiNam;
	}

	public void setthangCuoiNam(Integer thangCuoiNam) {
		this.thangCuoiNam = thangCuoiNam;
	}

}
