package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.dto.DonViDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.KhuVucDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThangDto;
import vn.vnpt.camau.toolbc.sgd.web.service.DonViService;
import vn.vnpt.camau.toolbc.sgd.web.service.KhuVucService;
import vn.vnpt.camau.toolbc.sgd.web.service.PhienLamViecService;
import vn.vnpt.camau.toolbc.sgd.web.service.ThangService;

@Service
public class PhienLamViecServiceImp implements PhienLamViecService {

	@Autowired
	private DonViService donViService;
	@Autowired
	private KhuVucService khuVucService;
	@Autowired
	private ThangService thangService;
	@Autowired
    private HttpSession httpSession;

	@Override
	public void thietLapPhienLamViec(long idNguoiDung) {
		DonViDto donViLamViec = donViService.layXiNghiepLamViec(idNguoiDung);
		Long idDonViLamViec = donViLamViec.getIdDonVi();
		KhuVucDto khuVucLamViec = khuVucService.layKhuVucLamViec(idDonViLamViec);
		ThangDto thangLamViec = thangService.layThangLamViec(khuVucLamViec.getIdKhuVuc());
		thietLapPhienLamViec(idDonViLamViec, khuVucLamViec, thangLamViec);
	}

	private void thietLapPhienLamViec(Long idDonViLamViec, KhuVucDto khuVucLamViec, ThangDto thangLamViec) {
		httpSession.setAttribute("idDonVi", idDonViLamViec);
		httpSession.setAttribute("khuVuc", khuVucLamViec);
		httpSession.setAttribute("thang", thangLamViec);
	}

	@Override
	public void thietLapPhienLamViec(long idDonViLamViec, long idKhucVucLamViec, long idThangLamViec) {
		KhuVucDto khuVucLamViec = khuVucService.layKhuVuc(idKhucVucLamViec);
		ThangDto thangLamViec = thangService.layThang(idThangLamViec);
		thietLapPhienLamViec(idDonViLamViec, khuVucLamViec, thangLamViec);
	}

}
