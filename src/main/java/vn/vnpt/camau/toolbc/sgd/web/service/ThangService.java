package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThangDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.Thang;

public interface ThangService {

	ThangDto layThangLamViec(long idKhuVuc);
	
	List<ThangDto> layDsThangLamViec(long idKhuVuc);
	
	ThangDto layThang(long idThang);
	
	Thang layThangPhanCongGhiThuMoiNhat(long idNguoiDung) throws Exception;
}
