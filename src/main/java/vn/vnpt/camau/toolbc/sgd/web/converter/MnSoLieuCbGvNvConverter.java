package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.MnSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.MnSoLieuCbGvNv;

@Component
public class MnSoLieuCbGvNvConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public MnSoLieuCbGvNvDto convertToDto(MnSoLieuCbGvNv entity) {
		if (entity == null) {
			return new MnSoLieuCbGvNvDto();
		}
		MnSoLieuCbGvNvDto dto = mapper.map(entity, MnSoLieuCbGvNvDto.class);
		return dto;
	}

	public MnSoLieuCbGvNv convertToEntity(MnSoLieuCbGvNvDto dto) {
		if (dto == null) {
			return new MnSoLieuCbGvNv();
		}
		MnSoLieuCbGvNv entity = mapper.map(dto, MnSoLieuCbGvNv.class);
		return entity;
	}

}
