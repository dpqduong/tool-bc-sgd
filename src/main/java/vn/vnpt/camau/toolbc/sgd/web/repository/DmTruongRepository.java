package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmTruong;

public interface DmTruongRepository extends CrudRepository<DmTruong, Integer> {
	@Query("select t from DmTruong t join t.dmPhongGd pgd "
			+ "order by pgd.idPhongGd Asc, t.idTruong Desc")
	@EntityGraph(attributePaths = { "dmPhongGd" })
	Page<DmTruong> layDsTruong(Pageable pageable);
	
	Page<DmTruong> findByOrderByIdTruongAsc(Pageable pageable);
	
	@Query("select t from DmTruong t join t.dmPhongGd pgd "
			+ " where t.tenTruong = ?1 and pgd.idPhongGd = ?2" )
	DmTruong layIdTruong(String tenTruong, int idPhongGd);
	
	DmTruong findByIdTruong(int idTruong);
	@EntityGraph(attributePaths = { "dmPhongGd" })
	List<DmTruong> findByDmPhongGd_IdPhongGd(Integer idDonVi);
	@EntityGraph(attributePaths = { "dmPhongGd" })
	List<DmTruong> findByLoaiTruongOrderByLoaiTruongAsc(Integer cap);
	@EntityGraph(attributePaths = { "dmPhongGd" })
	List<DmTruong> findByDmPhongGd_IdPhongGdAndLoaiTruongOrderByLoaiTruongAsc(Integer idDonVi, Integer cap);
}
