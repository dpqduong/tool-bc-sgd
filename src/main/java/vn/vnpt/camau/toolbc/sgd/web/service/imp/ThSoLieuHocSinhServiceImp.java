package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.ThSoLieuHocSinhConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.ThSoLieuHocSinhDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuCbGvNv;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThSoLieuHocSinh;
import vn.vnpt.camau.toolbc.sgd.web.repository.ThSoLieuHocSinhRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.ThSoLieuHocSinhService;

@Service
public class ThSoLieuHocSinhServiceImp implements ThSoLieuHocSinhService {

	@Autowired
	private ThSoLieuHocSinhConverter converter;
	@Autowired
	private ThSoLieuHocSinhRepository repository;
	
	@Override
	public void themSoLieuHocSinhFileExcel(ThSoLieuHocSinhDto soLieuHocSinh) throws Exception {
		ThSoLieuHocSinh soLieuHocSinhCanLuu= converter.convertToEntity(soLieuHocSinh);
		try {
			repository.save(soLieuHocSinhCanLuu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu lớp.");
		}
		
	}

	@Override
	public ThSoLieuHocSinhDto laySoLieu(int idTruong, int namHoc, int thoiGian) {
		ThSoLieuHocSinh soLieuCanLay = repository.findBydmTruong_IdTruongAndNamHocAndThoiGianImport(idTruong, namHoc, thoiGian);
		ThSoLieuHocSinhDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

}
