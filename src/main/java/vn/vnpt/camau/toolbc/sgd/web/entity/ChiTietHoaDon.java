package vn.vnpt.camau.toolbc.sgd.web.entity;
// Generated Apr 19, 2018 10:52:34 AM by Hibernate Tools 5.2.8.Final

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ChiTietHoaDon generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "CHI_TIET_HOA_DON")
public class ChiTietHoaDon implements java.io.Serializable {

	private Long idChiTietHoaDon;
	private ChiTietGiaNuoc chiTietGiaNuoc;
	private HoaDon hoaDon;
	private int tieuThu;
	private BigDecimal donGia;
	private long thanhTien;

	public ChiTietHoaDon() {
	}

	public ChiTietHoaDon(ChiTietGiaNuoc chiTietGiaNuoc, HoaDon hoaDon, int tieuThu, BigDecimal donGia, long thanhTien) {
		this.chiTietGiaNuoc = chiTietGiaNuoc;
		this.hoaDon = hoaDon;
		this.tieuThu = tieuThu;
		this.donGia = donGia;
		this.thanhTien = thanhTien;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID_CHI_TIET_HOA_DON", unique = true, nullable = false)
	public Long getIdChiTietHoaDon() {
		return this.idChiTietHoaDon;
	}

	public void setIdChiTietHoaDon(Long idChiTietHoaDon) {
		this.idChiTietHoaDon = idChiTietHoaDon;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CHI_TIET_GIA_NUOC")
	public ChiTietGiaNuoc getChiTietGiaNuoc() {
		return this.chiTietGiaNuoc;
	}

	public void setChiTietGiaNuoc(ChiTietGiaNuoc chiTietGiaNuoc) {
		this.chiTietGiaNuoc = chiTietGiaNuoc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_HOA_DON", nullable = false)
	public HoaDon getHoaDon() {
		return this.hoaDon;
	}

	public void setHoaDon(HoaDon hoaDon) {
		this.hoaDon = hoaDon;
	}

	@Column(name = "TIEU_THU", nullable = false)
	public int getTieuThu() {
		return this.tieuThu;
	}

	public void setTieuThu(int tieuThu) {
		this.tieuThu = tieuThu;
	}

	@Column(name = "DON_GIA", nullable = false, precision = 13, scale = 4)
	public BigDecimal getDonGia() {
		return this.donGia;
	}

	public void setDonGia(BigDecimal donGia) {
		this.donGia = donGia;
	}

	@Column(name = "THANH_TIEN", nullable = false, precision = 10, scale = 0)
	public long getThanhTien() {
		return this.thanhTien;
	}

	public void setThanhTien(long thanhTien) {
		this.thanhTien = thanhTien;
	}

}
