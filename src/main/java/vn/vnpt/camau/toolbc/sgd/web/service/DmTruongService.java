package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.DmTruongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;

public interface DmTruongService {

	Page<DmTruongDto> layDsDmTruong(Pageable pageable);

	List<DmTruongDto> layDsDmTruong();
	
	List<DmTruongDto> layDsDmTruongByIdDonVi(int idDonVi);
	
	DmTruongDto layTTTruong(int id);

	DmTruongDto layIdTruong(String tenTruong, int idPhongGd);
	
	List<DmTruongDto> layDsDmTruongTheoCap(int cap);
	
	List<DmTruongDto> layDsDmTruongByIdDonViAndTheoCap(int idDonVi, int cap);
}
