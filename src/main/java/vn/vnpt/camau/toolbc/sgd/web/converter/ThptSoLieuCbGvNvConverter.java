package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.ThptSoLieuCbGvNvDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.ThptSoLieuCbGvNv;

@Component
public class ThptSoLieuCbGvNvConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThptSoLieuCbGvNvDto convertToDto(ThptSoLieuCbGvNv entity) {
		if (entity == null) {
			return new ThptSoLieuCbGvNvDto();
		}
		ThptSoLieuCbGvNvDto dto = mapper.map(entity, ThptSoLieuCbGvNvDto.class);
		return dto;
	}

	public ThptSoLieuCbGvNv convertToEntity(ThptSoLieuCbGvNvDto dto) {
		if (dto == null) {
			return new ThptSoLieuCbGvNv();
		}
		ThptSoLieuCbGvNv entity = mapper.map(dto, ThptSoLieuCbGvNv.class);
		return entity;
	}

}
