package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektCbGvNvDto;

public interface SgdTrektCbGvNvService {
	public Page<SgdTrektCbGvNvDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	SgdTrektCbGvNvDto luuSoLieu(SgdTrektCbGvNvDto dto) throws Exception;
	SgdTrektCbGvNvDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
