package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrekt;

public interface SgdTrektRepository extends CrudRepository<SgdTrekt, Integer> {

	Page<SgdTrekt> findByNamAndThangOrderByThangAsc(int namHoc, int thang, Pageable pageable);
	Page<SgdTrekt> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt, Pageable pageable);
	SgdTrekt findByThangAndNamAndNoiDung(int thang, int nam, String noiDung);
	SgdTrekt findById(int id);
	SgdTrekt findByIdAndGuiBcSgd(int id, int flag);
}
