package vn.vnpt.camau.toolbc.sgd.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.toolbc.sgd.web.dto.SgdTrektDto;

public interface SgdTrektService {
	public Page<SgdTrektDto> layDanhSach(int idDonVi, int namHoc, int thang, Pageable pageable);
	SgdTrektDto luuSoLieu(SgdTrektDto dto) throws Exception;
	SgdTrektDto laySoLieu(int id);
	void xoaSoLieu(int id) throws Exception;
	void baoCaoSgd(int id, int tt) throws Exception;
}
