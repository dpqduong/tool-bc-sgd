package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class DmPhongGdDto {

	@Getter
	@Setter
	private Integer idPhongGd;
	@Getter
	@Setter
	private String tenPhongGd;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	
}
