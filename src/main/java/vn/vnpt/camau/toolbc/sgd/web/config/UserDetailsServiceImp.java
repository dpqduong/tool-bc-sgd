package vn.vnpt.camau.toolbc.sgd.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.repository.NguoiDungRepository;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

	@Autowired
	private NguoiDungRepository nguoiDungRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		NguoiDung nguoiDungCanKiemTra = nguoiDungRepository.findByTenNguoiDungAndVaiTro_Menus_Menu_IdMenuIsNotNull(username);
		if (nguoiDungCanKiemTra == null) {
			throw new UsernameNotFoundException("Tên người dùng hoặc mật khẩu không đúng.");
		}
		UserDetailsImp userDetails = new UserDetailsImp(nguoiDungCanKiemTra, nguoiDungCanKiemTra.getVaiTro().getMenus());
		return userDetails;
	}

}
