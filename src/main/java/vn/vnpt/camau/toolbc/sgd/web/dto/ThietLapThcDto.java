package vn.vnpt.camau.toolbc.sgd.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class ThietLapThcDto {

	@Getter
	@Setter
	private Integer id;
	@Getter
	@Setter
	private Integer nam;
	@Getter
	@Setter
	private Integer thangDauNam;
	@Getter
	@Setter
	private Integer thangCuoiNam;
	
}
