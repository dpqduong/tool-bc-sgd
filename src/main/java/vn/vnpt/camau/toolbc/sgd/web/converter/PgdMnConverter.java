package vn.vnpt.camau.toolbc.sgd.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.toolbc.sgd.web.dto.PgdMnDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.PgdMn;

@Component
public class PgdMnConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PgdMnDto convertToDto(PgdMn entity) {
		if (entity == null) {
			return new PgdMnDto();
		}
		PgdMnDto dto = mapper.map(entity, PgdMnDto.class);
		return dto;
	}

	public PgdMn convertToEntity(PgdMnDto dto) {
		if (dto == null) {
			return new PgdMn();
		}
		PgdMn entity = mapper.map(dto, PgdMn.class);
		return entity;
	}

}
