package vn.vnpt.camau.toolbc.sgd.web.service;

public interface PhienLamViecService {

	void thietLapPhienLamViec(long idNguoiDung);
	
	void thietLapPhienLamViec(long idDonViLamViec, long idKhucVucLamViec, long idThangLamViec);
}
