package vn.vnpt.camau.toolbc.sgd.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;
import vn.vnpt.camau.toolbc.sgd.web.entity.DmPhongGd;

public class DmTruongDto implements Comparable<DmTruongDto> {

	@Getter
	@Setter
	private Integer idTruong;
	@Getter
	@Setter
	private String tenTruong;
	@Getter
	@Setter
	private int laTruongCl;
	@Getter
	@Setter
	private int loaiTruong;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	@Mapping("dmPhongGd.tenPhongGd")
	private String tenPhongGd;
	
	@Override
	public int compareTo(DmTruongDto object) {
		return this.tenTruong.compareTo(object.getTenTruong());
	}
	
}
