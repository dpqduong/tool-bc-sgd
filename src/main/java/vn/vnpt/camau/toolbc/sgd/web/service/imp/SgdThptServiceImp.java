package vn.vnpt.camau.toolbc.sgd.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.toolbc.sgd.web.converter.SgdThptConverter;
import vn.vnpt.camau.toolbc.sgd.web.dto.DoiTuongDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.NguoiDungDto;
import vn.vnpt.camau.toolbc.sgd.web.dto.SgdThptDto;
import vn.vnpt.camau.toolbc.sgd.web.entity.DoiTuong;
import vn.vnpt.camau.toolbc.sgd.web.entity.KhuVuc;
import vn.vnpt.camau.toolbc.sgd.web.entity.NguoiDung;
import vn.vnpt.camau.toolbc.sgd.web.entity.SgdThpt;
import vn.vnpt.camau.toolbc.sgd.web.repository.SgdThptRepository;
import vn.vnpt.camau.toolbc.sgd.web.service.SgdThptService;

@Service
public class SgdThptServiceImp implements SgdThptService {

	@Autowired
	private SgdThptConverter converter;
	@Autowired
	private SgdThptRepository repository;

	@Override
	public List<SgdThptDto> layDanhSach(int idDonVi, int namHoc, int thang) {
		List<SgdThpt> soLieuCanLay;
		if(idDonVi == 999) {
			soLieuCanLay = repository.findByNamAndThangAndGuiBcSgdOrderByThangAsc(namHoc, thang, 1);
		}else {
			soLieuCanLay = repository.findByIdTruongAndNamOrderByThangAsc(idDonVi, namHoc);
		}
		List<SgdThptDto> soLieuDaChuyenDoi = soLieuCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return soLieuDaChuyenDoi;
	}

	@Override
	public SgdThptDto luuSoLieu(SgdThptDto dto) throws Exception {
		SgdThpt soLieuCanLuu;
		if (dto.getId() == null) {
			soLieuCanLuu = themSoLieu(dto);
		} else {
			soLieuCanLuu = capNhatSoLieu(dto);
		}
		SgdThptDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLuu);
		return soLieuDaChuyenDoi;
	}

	private SgdThpt themSoLieu(SgdThptDto dto) throws Exception {
		if (daNhapSoLieuThang(dto.getThang(), dto.getNam(), dto.getIdTruong())) {
			throw new Exception("Số liệu đã tồn tại.");
		}
		SgdThpt soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu số liệu.");
		}
	}
	
	private boolean daNhapSoLieuThang(int thang, int nam, int idDonVi) {
		SgdThpt canKiemTra = repository.findByThangAndNamAndIdTruong(thang, nam, idDonVi);
		if (canKiemTra == null) {
			return false;
		}
		return true;
	}
	
	private SgdThpt capNhatSoLieu(SgdThptDto dto) throws Exception {
		SgdThpt canKiemTra = kiemTraSoLieu(dto.getId());
		SgdThpt soLieuCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soLieuCanLuu);
			return soLieuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể số liệu.");
		}
	}
	
	private SgdThpt kiemTraSoLieu(int id) throws Exception {
		SgdThpt canKiemTra = repository.findOne(id);
		if (canKiemTra == null) {
			throw new Exception("Không tìm thấy số liệu.");
		}
		return canKiemTra;
	}
	
	private SgdThpt kiemTraSoLieuDaGui(int id) throws Exception {
		SgdThpt canKiemTra = repository.findByIdAndGuiBcSgd(id, 1);
		if (canKiemTra != null) {
			throw new Exception("Số liệu đã được gửi Sở giáo dục không thể xoá.");
		}
		return canKiemTra;
	}

	@Override
	public SgdThptDto laySoLieu(int id) {
		SgdThpt soLieuCanLay = repository.findById(id);
		SgdThptDto soLieuDaChuyenDoi = converter.convertToDto(soLieuCanLay);
		return soLieuDaChuyenDoi;
	}

	@Override
	public void xoaSoLieu(int id) throws Exception {
		kiemTraSoLieu(id);
		kiemTraSoLieuDaGui(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa số liệu.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void baoCaoSgd(int id, int tt) throws Exception {
		SgdThpt soLieuCanCapNhat = kiemTraSoLieu(id);
		int daGuiBc = 1;
		soLieuCanCapNhat.setGuiBcSgd(tt);
		try {
			repository.save(soLieuCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể gửi hoặc trả báo cáo số liệu cho Sở giáo dục.");
		}
	}
}
