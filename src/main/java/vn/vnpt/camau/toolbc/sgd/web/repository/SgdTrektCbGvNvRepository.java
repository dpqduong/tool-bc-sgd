package vn.vnpt.camau.toolbc.sgd.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.toolbc.sgd.web.entity.SgdTrektCbGvNv;

public interface SgdTrektCbGvNvRepository extends CrudRepository<SgdTrektCbGvNv, Integer> {

	Page<SgdTrektCbGvNv> findByNamAndThangOrderByThangAsc(int namHoc, int thang, Pageable pageable);
	Page<SgdTrektCbGvNv> findByNamAndThangAndGuiBcSgdOrderByThangAsc(int namHoc, int thang, int tt, Pageable pageable);
	SgdTrektCbGvNv findByThangAndNam(int thang, int nam);
	SgdTrektCbGvNv findById(int id);
	SgdTrektCbGvNv findByIdAndGuiBcSgd(int id, int flag);
}
