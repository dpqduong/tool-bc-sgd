var pref_url="/nhap-so-lieu-th";

const pgd = [
    { id: '1', name: 'PGD&ĐT TP. Cà Mau' }, 
    { id: '2', name: 'PGD&ĐT Thới Bình' }, 
    { id: '3', name: 'PGD&ĐT U Minh' }, 
    { id: '4', name: 'PGD&ĐT Trần Văn Thời' }, 
    { id: '5', name: 'PGD&ĐT Cái Nước' }, 
    { id: '6', name: 'PGD&ĐT  Phú Tân' },
    { id: '7', name: 'PGD&ĐT Đầm Dơi' },
    { id: '8', name: 'PGD&ĐT Năm Căn' },
    { id: '9', name: 'PGD&ĐT Ngọc Hiển' }
];

function getTenPgd(id){
	var tenPgd =  pgd.filter(function(item) {
		return item.id == id;
	});
	var result = tenPgd.map(({ name }) => name)[0];
	return result == undefined ?"":  result;
}

function getBtnThaoTac(idDonVi, tt, id){
	var btn = "";
	if(idDonVi == 10 || idDonVi == 11){
		var btn = tt === 1 ? "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-2' rid="+id+" title='Trả báo cáo Phòng GD'><i class='fa fa-reply-all'></i></a>" : "<td></td>";
	}else{
		var btn = tt === 1 ? "<td></td>" : "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-1' rid="+id+" title='Gửi báo cáo Sở GD'><i class='fa fa-reply-all'></i></a>&nbsp;&nbsp;<a href='#' class='row-edit-1' rid="+id+" title='Sửa' style=''><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='row-del-1 text-danger' rid="+id+" title='Xóa'><i class='fa fa-trash-o'></i></a></td>";
	}
	return btn;
}

function removeNull(val){
	return val == null ? "" : val; 
}

$(document).ready(function(){
	$('[data-toggle="push-menu"]').pushMenu('toggle');
	
	$('input').keyup(function(e)
            {
		if (/\D/g.test(this.value))
		{
		// Filter non-digits from input value.
		this.value = this.value.replace(/\D/g, '');
		}
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#cmb-nam-hoc-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#cmb-thang-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#showBox').click(function(){
		$('#box-ds').boxWidget('toggle');
	});
	
	//So lieu lop
	jQuery('#lopK1').on('input', function() {
		var lopK1 = layGiaTriInput("lopK1");
	    var lopK2 = layGiaTriInput("lopK2");
	    var lopK3 = layGiaTriInput("lopK3");
	    var lopK4 = layGiaTriInput("lopK4");
	    var lopK5 = layGiaTriInput("lopK5");
	    var kq = lopK1 + lopK2 + lopK3 + lopK4 + lopK5;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK2').on('input', function() {
		var lopK1 = layGiaTriInput("lopK1");
	    var lopK2 = layGiaTriInput("lopK2");
	    var lopK3 = layGiaTriInput("lopK3");
	    var lopK4 = layGiaTriInput("lopK4");
	    var lopK5 = layGiaTriInput("lopK5");
	    var kq = lopK1 + lopK2 + lopK3 + lopK4 + lopK5;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK3').on('input', function() {
		var lopK1 = layGiaTriInput("lopK1");
	    var lopK2 = layGiaTriInput("lopK2");
	    var lopK3 = layGiaTriInput("lopK3");
	    var lopK4 = layGiaTriInput("lopK4");
	    var lopK5 = layGiaTriInput("lopK5");
	    var kq = lopK1 + lopK2 + lopK3 + lopK4 + lopK5;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK4').on('input', function() {
		var lopK1 = layGiaTriInput("lopK1");
	    var lopK2 = layGiaTriInput("lopK2");
	    var lopK3 = layGiaTriInput("lopK3");
	    var lopK4 = layGiaTriInput("lopK4");
	    var lopK5 = layGiaTriInput("lopK5");
	    var kq = lopK1 + lopK2 + lopK3 + lopK4 + lopK5;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK5').on('input', function() {
		var lopK1 = layGiaTriInput("lopK1");
	    var lopK2 = layGiaTriInput("lopK2");
	    var lopK3 = layGiaTriInput("lopK3");
	    var lopK4 = layGiaTriInput("lopK4");
	    var lopK5 = layGiaTriInput("lopK5");
	    var kq = lopK1 + lopK2 + lopK3 + lopK4 + lopK5;
	    $('#tsLop').val(kq);
	});
	
	//So lieu hoc sinh
	jQuery('#hsK1').on('input', function() {
		var hsK1 = layGiaTriInput("hsK1");
	    var hsK2 = layGiaTriInput("hsK2");
	    var hsK3 = layGiaTriInput("hsK3");
	    var hsK4 = layGiaTriInput("hsK4");
	    var hsK5 = layGiaTriInput("hsK5");
	    var kq = hsK1 + hsK2 + hsK3 + hsK4 + hsK5;
	    $('#tsHs').val(kq);
	});
	
	jQuery('#hsK2').on('input', function() {
		var hsK1 = layGiaTriInput("hsK1");
	    var hsK2 = layGiaTriInput("hsK2");
	    var hsK3 = layGiaTriInput("hsK3");
	    var hsK4 = layGiaTriInput("hsK4");
	    var hsK5 = layGiaTriInput("hsK5");
	    var kq = hsK1 + hsK2 + hsK3 + hsK4 + hsK5;
	    $('#tsHs').val(kq);
	});
	
	jQuery('#hsK3').on('input', function() {
		var hsK1 = layGiaTriInput("hsK1");
	    var hsK2 = layGiaTriInput("hsK2");
	    var hsK3 = layGiaTriInput("hsK3");
	    var hsK4 = layGiaTriInput("hsK4");
	    var hsK5 = layGiaTriInput("hsK5");
	    var kq = hsK1 + hsK2 + hsK3 + hsK4 + hsK5;
	    $('#tsHs').val(kq);
	});
	
	jQuery('#hsK4').on('input', function() {
		var hsK1 = layGiaTriInput("hsK1");
	    var hsK2 = layGiaTriInput("hsK2");
	    var hsK3 = layGiaTriInput("hsK3");
	    var hsK4 = layGiaTriInput("hsK4");
	    var hsK5 = layGiaTriInput("hsK5");
	    var kq = hsK1 + hsK2 + hsK3 + hsK4 + hsK5;
	    $('#tsHs').val(kq);
	});
	
	jQuery('#hsK5').on('input', function() {
		var hsK1 = layGiaTriInput("hsK1");
	    var hsK2 = layGiaTriInput("hsK2");
	    var hsK3 = layGiaTriInput("hsK3");
	    var hsK4 = layGiaTriInput("hsK4");
	    var hsK5 = layGiaTriInput("hsK5");
	    var kq = hsK1 + hsK2 + hsK3 + hsK4 + hsK5;
	    $('#tsHs').val(kq);
	});
	
	//So lieu CBQL - GV - NV
	jQuery('#cbql').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv = layGiaTriInput("gv");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#gv').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv = layGiaTriInput("gv");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#nv').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv = layGiaTriInput("gv");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	//CSVC
	jQuery('#phKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phBanKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phTam').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvBanKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvTam').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvNhaCongVu').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	
	//bổ sung ngày 08/10/2020
	jQuery('#lopHaiBuoiK1').on('input', function() {
		tinhLopHaiBuoi();
	});
	
	jQuery('#lopHaiBuoiK2').on('input', function() {
		tinhLopHaiBuoi();
	});
	
	jQuery('#lopHaiBuoiK3').on('input', function() {
		tinhLopHaiBuoi();
	});
	
	jQuery('#lopHaiBuoiK4').on('input', function() {
		tinhLopHaiBuoi();
	});
	
	jQuery('#lopHaiBuoiK5').on('input', function() {
		tinhLopHaiBuoi();
	});
	
	jQuery('#lopBanTruK1').on('input', function() {
		tinhLopBanTru();
	});
	
	jQuery('#lopBanTruK2').on('input', function() {
		tinhLopBanTru();
	});
	
	jQuery('#lopBanTruK3').on('input', function() {
		tinhLopBanTru();
	});
	
	jQuery('#lopBanTruK4').on('input', function() {
		tinhLopBanTru();
	});
	
	jQuery('#lopBanTruK5').on('input', function() {
		tinhLopBanTru();
	});
	
	jQuery('#hsHaiBuoiK1').on('input', function() {
		tinhHsHaiBuoi();
	});
	
	jQuery('#hsHaiBuoiK2').on('input', function() {
		tinhHsHaiBuoi();
	});
	
	jQuery('#hsHaiBuoiK3').on('input', function() {
		tinhHsHaiBuoi();
	});
	
	jQuery('#hsHaiBuoiK4').on('input', function() {
		tinhHsHaiBuoi();
	});
	
	jQuery('#hsHaiBuoiK5').on('input', function() {
		tinhHsHaiBuoi();
	});
	
	jQuery('#hsBanTruK1').on('input', function() {
		tinhHsBanTru();
	});
	
	jQuery('#hsBanTruK2').on('input', function() {
		tinhHsBanTru();
	});
	
	jQuery('#hsBanTruK3').on('input', function() {
		tinhHsBanTru();
	});
	
	jQuery('#hsBanTruK4').on('input', function() {
		tinhHsBanTru();
	});
	
	jQuery('#hsBanTruK5').on('input', function() {
		tinhHsBanTru();
	});
	
	initForm();
});

function tinhLopHaiBuoi(){
	 var lopHaiBuoiK1 = layGiaTriInput("lopHaiBuoiK1");
	 var lopHaiBuoiK2 = layGiaTriInput("lopHaiBuoiK2");
	 var lopHaiBuoiK3 = layGiaTriInput("lopHaiBuoiK3");
	 var lopHaiBuoiK4 = layGiaTriInput("lopHaiBuoiK4");
	 var lopHaiBuoiK5 = layGiaTriInput("lopHaiBuoiK5");
	    
	 var kq = lopHaiBuoiK1+ lopHaiBuoiK2+lopHaiBuoiK3+lopHaiBuoiK4+lopHaiBuoiK5;
	 $('#lopHaiBuoi').val(kq);
}

function tinhLopBanTru(){
	 var lopBanTruK1 = layGiaTriInput("lopBanTruK1");
	 var lopBanTruK2 = layGiaTriInput("lopBanTruK2");
	 var lopBanTruK3 = layGiaTriInput("lopBanTruK3");
	 var lopBanTruK4 = layGiaTriInput("lopBanTruK4");
	 var lopBanTruK5 = layGiaTriInput("lopBanTruK5");
	    
	 var kq = lopBanTruK1+ lopBanTruK2+lopBanTruK3+lopBanTruK4+lopBanTruK5;
	 $('#lopBanTru').val(kq);
}

function tinhHsHaiBuoi(){
	 var hsHaiBuoiK1 = layGiaTriInput("hsHaiBuoiK1");
	 var hsHaiBuoiK2 = layGiaTriInput("hsHaiBuoiK2");
	 var hsHaiBuoiK3 = layGiaTriInput("hsHaiBuoiK3");
	 var hsHaiBuoiK4 = layGiaTriInput("hsHaiBuoiK4");
	 var hsHaiBuoiK5 = layGiaTriInput("hsHaiBuoiK5");
	    
	 var kq = hsHaiBuoiK1+ hsHaiBuoiK2+hsHaiBuoiK3+hsHaiBuoiK4+hsHaiBuoiK5;
	 $('#hsHaiBuoi').val(kq);
}

function tinhHsBanTru(){
	 var hsBanTruK1 = layGiaTriInput("hsBanTruK1");
	 var hsBanTruK2 = layGiaTriInput("hsBanTruK2");
	 var hsBanTruK3 = layGiaTriInput("hsBanTruK3");
	 var hsBanTruK4 = layGiaTriInput("hsBanTruK4");
	 var hsBanTruK5 = layGiaTriInput("hsBanTruK5");
	    
	 var kq = hsBanTruK1+ hsBanTruK2+hsBanTruK3+hsBanTruK4+hsBanTruK5;
	 $('#hsBanTru').val(kq);
}


function layGiaTriInput(tenInput){
	return $('#'+tenInput+'').val() != "" ? parseInt($('#'+tenInput+'').val()) : 0;
}

function check(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			
			// Return today's date and time
			var currentTime = new Date();

			// returns the month (from 0 to 11)
			var month = currentTime.getMonth() + 1;

			// returns the year (four digits)
			var year = currentTime.getFullYear();
			
			var namhoc=$.map(data.namhoc,function(obj){
				obj.id=obj.giaTri;
				obj.text=obj.giaTri;
				return obj;
			});
			
			
			$('#cmb-nam-hoc,#cmb-nam-hoc-filter').select2({
				data: namhoc
			});
			$('#cmb-nam-hoc').val(year);
		    $('#cmb-nam-hoc').select2().trigger('change');
		    
		    $('#cmb-thoi-gian').val(month);
		    $('#cmb-thang-filter').val(month);
		    
			if($('#cmb-nam-hoc-filter').val() !=null){
				layDanhSach(0);
			}
			
			if($('#cmb-thang-filter').val() !=null){
				layDanhSach(0);
			}
			
			$('#guiBcSgd').val(0);
			
			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
				$('#cmb-thang-filter').css("display","true");
			}
			else{
				$('#cmb-thang-filter').css("display","none");
			}
			},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds > tbody > tr").remove();
			$("#ds-1 > tbody > tr").remove();
			$("#ds-2 > tbody > tr").remove();
			var tr = "";
			var tr2 = "";
			var tr3 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
				$.each(data.content, function(i,row){
						tr += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.diemLe) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsHs) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsCbGvNv) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.cbql) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gv) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.nv) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
		    })
		    
		     $.each(data.content, function(i,row){
				tr2 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongLamViec) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvNhaCongVu) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthTinHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthNgoaiNgu) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
		    })
		    
		    $.each(data.content, function(i,row){
				tr3 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truongHaiBuoi) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truongBanTru) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopHaiBuoi) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopBanTru) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopHaiBuoiK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopBanTruK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopHaiBuoiK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopBanTruK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopHaiBuoiK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopBanTruK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopHaiBuoiK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopBanTruK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopHaiBuoiK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopBanTruK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsHaiBuoi) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsBanTru) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsHaiBuoiK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsBanTruK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsHaiBuoiK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsBanTruK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsHaiBuoiK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsBanTruK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsHaiBuoiK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsBanTruK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsHaiBuoiK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsBanTruK5) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
		    })
		    }
		    else{
				tr = "<tr><td colspan='22' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr2 = "<tr><td colspan='16' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr3 = "<tr><td colspan='30' style='text-align: center'>Không có dữ liệu</td></tr>";
			}

		    $('#ds > tbody:last-child').append(tr);
		    $('#ds-1 > tbody:last-child').append(tr2);
		    $('#ds-2 > tbody:last-child').append(tr3);
		    
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				$('#box-ds').boxWidget('expand');
				layChiTiet($(this).attr('rid'));
			});

			$('.row-edit-2').click(function(e){
				showError("Thông báo","Số liệu đã được gửi Sở giáo dục không được chỉnh sửa!");
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
			
			$('.row-reset-pwd-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCao('+$(this).attr('rid')+', 1)' );
				
			});
			
			$('.row-reset-pwd-2').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Phòng giáo dục?', 'baoCao('+$(this).attr('rid')+', 0)');
				
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				$('#box-ds').boxWidget('collapse');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
	$('#id').val('');
	$('#cmb-nam-hoc').val(year).trigger('change');
	$('#cmb-thoi-gian').val(month);
	$('#truong').val('');
	$('#lopK1').val('');
	$('#lopK2').val('');
	$('#lopK3').val('');
	$('#lopK4').val('');
	$('#lopK5').val('');
	$('#tsLop').val('');
	$('#hsK1').val('');
	$('#hsK2').val('');
	$('#hsK3').val('');
	$('#hsK4').val('');
	$('#hsK5').val('');
	$('#tsHs').val('');
	$('#cbql').val('');
	$('#gv').val('');
	$('#nv').val('');
	$('#tsCbGvNv').val('');
	$('#guiBcSgd').val(0);
	$('#tsPhong').val('');
	$('#tsPhongHoc').val('');
	$('#phKienCo').val('');
	$('#phBanKienCo').val('');
	$('#phTam').val('');
	$('#tsPhongLamViec').val('');
	$('#plvKienCo').val('');
	$('#plvBanKienCo').val('');
	$('#plvTam').val('');
	$('#plvNhaCongVu').val('');
	$('#pthTinHoc').val('');
	$('#pthNgoaiNgu').val('');
	$('#diemLe').val('');
	$('#truongHaiBuoi').val('');
	$('#truongBanTru').val('');
	$('#lopHaiBuoi').val('');
	$('#lopBanTru').val('');
	$('#lopHaiBuoiK1').val('');
	$('#lopHaiBuoiK2').val('');
	$('#lopHaiBuoiK3').val('');
	$('#lopHaiBuoiK4').val('');
	$('#lopHaiBuoiK5').val('');
	$('#lopBanTruK1').val('');
	$('#lopBanTruK2').val('');
	$('#lopBanTruK3').val('');
	$('#lopBanTruK4').val('');
	$('#lopBanTruK5').val('');
	$('#hsHaiBuoi').val('');
	$('#hsBanTru').val('');
	$('#hsHaiBuoiK1').val('');
	$('#hsHaiBuoiK2').val('');
	$('#hsHaiBuoiK3').val('');
	$('#hsHaiBuoiK4').val('');
	$('#hsHaiBuoiK5').val('');
	$('#hsBanTruK1').val('');
	$('#hsBanTruK2').val('');
	$('#hsBanTruK3').val('');
	$('#hsBanTruK4').val('');
	$('#hsBanTruK5').val('');
	$('#box-ds').boxWidget('collapse');
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id').val(data.id);
			$('#cmb-nam-hoc').val(data.nam).trigger('change');
			$('#cmb-thoi-gian').val(data.thang);
			$('#truong').val(data.truong);
			$('#lopK1').val(data.lopK1);
			$('#lopK2').val(data.lopK2);
			$('#lopK3').val(data.lopK3);
			$('#lopK4').val(data.lopK4);
			$('#lopK5').val(data.lopK5);
			$('#tsLop').val(data.tsLop);
			$('#hsK1').val(data.hsK1);
			$('#hsK2').val(data.hsK2);
			$('#hsK3').val(data.hsK3);
			$('#hsK4').val(data.hsK4);
			$('#hsK5').val(data.hsK5);
			$('#tsHs').val(data.tsHs);
			$('#cbql').val(data.cbql);
			$('#gv').val(data.gv);
			$('#nv').val(data.nv);
			$('#tsCbGvNv').val(data.tsCbGvNv);
			$('#guiBcSgd').val(data.guiBcSgd);
			$('#tsPhong').val(data.tsPhong);
			$('#tsPhongHoc').val(data.tsPhongHoc);
			$('#phKienCo').val(data.phKienCo);
			$('#phBanKienCo').val(data.phBanKienCo);
			$('#phTam').val(data.phTam);
			$('#tsPhongLamViec').val(data.tsPhongLamViec);
			$('#plvKienCo').val(data.plvKienCo);
			$('#plvBanKienCo').val(data.plvBanKienCo);
			$('#plvTam').val(data.plvTam);
			$('#plvNhaCongVu').val(data.plvNhaCongVu);
			$('#pthTinHoc').val(data.pthTinHoc);
			$('#pthNgoaiNgu').val(data.pthNgoaiNgu);
			$('#diemLe').val(data.diemLe);
			
			$('#truongHaiBuoi').val(data.truongHaiBuoi);
			$('#truongBanTru').val(data.truongBanTru);
			$('#lopHaiBuoi').val(data.lopHaiBuoi);
			$('#lopBanTru').val(data.lopBanTru);
			$('#lopHaiBuoiK1').val(data.lopHaiBuoiK1);
			$('#lopHaiBuoiK2').val(data.lopHaiBuoiK2);
			$('#lopHaiBuoiK3').val(data.lopHaiBuoiK3);
			$('#lopHaiBuoiK4').val(data.lopHaiBuoiK4);
			$('#lopHaiBuoiK5').val(data.lopHaiBuoiK5);
			$('#lopBanTruK1').val(data.lopBanTruK1);
			$('#lopBanTruK2').val(data.lopBanTruK2);
			$('#lopBanTruK3').val(data.lopBanTruK3);
			$('#lopBanTruK4').val(data.lopBanTruK4);
			$('#lopBanTruK5').val(data.lopBanTruK5);
			$('#hsHaiBuoi').val(data.hsHaiBuoi);
			$('#hsBanTru').val(data.hsBanTru);
			$('#hsHaiBuoiK1').val(data.hsHaiBuoiK1);
			$('#hsHaiBuoiK2').val(data.hsHaiBuoiK2);
			$('#hsHaiBuoiK3').val(data.hsHaiBuoiK3);
			$('#hsHaiBuoiK4').val(data.hsHaiBuoiK4);
			$('#hsHaiBuoiK5').val(data.hsHaiBuoiK5);
			$('#hsBanTruK1').val(data.hsBanTruK1);
			$('#hsBanTruK2').val(data.hsBanTruK2);
			$('#hsBanTruK3').val(data.hsBanTruK3);
			$('#hsBanTruK4').val(data.hsBanTruK4);
			$('#hsBanTruK5').val(data.hsBanTruK5);
			
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCao(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

