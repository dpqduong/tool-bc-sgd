var pref_url="/gia-nuoc";
var selectedPhuong=[];

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenGiaNuoc',title:'Tên giá nước'},
			{name:'ngayApDung',title:'Ngày áp dụng',type:'interval'},
			{name:'phanTram',title:'%',type:'check',css:{'text-align':'center','width':'40px'}},
			{name:'tenDoiTuong',title:'Đối tượng'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-1" rid="'+obj.idGiaNuoc+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idGiaNuoc+'" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'60px'}}
		],
		dateformat:'dd/mm/yyyy',
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#grid-detail').jdGrid({
		columns:[
			{name:'thuTu',title:'TT',css:{'text-align':'center','width':'40px'}},
			{name:'tieuThuTu',title:'Từ'},
			{name:'tieuThuDen',title:'Đến'},
			{name:'mucGia',title:'Đơn giá',format:true},
			{name:'col5',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-2" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-2 text-danger" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'60px'}}
		],
		height:'100px'
	});
	
	$('#cmb-huyen-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#cmb-huyen').change(function(){
		layDsPhuong();
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#cmb-phuong').multiselect({
		includeSelectAllOption: true,
        selectAllText: 'Chọn tất cả',
        maxHeight: 200,
        buttonWidth: '100%',
        nonSelectedText: 'Chọn phường',
        nSelectedText: ' phường được chọn',
        allSelectedText: 'Đã chọn tất cả các phường',
        buttonClass: 'btn btn-default btn-flat'
	});
	
	$('#chk-phantram').change(function() {
		$('#grid-detail').empty();
		$('#grid-detail').data('jdgrid').clearData();
	    if(this.checked) {
	        $('.co-so').addClass('hide');
	        $('.phan-tram').removeClass('hide');
	        
	        $('#grid-detail').jdGrid({
	    		columns:[
	    			{name:'thuTu',title:'TT',css:{'text-align':'center','width':'40px'}},
	    			{name:'phanTramTieuThu',title:'Phần trăm tiêu thụ'},
	    			{name:'mucGia',title:'Đơn giá'},
	    			{name:'col5',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-2" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-2 text-danger" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'60px'}}
	    		],
	    		height:'100px'
	    	});
	    }else{
	    	$('.phan-tram').addClass('hide');
	        $('.co-so').removeClass('hide');
	        
	        $('#grid-detail').jdGrid({
	    		columns:[
	    			{name:'thuTu',title:'TT',css:{'text-align':'center','width':'40px'}},
	    			{name:'tieuThuTu',title:'Từ'},
	    			{name:'tieuThuDen',title:'Đến'},
	    			{name:'mucGia',title:'Đơn giá'},
	    			{name:'col5',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-2" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-2 text-danger" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'60px'}}
	    		],
	    		height:'100px'
	    	});
	    }
	});
	
	$('.input-mask').inputmask();
	
	$('.chon-ngay').datepicker({
		autoclose: true,
		todayHighlight:true,
		format:'dd/mm/yyyy',
		zIndexOffset:10000
    }).datepicker('setDate', new Date());
	
	$('#btn-add').click(function(){
		if($('#chk-phantram').prop('checked')){
			var pt=$('#txt-phantram').val();
			var dg=$('#txt-dongia').val();
			if(pt===''||isNaN(pt)){
				showError('Thông báo','Vui lòng nhập phần trăm tiêu thụ!');
				return;
			}
			if(dg===''||isNaN(dg)){
				showError('Thông báo','Vui lòng nhập đơn giá!');
				return;
			}
		}else{
			var tu=$('#txt-tu').val();
			var den=$('#txt-den').val();
			var dg=$('#txt-dongia').val();
			if(tu===''||isNaN(tu)){
				showError('Thông báo','Vui lòng nhập chỉ số từ!');
				return;
			}
			/*if(den===''||isNaN(den)){
				showError('Thông báo','Vui lòng nhập chỉ số đến!');
				return;
			}*/
			if(dg===''||isNaN(dg)){
				showError('Thông báo','Vui lòng nhập đơn giá!');
				return;
			}
			if(Number(tu)>=Number(den) && den!=''){
				showError('Thông báo','Chỉ số từ phải nhỏ hơn chỉ số đến!');
				return;
			}
		}
		
		var obj={'idChiTietGiaNuoc':-($('.row-edit-2').length),'thuTu':$('.row-edit-2').length+1,'tieuThuTu':$('#txt-tu').val(),'tieuThuDen':$('#txt-den').val(),'mucGia':$('#txt-dongia').val(),'phanTramTieuThu':$('#txt-phantram').val()};
		$('#grid-detail').data('jdgrid').addRow(obj);
		
		$('#txt-tu').val('');
		$('#txt-den').val('');
		$('#txt-dongia').val('');
		$('#txt-phantram').val('');
		if($('#txt-tu').is(':visible')){
			$('#txt-tu').focus();
		}else{
			$('#txt-phantram').focus();
		}
		
		regEvent();
	});
	
	$('#btn-clr-add').click(function(){
		$('#txt-tu').val('');
		$('#txt-den').val('');
		$('#txt-dongia').val('');
		$('#txt-phantram').val('');
		if($('#txt-tu').is(':visible')){
			$('#txt-tu').focus();
		}else{
			$('#txt-phantram').focus();
		}
		$('#txt-stt').val('');
	});
	
	initForm();
});

function regEvent(){
	$('.row-edit-2').off('click').on('click',function(e){
		e.preventDefault();
		var i=$('.row-edit-2').index($(this));
		var obj=$('#grid-detail').data('jdgrid').getData()[i];
		
		$('#txt-tu').val(obj.tieuThuTu);
		$('#txt-den').val(obj.tieuThuDen);
		$('#txt-dongia').val(obj.mucGia);
		$('#txt-phantram').val(obj.phanTramTieuThu);
		$('#txt-stt').val(i);
	});
	
	$('.row-del-2').off('click').on('click',function(e){
		e.preventDefault();
		showConfirm('Xác nhận','Bạn chắc muốn xóa?','xoact('+$('.row-del-2').index($(this))+')');
	});
}

function xoact(i){
	$('#grid-detail').data('jdgrid').removeRow(i);
	$.each($('#grid-detail').data('jdgrid').getData(),function(i,obj){
		$('#grid-detail').data('jdgrid').setCellValue(i,'thuTu',i+1);
	});
	regEvent();
}

function check(){
	if($('#txt-tenGiaNuoc').val()===''){
		showError('Thông báo','Vui lòng nhập tên giá nước!');
		return false;
	}
	if(!$('#txt-ngayApDung').inputmask('isComplete')){
		showError('Thông báo','Vui lòng nhập ngày áp dụng!');
		return false;
	}
	if($('#cmb-doituong').val()==null){
		showError('Thông báo','Vui lòng chọn đối tượng!');
		return false;
	}
	if($('#cmb-huyen').val()==null){
		showError('Thông báo','Vui lòng chọn huyện!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			var huyens=$.map(data.huyens,function(obj){
				obj.id=obj.idHuyen;
				obj.text=obj.tenHuyen;
				return obj;
			});
			
			$('#cmb-huyen-filter,#cmb-huyen').select2({
				data: huyens
			});
			
			var doiTuongs=$.map(data.doiTuongs,function(obj){
				obj.id=obj.idDoiTuong;
				obj.text=obj.tenDoiTuong;
				return obj;
			});
			
			$('#cmb-doituong').select2({
				data: doiTuongs
			});
			
			if($('#cmb-huyen-filter').val()!=null){
				layDanhSach(0);
				layDsPhuong();
			}
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idHuyen:$('#cmb-huyen-filter').val(),thongTinCanTim:$('#txt-keyword').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
			
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	var data=new FormData($('#frm-1')[0]);
	data.append('ct',JSON.stringify($('#grid-detail').data('jdgrid').getData()));
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:data,
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	$('#txt-tenGiaNuoc').val('');
	$('#txt-ngayApDung').datepicker('setDate', new Date());
	$('#txt-idGiaNuoc').val('');
	$('#grid-detail').data('jdgrid').clearData();
	selectedPhuong=[];
	$('#cmb-phuong').multiselect('deselectAll', false);
	$('#cmb-phuong').multiselect('updateButtonText');
	$('#chk-phantram').removeAttr('onclick');
}


function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#chk-phantram').prop('checked',data.phanTram).attr('onclick','return false').change();
			$('#txt-tenGiaNuoc').val(data.tenGiaNuoc);
			$('#txt-ngayApDung').datepicker('setDate', new Date(data.ngayApDung));
			$('#cmb-doituong').val(data.idDoiTuong).trigger('change');
			$('#cmb-huyen').val(data.idHuyen).trigger('change');
			$('#txt-idGiaNuoc').val(data.idGiaNuoc);
			selectedPhuong=[];
			$.each(data.duongDtos,function(i,obj){
				selectedPhuong.push(obj.idDuong);
			});
			$('#grid-detail').data('jdgrid').fillData(data.chiTietGiaNuocDtos);
			
			regEvent();
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
			$('#box-ds').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDsPhuong(){
	$.ajax({
		url:pref_url+'/lay-ds-phuong',
		method:'post',
		dataType:'json',
		data:{id:$('#cmb-huyen').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-phuong option').remove();
			$.each(data, function(j, item) {
			    var selected=false;
			    for(var i=0;i<selectedPhuong.length;i++){
			    	if(selectedPhuong[i]==item.idDuong){
			    		selected=true;
			    		break;
			    	}
			    }
				$('#cmb-phuong').append($('<option>', { 
			        value: item.idDuong,
			        text : item.tenDuong,
			        selected:selected
			    }));
			});
			$('#cmb-phuong').multiselect('rebuild');
		},
		error:function(){
			showError('Thông báo','Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}