var pref_url="/xuat-bao-cao";

$(document).ready(function(){
	layDsNamHoc();
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
    
	$("#cmb-thang").val(month);
	
	var idDonViSS = $('#idDonVi').val();
	if(idDonViSS != 10 && idDonViSS != 11){
		$("#cmb-maubc option[value='3']").remove();
		$("#cmb-maubc option[value='4']").remove();
	}else{
		$("#div-thang").hide();
	}
});

function layDsNamHoc(){
	$.ajax({
		url:pref_url+'/lay-ds-nam-hoc',
		method:'get',
		dataType:'json',
		success:function(data){
			var namhoc=$.map(data,function(obj){
					obj.id=obj.giaTri;
					obj.text=obj.giaTri;
				return obj;
			});
			$('#cmb-nam').select2({
				data: namhoc
			});
			// Return today's date and time
			var currentTime = new Date();

			// returns the month (from 0 to 11)
			var month = currentTime.getMonth() + 1;

			// returns the year (four digits)
			var year = currentTime.getFullYear();
			$('#cmb-nam').val(year);
		    $('#cmb-nam').select2().trigger('change');
		},
		error:function(){
			showError('Thông báo','Lấy danh sách năm học không thành công, vui lòng thử lại sau!');
		}
	});
}

$('#view').click(function() {
	var urlp = pref_url+'/xem-bao-cao?nam='+$('#cmb-nam').val()+'&maubc='+$('#cmb-maubc').val()+'&thang='+$('#cmb-thang').val()+'&donvi='+$('#idDonVi').val();
	$.ajax({
        url: urlp,
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
		},
        success: function(response){
			PDFObject.embed(response, "#pdfRenderer");
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
		}
    });
 });

    $('#xls').click(function() {
    	var urlp = pref_url+'/xuat-file-xls?nam='+$('#cmb-nam').val()+'&maubc='+$('#cmb-maubc').val()+'&thang='+$('#cmb-thang').val()+'&donvi='+$('#idDonVi').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
    $('#xlsx').click(function() {
    	var urlp = pref_url+'/xuat-file-xlsx?nam='+$('#cmb-nam').val()+'&maubc='+$('#cmb-maubc').val()+'&thang='+$('#cmb-thang').val()+'&donvi='+$('#idDonVi').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
	$('#cmb-maubc').on('change', function() {
		if($('#cmb-maubc').val()== 4){
			$("#div-thang").hide();
		}
		else{
			$("#div-thang").show();
		}
	});
    