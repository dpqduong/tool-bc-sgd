var pref_url="/nhap-so-lieu-trekt";

const pgd = [
    { id: '1', name: 'PGD&ĐT TP. Cà Mau' }, 
    { id: '2', name: 'PGD&ĐT Thới Bình' }, 
    { id: '3', name: 'PGD&ĐT U Minh' }, 
    { id: '4', name: 'PGD&ĐT Trần Văn Thời' }, 
    { id: '5', name: 'PGD&ĐT Cái Nước' }, 
    { id: '6', name: 'PGD&ĐT Đầm Dơi' },
    { id: '7', name: 'PGD&ĐT Đầm Dơi' },
    { id: '8', name: 'PGD&ĐT Năm Căn' },
    { id: '9', name: 'PGD&ĐT Ngọc Hiển' }
];

function getTenPgd(id){
	var tenPgd =  pgd.filter(function(item) {
		return item.id == id;
	});
	var result = tenPgd.map(({ name }) => name)[0];
	return result == undefined ?"":  result;
}

function getBtnThaoTac(idDonVi, tt, id, stt){
	var btn = "";
	if(idDonVi == 999){
		var btn = tt === 1 ? "<td style='text-align: center; width: auto;' id='btn_"+stt+"'><a href='#' class='row-reset-pwd-2-"+stt+"' rid="+id+" title='Trả báo cáo Phòng GD'><i class='fa fa-reply-all'></i></a>" : "<td></td>";
	}else{
		var btn = tt === 1 ? "<td></td>" : "<td style='text-align: center; width: auto;' id='btn_"+stt+"'><a href='#' class='row-reset-pwd-"+stt+"' rid="+id+" title='Gửi báo cáo Sở GD'><i class='fa fa-reply-all'></i></a>&nbsp;&nbsp;<a href='#' class='row-edit-"+stt+"' rid="+id+" title='Sửa' style=''><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='row-del-"+stt+" text-danger' rid="+id+" title='Xóa'><i class='fa fa-trash-o'></i></a></td>";
	}
	return btn;
}

function removeNull(val){
	return val == null ? "" : val; 
}

$(document).ready(function(){
	$('[data-toggle="push-menu"]').pushMenu('toggle');
	
	$('input').keyup(function(e)
            {
		if (/\D/g.test(this.value))
		{
		// Filter non-digits from input value.
		this.value = this.value.replace(/\D/g, '');
		}
	});

	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
			layDanhSachCsvc(p-1);
			layDanhSachKqht(p-1);
			layDanhSachCbGvNv(p-1);
		}
	});
	
	$('#cmb-nam-hoc-filter').change(function(){
		layDanhSach(0);
		layDanhSachCsvc(0);
		layDanhSachKqht(0);
		layDanhSachCbGvNv(0);
	});
	
	$('#cmb-thang-filter').change(function(){
		layDanhSach(0);
		layDanhSachCsvc(0);
		layDanhSachKqht(0);
		layDanhSachCbGvNv(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
		layDanhSachCsvc(0);
		layDanhSachKqht(0);
		layDanhSachCbGvNv(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
		layDanhSachCsvc(0);
		layDanhSachKqht(0);
		layDanhSachCbGvNv(0);
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			var nam = $('#cmb-nam-hoc').val();
			var thang = $('#cmb-thoi-gian').val();
			$('#nam_1').val(nam);
			$('#thang_1').val(thang);
			$('#nam_2').val(nam);
			$('#thang_2').val(thang);
			$('#nam_3').val(nam);
			$('#thang_3').val(thang);
			$('#nam_4').val(nam);
			$('#thang_4').val(thang);
			$('#nam_5').val(nam);
			$('#thang_5').val(thang);
			$('#nam_6').val(nam);
			$('#thang_6').val(thang);
			luu(1);
			luu(2);
			luu(3);
			luuKqht();
			luuCbGvNv();
			luuCsvc();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#showBox').click(function(){
		$('#box-ds').boxWidget('toggle');
	});
	
	//So lieu lop
	jQuery('#lopTk_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopDb_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK1_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK2_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK3_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK4_1').on('input', function() {
		layGiaTriLop(1);
	});

	jQuery('#lopK5_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK6_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK7_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK8_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK9_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK10_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK11_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopK12_1').on('input', function() {
		layGiaTriLop(1);
	});
	
	jQuery('#lopTk_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopDb_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK1_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK2_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK3_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK4_2').on('input', function() {
		layGiaTriLop(2);
	});

	jQuery('#lopK5_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK6_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK7_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK8_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK9_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK10_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK11_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopK12_2').on('input', function() {
		layGiaTriLop(2);
	});
	
	jQuery('#lopTk_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopDb_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK1_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK2_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK3_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK4_3').on('input', function() {
		layGiaTriLop(3);
	});

	jQuery('#lopK5_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK6_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK7_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK8_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK9_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK10_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK11_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	jQuery('#lopK12_3').on('input', function() {
		layGiaTriLop(3);
	});
	
	//Số liệu hs
	
	jQuery('#hsTk_1').on('input', function() {
		layGiaTriHs(1);
	});
	
	jQuery('#hsDb_1').on('input', function() {
		layGiaTriHs(1);
	});
	
	jQuery('#hsK1_1').on('input', function() {
		layGiaTriHs(1);
	});
	
	jQuery('#hsK2_1').on('input', function() {
		layGiaTriHs(1);
	});
	
	jQuery('#hsK3_1').on('input', function() {
		layGiaTriHs(1);
	});
	
	jQuery('#hsK4_1').on('input', function() {
		layGiaTriHs(1);
	});

	jQuery('#hsK5_1').on('input', function() {
		layGiaTriHs(1);
	});
	
	jQuery('#hsK6_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK7_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK8_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK9_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK10_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsK11_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsK12_1').on('input', function() {
		layGiaTriHs(1);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsTk_2').on('input', function() {
		layGiaTriHs(2);
	});
	
	jQuery('#hsDb_2').on('input', function() {
		layGiaTriHs(2);
	});
	
	jQuery('#hsK1_2').on('input', function() {
		layGiaTriHs(2);
	});
	
	jQuery('#hsK2_2').on('input', function() {
		layGiaTriHs(2);
	});
	
	jQuery('#hsK3_2').on('input', function() {
		layGiaTriHs(2);
	});
	
	jQuery('#hsK4_2').on('input', function() {
		layGiaTriHs(2);
	});

	jQuery('#hsK5_2').on('input', function() {
		layGiaTriHs(2);
	});
	
	jQuery('#hsK6_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK7_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK8_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK9_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK10_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsK11_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsK12_2').on('input', function() {
		layGiaTriHs(2);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsTk_3').on('input', function() {
		layGiaTriHs(3);
	});
	
	jQuery('#hsDb_3').on('input', function() {
		layGiaTriHs(3);
	});
	
	jQuery('#hsK1_3').on('input', function() {
		layGiaTriHs(3);
	});
	
	jQuery('#hsK2_3').on('input', function() {
		layGiaTriHs(3);
	});
	
	jQuery('#hsK3_3').on('input', function() {
		layGiaTriHs(3);
	});
	
	jQuery('#hsK4_3').on('input', function() {
		layGiaTriHs(3);
	});

	jQuery('#hsK5_3').on('input', function() {
		layGiaTriHs(3);
	});
	
	jQuery('#hsK6_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK7_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK8_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK9_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThcs();
	});
	
	jQuery('#hsK10_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsK11_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThpt();
	});
	
	jQuery('#hsK12_3').on('input', function() {
		layGiaTriHs(3);
		layGiaTriHsThpt();
	});
	//
	
	//Hanh Kiem'
	jQuery('#hkTotK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK6/hsK6)*100).toFixed(2);
	    $('#tlHkTotK6').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThcs').val(sum);
	    $('#tlHkTotThcs').val(kqt);
	    
	});
	
	jQuery('#hkKhaK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK6/hsK6)*100).toFixed(2);
	    $('#tlHkKhaK6').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThcs').val(sum);
	    $('#tlHkKhaThcs').val(kqt);
	});
	
	jQuery('#hkTbK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK6/hsK6)*100).toFixed(2);
	    $('#tlHkTbK6').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThcs').val(sum);
	    $('#tlHkTbThcs').val(kqt);
	});
	
	jQuery('#hkYeuK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK6/hsK6)*100).toFixed(2);
	    $('#tlHkYeuK6').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThcs').val(sum);
	    $('#tlHkYeuThcs').val(kqt);
	});
	
	jQuery('#hkTotK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK7/hsK7)*100).toFixed(2);
	    $('#tlHkTotK7').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThcs').val(sum);
	    $('#tlHkTotThcs').val(kqt);
	    
	});
	
	jQuery('#hkKhaK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK7/hsK7)*100).toFixed(2);
	    $('#tlHkKhaK7').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThcs').val(sum);
	    $('#tlHkKhaThcs').val(kqt);
	});
	
	jQuery('#hkTbK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK7/hsK7)*100).toFixed(2);
	    $('#tlHkTbK7').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThcs').val(sum);
	    $('#tlHkTbThcs').val(kqt);
	});
	
	jQuery('#hkYeuK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK7/hsK7)*100).toFixed(2);
	    $('#tlHkYeuK7').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThcs').val(sum);
	    $('#tlHkYeuThcs').val(kqt);
	});
	
	jQuery('#hkTotK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK8/hsK8)*100).toFixed(2);
	    $('#tlHkTotK8').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThcs').val(sum);
	    $('#tlHkTotThcs').val(kqt);
	});
	
	jQuery('#hkKhaK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK8/hsK8)*100).toFixed(2);
	    $('#tlHkKhaK8').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThcs').val(sum);
	    $('#tlHkKhaThcs').val(kqt);
	});
	
	jQuery('#hkTbK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK8/hsK8)*100).toFixed(2);
	    $('#tlHkTbK8').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThcs').val(sum);
	    $('#tlHkTbThcs').val(kqt);
	});
	
	jQuery('#hkYeuK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK8/hsK8)*100).toFixed(2);
	    $('#tlHkYeuK8').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThcs').val(sum);
	    $('#tlHkYeuThcs').val(kqt);
	});
	
	jQuery('#hkTotK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK9/hsK9)*100).toFixed(2);
	    $('#tlHkTotK9').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThcs').val(sum);
	    $('#tlHkTotThcs').val(kqt);
	});
	
	jQuery('#hkKhaK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK9/hsK9)*100).toFixed(2);
	    $('#tlHkKhaK9').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThcs').val(sum);
	    $('#tlHkKhaThcs').val(kqt);
	});
	
	jQuery('#hkTbK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK9/hsK9)*100).toFixed(2);
	    $('#tlHkTbK9').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThcs').val(sum);
	    $('#tlHkTbThcs').val(kqt);
	});
	
	jQuery('#hkYeuK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK9/hsK9)*100).toFixed(2);
	    $('#tlHkYeuK9').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThcs').val(sum);
	    $('#tlHkYeuThcs').val(kqt);
	});
	
	//Hoc luc
	jQuery('#hlGioiK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK6/hsK6)*100).toFixed(2);
	    $('#tlHlGioiK6').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThcs').val(sum);
	    $('#tlHlGioiThcs').val(kqt);
	    
	});
	
	jQuery('#hlKhaK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK6/hsK6)*100).toFixed(2);
	    $('#tlHlKhaK6').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThcs').val(sum);
	    $('#tlHlKhaThcs').val(kqt);
	});
	
	jQuery('#hlTbK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK6/hsK6)*100).toFixed(2);
	    $('#tlHlTbK6').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThcs').val(sum);
	    $('#tlHlTbThcs').val(kqt);
	});
	
	jQuery('#hlYeuK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK6/hsK6)*100).toFixed(2);
	    $('#tlHlYeuK6').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThcs').val(sum);
	    $('#tlHlYeuThcs').val(kqt);
	});
	
	jQuery('#hlKemK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK6/hsK6)*100).toFixed(2);
	    $('#tlHlKemK6').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThcs').val(sum);
	    $('#tlHlKemThcs').val(kqt);
	});
	
	jQuery('#hlGioiK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK7/hsK7)*100).toFixed(2);
	    $('#tlHlGioiK7').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThcs').val(sum);
	    $('#tlHlGioiThcs').val(kqt);
	    
	});
	
	jQuery('#hlKhaK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK7/hsK7)*100).toFixed(2);
	    $('#tlHlKhaK7').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThcs').val(sum);
	    $('#tlHlKhaThcs').val(kqt);
	});
	
	jQuery('#hlTbK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK7/hsK7)*100).toFixed(2);
	    $('#tlHlTbK7').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThcs').val(sum);
	    $('#tlHlTbThcs').val(kqt);
	});
	
	jQuery('#hlYeuK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK7/hsK7)*100).toFixed(2);
	    $('#tlHlYeuK7').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThcs').val(sum);
	    $('#tlHlYeuThcs').val(kqt);
	});
	
	jQuery('#hlKemK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK7/hsK7)*100).toFixed(2);
	    $('#tlHlKemK7').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThcs').val(sum);
	    $('#tlHlKemThcs').val(kqt);
	});
	
	jQuery('#hlGioiK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK8/hsK8)*100).toFixed(2);
	    $('#tlHlGioiK8').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThcs').val(sum);
	    $('#tlHlGioiThcs').val(kqt);
	});
	
	jQuery('#hlKhaK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK8/hsK8)*100).toFixed(2);
	    $('#tlHlKhaK8').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThcs').val(sum);
	    $('#tlHlKhaThcs').val(kqt);
	});
	
	jQuery('#hlTbK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK8/hsK8)*100).toFixed(2);
	    $('#tlHlTbK8').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThcs').val(sum);
	    $('#tlHlTbThcs').val(kqt);
	});
	
	jQuery('#hlYeuK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK8/hsK8)*100).toFixed(2);
	    $('#tlHlYeuK8').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThcs').val(sum);
	    $('#tlHlYeuThcs').val(kqt);
	});
	
	jQuery('#hlKemK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK8/hsK8)*100).toFixed(2);
	    $('#tlHlKemK8').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThcs').val(sum);
	    $('#tlHlKemThcs').val(kqt);
	});
	
	jQuery('#hlGioiK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK9/hsK9)*100).toFixed(2);
	    $('#tlHlGioiK9').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThcs').val(sum);
	    $('#tlHlGioiThcs').val(kqt);
	});
	
	jQuery('#hlKhaK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK9/hsK9)*100).toFixed(2);
	    $('#tlHlKhaK9').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThcs').val(sum);
	    $('#tlHlKhaThcs').val(kqt);
	});
	
	jQuery('#hlTbK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK9/hsK9)*100).toFixed(2);
	    $('#tlHlTbK9').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThcs').val(sum);
	    $('#tlHlTbThcs').val(kqt);
	});
	
	jQuery('#hlYeuK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK9/hsK9)*100).toFixed(2);
	    $('#tlHlYeuK9').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThcs').val(sum);
	    $('#tlHlYeuThcs').val(kqt);
	});
	
	jQuery('#hlKemK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHsThcs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK9/hsK9)*100).toFixed(2);
	    $('#tlHlKemK9').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThcs').val(sum);
	    $('#tlHlKemThcs').val(kqt);
	});
	
	//Hanh Kiem'
	jQuery('#hkTotK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTotK10 = layGiaTriInput("hkTotK10");
	    var hkTotK11 = layGiaTriInput("hkTotK11");
	    var hkTotK12 = layGiaTriInput("hkTotK12");
	    
	    var kq = ((hkTotK10/hsK10)*100).toFixed(2);
	    $('#tlHkTotK10').val(kq);
	    var sum = hkTotK10+hkTotK11+hkTotK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThpt').val(sum);
	    $('#tlHkTotThpt').val(kqt);
	    
	});
	
	jQuery('#hkKhaK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkKhaK10 = layGiaTriInput("hkKhaK10");
	    var hkKhaK11 = layGiaTriInput("hkKhaK11");
	    var hkKhaK12 = layGiaTriInput("hkKhaK12");

	    var kq = ((hkKhaK10/hsK10)*100).toFixed(2);
	    $('#tlHkKhaK10').val(kq);
	    var sum = hkKhaK10+hkKhaK11+hkKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThpt').val(sum);
	    $('#tlHkKhaThpt').val(kqt);
	});
	
	jQuery('#hkTbK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTbK10 = layGiaTriInput("hkTbK10");
	    var hkTbK11 = layGiaTriInput("hkTbK11");
	    var hkTbK12 = layGiaTriInput("hkTbK12");
	    
	    var kq = ((hkTbK10/hsK10)*100).toFixed(2);
	    $('#tlHkTbK10').val(kq);
	    var sum = hkTbK10+hkTbK11+hkTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThpt').val(sum);
	    $('#tlHkTbThpt').val(kqt);
	});
	
	jQuery('#hkYeuK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkYeuK10 = layGiaTriInput("hkYeuK10");
	    var hkYeuK11 = layGiaTriInput("hkYeuK11");
	    var hkYeuK12 = layGiaTriInput("hkYeuK12");

	    var kq = ((hkYeuK10/hsK10)*100).toFixed(2);
	    $('#tlHkYeuK10').val(kq);
	    var sum = hkYeuK10+hkYeuK11+hkYeuK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThpt').val(sum);
	    $('#tlHkYeuThpt').val(kqt);
	});
	
	jQuery('#hkTotK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTotK10 = layGiaTriInput("hkTotK10");
	    var hkTotK11 = layGiaTriInput("hkTotK11");
	    var hkTotK12 = layGiaTriInput("hkTotK12");

	    var kq = ((hkTotK11/hsK11)*100).toFixed(2);
	    $('#tlHkTotK11').val(kq);
	    var sum = hkTotK10+hkTotK11+hkTotK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThpt').val(sum);
	    $('#tlHkTotThpt').val(kqt);
	    
	});
	
	jQuery('#hkKhaK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkKhaK10 = layGiaTriInput("hkKhaK10");
	    var hkKhaK11 = layGiaTriInput("hkKhaK11");
	    var hkKhaK12 = layGiaTriInput("hkKhaK12");

	    var kq = ((hkKhaK11/hsK11)*100).toFixed(2);
	    $('#tlHkKhaK11').val(kq);
	    var sum = hkKhaK10+hkKhaK11+hkKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThpt').val(sum);
	    $('#tlHkKhaThpt').val(kqt);
	});
	
	jQuery('#hkTbK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTbK10 = layGiaTriInput("hkTbK10");
	    var hkTbK11 = layGiaTriInput("hkTbK11");
	    var hkTbK12 = layGiaTriInput("hkTbK12");
	    
	    var kq = ((hkTbK11/hsK11)*100).toFixed(2);
	    $('#tlHkTbK11').val(kq);
	    var sum = hkTbK10+hkTbK11+hkTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThpt').val(sum);
	    $('#tlHkTbThpt').val(kqt);
	});
	
	jQuery('#hkYeuK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkYeuK10 = layGiaTriInput("hkYeuK10");
	    var hkYeuK11 = layGiaTriInput("hkYeuK11");
	    var hkYeuK12 = layGiaTriInput("hkYeuK12");

	    var kq = ((hkYeuK11/hsK11)*100).toFixed(2);
	    $('#tlHkYeuK11').val(kq);
	    var sum = hkYeuK10+hkYeuK11+hkYeuK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThpt').val(sum);
	    $('#tlHkYeuThpt').val(kqt);
	});
	
	jQuery('#hkTotK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTotK10 = layGiaTriInput("hkTotK10");
	    var hkTotK11 = layGiaTriInput("hkTotK11");
	    var hkTotK12 = layGiaTriInput("hkTotK12");

	    var kq = ((hkTotK12/hsK12)*100).toFixed(2);
	    $('#tlHkTotK12').val(kq);
	    var sum = hkTotK10+hkTotK11+hkTotK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTotThpt').val(sum);
	    $('#tlHkTotThpt').val(kqt);
	});
	
	jQuery('#hkKhaK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkKhaK10 = layGiaTriInput("hkKhaK10");
	    var hkKhaK11 = layGiaTriInput("hkKhaK11");
	    var hkKhaK12 = layGiaTriInput("hkKhaK12");

	    var kq = ((hkKhaK12/hsK12)*100).toFixed(2);
	    $('#tlHkKhaK12').val(kq);
	    var sum = hkKhaK10+hkKhaK11+hkKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKhaThpt').val(sum);
	    $('#tlHkKhaThpt').val(kqt);
	});
	
	jQuery('#hkTbK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTbK10 = layGiaTriInput("hkTbK10");
	    var hkTbK11 = layGiaTriInput("hkTbK11");
	    var hkTbK12 = layGiaTriInput("hkTbK12");
	    
	    var kq = ((hkTbK12/hsK12)*100).toFixed(2);
	    $('#tlHkTbK12').val(kq);
	    var sum = hkTbK10+hkTbK11+hkTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTbThpt').val(sum);
	    $('#tlHkTbThpt').val(kqt);
	});
	
	jQuery('#hkYeuK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkYeuK10 = layGiaTriInput("hkYeuK10");
	    var hkYeuK11 = layGiaTriInput("hkYeuK11");
	    var hkYeuK12 = layGiaTriInput("hkYeuK12");

	    var kq = ((hkYeuK12/hsK12)*100).toFixed(2);
	    $('#tlHkYeuK12').val(kq);
	    var sum = hkYeuK10+hkYeuK11+hkYeuK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeuThpt').val(sum);
	    $('#tlHkYeuThpt').val(kqt);
	});
	
	//Hoc luc
	jQuery('#hlGioiK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlGioiK10 = layGiaTriInput("hlGioiK10");
	    var hlGioiK11 = layGiaTriInput("hlGioiK11");
	    var hlGioiK12 = layGiaTriInput("hlGioiK12");

	    var kq = ((hlGioiK10/hsK10)*100).toFixed(2);
	    $('#tlHlGioiK10').val(kq);
	    var sum = hlGioiK10+hlGioiK11+hlGioiK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThpt').val(sum);
	    $('#tlHlGioiThpt').val(kqt);
	    
	});
	
	jQuery('#hlKhaK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKhaK10 = layGiaTriInput("hlKhaK10");
	    var hlKhaK11 = layGiaTriInput("hlKhaK11");
	    var hlKhaK12 = layGiaTriInput("hlKhaK12");

	    var kq = ((hlKhaK10/hsK10)*100).toFixed(2);
	    $('#tlHlKhaK10').val(kq);
	    var sum = hlKhaK10+hlKhaK11+hlKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThpt').val(sum);
	    $('#tlHlKhaThpt').val(kqt);
	});
	
	jQuery('#hlTbK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlTbK10 = layGiaTriInput("hlTbK10");
	    var hlTbK11 = layGiaTriInput("hlTbK11");
	    var hlTbK12 = layGiaTriInput("hlTbK12");

	    var kq = ((hlTbK10/hsK10)*100).toFixed(2);
	    $('#tlHlTbK10').val(kq);
	    var sum = hlTbK10+hlTbK11+hlTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThpt').val(sum);
	    $('#tlHlTbThpt').val(kqt);
	});
	
	jQuery('#hlYeuK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlYeuK10 = layGiaTriInput("hlYeuK10");
	    var hlYeuK11 = layGiaTriInput("hlYeuK11");
	    var hlYeuK12 = layGiaTriInput("hlYeuK12");

	    var kq = ((hlYeuK10/hsK10)*100).toFixed(2);
	    $('#tlHlYeuK10').val(kq);
	    var sum = hlYeuK10+hlYeuK11+hlYeuK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThpt').val(sum);
	    $('#tlHlYeuThpt').val(kqt);
	});
	
	jQuery('#hlKemK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKemK10 = layGiaTriInput("hlKemK10");
	    var hlKemK11 = layGiaTriInput("hlKemK11");
	    var hlKemK12 = layGiaTriInput("hlKemK12");

	    var kq = ((hlKemK10/hsK10)*100).toFixed(2);
	    $('#tlHlKemK10').val(kq);
	    var sum = hlKemK10+hlKemK11+hlKemK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThpt').val(sum);
	    $('#tlHlKemThpt').val(kqt);
	});
	
	jQuery('#hlGioiK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlGioiK10 = layGiaTriInput("hlGioiK10");
	    var hlGioiK11 = layGiaTriInput("hlGioiK11");
	    var hlGioiK12 = layGiaTriInput("hlGioiK12");

	    var kq = ((hlGioiK11/hsK11)*100).toFixed(2);
	    $('#tlHlGioiK11').val(kq);
	    var sum = hlGioiK10+hlGioiK11+hlGioiK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThpt').val(sum);
	    $('#tlHlGioiThpt').val(kqt);
	    
	});
	
	jQuery('#hlKhaK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKhaK10 = layGiaTriInput("hlKhaK10");
	    var hlKhaK11 = layGiaTriInput("hlKhaK11");
	    var hlKhaK12 = layGiaTriInput("hlKhaK12");

	    var kq = ((hlKhaK11/hsK11)*100).toFixed(2);
	    $('#tlHlKhaK11').val(kq);
	    var sum = hlKhaK10+hlKhaK11+hlKhaK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThpt').val(sum);
	    $('#tlHlKhaThpt').val(kqt);
	});
	
	jQuery('#hlTbK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlTbK10 = layGiaTriInput("hlTbK10");
	    var hlTbK11 = layGiaTriInput("hlTbK11");
	    var hlTbK12 = layGiaTriInput("hlTbK12");

	    var kq = ((hlTbK11/hsK11)*100).toFixed(2);
	    $('#tlHlTbK11').val(kq);
	    var sum = hlTbK10+hlTbK11+hlTbK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThpt').val(sum);
	    $('#tlHlTbThpt').val(kqt);
	});
	
	jQuery('#hlYeuK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlYeuK10 = layGiaTriInput("hlYeuK10");
	    var hlYeuK11 = layGiaTriInput("hlYeuK11");
	    var hlYeuK12 = layGiaTriInput("hlYeuK12");

	    var kq = ((hlYeuK11/hsK11)*100).toFixed(2);
	    $('#tlHlYeuK11').val(kq);
	    var sum = hlYeuK10+hlYeuK11+hlYeuK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThpt').val(sum);
	    $('#tlHlYeuThpt').val(kqt);
	});
	
	jQuery('#hlKemK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKemK10 = layGiaTriInput("hlKemK10");
	    var hlKemK11 = layGiaTriInput("hlKemK11");
	    var hlKemK12 = layGiaTriInput("hlKemK12");

	    var kq = ((hlKemK11/hsK11)*100).toFixed(2);
	    $('#tlHlKemK11').val(kq);
	    var sum = hlKemK10+hlKemK11+hlKemK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThpt').val(sum);
	    $('#tlHlKemThpt').val(kqt);
	});
	
	jQuery('#hlGioiK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlGioiK10 = layGiaTriInput("hlGioiK10");
	    var hlGioiK11 = layGiaTriInput("hlGioiK11");
	    var hlGioiK12 = layGiaTriInput("hlGioiK12");

	    var kq = ((hlGioiK12/hsK12)*100).toFixed(2);
	    $('#tlHlGioiK12').val(kq);
	    var sum = hlGioiK10+hlGioiK11+hlGioiK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioiThpt').val(sum);
	    $('#tlHlGioiThpt').val(kqt);
	});
	
	jQuery('#hlKhaK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKhaK10 = layGiaTriInput("hlKhaK10");
	    var hlKhaK11 = layGiaTriInput("hlKhaK11");
	    var hlKhaK12 = layGiaTriInput("hlKhaK12");

	    var kq = ((hlKhaK12/hsK12)*100).toFixed(2);
	    $('#tlHlKhaK12').val(kq);
	    var sum = hlKhaK10+hlKhaK11+hlKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKhaThpt').val(sum);
	    $('#tlHlKhaThpt').val(kqt);
	});
	
	jQuery('#hlTbK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlTbK10 = layGiaTriInput("hlTbK10");
	    var hlTbK11 = layGiaTriInput("hlTbK11");
	    var hlTbK12 = layGiaTriInput("hlTbK12");

	    var kq = ((hlTbK12/hsK12)*100).toFixed(2);
	    $('#tlHlTbK12').val(kq);
	    var sum = hlTbK10+hlTbK11+hlTbK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTbThpt').val(sum);
	    $('#tlHlTbThpt').val(kqt);
	});
	
	jQuery('#hlYeuK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlYeuK10 = layGiaTriInput("hlYeuK10");
	    var hlYeuK11 = layGiaTriInput("hlYeuK11");
	    var hlYeuK12 = layGiaTriInput("hlYeuK12");

	    var kq = ((hlYeuK12/hsK12)*100).toFixed(2);
	    $('#tlHlYeuK12').val(kq);
	    var sum = hlYeuK10+hlYeuK11+hlYeuK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeuThpt').val(sum);
	    $('#tlHlYeuThpt').val(kqt);
	});
	
	jQuery('#hlKemK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKemK10 = layGiaTriInput("hlKemK10");
	    var hlKemK11 = layGiaTriInput("hlKemK11");
	    var hlKemK12 = layGiaTriInput("hlKemK12");

	    var kq = ((hlKemK12/hsK12)*100).toFixed(2);
	    $('#tlHlKemK12').val(kq);
	    var sum = hlKemK10+hlKemK11+hlKemK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKemThpt').val(sum);
	    $('#tlHlKemThpt').val(kqt);
	});
	
	//So lieu CBQL - GV - NV
	jQuery('#cbql').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
//	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#gv2').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
//	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
//	jQuery('#gv3').on('input', function() {
//		var cbql = layGiaTriInput("cbql");
//	    var gv2 = layGiaTriInput("gv2");
//	    var gv3 = layGiaTriInput("gv3");
//	    var nv = layGiaTriInput("nv");
//	    var kq = cbql + gv2 + gv3 + nv;
//	    $('#tsCbGvNv').val(kq);
//	});
	
	jQuery('#nv').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
//	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	
	//CSVC
	jQuery('#phKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phBanKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phTam').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvBanKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvTam').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvNhaCongVu').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	initForm();
});

function layGiaTriInput(tenInput){
	return $('#'+tenInput+'').val() != "" ? parseInt($('#'+tenInput+'').val()) : 0;
}

function layGiaTriLop(tt){
	var lopTk = layGiaTriInput("lopTk_"+tt);
    var lopDb = layGiaTriInput("lopDb_"+tt);
    var lopK1 = layGiaTriInput("lopK1_"+tt);
    var lopK2 = layGiaTriInput("lopK2_"+tt);
    var lopK3 = layGiaTriInput("lopK3_"+tt);
    var lopK4 = layGiaTriInput("lopK4_"+tt);
    var lopK5 = layGiaTriInput("lopK5_"+tt);
    var lopK6 = layGiaTriInput("lopK6_"+tt);
    var lopK7 = layGiaTriInput("lopK7_"+tt);
    var lopK8 = layGiaTriInput("lopK8_"+tt);
    var lopK9 = layGiaTriInput("lopK9_"+tt);
    var lopK10 = layGiaTriInput("lopK10_"+tt);
    var lopK11 = layGiaTriInput("lopK11_"+tt);
    var lopK12 = layGiaTriInput("lopK12_"+tt);
    var kq = lopTk+ lopDb+lopK1+lopK2+lopK3+lopK4+lopK5+lopK6+lopK7+lopK8+lopK9+lopK10+lopK11+lopK12;
    $('#tsLop_'+tt).val(kq);
}

function layGiaTriHs(tt){
	var hsTk = layGiaTriInput("hsTk_"+tt);
    var hsDb = layGiaTriInput("hsDb_"+tt);
    var hsK1 = layGiaTriInput("hsK1_"+tt);
    var hsK2 = layGiaTriInput("hsK2_"+tt);
    var hsK3 = layGiaTriInput("hsK3_"+tt);
    var hsK4 = layGiaTriInput("hsK4_"+tt);
    var hsK5 = layGiaTriInput("hsK5_"+tt);
    var hsK6 = layGiaTriInput("hsK6_"+tt);
    var hsK7 = layGiaTriInput("hsK7_"+tt);
    var hsK8 = layGiaTriInput("hsK8_"+tt);
    var hsK9 = layGiaTriInput("hsK9_"+tt);
    var hsK10 = layGiaTriInput("hsK10_"+tt);
    var hsK11 = layGiaTriInput("hsK11_"+tt);
    var hsK12 = layGiaTriInput("hsK12_"+tt);
    var kq = hsTk+ hsDb+hsK1+hsK2+hsK3+hsK4+hsK5+hsK6+hsK7+hsK8+hsK9+hsK10+hsK11+hsK12;
    $('#tsHs_'+tt).val(kq);
}

function layGiaTriHsThcs(){
	var k6 = layGiaTriHsTheoKhoi("K6");
	var k7 =layGiaTriHsTheoKhoi("K7");
	var k8 =layGiaTriHsTheoKhoi("K8");
	var k9 =layGiaTriHsTheoKhoi("K9");
	var kq = k6+k7+k8+k9;
	$('#tsHsThcs').val(kq);
}

function layGiaTriHsThpt(){
	var k10 =layGiaTriHsTheoKhoi("K10");
	var k11 =layGiaTriHsTheoKhoi("K11");
	var k12 =layGiaTriHsTheoKhoi("K12");
	var kq = k10+k11+k12;
	$('#tsHsThpt').val(kq);
}

function layGiaTriHsTheoKhoi(khoi){
    var hs1 = layGiaTriInput("hs"+khoi+"_1");
    var hs2 = layGiaTriInput("hs"+khoi+"_2");
    var hs3 = layGiaTriInput("hs"+khoi+"_3");
    
    var kq = hs1+hs2+hs3;
    $('#hs'+khoi).val(kq);
    return kq;
}

function check(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			
			// Return today's date and time
			var currentTime = new Date();

			// returns the month (from 0 to 11)
			var month = currentTime.getMonth() + 1;

			// returns the year (four digits)
			var year = currentTime.getFullYear();
			
			var namhoc=$.map(data.namhoc,function(obj){
				obj.id=obj.giaTri;
				obj.text=obj.giaTri;
				return obj;
			});
			
			
			$('#cmb-nam-hoc,#cmb-nam-hoc-filter').select2({
				data: namhoc
			});
			$('#cmb-nam-hoc').val(year);
		    $('#cmb-nam-hoc').select2().trigger('change');
		    
		    $('#cmb-thoi-gian').val(month);
		    $('#cmb-thang-filter').val(month);
		    
			if($('#cmb-nam-hoc-filter').val() !=null){
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}
			
			if($('#cmb-thang-filter').val() !=null){
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}
			
			$('#guiBcSgd_1').val(0);
			$('#guiBcSgd_2').val(0);
			$('#guiBcSgd_3').val(0);
			$('#guiBcSgd_4').val(0);
			$('#guiBcSgd_5').val(0);
			$('#guiBcSgd_6').val(0);
//			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
//				$('#cmb-thang-filter').css("display","true");
//			}
//			else{
//				$('#cmb-thang-filter').css("display","none");
//			}
			},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds > tbody > tr").remove();
			var tr = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
				$.each(data.content, function(i,row){
						tr += "<tr>" +
						"<td>" + removeNull(row.noiDung) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsHs) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopTk) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsTk) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopDb) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsDb) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK1) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK4) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK5) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK12) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id, i+1) +
						'</tr>';
		    })
		    	    
		    }
		    else{
				tr = "<tr><td colspan='32' style='text-align: center'>Không có dữ liệu</td></tr>";
			}

		    $('#ds > tbody:last-child').append(tr);
		    
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('#btn_1').attr("rowspan","3");
			$('#btn_2').hide();
			$('#btn_3').hide();
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				$('#box-ds').boxWidget('expand');
				layChiTiet($(this).attr('rid'),1);
				jQuery(".row-edit-2").trigger('click');
				jQuery(".row-edit-3").trigger('click');
				jQuery(".row-edit-4").trigger('click');
				jQuery(".row-edit-5").trigger('click');
				jQuery(".row-edit-6").trigger('click');
			});
			
			$('.row-edit-2').click(function(e){
				e.preventDefault();
//				$('#box-ds').boxWidget('expand');
				layChiTiet($(this).attr('rid'),2);
			});

			$('.row-edit-3').click(function(e){
				e.preventDefault();
//				$('#box-ds').boxWidget('expand');
				layChiTiet($(this).attr('rid'),3);
			});
			
			
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+');xoaAll();');
			});
			
			$('.row-del-2').click(function(e){
				e.preventDefault();
				xoa($(this).attr('rid'));
			});
			
			$('.row-del-3').click(function(e){
				e.preventDefault();
				xoa($(this).attr('rid'));
			});
			
			$('.row-reset-pwd-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCao('+$(this).attr('rid')+', 1);guiBcAll();' );
				
			});
			
			$('.row-reset-pwd-2').click(function(e){
				e.preventDefault();
				baoCao($(this).attr('rid'),1);
			});
			
			$('.row-reset-pwd-3').click(function(e){
				e.preventDefault();
				baoCao($(this).attr('rid'),1);
			});
			
			$('.row-reset-pwd-2-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Phòng giáo dục?', 'baoCao('+$(this).attr('rid')+', 0);traBcAll();');
			});
			
			$('.row-reset-pwd-2-2').click(function(e){
				e.preventDefault();
				baoCao($(this).attr('rid'),0);
			});
			
			$('.row-reset-pwd-2-3').click(function(e){
				e.preventDefault();
				baoCao($(this).attr('rid'),0);
			});
			
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachCsvc(p){
	$.ajax({
		url:pref_url+'/lay-ds-csvc',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds-1 > tbody > tr").remove();
			var tr2 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
		     $.each(data.content, function(i,row){
				tr2 += "<tr>" +
						"<td>" + removeNull(row.tenTruong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongLamViec) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvNhaCongVu) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthTinHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthNgoaiNgu) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id,4) +
						'</tr>';
		    })
		    }
		    else{
				tr2 = "<tr><td colspan='15' style='text-align: center'>Không có dữ liệu</td></tr>";
			}

		    $('#ds-1 > tbody:last-child').append(tr2);
		    
		    $('#btn_4').hide();
			$('.row-edit-4').hide();
			$('.row-del-4').hide();
			$('.row-reset-pwd-4').hide();
			$('.row-reset-pwd-2-4').hide();
			
			$('.row-edit-4').click(function(e){
				e.preventDefault();
//				$('#box-ds').boxWidget('expand');
				layChiTietCsvc($(this).attr('rid'));
			});
			
			$('.row-del-4').click(function(e){
				e.preventDefault();
				xoaCsvc($(this).attr('rid'));
			});
			
			$('.row-reset-pwd-4').click(function(e){
				e.preventDefault();
				baoCaoCsvc($(this).attr('rid'),1);
			});
			
			$('.row-reset-pwd-2-4').click(function(e){
				e.preventDefault();
				baoCaoCsvc($(this).attr('rid'),0);
			});

		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachKqht(p){
	$.ajax({
		url:pref_url+'/lay-ds-kqht',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds-3 > tbody > tr").remove();
			var tr3 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
				$.each(data.content, function(i,row){
						tr3 += "<tr>" +
				    	"<td style='text-align:center'>1</td>" +
						"<td>Khối 6</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK6) + "</td>"  +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id,5) +
						'</tr>';
						
						tr3 += "<tr>" +
				    	"<td style='text-align:center'>2</td>" +
						"<td>Khối 7</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK7) + "</td>"  +
						'</tr>';
						
						tr3 += "<tr>" +
				    	"<td style='text-align:center'>3</td>" +
						"<td>Khối 8</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK8) + "</td>"  +
						'</tr>';
						
						tr3 += "<tr>" +
				    	"<td style='text-align:center'>4</td>" +
						"<td>Khối 9</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK9) + "</td>"  +
						'</tr>';
						
						tr3 += "<tr>" +
				    	"<td style='text-align:center'>5</td>" +
						"<td>Khối 10</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK10) + "</td>"  +
						'</tr>';
						
						tr3 += "<tr>" +
				    	"<td style='text-align:center'>6</td>" +
						"<td>Khối 11</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK11) + "</td>"  +
						'</tr>';
						
						tr3 += "<tr>" +
						"<td style='text-align:center'>7</td>" +
						"<td>Khối 12</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTotK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTotK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkKhaK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkKhaK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkTbK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkTbK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hkYeuK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHkYeuK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlGioiK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlGioiK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKhaK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKhaK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlTbK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlTbK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlYeuK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlYeuK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hlKemK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tlHlKemK12) + "</td>"  +
						'</tr>';
			    	
			    })
		    }
		    else{
				tr3 = "<tr><td colspan='22' style='text-align: center'>Không có dữ liệu</td></tr>";
			}

		    $('#ds-3 > tbody:last-child').append(tr3);
		    
		    $('#btn_5').hide();
		    $('.row-edit-5').hide();
			$('.row-del-5').hide();
			$('.row-reset-pwd-5').hide();
			$('.row-reset-pwd-2-5').hide();
			
			$('.row-edit-5').click(function(e){
				e.preventDefault();
//				$('#box-ds').boxWidget('expand');
				layChiTietKqht($(this).attr('rid'));
			});
			
			$('.row-del-5').click(function(e){
				e.preventDefault();
				xoaKqht($(this).attr('rid'));
			});
			
			$('.row-reset-pwd-5').click(function(e){
				e.preventDefault();
				baoCaoKqht($(this).attr('rid'),1);
			});
			
			$('.row-reset-pwd-2-5').click(function(e){
				e.preventDefault();
				baoCaoKqht($(this).attr('rid'),0);
			});

		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachCbGvNv(p){
	$.ajax({
		url:pref_url+'/lay-ds-cbgvnv',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds-4 > tbody > tr").remove();
			var tr4 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
		     $.each(data.content, function(i,row){
				tr4 += "<tr>" +
						"<td style='text-align:center'>1</td>" +
						"<td>" + removeNull(row.tenTruong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsCbGvNv) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.cbql) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gv2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.nv) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id,6) +
						'</tr>';
		    })
		    }
		    else{
				tr4 = "<tr><td colspan='8' style='text-align: center'>Không có dữ liệu</td></tr>";
			}

		    $('#ds-4 > tbody:last-child').append(tr4);
		    
		    $('#btn_6').hide();
		    $('.row-edit-6').hide();
			$('.row-del-6').hide();
			$('.row-reset-pwd-6').hide();
			$('.row-reset-pwd-2-6').hide();
			
			$('.row-edit-6').click(function(e){
				e.preventDefault();
//				$('#box-ds').boxWidget('expand');
				layChiTietCbGvNv($(this).attr('rid'));
			});
			
			$('.row-del-6').click(function(e){
				e.preventDefault();
				xoaCbGvNv($(this).attr('rid'));
			});
			
			$('.row-reset-pwd-6').click(function(e){
				e.preventDefault();
				baoCaoCbGvNv($(this).attr('rid'),1);
			});
			
			$('.row-reset-pwd-2-6').click(function(e){
				e.preventDefault();
				baoCaoCbGvNv($(this).attr('rid'),0);
			});

		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function guiBcAll(){
	jQuery(".row-reset-pwd-2").trigger('click');
	jQuery(".row-reset-pwd-3").trigger('click');
	jQuery(".row-reset-pwd-4").trigger('click');
	jQuery(".row-reset-pwd-5").trigger('click');
	jQuery(".row-reset-pwd-6").trigger('click');
}

function traBcAll(){
	jQuery(".row-reset-pwd-2-2").trigger('click');
	jQuery(".row-reset-pwd-2-3").trigger('click');
	jQuery(".row-reset-pwd-2-4").trigger('click');
	jQuery(".row-reset-pwd-2-5").trigger('click');
	jQuery(".row-reset-pwd-2-6").trigger('click');
}

function xoaAll(){
	jQuery(".row-del-2").trigger('click');
	jQuery(".row-del-3").trigger('click');
	jQuery(".row-del-4").trigger('click');
	jQuery(".row-del-5").trigger('click');
	jQuery(".row-del-6").trigger('click');
}

function luu(id){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-'+id)[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
				$('#box-ds').boxWidget('collapse');
			}else{
				if(id == 1){
					showError('Thông báo',data.resMessage);
				}
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuCsvc(){
	$.ajax({
		url:pref_url+'/luu-csvc',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-4')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
				$('#box-ds').boxWidget('collapse');
			}else{
//				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuKqht(){
	$.ajax({
		url:pref_url+'/luu-kqht',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-5')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
				$('#box-ds').boxWidget('collapse');
			}else{
//				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuCbGvNv(){
	$.ajax({
		url:pref_url+'/luu-cbgvnv',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-6')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
				$('#box-ds').boxWidget('collapse');
			}else{
//				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
//	$('#id').val('');
	$('#cmb-nam-hoc').val(year).trigger('change');
	$('#cmb-thoi-gian').val(month);
//	$('#tsPhong').val('');
//	$('#tsPhongHoc').val('');
//	$('#phKienCo').val('');
//	$('#phBanKienCo').val('');
//	$('#phTam').val('');
//	$('#tsPhongLamViec').val('');
//	$('#plvKienCo').val('');
//	$('#plvBanKienCo').val('');
//	$('#plvTam').val('');
//	$('#plvNhaCongVu').val('');
//	$('#pthTinHoc').val('');
//	$('#pthNgoaiNgu').val('');
//	
	huyAll(1);
	huyAll(2);
	huyAll(3);
	resetForm('form');
	$('#box-ds').boxWidget('collapse');
}

function huyAll(stt){
	$('#id_'+stt).val('');
	$('#nam_'+stt).val('');
	$('#thang_'+stt).val('');
	$('#guiBcSgd_'+stt).val(0);
	$('#tsLop_'+stt).val('');
	$('#tsHs_'+stt).val('');
	$('#lopTk_'+stt).val('');
	$('#hsTk_'+stt).val('');
	$('#lopDb_'+stt).val('');
	$('#hsDb_'+stt).val('');
	$('#lopK1_'+stt).val('');
	$('#hsK1_'+stt).val('');
	$('#lopK2_'+stt).val('');
	$('#hsK2_'+stt).val('');
	$('#lopK3_'+stt).val('');
	$('#hsK3_'+stt).val('');
	$('#lopK4_'+stt).val('');
	$('#hsK4_'+stt).val('');
	$('#lopK5_'+stt).val('');
	$('#hsK5_'+stt).val('');
	$('#lopK6_'+stt).val('');
	$('#hsK6_'+stt).val('');
	$('#lopK7_'+stt).val('');
	$('#hsK7_'+stt).val('');
	$('#lopK8_'+stt).val('');
	$('#hsK8_'+stt).val('');
	$('#lopK9_'+stt).val('');
	$('#hsK9_'+stt).val('');
	$('#lopK10_'+stt).val('');
	$('#hsK10_'+stt).val('');
	$('#lopK11_'+stt).val('');
	$('#hsK11_'+stt).val('');
	$('#lopK12_'+stt).val('');
	$('#hsK12_'+stt).val('');
}

function layChiTiet(id,stt){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id_'+stt).val(data.id);
			$('#cmb-nam-hoc').val(data.nam).trigger('change');
			$('#cmb-thoi-gian').val(data.thang);
			
			$('#noiDung_'+stt).val(data.noiDung);
			$('#tsLop_'+stt).val(data.tsLop);
			$('#tsHs_'+stt).val(data.tsHs);
			$('#lopTk_'+stt).val(data.lopTk);
			$('#hsTk_'+stt).val(data.hsTk);
			$('#lopDb_'+stt).val(data.lopDb);
			$('#hsDb_'+stt).val(data.hsDb);
			$('#lopK1_'+stt).val(data.lopK1);
			$('#hsK1_'+stt).val(data.hsK1);
			$('#lopK2_'+stt).val(data.lopK2);
			$('#hsK2_'+stt).val(data.hsK2);
			$('#lopK3_'+stt).val(data.lopK3);
			$('#hsK3_'+stt).val(data.hsK3);
			$('#lopK4_'+stt).val(data.lopK4);
			$('#hsK4_'+stt).val(data.hsK4);
			$('#lopK5_'+stt).val(data.lopK5);
			$('#hsK5_'+stt).val(data.hsK5);
			$('#lopK6_'+stt).val(data.lopK6);
			$('#hsK6_'+stt).val(data.hsK6);
			$('#hsK6_'+stt).trigger('input');
			$('#lopK7_'+stt).val(data.lopK7);
			$('#hsK7_'+stt).val(data.hsK7);
			$('#hsK7_'+stt).trigger('input');
			$('#lopK8_'+stt).val(data.lopK8);
			$('#hsK8_'+stt).val(data.hsK8);
			$('#hsK8_'+stt).trigger('input');
			$('#lopK9_'+stt).val(data.lopK9);
			$('#hsK9_'+stt).val(data.hsK9);
			$('#hsK9_'+stt).trigger('input');
			$('#lopK10_'+stt).val(data.lopK10);
			$('#hsK10_'+stt).val(data.hsK10);
			$('#hsK10_'+stt).trigger('input');
			$('#lopK11_'+stt).val(data.lopK11);
			$('#hsK11_'+stt).val(data.hsK11);
			$('#hsK11_'+stt).trigger('input');
			$('#lopK12_'+stt).val(data.lopK12);
			$('#hsK12_'+stt).val(data.hsK12);
			$('#hsK12_'+stt).trigger('input');
			$('#nam_'+stt).val(data.nam);
			$('#thang_'+stt).val(data.thang);
			$('#guiBcSgd_'+stt).val(data.guiBcSgd);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layChiTietCsvc(id){
	$.ajax({
		url:pref_url+'/lay-ct-csvc',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id_4').val(data.id);
			$('#nam_4').val(data.nam);
			$('#thang_4').val(data.thang);
			$('#guiBcSgd_4').val(data.guiBcSgd);
			$('#tsPhong').val(data.tsPhong);
			$('#tsPhongHoc').val(data.tsPhongHoc);
			$('#phKienCo').val(data.phKienCo);
			$('#phBanKienCo').val(data.phBanKienCo);
			$('#phTam').val(data.phTam);
			$('#tsPhongLamViec').val(data.tsPhongLamViec);
			$('#plvKienCo').val(data.plvKienCo);
			$('#plvBanKienCo').val(data.plvBanKienCo);
			$('#plvTam').val(data.plvTam);
			$('#plvNhaCongVu').val(data.plvNhaCongVu);
			$('#pthTinHoc').val(data.pthTinHoc);
			$('#pthNgoaiNgu').val(data.pthNgoaiNgu);
			
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layChiTietKqht(id){
	$.ajax({
		url:pref_url+'/lay-ct-kqht',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id_5').val(data.id);
			$('#nam_5').val(data.nam);
			$('#thang_5').val(data.thang);
			$('#guiBcSgd_5').val(data.guiBcSgd);
			$('#hkTotThcs').val(data.hkTotThcs);
			$('#tlHkTotThcs').val(data.tlHkTotThcs);
			$('#hkKhaThcs').val(data.hkKhaThcs);
			$('#tlHkKhaThcs').val(data.tlHkKhaThcs);
			$('#hkTbThcs').val(data.hkTbThcs);
			$('#tlHkTbThcs').val(data.tlHkTbThcs);
			$('#hkYeuThcs').val(data.hkYeuThcs);
			$('#tlHkYeuThcs').val(data.tlHkYeuThcs);
			$('#hlGioiThcs').val(data.hlGioiThcs);
			$('#tlHlGioiThcs').val(data.tlHlGioiThcs);
			$('#hlKhaThcs').val(data.hlKhaThcs);
			$('#tlHlKhaThcs').val(data.tlHlKhaThcs);
			$('#hlTbThcs').val(data.hlTbThcs);
			$('#tlHlTbThcs').val(data.tlHlTbThcs);
			$('#hlYeuThcs').val(data.hlYeuThcs);
			$('#tlHlYeuThcs').val(data.tlHlYeuThcs);
			$('#hlKemThcs').val(data.hlKemThcs);
			$('#tlHlKemThcs').val(data.tlHlKemThcs);
			$('#hkTotK6').val(data.hkTotK6);
			$('#tlHkTotK6').val(data.tlHkTotK6);
			$('#hkKhaK6').val(data.hkKhaK6);
			$('#tlHkKhaK6').val(data.tlHkKhaK6);
			$('#hkTbK6').val(data.hkTbK6);
			$('#tlHkTbK6').val(data.tlHkTbK6);
			$('#hkYeuK6').val(data.hkYeuK6);
			$('#tlHkYeuK6').val(data.tlHkYeuK6);
			$('#hlGioiK6').val(data.hlGioiK6);
			$('#tlHlGioiK6').val(data.tlHlGioiK6);
			$('#hlKhaK6').val(data.hlKhaK6);
			$('#tlHlKhaK6').val(data.tlHlKhaK6);
			$('#hlTbK6').val(data.hlTbK6);
			$('#tlHlTbK6').val(data.tlHlTbK6);
			$('#hlYeuK6').val(data.hlYeuK6);
			$('#tlHlYeuK6').val(data.tlHlYeuK6);
			$('#hlKemK6').val(data.hlKemK6);
			$('#tlHlKemK6').val(data.tlHlKemK6);
			$('#hkTotK7').val(data.hkTotK7);
			$('#tlHkTotK7').val(data.tlHkTotK7);
			$('#hkKhaK7').val(data.hkKhaK7);
			$('#tlHkKhaK7').val(data.tlHkKhaK7);
			$('#hkTbK7').val(data.hkTbK7);
			$('#tlHkTbK7').val(data.tlHkTbK7);
			$('#hkYeuK7').val(data.hkYeuK7);
			$('#tlHkYeuK7').val(data.tlHkYeuK7);
			$('#hlGioiK7').val(data.hlGioiK7);
			$('#tlHlGioiK7').val(data.tlHlGioiK7);
			$('#hlKhaK7').val(data.hlKhaK7);
			$('#tlHlKhaK7').val(data.tlHlKhaK7);
			$('#hlTbK7').val(data.hlTbK7);
			$('#tlHlTbK7').val(data.tlHlTbK7);
			$('#hlYeuK7').val(data.hlYeuK7);
			$('#tlHlYeuK7').val(data.tlHlYeuK7);
			$('#hlKemK7').val(data.hlKemK7);
			$('#tlHlKemK7').val(data.tlHlKemK7);
			$('#hkTotK8').val(data.hkTotK8);
			$('#tlHkTotK8').val(data.tlHkTotK8);
			$('#hkKhaK8').val(data.hkKhaK8);
			$('#tlHkKhaK8').val(data.tlHkKhaK8);
			$('#hkTbK8').val(data.hkTbK8);
			$('#tlHkTbK8').val(data.tlHkTbK8);
			$('#hkYeuK8').val(data.hkYeuK8);
			$('#tlHkYeuK8').val(data.tlHkYeuK8);
			$('#hlGioiK8').val(data.hlGioiK8);
			$('#tlHlGioiK8').val(data.tlHlGioiK8);
			$('#hlKhaK8').val(data.hlKhaK8);
			$('#tlHlKhaK8').val(data.tlHlKhaK8);
			$('#hlTbK8').val(data.hlTbK8);
			$('#tlHlTbK8').val(data.tlHlTbK8);
			$('#hlYeuK8').val(data.hlYeuK8);
			$('#tlHlYeuK8').val(data.tlHlYeuK8);
			$('#hlKemK8').val(data.hlKemK8);
			$('#tlHlKemK8').val(data.tlHlKemK8);
			$('#hkTotK9').val(data.hkTotK9);
			$('#tlHkTotK9').val(data.tlHkTotK9);
			$('#hkKhaK9').val(data.hkKhaK9);
			$('#tlHkKhaK9').val(data.tlHkKhaK9);
			$('#hkTbK9').val(data.hkTbK9);
			$('#tlHkTbK9').val(data.tlHkTbK9);
			$('#hkYeuK9').val(data.hkYeuK9);
			$('#tlHkYeuK9').val(data.tlHkYeuK9);
			$('#hlGioiK9').val(data.hlGioiK9);
			$('#tlHlGioiK9').val(data.tlHlGioiK9);
			$('#hlKhaK9').val(data.hlKhaK9);
			$('#tlHlKhaK9').val(data.tlHlKhaK9);
			$('#hlTbK9').val(data.hlTbK9);
			$('#tlHlTbK9').val(data.tlHlTbK9);
			$('#hlYeuK9').val(data.hlYeuK9);
			$('#tlHlYeuK9').val(data.tlHlYeuK9);
			$('#hlKemK9').val(data.hlKemK9);
			$('#tlHlKemK9').val(data.tlHlKemK9);
			
			$('#hsK10').val(data.hsK10);
			$('#hsK11').val(data.hsK11);
			$('#hsK12').val(data.hsK12);
			$('#tsHsThcs').val(data.tsHsThcs);
			$('#tsHsThpt').val(data.tsHsThpt);
			$('#hkTotThpt').val(data.hkTotThpt);
			$('#tlHkTotThpt').val(data.tlHkTotThpt);
			$('#hkKhaThpt').val(data.hkKhaThpt);
			$('#tlHkKhaThpt').val(data.tlHkKhaThpt);
			$('#hkTbThpt').val(data.hkTbThpt);
			$('#tlHkTbThpt').val(data.tlHkTbThpt);
			$('#hkYeuThpt').val(data.hkYeuThpt);
			$('#tlHkYeuThpt').val(data.tlHkYeuThpt);
			$('#hlGioiThpt').val(data.hlGioiThpt);
			$('#tlHlGioiThpt').val(data.tlHlGioiThpt);
			$('#hlKhaThpt').val(data.hlKhaThpt);
			$('#tlHlKhaThpt').val(data.tlHlKhaThpt);
			$('#hlTbThpt').val(data.hlTbThpt);
			$('#tlHlTbThpt').val(data.tlHlTbThpt);
			$('#hlYeuThpt').val(data.hlYeuThpt);
			$('#tlHlYeuThpt').val(data.tlHlYeuThpt);
			$('#hlKemThpt').val(data.hlKemThpt);
			$('#tlHlKemThpt').val(data.tlHlKemThpt);
			$('#hkTotK10').val(data.hkTotK10);
			$('#tlHkTotK10').val(data.tlHkTotK10);
			$('#hkKhaK10').val(data.hkKhaK10);
			$('#tlHkKhaK10').val(data.tlHkKhaK10);
			$('#hkTbK10').val(data.hkTbK10);
			$('#tlHkTbK10').val(data.tlHkTbK10);
			$('#hkYeuK10').val(data.hkYeuK10);
			$('#tlHkYeuK10').val(data.tlHkYeuK10);
			$('#hlGioiK10').val(data.hlGioiK10);
			$('#tlHlGioiK10').val(data.tlHlGioiK10);
			$('#hlKhaK10').val(data.hlKhaK10);
			$('#tlHlKhaK10').val(data.tlHlKhaK10);
			$('#hlTbK10').val(data.hlTbK10);
			$('#tlHlTbK10').val(data.tlHlTbK10);
			$('#hlYeuK10').val(data.hlYeuK10);
			$('#tlHlYeuK10').val(data.tlHlYeuK10);
			$('#hlKemK10').val(data.hlKemK10);
			$('#tlHlKemK10').val(data.tlHlKemK10);
			$('#hkTotK11').val(data.hkTotK11);
			$('#tlHkTotK11').val(data.tlHkTotK11);
			$('#hkKhaK11').val(data.hkKhaK11);
			$('#tlHkKhaK11').val(data.tlHkKhaK11);
			$('#hkTbK11').val(data.hkTbK11);
			$('#tlHkTbK11').val(data.tlHkTbK11);
			$('#hkYeuK11').val(data.hkYeuK11);
			$('#tlHkYeuK11').val(data.tlHkYeuK11);
			$('#hlGioiK11').val(data.hlGioiK11);
			$('#tlHlGioiK11').val(data.tlHlGioiK11);
			$('#hlKhaK11').val(data.hlKhaK11);
			$('#tlHlKhaK11').val(data.tlHlKhaK11);
			$('#hlTbK11').val(data.hlTbK11);
			$('#tlHlTbK11').val(data.tlHlTbK11);
			$('#hlYeuK11').val(data.hlYeuK11);
			$('#tlHlYeuK11').val(data.tlHlYeuK11);
			$('#hlKemK11').val(data.hlKemK11);
			$('#tlHlKemK11').val(data.tlHlKemK11);
			$('#hkTotK12').val(data.hkTotK12);
			$('#tlHkTotK12').val(data.tlHkTotK12);
			$('#hkKhaK12').val(data.hkKhaK12);
			$('#tlHkKhaK12').val(data.tlHkKhaK12);
			$('#hkTbK12').val(data.hkTbK12);
			$('#tlHkTbK12').val(data.tlHkTbK12);
			$('#hkYeuK12').val(data.hkYeuK12);
			$('#tlHkYeuK12').val(data.tlHkYeuK12);
			$('#hlGioiK12').val(data.hlGioiK12);
			$('#tlHlGioiK12').val(data.tlHlGioiK12);
			$('#hlKhaK12').val(data.hlKhaK12);
			$('#tlHlKhaK12').val(data.tlHlKhaK12);
			$('#hlTbK12').val(data.hlTbK12);
			$('#tlHlTbK12').val(data.tlHlTbK12);
			$('#hlYeuK12').val(data.hlYeuK12);
			$('#tlHlYeuK12').val(data.tlHlYeuK12);
			$('#hlKemK12').val(data.hlKemK12);
			$('#tlHlKemK12').val(data.tlHlKemK12);
			
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layChiTietCbGvNv(id){
	$.ajax({
		url:pref_url+'/lay-ct-cbgvnv',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id_6').val(data.id);
			$('#nam_6').val(data.nam);
			$('#thang_6').val(data.thang);
			$('#guiBcSgd_6').val(data.guiBcSgd);
			$('#cbql').val(data.cbql);
			$('#gv2').val(data.gv2);
//			$('#gv3').val(data.gv3);
			$('#nv').val(data.nv);
			$('#tsCbGvNv').val(data.tsCbGvNv);
			
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function xoaCsvc(id){
	$.ajax({
		url:pref_url+'/xoa-csvc',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function xoaKqht(id){
	$.ajax({
		url:pref_url+'/xoa-kqht',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function xoaCbGvNv(id){
	$.ajax({
		url:pref_url+'/xoa-cbgvnv',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCao(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 999){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 999){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCaoCsvc(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao-csvc',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 999){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 999){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCaoKqht(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao-kqht',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 999){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 999){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCaoCbGvNv(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao-cbgvnv',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 999){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
				layDanhSachCsvc(0);
				layDanhSachKqht(0);
				layDanhSachCbGvNv(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 999){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

