var pref_url="/import-so-lieu";

$(document).ready(function(){
	$('#btn-ok').click(function(){
		if(check()){
//			luu();
			kiemTra();
		}
	});
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
    
	$("#cmb-thoi-gian").val(month);
	
	layDsNamHoc();
	
	
	var idTruong = $('#idTruongSession').val();
	var idLoaiTruong = $('#idLoaiTruongSession').val();
	var idDonViSs = $('#idDonVi').val();
	
//	layDsTruong(idDonViSs);
	
	if(idTruong != 999){
		$('#div_truong').hide();
	}
	if(idLoaiTruong == 3 || idLoaiTruong ==4 || idLoaiTruong ==99 ||  idLoaiTruong == 34||  idLoaiTruong == 23 || idLoaiTruong == 8){
		$('#div_kqht').show();
	}else{
		$('#div_kqht').hide();
	}
	if(idLoaiTruong == 34){
			$('#cmb-cap').append($('<option>', {
			    value: 3,
			    text: 'Trung học cở sở'
			}));
			$('#cmb-cap').append($('<option>', {
			    value: 4,
			    text: 'Trung học phổ thông'
			}));
			$('#div_cap').show();
	}else if(idLoaiTruong == 23){
		$('#cmb-cap').append($('<option>', {
		    value: 2,
		    text: 'Tiểu học'
		}));
		$('#cmb-cap').append($('<option>', {
		    value: 3,
		    text: 'Trung học cở sở'
		}));
		$('#div_cap').show();
	}else if(idLoaiTruong == 8){
		$('#cmb-cap').append($('<option>', {
		    value: 2,
		    text: 'Tiểu học'
		}));
		$('#cmb-cap').append($('<option>', {
		    value: 3,
		    text: 'Trung học cở sở'
		}));
		$('#div_cap').show();
	}else if(idLoaiTruong == 1){
		$('#cmb-cap').append($('<option>', {
		    value: 1,
		    text: 'Mầm non, Mẫu giáo'
		}));
		$('#div_cap').hide();
	}else if(idLoaiTruong == 2){
		$('#cmb-cap').append($('<option>', {
		    value: 2,
		    text: 'Tiểu học'
		}));
		$('#div_cap').hide();
	}else if(idLoaiTruong == 3){
		$('#cmb-cap').append($('<option>', {
		    value: 3,
		    text: 'Trung học cở sở'
		}));
		$('#div_cap').hide();
	}else if(idLoaiTruong == 4){
		$('#cmb-cap').append($('<option>', {
		    value: 4,
		    text: 'Trung học phổ thông'
		}));
		$('#div_cap').hide();
	}else{
		if(idDonViSs == 10 || idDonViSs == 11){
			$('#cmb-cap').append($('<option>', {
			    value: 1,
			    text: 'Mầm non, Mẫu giáo'
			}));
			$('#cmb-cap').append($('<option>', {
			    value: 2,
			    text: 'Tiểu học'
			}));
			$('#cmb-cap').append($('<option>', {
			    value: 3,
			    text: 'Trung học cở sở'
			}));
			$('#cmb-cap').append($('<option>', {
			    value: 4,
			    text: 'Trung học phổ thông'
			}));
		}else{
			$('#cmb-cap').append($('<option>', {
			    value: 1,
			    text: 'Mầm non, Mẫu giáo'
			}));
			$('#cmb-cap').append($('<option>', {
			    value: 2,
			    text: 'Tiểu học'
			}));
			$('#cmb-cap').append($('<option>', {
			    value: 3,
			    text: 'Trung học cở sở'
			}));
		}
		
		$('#div_cap').show();
	}
	console.log(idTruong);
	var cap = $('#cmb-cap').val();
	if(cap != null){
		layDsTruongTheoCap(idDonViSs,cap);
	}
	
	$('#cmb-cap').change(function(){
		var idDonViSs = $('#idDonVi').val();
		var cap = $('#cmb-cap').val();
		layDsTruongTheoCap(idDonViSs,cap);
	});
	
});

function check(){
	var idTruong = $('#idTruongSession').val();
	if(idTruong == 999){
		if($('#cmb-truong').val()===''){
			showError('Thông báo','Vui lòng chọn trường!');
			return false;
		}
	}
	if($('#cmb-cap').val()===''){
		showError('Thông báo','Vui lòng chọn cấp!');
		return false;
	}
	
	if($('#cmb-thoi-gian').val()===''){
		showError('Thông báo','Vui lòng chọn Tháng!');
		return false;
	}
	
	if($('#cmb-nam-hoc').val()===''){
		showError('Thông báo','Vui lòng chọn Năm!');
		return false;
	}
	
	if($('#fileData').val()===''){
		showError('Thông báo','Vui lòng chọn file excel số liệu lớp học!');
		return false;
	}
	if($('#fileData1').val()===''){
		showError('Thông báo','Vui lòng chọn file excel số liệu học sinh!');
		return false;
	}
	if($('#fileData2').val()===''){
		showError('Thông báo','Vui lòng chọn file excel số liệu CBQL GV, NV!');
		return false;
	}
	return true;
}

function kiemTra(){
	var idTruong = $('#idTruongSession').val();
	if(idTruong == 999){
		var idTruong = $('#cmb-truong').val();
	}
	
	var namHoc = $('#cmb-nam-hoc').val();
	var thoiGian = $('#cmb-thoi-gian').val();
	var cap =  $('#cmb-cap').val();
//	var thoiGian = $("input[name=thoiGianImport]:checked").val();
	$.ajax({
		url:pref_url+'/kiem-tra?idTruong='+idTruong+'&namHoc='+namHoc+'&thoiGian='+thoiGian+'&cap='+cap,
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		enctype: 'multipart/form-data',
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				luu(0);
			}else{
				showConfirm('Xác nhận', data.resMessage, 'luu(1)');
			}
		},
		error:function(){
			showError('Thông báo','Import dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

// 0: thêm mới
// 1: cập nhật
function luu(action){
	
	var idTruong = $('#idTruongSession').val();
	if(idTruong == 999){
		var idTruong = $('#cmb-truong').val();
	}
	
	var namHoc = $('#cmb-nam-hoc').val();
	var thoiGian = $('#cmb-thoi-gian').val();
	var cap =  $('#cmb-cap').val();
//	var thoiGian = $("input[name=thoiGianImport]:checked").val();
	$.ajax({
		url:pref_url+'/xu-ly?idTruong='+idTruong+'&namHoc='+namHoc+'&thoiGian='+thoiGian+'&action='+action+'&cap='+cap,
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		enctype: 'multipart/form-data',
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Import dữ liệu thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Import dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}


function layDsNamHoc(){
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
	$.ajax({
		url:pref_url+'/lay-ds-nam-hoc',
		method:'get',
		dataType:'json',
		success:function(data){
			var namhoc=$.map(data,function(obj){
					obj.id=obj.giaTri;
					obj.text=obj.giaTri;
				return obj;
			});
			$('#cmb-nam-hoc').select2({
				data: namhoc
			});
			$('#cmb-nam-hoc').val(year);
		    $('#cmb-nam-hoc').select2().trigger('change');
		},
		error:function(){
			showError('Thông báo','Lấy danh sách năm học không thành công, vui lòng thử lại sau!');
		}
	});
}

//function layDsTruong(){
//	$.ajax({
//		url:pref_url+'/lay-ds-truong',
//		method:'get',
//		dataType:'json',
//		success:function(data){
//			console.log(data);
//			var truong=$.map(data,function(obj){
//					obj.id=obj.idTruong;
//					obj.text=obj.tenTruong+" - "+obj.tenPhongGd;
//				return obj;
//			});
//			// get index of object with id:999
//			var removeIndex = truong.map(function(item) { return item.id; }).indexOf(999);
//
//			// remove object
//			truong.splice(removeIndex, 1);
//			
//			$('#cmb-truong').select2({
//				data: truong
//			});
//		},
//		error:function(){
//			showError('Thông báo','Lấy danh sách năm học không thành công, vui lòng thử lại sau!');
//		}
//	});
//	
//	$('#cmb-cap').on('change', function() {
//		$('#fileData').val('');
//		$('#fileData1').val('');
//		$('#fileData2').val('');
//		$('#fileData3').val('');
//	});
//}

//function layDsTruong(id){
//	$('#cmb-truong').find('option').remove();
//    if(id==null) {
//        $('#cmb-truong').trigger('change');
//        return;
//    }
//	$.ajax({
//		url:pref_url+'/lay-ds-truong',
//		method:'post',
//		dataType:'json',
//		data:{id:id},
//		beforeSend:function(){
//			$('#box-ds').showLoading();
//		},
//		success:function(data){
//			if(data != null){
//				
//				if(id != 10  && id != 11){
//					var truong=$.map(data.data,function(obj){
//						obj.id=obj.idTruong;
//						obj.text=obj.tenTruong;
//					return obj;
//				});
//				}else{
//					var truong=$.map(data.truongs,function(obj){
//						obj.id=obj.idTruong;
//						if(obj.idTruong !=999)
//						obj.text=obj.tenTruong+" - "+obj.tenPhongGd;
//					return obj;
//					});
//				}
//				$('#cmb-truong').select2({
//					data: truong
//				});
//				
//				$('#cmb-truong').trigger('change')
//			}else{
//				showError('Thông báo',data.resMessage);
//			}
//		},
//		error:function(){
//			showError('Thông báo','Lỗi lấy dữ liệu trường!');
//		},
//		complete:function(){
//			$('#box-ds').hideLoading();
//		}
//	});
//	
//	$('#cmb-cap').on('change', function() {
//		$('#fileData').val('');
//		$('#fileData1').val('');
//		$('#fileData2').val('');
//		$('#fileData3').val('');
//	});
//}


function layDsTruongTheoCap(id,cap){
	$('#cmb-truong').find('option').remove();
    if(id==null) {
        $('#cmb-truong').trigger('change');
        return;
    }
	$.ajax({
		url:pref_url+'/lay-ds-truong-theo-cap',
		method:'post',
		dataType:'json',
		data:{id:id,cap:cap},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data != null){
				
				if(id != 10  && id != 11){
					var truong=$.map(data.data,function(obj){
						obj.id=obj.idTruong;
						obj.text=obj.tenTruong;
					return obj;
				});
				}else{
					var truong=$.map(data.truongs,function(obj){
						obj.id=obj.idTruong;
						if(obj.idTruong !=999)
						obj.text=obj.tenTruong+" - "+obj.tenPhongGd;
					return obj;
					});
				}
				$('#cmb-truong').select2({
					data: truong
				});
				
				$('#cmb-truong').trigger('change')
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lỗi lấy dữ liệu trường!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
	
	$('#cmb-cap').on('change', function() {
		$('#fileData').val('');
		$('#fileData1').val('');
		$('#fileData2').val('');
		$('#fileData3').val('');
	});
}