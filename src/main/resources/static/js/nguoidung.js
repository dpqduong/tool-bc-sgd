var pref_url="/nguoi-dung";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenNguoiDung',title:'Tên đăng nhập'},
			{name:'hoTen',title:'Họ tên'},
			{name:'tenVaiTro',title:'Nhóm'},
			{name:'tenDonVi',title:'Đơn vị'},
			{name:'khoa',title:'Khóa',type:'check',css:{'text-align':'center','width':'50px'}},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){var cls='row-lock-1';var title='Khóa';var ico='fa-lock';if(obj.khoa){cls='row-unlock-1'; title='Mở khóa';ico='fa-unlock-alt';}return '<a href="#" class="'+cls+'" rid="'+obj.idNguoiDung+'" title="'+title+'"><i class="fa '+ico+'"></i></a>&nbsp;&nbsp;<a href="#" class="row-reset-pwd-1" rid="'+obj.idNguoiDung+'" title="Reset mật khẩu"><i class="fa fa-reply-all"></i></a>&nbsp;&nbsp;<a href="#" class="row-edit-1" rid="'+obj.idNguoiDung+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idNguoiDung+'" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'80px'}}
		]
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#cmb-donvi-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
//	$('#cmb-donvi').change(function(){
//		layDsTruong($(this).val());
//	});
	
	initForm();
});

function check(){
	if($('#txt-tenNguoiDung').val()===''){
		showError('Thông báo','Vui lòng nhập tên đăng nhập!');
		return false;
	}
	if($('#txt-hoTen').val()===''){
		showError('Thông báo','Vui lòng nhập họ tên người dùng!');
		return false;
	}
	if($('#cmb-nhom').val()==null){
		showError('Thông báo','Vui lòng chọn nhóm!');
		return false;
	}
	if($('#cmb-donvi').val()==null){
		showError('Thông báo','Vui lòng chọn đơn vị!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			var idDonViSs = $('#idDonVi').val();
			var donvi=$.map(data.donVis,function(obj){
				obj.id=obj.idDonVi;
				obj.text=obj.tenDonVi;
				return obj;
			});
			if(idDonViSs != 11){
				// get index of object with id:11
				var removeIndex = donvi.map(function(item) { return item.id; }).indexOf(11);
		
				// remove object
				donvi.splice(removeIndex, 1);
			}
			
			$('#cmb-donvi,#cmb-donvi-filter').select2({
				data: donvi
			});
			
			$('#cmb-donvi-filter').val(idDonViSs).select2().trigger('change');
			$('#cmb-donvi').val(idDonViSs).select2().trigger('change');
			if(idDonViSs != 10 && idDonViSs != 11){
				$('#cmb-donvi-filter').select2({ disabled:'readonly' });
				$('#cmb-donvi').select2({ disabled:'readonly' });
				$('#cmb-nhom').select2({ disabled:'readonly' });
			}

		    layDsTruong(idDonViSs);
			
			
			var nhom=$.map(data.nhoms,function(obj){
				obj.id=obj.idVaiTro;
				obj.text=obj.tenVaiTro;
				return obj;
			});
			
			$('#cmb-nhom').select2({
				data: nhom
			});
			
			if($('#cmb-donvi-filter').val()!=null){
				layDanhSach(0);
			}
			
//			$('#cmb-donvi').change(function(){
//				layDsTruong($(this).val());
//			});
			
			var truong=$.map(data.truongs,function(obj){
				obj.id=obj.idTruong;
				if(obj.idTruong != 999)
				obj.text=obj.tenTruong+" - "+obj.tenPhongGd;
			return obj;
			});
//			// get index of object with id:999
//			var removeIndex = truong.map(function(item) { return item.id; }).indexOf(999);
//	
//			// remove object
//			truong.splice(removeIndex, 1);
			
//			$('#cmb-truong').select2({
//				data: truong
//			});
			
			},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#cmb-donvi-filter').val(),thongTinCanTim:$('#txt-keyword').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
			
			$('.row-lock-1').click(function(e){
				e.preventDefault();
				khoa($(this).attr('rid'));
			});
			
			$('.row-unlock-1').click(function(e){
				e.preventDefault();
				moKhoa($(this).attr('rid'));
			});
			
			$('.row-reset-pwd-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn reset mật khẩu người dùng?', 'resetPwd('+$(this).attr('rid')+')');
				
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	$('#txt-idNguoiDung').val('');
	$('#cmb-truong').val($("#cmb-truong option:first").val()).trigger('change');
	$('#txt-hoTen').val('');
	$('#txt-tenNguoiDung').val('').focus();
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#txt-idNguoiDung').val(data.idNguoiDung);
			$('#cmb-nhom').val(data.idVaiTro).trigger('change');
			$('#cmb-donvi').val(data.idDonVi).trigger('change');
			$('#txt-tenNguoiDung').val(data.tenNguoiDung);
			$('#txt-hoTen').val(data.hoTen);
			$('#cmb-truong').val(data.idTruong).trigger('change');
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function khoa(id){
	$.ajax({
		url:pref_url+'/khoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Đã khóa người dùng');
				layDanhSach($('#page-ds').data('jdpage').getCurrentPage()-1);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Khóa người dùng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function moKhoa(id){
	$.ajax({
		url:pref_url+'/mo-khoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Đã mở khóa người dùng');
				layDanhSach($('#page-ds').data('jdpage').getCurrentPage()-1);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Mở khóa người dùng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function resetPwd(id){
	$.ajax({
		url:pref_url+'/reset-pwd',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Reset mật khẩu thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Reset mật khẩu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDsTruong(id){
	$('#cmb-truong').find('option').remove();
    if(id==null) {
        $('#cmb-truong').trigger('change');
        return;
    }
	$.ajax({
		url:pref_url+'/lay-ds-truong',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data != null){
				
				if(id != 10  && id != 11){
					var truong=$.map(data.data,function(obj){
						obj.id=obj.idTruong;
						obj.text=obj.tenTruong;
					return obj;
				});
				}else{
					var truong=$.map(data.truongs,function(obj){
						obj.id=obj.idTruong;
						if(obj.idTruong !=999)
						obj.text=obj.tenTruong+" - "+obj.tenPhongGd;
					return obj;
					});
				}
				$('#cmb-truong').select2({
					data: truong
				});
				
				$('#cmb-truong').trigger('change')
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lỗi lấy dữ liệu trường!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}
