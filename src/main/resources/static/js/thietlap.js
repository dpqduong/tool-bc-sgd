
$(document).ready(function(){
	Lobibox.notify(
			'success',  // Available types 'warning', 'info', 'success', 'error'
			{
				title: "Thông báo",                // Title of notification. Do not include it for default title or set custom string. Set this false to disable title
				size: 'normal',             // normal, mini, large
				showClass: 'zoomIn',        // Show animation class.
				hideClass: 'zoomOut',       // Hide animation class.
				icon: true,                 // Icon of notification. Leave as is for default icon or set custom string
				msg: 'Đăng nhập thành công!',                    // Message of notification
				closable: true,             // Make notifications closable
				delay: 5000,                // Hide notification after this time (in miliseconds)
				delayIndicator: true,       // Show timer indicator
				closeOnClick: true,         // Close notifications by clicking on them
				width: 400,                 // Width of notification box
				sound: false,                // Sound of notification. Set this false to disable sound. Leave as is for default sound or set custom soud path
				position: "top right",    // Place to show notification. Available options: "top left", "top right", "bottom left", "bottom right"
				iconSource: "bootstrap",     // "bootstrap" or "fontAwesome" the library which will be used for icons
				rounded: false,             // Whether to make notification corners rounded
				messageHeight: 60,          // Notification message maximum height. This is not for notification itself, this is for .lobibox-notify-msg
				pauseDelayOnHover: true,    // When you mouse over on notification, delay will be paused, only if continueDelayOnInactiveTab is false.
				onClickUrl: null,           // The url which will be opened when notification is clicked
				showAfterPrevious: false,   // Set this to true if you want notification not to be shown until previous notification is closed. This is useful for notification queues
				continueDelayOnInactiveTab: true, // Continue delay when browser tab is inactive
			}
			);
});

