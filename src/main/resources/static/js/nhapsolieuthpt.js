var pref_url="/nhap-so-lieu-thpt";
var loaiTruong = $('#staticLT').val();
const truong = [
	{ id: '492', name:'THPT Chuyên PNH' }, 
	{ id: '493', name:'PT Dân tộc Nội trú' }, 
	{ id: '494', name:'THPT Tắc Vân' }, 
	{ id: '495', name:'THPT Cà Mau' }, 
	{ id: '496', name:'THPT N V Khái' }, 
	{ id: '497', name:'THPT Thới Bình' }, 
	{ id: '498', name:'THPT Tân Bằng' }, 
	{ id: '499', name:'THPT N V Nguyễn' }, 
	{ id: '500', name:'THPT Khánh Lâm' }, 
	{ id: '501', name:'THPT U Minh' }, 
	{ id: '502', name:'THPT Khánh An' }, 
	{ id: '503', name:'THPT Trần V Thời' }, 
	{ id: '504', name:'THPT Sông Đốc' }, 
	{ id: '505', name:'THPT H P.Hùng' }, 
	{ id: '506', name:'THPT Võ Thị Hồng' }, 
	{ id: '507', name:'THPT Tân Lộc' }, 
	{ id: '508', name:'THPT Cái Nước' }, 
	{ id: '509', name:'THPT Phú Hưng' }, 
	{ id: '510', name:'THPT Phú Tân' }, 
	{ id: '511', name:'THPT Ng T M Khai' }, 
	{ id: '512', name:'THPT Đầm Dơi' }, 
	{ id: '513', name:'THPT Tân Đức' }, 
	{ id: '514', name:'THPT Thái Thanh Hòa' }, 
	{ id: '515', name:'THPT Quách Văn Phẩm' }, 
	{ id: '516', name:'THPT Ngọc Hiển' }, 
	{ id: '517', name:'THPT PN Hiển' }, 
	{ id: '518', name:'THPT Vàm Đình' }, 
	{ id: '519', name:'THPT Hồ Thị Kỷ' }, 
	{ id: '520', name:'THPT Lý Văn Lâm' }, 
	{ id: '521', name:'PT Hermann Gmeiner' }, 
	{ id: '522', name:'THPT Viên An' }, 
	{ id: '523', name:'THPT Khánh Hưng' }, 
	{ id: '525', name:'PTDT Danh Thị Tươi' }, 
];

function getTenTruong(id){
	var tenTruong =  truong.filter(function(item) {
		return item.id == id;
	});
	var result = tenTruong.map(({ name }) => name)[0];
	return result == undefined ?"":  result;
}

function getBtnThaoTac(idDonVi, tt, id){
	var btn = "";
	if(idDonVi == 999){
		var btn = tt === 1 ? "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-2' rid="+id+" title='Trả báo cáo Trường'><i class='fa fa-reply-all'></i></a>" : "<td></td>";
	}else{
		var btn = tt === 1 ? "<td></td>" : "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-1' rid="+id+" title='Gửi báo cáo Sở GD'><i class='fa fa-reply-all'></i></a>&nbsp;&nbsp;<a href='#' class='row-edit-1' rid="+id+" title='Sửa' style=''><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='row-del-1 text-danger' rid="+id+" title='Xóa'><i class='fa fa-trash-o'></i></a></td>";
	}
	return btn;
}

function getBtnThaoTacThcs(idDonVi, tt, id){
	var btn = "";
	if(idDonVi == 999){
		var btn = tt === 1 ? "<td style='display:none'><a href='#' class='row-reset-pwd-3' rid="+id+" title='Trả báo cáo Trường'><i class='fa fa-reply-all'></i></a>" : "<td></td>";
	}else{
		var btn = tt === 1 ? "" : "<td style='display:none'><a href='#' class='row-reset-pwd-4' rid="+id+" title='Gửi báo cáo Sở GD'><i class='fa fa-reply-all'></i></a>&nbsp;&nbsp;<a href='#' class='row-edit-2' rid="+id+" title='Sửa' style=''><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='row-del-2 text-danger' rid="+id+" title='Xóa'><i class='fa fa-trash-o'></i></a></td>";
	}
	return btn;
}

function removeNull(val){
	return val == null ? "" : val; 
}

$(document).ready(function(){
	$('[data-toggle="push-menu"]').pushMenu('toggle');
	
	$('input').keyup(function(e)
            {
		if (/\D/g.test(this.value))
		{
		// Filter non-digits from input value.
		this.value = this.value.replace(/\D/g, '');
		}
	});
	
	$('#cmb-nam-hoc-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#cmb-thang-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-ok').click(function(){
		if(checkChung()){
			$('#nam_thcs').val($('#cmb-nam-hoc').val());
			$('#thang_thcs').val($('#cmb-thoi-gian').val());
			$('#nam_thpt').val($('#cmb-nam-hoc').val());
			$('#thang_thpt').val($('#cmb-thoi-gian').val());

			
			if(loaiTruong == 34){
				if($('#checkc2').is(':checked')){
					if(check_thcs()){
						luu_thcs();
						luuTHPT();
					}
				}else{
					luu_thcs();
					luuTHPT();
				}
				
				
			}else{
				luuTHPT();
			}
		}
	});
	
	function luuTHPT(){
		if($('#checkc3').is(':checked')){
			if(check()){
				luu();
			}
		}else{
			luu();
		}
	}
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#showBox').click(function(){
		$('#box-ds').boxWidget('toggle');
	});
	
	$('#showCt').click(function(){
		$('#box-ct').boxWidget('toggle');
	});
	//So lieu lop
	jQuery('#lopK6').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK7').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK8').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK9').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	//So lieu hoc sinh
	jQuery('#hsK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9;
	    $('#tsHs').val(kq);
	    $('#tsHs_1_thcs').val(kq);
	    $('#hsK6_1_thcs').val(hsK6);
	});
	
	jQuery('#hsK7').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9;
	    $('#tsHs').val(kq);
	    $('#tsHs_1_thcs').val(kq);
	    $('#hsK7_1_thcs').val(hsK7);
	});
	
	jQuery('#hsK8').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9 ;
	    $('#tsHs').val(kq);
	    $('#tsHs_1_thcs').val(kq);
	    $('#hsK8_1_thcs').val(hsK8);
	});
	
	jQuery('#hsK9').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9;
	    $('#tsHs').val(kq);
	    $('#tsHs_1_thcs').val(kq);
	    $('#hsK9_1_thcs').val(hsK9);
	});
	
	//So lieu CBQL - GV - NV
	jQuery('#cbql').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv = layGiaTriInput("gv");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#gv').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv = layGiaTriInput("gv");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#nv').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv = layGiaTriInput("gv");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	//Hanh Kiem'
	jQuery('#hkTotK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK6/hsK6)*100).toFixed(2);
	    $('#tlHkTotK6').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot_thcs').val(sum);
	    $('#tlHkTot_thcs').val(kqt);
	    
	});
	
	jQuery('#hkKhaK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK6/hsK6)*100).toFixed(2);
	    $('#tlHkKhaK6').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha_thcs').val(sum);
	    $('#tlHkKha_thcs').val(kqt);
	});
	
	jQuery('#hkTbK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK6/hsK6)*100).toFixed(2);
	    $('#tlHkTbK6').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb_thcs').val(sum);
	    $('#tlHkTb_thcs').val(kqt);
	});
	
	jQuery('#hkYeuK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK6/hsK6)*100).toFixed(2);
	    $('#tlHkYeuK6').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu_thcs').val(sum);
	    $('#tlHkYeu_thcs').val(kqt);
	});
	
	jQuery('#hkTotK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK7/hsK7)*100).toFixed(2);
	    $('#tlHkTotK7').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot_thcs').val(sum);
	    $('#tlHkTot_thcs').val(kqt);
	    
	});
	
	jQuery('#hkKhaK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK7/hsK7)*100).toFixed(2);
	    $('#tlHkKhaK7').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha_thcs').val(sum);
	    $('#tlHkKha_thcs').val(kqt);
	});
	
	jQuery('#hkTbK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK7/hsK7)*100).toFixed(2);
	    $('#tlHkTbK7').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb_thcs').val(sum);
	    $('#tlHkTb_thcs').val(kqt);
	});
	
	jQuery('#hkYeuK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK7/hsK7)*100).toFixed(2);
	    $('#tlHkYeuK7').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu_thcs').val(sum);
	    $('#tlHkYeu_thcs').val(kqt);
	});
	
	jQuery('#hkTotK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK8/hsK8)*100).toFixed(2);
	    $('#tlHkTotK8').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot_thcs').val(sum);
	    $('#tlHkTot_thcs').val(kqt);
	});
	
	jQuery('#hkKhaK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK8/hsK8)*100).toFixed(2);
	    $('#tlHkKhaK8').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha_thcs').val(sum);
	    $('#tlHkKha_thcs').val(kqt);
	});
	
	jQuery('#hkTbK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK8/hsK8)*100).toFixed(2);
	    $('#tlHkTbK8').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb_thcs').val(sum);
	    $('#tlHkTb_thcs').val(kqt);
	});
	
	jQuery('#hkYeuK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK8/hsK8)*100).toFixed(2);
	    $('#tlHkYeuK8').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu_thcs').val(sum);
	    $('#tlHkYeu_thcs').val(kqt);
	});
	
	jQuery('#hkTotK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK9/hsK9)*100).toFixed(2);
	    $('#tlHkTotK9').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot_thcs').val(sum);
	    $('#tlHkTot_thcs').val(kqt);
	});
	
	jQuery('#hkKhaK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK9/hsK9)*100).toFixed(2);
	    $('#tlHkKhaK9').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha_thcs').val(sum);
	    $('#tlHkKha_thcs').val(kqt);
	});
	
	jQuery('#hkTbK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK9/hsK9)*100).toFixed(2);
	    $('#tlHkTbK9').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb_thcs').val(sum);
	    $('#tlHkTb_thcs').val(kqt);
	});
	
	jQuery('#hkYeuK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK9/hsK9)*100).toFixed(2);
	    $('#tlHkYeuK9').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu_thcs').val(sum);
	    $('#tlHkYeu_thcs').val(kqt);
	});
	
	//Hoc luc
	jQuery('#hlGioiK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK6/hsK6)*100).toFixed(2);
	    $('#tlHlGioiK6').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi_thcs').val(sum);
	    $('#tlHlGioi_thcs').val(kqt);
	    
	});
	
	jQuery('#hlKhaK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK6/hsK6)*100).toFixed(2);
	    $('#tlHlKhaK6').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha_thcs').val(sum);
	    $('#tlHlKha_thcs').val(kqt);
	});
	
	jQuery('#hlTbK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK6/hsK6)*100).toFixed(2);
	    $('#tlHlTbK6').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb_thcs').val(sum);
	    $('#tlHlTb_thcs').val(kqt);
	});
	
	jQuery('#hlYeuK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK6/hsK6)*100).toFixed(2);
	    $('#tlHlYeuK6').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu_thcs').val(sum);
	    $('#tlHlYeu_thcs').val(kqt);
	});
	
	jQuery('#hlKemK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK6/hsK6)*100).toFixed(2);
	    $('#tlHlKemK6').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem_thcs').val(sum);
	    $('#tlHlKem_thcs').val(kqt);
	});
	
	jQuery('#hlGioiK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK7/hsK7)*100).toFixed(2);
	    $('#tlHlGioiK7').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi_thcs').val(sum);
	    $('#tlHlGioi_thcs').val(kqt);
	    
	});
	
	jQuery('#hlKhaK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK7/hsK7)*100).toFixed(2);
	    $('#tlHlKhaK7').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha_thcs').val(sum);
	    $('#tlHlKha_thcs').val(kqt);
	});
	
	jQuery('#hlTbK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK7/hsK7)*100).toFixed(2);
	    $('#tlHlTbK7').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb_thcs').val(sum);
	    $('#tlHlTb_thcs').val(kqt);
	});
	
	jQuery('#hlYeuK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK7/hsK7)*100).toFixed(2);
	    $('#tlHlYeuK7').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu_thcs').val(sum);
	    $('#tlHlYeu_thcs').val(kqt);
	});
	
	jQuery('#hlKemK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK7/hsK7)*100).toFixed(2);
	    $('#tlHlKemK7').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem_thcs').val(sum);
	    $('#tlHlKem_thcs').val(kqt);
	});
	
	jQuery('#hlGioiK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK8/hsK8)*100).toFixed(2);
	    $('#tlHlGioiK8').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi_thcs').val(sum);
	    $('#tlHlGioi_thcs').val(kqt);
	});
	
	jQuery('#hlKhaK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK8/hsK8)*100).toFixed(2);
	    $('#tlHlKhaK8').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha_thcs').val(sum);
	    $('#tlHlKha_thcs').val(kqt);
	});
	
	jQuery('#hlTbK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK8/hsK8)*100).toFixed(2);
	    $('#tlHlTbK8').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb_thcs').val(sum);
	    $('#tlHlTb_thcs').val(kqt);
	});
	
	jQuery('#hlYeuK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK8/hsK8)*100).toFixed(2);
	    $('#tlHlYeuK8').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu_thcs').val(sum);
	    $('#tlHlYeu_thcs').val(kqt);
	});
	
	jQuery('#hlKemK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK8/hsK8)*100).toFixed(2);
	    $('#tlHlKemK8').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem_thcs').val(sum);
	    $('#tlHlKem_thcs').val(kqt);
	});
	
	jQuery('#hlGioiK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK9/hsK9)*100).toFixed(2);
	    $('#tlHlGioiK9').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi_thcs').val(sum);
	    $('#tlHlGioi_thcs').val(kqt);
	});
	
	jQuery('#hlKhaK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK9/hsK9)*100).toFixed(2);
	    $('#tlHlKhaK9').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha_thcs').val(sum);
	    $('#tlHlKha_thcs').val(kqt);
	});
	
	jQuery('#hlTbK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK9/hsK9)*100).toFixed(2);
	    $('#tlHlTbK9').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb_thcs').val(sum);
	    $('#tlHlTb_thcs').val(kqt);
	});
	
	jQuery('#hlYeuK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK9/hsK9)*100).toFixed(2);
	    $('#tlHlYeuK9').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu_thcs').val(sum);
	    $('#tlHlYeu_thcs').val(kqt);
	});
	
	jQuery('#hlKemK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK9/hsK9)*100).toFixed(2);
	    $('#tlHlKemK9').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem_thcs').val(sum);
	    $('#tlHlKem_thcs').val(kqt);
	});
	
	
	//So lieu lop
	
	jQuery('#lopK10').on('input', function() {
		var lopK10 = layGiaTriInput("lopK10");
	    var lopK11 = layGiaTriInput("lopK11");
	    var lopK12 = layGiaTriInput("lopK12");
	    
	    var kq = lopK10 + lopK11 + lopK12 ;
	    $('#tsLopThpt').val(kq);
	});
	
	jQuery('#lopK11').on('input', function() {
		var lopK10 = layGiaTriInput("lopK10");
	    var lopK11 = layGiaTriInput("lopK11");
	    var lopK12 = layGiaTriInput("lopK12");
	    
	    var kq = lopK10 + lopK11 + lopK12 ;
	    $('#tsLopThpt').val(kq);
	});
	
	jQuery('#lopK12').on('input', function() {
		var lopK10 = layGiaTriInput("lopK10");
	    var lopK11 = layGiaTriInput("lopK11");
	    var lopK12 = layGiaTriInput("lopK12");
	    
	    var kq = lopK10 + lopK11 + lopK12 ;
	    $('#tsLopThpt').val(kq);
	});
	
	//So lieu hoc sinh
	
	jQuery('#hsK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
	    var hsK11 = layGiaTriInput("hsK11");
	    var hsK12 = layGiaTriInput("hsK12");
	    
	    var kq = hsK10 + hsK11 + hsK12 ;
	    $('#tsHsThpt').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK10_1').val(hsK10);
	});
	
	jQuery('#hsK11').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
	    var hsK11 = layGiaTriInput("hsK11");
	    var hsK12 = layGiaTriInput("hsK12");
	    
	    var kq = hsK10 + hsK11 + hsK12 ;
	    $('#tsHsThpt').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK11_1').val(hsK11);
	});
	
	jQuery('#hsK12').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
	    var hsK11 = layGiaTriInput("hsK11");
	    var hsK12 = layGiaTriInput("hsK12");
	    
	    var kq = hsK10 + hsK11 + hsK12  ;
	    $('#tsHsThpt').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK12_1').val(hsK12);
	});
	
	//So lieu CBQL - GV - NV
	jQuery('#cbql').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + gv3 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#gv2').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + gv3 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#gv3').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + gv3 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	jQuery('#nv').on('input', function() {
		var cbql = layGiaTriInput("cbql");
	    var gv2 = layGiaTriInput("gv2");
	    var gv3 = layGiaTriInput("gv3");
	    var nv = layGiaTriInput("nv");
	    var kq = cbql + gv2 + gv3 + nv;
	    $('#tsCbGvNv').val(kq);
	});
	
	//Hanh Kiem'
	jQuery('#hkTotK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTotK10 = layGiaTriInput("hkTotK10");
	    var hkTotK11 = layGiaTriInput("hkTotK11");
	    var hkTotK12 = layGiaTriInput("hkTotK12");
	    
	    var kq = ((hkTotK10/hsK10)*100).toFixed(2);
	    $('#tlHkTotK10').val(kq);
	    var sum = hkTotK10+hkTotK11+hkTotK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	    
	});
	
	jQuery('#hkKhaK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkKhaK10 = layGiaTriInput("hkKhaK10");
	    var hkKhaK11 = layGiaTriInput("hkKhaK11");
	    var hkKhaK12 = layGiaTriInput("hkKhaK12");

	    var kq = ((hkKhaK10/hsK10)*100).toFixed(2);
	    $('#tlHkKhaK10').val(kq);
	    var sum = hkKhaK10+hkKhaK11+hkKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTbK10 = layGiaTriInput("hkTbK10");
	    var hkTbK11 = layGiaTriInput("hkTbK11");
	    var hkTbK12 = layGiaTriInput("hkTbK12");
	    
	    var kq = ((hkTbK10/hsK10)*100).toFixed(2);
	    $('#tlHkTbK10').val(kq);
	    var sum = hkTbK10+hkTbK11+hkTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkYeuK10 = layGiaTriInput("hkYeuK10");
	    var hkYeuK11 = layGiaTriInput("hkYeuK11");
	    var hkYeuK12 = layGiaTriInput("hkYeuK12");

	    var kq = ((hkYeuK10/hsK10)*100).toFixed(2);
	    $('#tlHkYeuK10').val(kq);
	    var sum = hkYeuK10+hkYeuK11+hkYeuK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	jQuery('#hkTotK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTotK10 = layGiaTriInput("hkTotK10");
	    var hkTotK11 = layGiaTriInput("hkTotK11");
	    var hkTotK12 = layGiaTriInput("hkTotK12");

	    var kq = ((hkTotK11/hsK11)*100).toFixed(2);
	    $('#tlHkTotK11').val(kq);
	    var sum = hkTotK10+hkTotK11+hkTotK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	    
	});
	
	jQuery('#hkKhaK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkKhaK10 = layGiaTriInput("hkKhaK10");
	    var hkKhaK11 = layGiaTriInput("hkKhaK11");
	    var hkKhaK12 = layGiaTriInput("hkKhaK12");

	    var kq = ((hkKhaK11/hsK11)*100).toFixed(2);
	    $('#tlHkKhaK11').val(kq);
	    var sum = hkKhaK10+hkKhaK11+hkKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTbK10 = layGiaTriInput("hkTbK10");
	    var hkTbK11 = layGiaTriInput("hkTbK11");
	    var hkTbK12 = layGiaTriInput("hkTbK12");
	    
	    var kq = ((hkTbK11/hsK11)*100).toFixed(2);
	    $('#tlHkTbK11').val(kq);
	    var sum = hkTbK10+hkTbK11+hkTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkYeuK10 = layGiaTriInput("hkYeuK10");
	    var hkYeuK11 = layGiaTriInput("hkYeuK11");
	    var hkYeuK12 = layGiaTriInput("hkYeuK12");

	    var kq = ((hkYeuK11/hsK11)*100).toFixed(2);
	    $('#tlHkYeuK11').val(kq);
	    var sum = hkYeuK10+hkYeuK11+hkYeuK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	jQuery('#hkTotK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTotK10 = layGiaTriInput("hkTotK10");
	    var hkTotK11 = layGiaTriInput("hkTotK11");
	    var hkTotK12 = layGiaTriInput("hkTotK12");

	    var kq = ((hkTotK12/hsK12)*100).toFixed(2);
	    $('#tlHkTotK12').val(kq);
	    var sum = hkTotK10+hkTotK11+hkTotK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	});
	
	jQuery('#hkKhaK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkKhaK10 = layGiaTriInput("hkKhaK10");
	    var hkKhaK11 = layGiaTriInput("hkKhaK11");
	    var hkKhaK12 = layGiaTriInput("hkKhaK12");

	    var kq = ((hkKhaK12/hsK12)*100).toFixed(2);
	    $('#tlHkKhaK12').val(kq);
	    var sum = hkKhaK10+hkKhaK11+hkKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkTbK10 = layGiaTriInput("hkTbK10");
	    var hkTbK11 = layGiaTriInput("hkTbK11");
	    var hkTbK12 = layGiaTriInput("hkTbK12");
	    
	    var kq = ((hkTbK12/hsK12)*100).toFixed(2);
	    $('#tlHkTbK12').val(kq);
	    var sum = hkTbK10+hkTbK11+hkTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hkYeuK10 = layGiaTriInput("hkYeuK10");
	    var hkYeuK11 = layGiaTriInput("hkYeuK11");
	    var hkYeuK12 = layGiaTriInput("hkYeuK12");

	    var kq = ((hkYeuK12/hsK12)*100).toFixed(2);
	    $('#tlHkYeuK12').val(kq);
	    var sum = hkYeuK10+hkYeuK11+hkYeuK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	//Hoc luc
	jQuery('#hlGioiK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlGioiK10 = layGiaTriInput("hlGioiK10");
	    var hlGioiK11 = layGiaTriInput("hlGioiK11");
	    var hlGioiK12 = layGiaTriInput("hlGioiK12");

	    var kq = ((hlGioiK10/hsK10)*100).toFixed(2);
	    $('#tlHlGioiK10').val(kq);
	    var sum = hlGioiK10+hlGioiK11+hlGioiK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	    
	});
	
	jQuery('#hlKhaK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKhaK10 = layGiaTriInput("hlKhaK10");
	    var hlKhaK11 = layGiaTriInput("hlKhaK11");
	    var hlKhaK12 = layGiaTriInput("hlKhaK12");

	    var kq = ((hlKhaK10/hsK10)*100).toFixed(2);
	    $('#tlHlKhaK10').val(kq);
	    var sum = hlKhaK10+hlKhaK11+hlKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlTbK10 = layGiaTriInput("hlTbK10");
	    var hlTbK11 = layGiaTriInput("hlTbK11");
	    var hlTbK12 = layGiaTriInput("hlTbK12");

	    var kq = ((hlTbK10/hsK10)*100).toFixed(2);
	    $('#tlHlTbK10').val(kq);
	    var sum = hlTbK10+hlTbK11+hlTbK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlYeuK10 = layGiaTriInput("hlYeuK10");
	    var hlYeuK11 = layGiaTriInput("hlYeuK11");
	    var hlYeuK12 = layGiaTriInput("hlYeuK12");

	    var kq = ((hlYeuK10/hsK10)*100).toFixed(2);
	    $('#tlHlYeuK10').val(kq);
	    var sum = hlYeuK10+hlYeuK11+hlYeuK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK10').on('input', function() {
		var hsK10 = layGiaTriInput("hsK10");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKemK10 = layGiaTriInput("hlKemK10");
	    var hlKemK11 = layGiaTriInput("hlKemK11");
	    var hlKemK12 = layGiaTriInput("hlKemK12");

	    var kq = ((hlKemK10/hsK10)*100).toFixed(2);
	    $('#tlHlKemK10').val(kq);
	    var sum = hlKemK10+hlKemK11+hlKemK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	
	jQuery('#hlGioiK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlGioiK10 = layGiaTriInput("hlGioiK10");
	    var hlGioiK11 = layGiaTriInput("hlGioiK11");
	    var hlGioiK12 = layGiaTriInput("hlGioiK12");

	    var kq = ((hlGioiK11/hsK11)*100).toFixed(2);
	    $('#tlHlGioiK11').val(kq);
	    var sum = hlGioiK10+hlGioiK11+hlGioiK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	    
	});
	
	jQuery('#hlKhaK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKhaK10 = layGiaTriInput("hlKhaK10");
	    var hlKhaK11 = layGiaTriInput("hlKhaK11");
	    var hlKhaK12 = layGiaTriInput("hlKhaK12");

	    var kq = ((hlKhaK11/hsK11)*100).toFixed(2);
	    $('#tlHlKhaK11').val(kq);
	    var sum = hlKhaK10+hlKhaK11+hlKhaK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlTbK10 = layGiaTriInput("hlTbK10");
	    var hlTbK11 = layGiaTriInput("hlTbK11");
	    var hlTbK12 = layGiaTriInput("hlTbK12");

	    var kq = ((hlTbK11/hsK11)*100).toFixed(2);
	    $('#tlHlTbK11').val(kq);
	    var sum = hlTbK10+hlTbK11+hlTbK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlYeuK10 = layGiaTriInput("hlYeuK10");
	    var hlYeuK11 = layGiaTriInput("hlYeuK11");
	    var hlYeuK12 = layGiaTriInput("hlYeuK12");

	    var kq = ((hlYeuK11/hsK11)*100).toFixed(2);
	    $('#tlHlYeuK11').val(kq);
	    var sum = hlYeuK10+hlYeuK11+hlYeuK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK11').on('input', function() {
		var hsK11 = layGiaTriInput("hsK11");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKemK10 = layGiaTriInput("hlKemK10");
	    var hlKemK11 = layGiaTriInput("hlKemK11");
	    var hlKemK12 = layGiaTriInput("hlKemK12");

	    var kq = ((hlKemK11/hsK11)*100).toFixed(2);
	    $('#tlHlKemK11').val(kq);
	    var sum = hlKemK10+hlKemK11+hlKemK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	
	jQuery('#hlGioiK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlGioiK10 = layGiaTriInput("hlGioiK10");
	    var hlGioiK11 = layGiaTriInput("hlGioiK11");
	    var hlGioiK12 = layGiaTriInput("hlGioiK12");

	    var kq = ((hlGioiK12/hsK12)*100).toFixed(2);
	    $('#tlHlGioiK12').val(kq);
	    var sum = hlGioiK10+hlGioiK11+hlGioiK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	});
	
	jQuery('#hlKhaK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKhaK10 = layGiaTriInput("hlKhaK10");
	    var hlKhaK11 = layGiaTriInput("hlKhaK11");
	    var hlKhaK12 = layGiaTriInput("hlKhaK12");

	    var kq = ((hlKhaK12/hsK12)*100).toFixed(2);
	    $('#tlHlKhaK12').val(kq);
	    var sum = hlKhaK10+hlKhaK11+hlKhaK12 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlTbK10 = layGiaTriInput("hlTbK10");
	    var hlTbK11 = layGiaTriInput("hlTbK11");
	    var hlTbK12 = layGiaTriInput("hlTbK12");

	    var kq = ((hlTbK12/hsK12)*100).toFixed(2);
	    $('#tlHlTbK12').val(kq);
	    var sum = hlTbK10+hlTbK11+hlTbK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlYeuK10 = layGiaTriInput("hlYeuK10");
	    var hlYeuK11 = layGiaTriInput("hlYeuK11");
	    var hlYeuK12 = layGiaTriInput("hlYeuK12");

	    var kq = ((hlYeuK12/hsK12)*100).toFixed(2);
	    $('#tlHlYeuK12').val(kq);
	    var sum = hlYeuK10+hlYeuK11+hlYeuK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK12').on('input', function() {
		var hsK12 = layGiaTriInput("hsK12");
		var tsHs = layGiaTriInput("tsHsThpt");
		var hlKemK10 = layGiaTriInput("hlKemK10");
	    var hlKemK11 = layGiaTriInput("hlKemK11");
	    var hlKemK12 = layGiaTriInput("hlKemK12");

	    var kq = ((hlKemK12/hsK12)*100).toFixed(2);
	    $('#tlHlKemK12').val(kq);
	    var sum = hlKemK10+hlKemK11+hlKemK12;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	//CSVC
	jQuery('#phKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phBanKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phTam').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var kq = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongHoc').val(kq);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
//		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvBanKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
//		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvTam').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
//		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
		    
		 var kq = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViec').val(kq);
		    
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
//	jQuery('#plvNhaCongVu').on('input', function() {
//		 var plvKienCo = layGiaTriInput("plvKienCo");
//		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
//		 var plvTam = layGiaTriInput("plvTam");
//		 var plvNhaCongVu = layGiaTriInput("plvNhaCongVu");
//		    
//		 var kq = plvKienCo+ plvBanKienCo+plvTam+plvNhaCongVu;
//		 $('#tsPhongLamViec').val(kq);
//		    
//		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
//		 $('#tsPhong').val(tsPhong);
//	});
	
	initForm();
	
	$('.kqhtc2').hide();
	$('.kqhtc3').hide();
	
	$('#checkc2').change(function() {
        if(this.checked) {
        	$('.kqhtc2').show();
        }else{
        	$('.kqhtc2').hide();
        }
    });
	
	$('#checkc3').change(function() {
        if(this.checked) {
        	$('.kqhtc3').show();
        }else{
        	$('.kqhtc3').hide();
        }
    });
});

function layGiaTriInput(tenInput){
	return $('#'+tenInput+'').val() != "" ? parseInt($('#'+tenInput+'').val()) : 0;
}

function checkChung(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	return true;
}

function check(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	var tongHsHkK10DaNhap = layGiaTriInput("hkTotK10")+layGiaTriInput("hkKhaK10")+layGiaTriInput("hkTbK10")+layGiaTriInput("hkYeuK10");
	if($('#hsK10').val() != tongHsHkK10DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 10 không khớp với tổng số học sinh Khối 10. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK11DaNhap = layGiaTriInput("hkTotK11")+layGiaTriInput("hkKhaK11")+layGiaTriInput("hkTbK11")+layGiaTriInput("hkYeuK11");
	if($('#hsK11').val() != tongHsHkK11DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 11 không khớp với tổng số học sinh Khối 11. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK12DaNhap = layGiaTriInput("hkTotK12")+layGiaTriInput("hkKhaK12")+layGiaTriInput("hkTbK12")+layGiaTriInput("hkYeuK12");
	if($('#hsK12').val() != tongHsHkK12DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 12 không khớp với tổng số học sinh Khối 12. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK10DaNhap = layGiaTriInput("hlGioiK10")+layGiaTriInput("hlKhaK10")+layGiaTriInput("hlTbK10")+layGiaTriInput("hlYeuK10")+layGiaTriInput("hlKemK10");
	if($('#hsK10').val() != tongHsHlK10DaNhap){
		showError('Thông báo','Số lượng học lực Khối 10 không khớp với tổng số học sinh Khối 10. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK11DaNhap = layGiaTriInput("hlGioiK11")+layGiaTriInput("hlKhaK11")+layGiaTriInput("hlTbK11")+layGiaTriInput("hlYeuK11")+layGiaTriInput("hlKemK11");
	if($('#hsK11').val() != tongHsHlK11DaNhap){
		showError('Thông báo','Số lượng học lực Khối 11 không khớp với tổng số học sinh Khối 11. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK12DaNhap = layGiaTriInput("hlGioiK12")+layGiaTriInput("hlKhaK12")+layGiaTriInput("hlTbK12")+layGiaTriInput("hlYeuK12")+layGiaTriInput("hlKemK12");
	if($('#hsK12').val() != tongHsHlK12DaNhap){
		showError('Thông báo','Số lượng học lực Khối 12 không khớp với tổng số học sinh Khối 12. Vui lòng kiểm tra lại!');
		return false;
	}
	
	return true;
}

function check_thcs(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	var tongHsHkK6DaNhap = layGiaTriInput("hkTotK6")+layGiaTriInput("hkKhaK6")+layGiaTriInput("hkTbK6")+layGiaTriInput("hkYeuK6");
	if($('#hsK6').val() != tongHsHkK6DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 6 không khớp với tổng số học sinh Khối 6. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK7DaNhap = layGiaTriInput("hkTotK7")+layGiaTriInput("hkKhaK7")+layGiaTriInput("hkTbK7")+layGiaTriInput("hkYeuK7");
	if($('#hsK7').val() != tongHsHkK7DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 7 không khớp với tổng số học sinh Khối 7. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK8DaNhap = layGiaTriInput("hkTotK8")+layGiaTriInput("hkKhaK8")+layGiaTriInput("hkTbK8")+layGiaTriInput("hkYeuK8");
	if($('#hsK8').val() != tongHsHkK8DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 8 không khớp với tổng số học sinh Khối 8. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK9DaNhap = layGiaTriInput("hkTotK9")+layGiaTriInput("hkKhaK9")+layGiaTriInput("hkTbK9")+layGiaTriInput("hkYeuK9");
	if($('#hsK9').val() != tongHsHkK9DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 9 không khớp với tổng số học sinh Khối 9. Vui lòng kiểm tra lại!');
		return false;
	}

	var tongHsHlK6DaNhap = layGiaTriInput("hlGioiK6")+layGiaTriInput("hlKhaK6")+layGiaTriInput("hlTbK6")+layGiaTriInput("hlYeuK6")+layGiaTriInput("hlKemK6");
	if($('#hsK6').val() != tongHsHlK6DaNhap){
		showError('Thông báo','Số lượng học lực Khối 6 không khớp với tổng số học sinh Khối 6. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK7DaNhap = layGiaTriInput("hlGioiK7")+layGiaTriInput("hlKhaK7")+layGiaTriInput("hlTbK7")+layGiaTriInput("hlYeuK7")+layGiaTriInput("hlKemK7");
	if($('#hsK7').val() != tongHsHlK7DaNhap){
		showError('Thông báo','Số lượng học lực Khối 7 không khớp với tổng số học sinh Khối 7. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK8DaNhap = layGiaTriInput("hlGioiK8")+layGiaTriInput("hlKhaK8")+layGiaTriInput("hlTbK8")+layGiaTriInput("hlYeuK8")+layGiaTriInput("hlKemK8");
	if($('#hsK8').val() != tongHsHlK8DaNhap){
		showError('Thông báo','Số lượng học lực Khối 8 không khớp với tổng số học sinh Khối 8. Vui lòng kiểm tra lại!');
		return false;
	}

	var tongHsHlK9DaNhap = layGiaTriInput("hlGioiK9")+layGiaTriInput("hlKhaK9")+layGiaTriInput("hlTbK9")+layGiaTriInput("hlYeuK9")+layGiaTriInput("hlKemK9");
	if($('#hsK9').val() != tongHsHlK9DaNhap){
		showError('Thông báo','Số lượng học lực Khối 9 không khớp với tổng số học sinh Khối 9. Vui lòng kiểm tra lại!');
		return false;
	}
	
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			
			// Return today's date and time
			var currentTime = new Date();

			// returns the month (from 0 to 11)
			var month = currentTime.getMonth() + 1;

			// returns the year (four digits)
			var year = currentTime.getFullYear();
			
			var namhoc=$.map(data.namhoc,function(obj){
				obj.id=obj.giaTri;
				obj.text=obj.giaTri;
				return obj;
			});
			
			
			$('#cmb-nam-hoc,#cmb-nam-hoc-filter').select2({
				data: namhoc
			});
			
			$('#cmb-nam-hoc').val(year);
		    $('#cmb-nam-hoc').select2().trigger('change');
		    
		    $('#cmb-thoi-gian').val(month);
		    $('#cmb-thang-filter').val(month);
		    
		    if($('#cmb-nam-hoc-filter').val() !=null){
				layDanhSach(0);
			}
			
			if($('#cmb-thang-filter').val() !=null){
				layDanhSach(0);
			}
			
			$('#guiBcSgd').val(0);
			
			if($('#idPhongGd').val() == 999){
				$('#cmb-thang-filter').css("display","true");
			}
			else{
				$('#cmb-thang-filter').css("display","none");
			}
			},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(),thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){

			$("#ds-1 > tbody > tr").remove();
			$("#ds-2 > tbody > tr").remove();
			$("#ds-3 > tbody > tr").remove();
			$("#ds-4 > tbody > tr").remove();
			$("#ds-5 > tbody > tr").remove();
			$("#ds-7 > tbody > tr").remove();

			var tr1 = "";
			var tr2 = "";
			var tr3 = "";
			var tr4 = "";
			var tr5 = "";
			var tr7 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
				$.each(data.content, function(i,row){
						tr1 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenTruong(row.idTruong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLopThpt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsHsThpt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK10) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK11) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK12) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsCbGvNv) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.cbql) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gv2) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gv3) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.nv) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  + getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
				})
		    
			    $.each(data.content, function(i,row){
					tr2 += "<tr>" +
					"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenTruong(row.idTruong) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tsHsThpt) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTot) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTot) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioi) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioi) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKem) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" +"<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
							'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
			    	tr3 += "<tr>" +
			    	"<td style='text-align:center'>" + (i+1) + "</td>" +
					"<td>" + getTenTruong(row.idTruong) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hsK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkTotK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkTotK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkKhaK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkKhaK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkTbK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkTbK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkYeuK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkYeuK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlGioiK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlGioiK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlKhaK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlKhaK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlTbK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlTbK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlYeuK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlYeuK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlKemK10) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlKemK10) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"   +
					'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
			    	tr4 += "<tr>" +
			    	"<td style='text-align:center'>" + (i+1) + "</td>" +
					"<td>" + getTenTruong(row.idTruong) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hsK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkTotK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkTotK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkKhaK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkKhaK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkTbK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkTbK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkYeuK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkYeuK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlGioiK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlGioiK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlKhaK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlKhaK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlTbK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlTbK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlYeuK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlYeuK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlKemK11) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlKemK11) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"   +
					'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
			    	tr5 += "<tr>" +
			    	"<td style='text-align:center'>" + (i+1) + "</td>" +
					"<td>" + getTenTruong(row.idTruong) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hsK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkTotK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkTotK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkKhaK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkKhaK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkTbK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkTbK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hkYeuK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHkYeuK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlGioiK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlGioiK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlKhaK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlKhaK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlTbK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlTbK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlYeuK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlYeuK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.hlKemK12) + "</td>" +
					"<td style='text-align:right'>" + removeNull(row.tlHlKemK12) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
					'</tr>';
			    })
			    
		     $.each(data.content, function(i,row){
				tr7 += "<tr>" +
				"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenTruong(row.idTruong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongLamViec) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvNhaCongVu) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthTinHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthNgoaiNgu) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthLy) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthHoa) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.pthSinh) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
						'</tr>';
		    })
		    
			}
			else{
				tr1 = "<tr><td colspan='27' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr2 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr3 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr4 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr5 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr7 = "<tr><td colspan='19' style='text-align: center'>Không có dữ liệu</td></tr>";
			}
		    
			$('#ds-1 > tbody:last-child').append(tr1);
			$('#ds-2 > tbody:last-child').append(tr2);
			$('#ds-3 > tbody:last-child').append(tr3);
			$('#ds-4 > tbody:last-child').append(tr4);
			$('#ds-5 > tbody:last-child').append(tr5);
			$('#ds-7 > tbody:last-child').append(tr7);
			
//			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
				if(loaiTruong == 34){
					jQuery(".row-edit-2").trigger('click');
				}
				$('#box-ds').boxWidget('expand');
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				
				if(loaiTruong == 34){
					showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+');jQuery(".row-del-2").trigger("click");');
				}else{
					showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
				}
			});
			
			$('.row-reset-pwd-1').click(function(e){
				e.preventDefault();
				if(loaiTruong == 34){
					showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCao('+$(this).attr('rid')+', 1);jQuery(".row-reset-pwd-4").trigger("click");' );
				}else{
					showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCao('+$(this).attr('rid')+', 1)' );
				}
				
			});
			
			$('.row-reset-pwd-2').click(function(e){
				e.preventDefault();
				if(loaiTruong == 99){
					showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Trường?', 'baoCao('+$(this).attr('rid')+', 0);jQuery(".row-reset-pwd-3").trigger("click");');
				}else{
					showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Trường?', 'baoCao('+$(this).attr('rid')+', 0)');
				}
				
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
			layDanhSachThcs(0, $('#idPhongGd').val(),$('#cmb-nam-hoc-filter').val(),$('#cmb-thang-filter').val());
		}
	});
}

function layDanhSachThcs(p, idDonVi, nam, thang){
	$.ajax({
		url:pref_url+'/lay-ds-thcs',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:idDonVi,nam:nam, thang:thang},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds-8 > tbody > tr").remove();
			$("#ds-9 > tbody > tr").remove();
			$("#ds-10 > tbody > tr").remove();
			$("#ds-11 > tbody > tr").remove();
			$("#ds-12 > tbody > tr").remove();
			$("#ds-13 > tbody > tr").remove();
			
			var tr8 = "";
			var tr9 = "";
			var tr10 = "";
			var tr11 = "";
			var tr12 = "";
			var tr13 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
			    
			    $.each(data.content, function(i,row){
					tr8 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenTruong(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTacThcs(idDv,row.guiBcSgd, row.id) +
							"</tr>";
			    })
			    
			    $.each(data.content, function(i,row){
					tr9 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenTruong(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
							"</tr>";
			    })
			    
			    $.each(data.content, function(i,row){
					tr10 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenTruong(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
							"</tr>";
			    })
			    
			    $.each(data.content, function(i,row){
					tr11 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenTruong(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
							"</tr>";
			    })
			    
			    $.each(data.content, function(i,row){
						tr12 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenTruong(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsHs) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK9) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
						'</tr>';
				})
		    
			    $.each(data.content, function(i,row){
					tr13 += "<tr>" +
					"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenTruong(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tsHs) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTot) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTot) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioi) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioi) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKem) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" +"<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +
							'</tr>';
			    })
		    
			}
			else{
				tr8 = "<tr><td colspan='22' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr9 = "<tr><td colspan='22' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr10 = "<tr><td colspan='22' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr11 = "<tr><td colspan='22' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr12 = "<tr><td colspan='14' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr13 = "<tr><td colspan='14' style='text-align: center'>Không có dữ liệu</td></tr>";
			}
		    
			$('#ds-8 > tbody:last-child').append(tr8);
			$('#ds-9 > tbody:last-child').append(tr9);
			$('#ds-10 > tbody:last-child').append(tr10);
			$('#ds-11 > tbody:last-child').append(tr11);
			$('#ds-12 > tbody:last-child').append(tr12);
			$('#ds-13 > tbody:last-child').append(tr13);
			
//			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-2').click(function(e){
				e.preventDefault();
				layChiTietThcs($(this).attr('rid'));
				$('#box-ds').boxWidget('expand');
			});
			
			$('.row-del-2').click(function(e){
				e.preventDefault();
//				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoaThcs('+$(this).attr('rid')+')');
				xoaThcs($(this).attr('rid'));
			});
			
			$('.row-reset-pwd-4').click(function(e){
				e.preventDefault();
//				showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCaoThcs('+$(this).attr('rid')+', 1)' );
				baoCaoThcs($(this).attr('rid'), 1);
				
			});
			
			$('.row-reset-pwd-3').click(function(e){
				e.preventDefault();
//				showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Phòng giáo dục?', 'baoCaoThcs('+$(this).attr('rid')+', 0)');
				baoCaoThcs($(this).attr('rid'), 0);
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				$('#box-ds').boxWidget('collapse');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luu_thcs(){
	$.ajax({
		url:pref_url+'/luu-thcs',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-2')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				$('#box-ds').boxWidget('collapse');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
	$('#id_thpt').val('');
	$('#id_thcs').val('');
	$('#nam_thcs').val('');
	$('#thang_thcs').val('');
	$('#guiBcSgd_thcs').val('');
	$('#cmb-nam-hoc').val(year).trigger('change');
	$('#cmb-thoi-gian').val(month);
	$('#lopK6').val('');
	$('#lopK7').val('');
	$('#lopK8').val('');
	$('#lopK9').val('');
	$('#lopK10').val('');
	$('#lopK11').val('');
	$('#lopK12').val('');
	$('#tsLopThcs').val('');
	$('#tsLopThpt').val('');
	$('#hsK6').val('');
	$('#hsK7').val('');
	$('#hsK8').val('');
	$('#hsK9').val('');
	$('#hsK10').val('');
	$('#hsK11').val('');
	$('#hsK12').val('');
	$('#tsHsThcs').val('');
	$('#tsHsThpt').val('');
	$('#cbql').val('');
	$('#gv2').val('');
	$('#gv3').val('');
	$('#nv').val('');
	$('#tsCbGvNv').val('');
	$('#guiBcSgd').val(0);
	$('#hkTot').val('');
	$('#tlHkTot').val('');
	$('#hkKha').val('');
	$('#tlHkKha').val('');
	$('#hkTb').val('');
	$('#tlHkTb').val('');
	$('#hkYeu').val('');
	$('#tlHkYeu').val('');
	$('#hlGioi').val('');
	$('#tlHlGioi').val('');
	$('#hlKha').val('');
	$('#tlHlKha').val('');
	$('#hlTb').val('');
	$('#tlHlTb').val('');
	$('#hlYeu').val('');
	$('#tlHlYeu').val('');
	$('#hlKem').val('');
	$('#tlHlKem').val('');
	$('#guiBcSgd').val('');
	$('#hkTotK10').val('');
	$('#tlHkTotK10').val('');
	$('#hkKhaK10').val('');
	$('#tlHkKhaK10').val('');
	$('#hkTbK10').val('');
	$('#tlHkTbK10').val('');
	$('#hkYeuK10').val('');
	$('#tlHkYeuK10').val('');
	$('#hlGioiK10').val('');
	$('#tlHlGioiK10').val('');
	$('#hlKhaK10').val('');
	$('#tlHlKhaK10').val('');
	$('#hlTbK10').val('');
	$('#tlHlTbK10').val('');
	$('#hlYeuK10').val('');
	$('#tlHlYeuK10').val('');
	$('#hlKemK10').val('');
	$('#tlHlKemK10').val('');
	$('#hkTotK11').val('');
	$('#tlHkTotK11').val('');
	$('#hkKhaK11').val('');
	$('#tlHkKhaK11').val('');
	$('#hkTbK11').val('');
	$('#tlHkTbK11').val('');
	$('#hkYeuK11').val('');
	$('#tlHkYeuK11').val('');
	$('#hlGioiK11').val('');
	$('#tlHlGioiK11').val('');
	$('#hlKhaK11').val('');
	$('#tlHlKhaK11').val('');
	$('#hlTbK11').val('');
	$('#tlHlTbK11').val('');
	$('#hlYeuK11').val('');
	$('#tlHlYeuK11').val('');
	$('#hlKemK11').val('');
	$('#tlHlKemK11').val('');
	$('#hkTotK12').val('');
	$('#tlHkTotK12').val('');
	$('#hkKhaK12').val('');
	$('#tlHkKhaK12').val('');
	$('#hkTbK12').val('');
	$('#tlHkTbK12').val('');
	$('#hkYeuK12').val('');
	$('#tlHkYeuK12').val('');
	$('#hlGioiK12').val('');
	$('#tlHlGioiK12').val('');
	$('#hlKhaK12').val('');
	$('#tlHlKhaK12').val('');
	$('#hlTbK12').val('');
	$('#tlHlTbK12').val('');
	$('#hlYeuK12').val('');
	$('#tlHlYeuK12').val('');
	$('#hlKemK12').val('');
	$('#tlHlKemK12').val('');
	
	$('#hsK10_1').val('');
	$('#hsK11_1').val('');
	$('#hsK12_1').val('');
	$('#tsHs_1').val('');
	
	$('#tsPhong').val('');
	$('#tsPhongHoc').val('');
	$('#phKienCo').val('');
	$('#phBanKienCo').val('');
	$('#phTam').val('');
	$('#tsPhongLamViec').val('');
	$('#plvKienCo').val('');
	$('#plvBanKienCo').val('');
	$('#plvTam').val('');
	$('#plvNhaCongVu').val('');
	$('#pthTinHoc').val('');
	$('#pthNgoaiNgu').val('');
	$('#pthLy').val('');
	$('#pthHoa').val('');
	$('#pthSinh').val('');
	$('#box-ds').boxWidget('collapse');
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id_thpt').val(data.id);
			$('#cmb-nam-hoc').val(data.nam).trigger('change');
			$('#cmb-thoi-gian').val(data.thang);
			$('#truong').val(data.truong);
			$('#lopK6').val(data.lopK6);
			$('#lopK7').val(data.lopK7);
			$('#lopK8').val(data.lopK8);
			$('#lopK9').val(data.lopK9);
			$('#lopK10').val(data.lopK10);
			$('#lopK11').val(data.lopK11);
			$('#lopK12').val(data.lopK12);
			$('#tsLopThcs').val(data.tsLopThcs);
			$('#tsLopThpt').val(data.tsLopThpt);
			$('#hsK6').val(data.hsK6);
			$('#hsK7').val(data.hsK7);
			$('#hsK8').val(data.hsK8);
			$('#hsK9').val(data.hsK9);
			$('#hsK10').val(data.hsK10);
			$('#hsK11').val(data.hsK11);
			$('#hsK12').val(data.hsK12);
			$('#tsHsThcs').val(data.tsHsThcs);
			$('#tsHsThpt').val(data.tsHsThpt);
			$('#cbql').val(data.cbql);
			$('#gv2').val(data.gv2);
			$('#gv3').val(data.gv3);
			$('#nv').val(data.nv);
			$('#tsCbGvNv').val(data.tsCbGvNv);
			$('#guiBcSgd').val(data.guiBcSgd);
			$('#hkTot').val(data.hkTot);
			$('#tlHkTot').val(data.tlHkTot);
			$('#hkKha').val(data.hkKha);
			$('#tlHkKha').val(data.tlHkKha);
			$('#hkTb').val(data.hkTb);
			$('#tlHkTb').val(data.tlHkTb);
			$('#hkYeu').val(data.hkYeu);
			$('#tlHkYeu').val(data.tlHkYeu);
			$('#hlGioi').val(data.hlGioi);
			$('#tlHlGioi').val(data.tlHlGioi);
			$('#hlKha').val(data.hlKha);
			$('#tlHlKha').val(data.tlHlKha);
			$('#hlTb').val(data.hlTb);
			$('#tlHlTb').val(data.tlHlTb);
			$('#hlYeu').val(data.hlYeu);
			$('#tlHlYeu').val(data.tlHlYeu);
			$('#hlKem').val(data.hlKem);
			$('#tlHlKem').val(data.tlHlKem);
			$('#guiBcSgd').val(data.guiBcSgd);
			$('#hkTotK10').val(data.hkTotK10);
			$('#tlHkTotK10').val(data.tlHkTotK10);
			$('#hkKhaK10').val(data.hkKhaK10);
			$('#tlHkKhaK10').val(data.tlHkKhaK10);
			$('#hkTbK10').val(data.hkTbK10);
			$('#tlHkTbK10').val(data.tlHkTbK10);
			$('#hkYeuK10').val(data.hkYeuK10);
			$('#tlHkYeuK10').val(data.tlHkYeuK10);
			$('#hlGioiK10').val(data.hlGioiK10);
			$('#tlHlGioiK10').val(data.tlHlGioiK10);
			$('#hlKhaK10').val(data.hlKhaK10);
			$('#tlHlKhaK10').val(data.tlHlKhaK10);
			$('#hlTbK10').val(data.hlTbK10);
			$('#tlHlTbK10').val(data.tlHlTbK10);
			$('#hlYeuK10').val(data.hlYeuK10);
			$('#tlHlYeuK10').val(data.tlHlYeuK10);
			$('#hlKemK10').val(data.hlKemK10);
			$('#tlHlKemK10').val(data.tlHlKemK10);
			$('#hkTotK11').val(data.hkTotK11);
			$('#tlHkTotK11').val(data.tlHkTotK11);
			$('#hkKhaK11').val(data.hkKhaK11);
			$('#tlHkKhaK11').val(data.tlHkKhaK11);
			$('#hkTbK11').val(data.hkTbK11);
			$('#tlHkTbK11').val(data.tlHkTbK11);
			$('#hkYeuK11').val(data.hkYeuK11);
			$('#tlHkYeuK11').val(data.tlHkYeuK11);
			$('#hlGioiK11').val(data.hlGioiK11);
			$('#tlHlGioiK11').val(data.tlHlGioiK11);
			$('#hlKhaK11').val(data.hlKhaK11);
			$('#tlHlKhaK11').val(data.tlHlKhaK11);
			$('#hlTbK11').val(data.hlTbK11);
			$('#tlHlTbK11').val(data.tlHlTbK11);
			$('#hlYeuK11').val(data.hlYeuK11);
			$('#tlHlYeuK11').val(data.tlHlYeuK11);
			$('#hlKemK11').val(data.hlKemK11);
			$('#tlHlKemK11').val(data.tlHlKemK11);
			$('#hkTotK12').val(data.hkTotK12);
			$('#tlHkTotK12').val(data.tlHkTotK12);
			$('#hkKhaK12').val(data.hkKhaK12);
			$('#tlHkKhaK12').val(data.tlHkKhaK12);
			$('#hkTbK12').val(data.hkTbK12);
			$('#tlHkTbK12').val(data.tlHkTbK12);
			$('#hkYeuK12').val(data.hkYeuK12);
			$('#tlHkYeuK12').val(data.tlHkYeuK12);
			$('#hlGioiK12').val(data.hlGioiK12);
			$('#tlHlGioiK12').val(data.tlHlGioiK12);
			$('#hlKhaK12').val(data.hlKhaK12);
			$('#tlHlKhaK12').val(data.tlHlKhaK12);
			$('#hlTbK12').val(data.hlTbK12);
			$('#tlHlTbK12').val(data.tlHlTbK12);
			$('#hlYeuK12').val(data.hlYeuK12);
			$('#tlHlYeuK12').val(data.tlHlYeuK12);
			$('#hlKemK12').val(data.hlKemK12);
			$('#tlHlKemK12').val(data.tlHlKemK12);
			$('#hsK10_1').val(data.hsK10);
			$('#hsK11_1').val(data.hsK11);
			$('#hsK12_1').val(data.hsK12);
			$('#tsHs_1').val(data.tsHsThpt);
			$('#tsPhong').val(data.tsPhong);
			$('#tsPhongHoc').val(data.tsPhongHoc);
			$('#phKienCo').val(data.phKienCo);
			$('#phBanKienCo').val(data.phBanKienCo);
			$('#phTam').val(data.phTam);
			$('#tsPhongLamViec').val(data.tsPhongLamViec);
			$('#plvKienCo').val(data.plvKienCo);
			$('#plvBanKienCo').val(data.plvBanKienCo);
			$('#plvTam').val(data.plvTam);
			$('#plvNhaCongVu').val(data.plvNhaCongVu);
			$('#pthTinHoc').val(data.pthTinHoc);
			$('#pthNgoaiNgu').val(data.pthNgoaiNgu);
			$('#pthLy').val(data.pthLy);
			$('#pthHoa').val(data.pthHoa);
			$('#pthSinh').val(data.pthSinh);
			if(data.hkTotK10 != null || data.hlGioiK10 != null){
				$('#checkc3').prop('checked', true);
				$('#checkc3').trigger("change");
			}else{
				$('#checkc3').prop('checked', false);
				$('#checkc3').trigger("change");
			}
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layChiTietThcs(id){
	$.ajax({
		url:pref_url+'/lay-ct-thcs',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id_thcs').val(data.id);
			$('#cmb-nam-hoc').val(data.nam).trigger('change');
			$('#cmb-thoi-gian').val(data.thang);
			$('#truong').val(data.truong);
			$('#lopK6').val(data.lopK6);
			$('#lopK7').val(data.lopK7);
			$('#lopK8').val(data.lopK8);
			$('#lopK9').val(data.lopK9);
			$('#tsLop').val(data.tsLop);
			$('#hsK6').val(data.hsK6);
			$('#hsK7').val(data.hsK7);
			$('#hsK8').val(data.hsK8);
			$('#hsK9').val(data.hsK9);
			$('#tsHs').val(data.tsHs);
			$('#guiBcSgd_thsc').val(data.guiBcSgd);
			$('#hkTot_thcs').val(data.hkTot);
			$('#tlHkTot_thcs').val(data.tlHkTot);
			$('#hkKha_thcs').val(data.hkKha);
			$('#tlHkKha_thcs').val(data.tlHkKha);
			$('#hkTb_thcs').val(data.hkTb);
			$('#tlHkTb_thcs').val(data.tlHkTb);
			$('#hkYeu_thcs').val(data.hkYeu);
			$('#tlHkYeu_thcs').val(data.tlHkYeu);
			$('#hlGioi_thcs').val(data.hlGioi);
			$('#tlHlGioi_thcs').val(data.tlHlGioi);
			$('#hlKha_thcs').val(data.hlKha);
			$('#tlHlKha_thcs').val(data.tlHlKha);
			$('#hlTb_thcs').val(data.hlTb);
			$('#tlHlTb_thcs').val(data.tlHlTb);
			$('#hlYeu_thcs').val(data.hlYeu);
			$('#tlHlYeu_thcs').val(data.tlHlYeu);
			$('#hlKem_thcs').val(data.hlKem);
			$('#tlHlKem_thcs').val(data.tlHlKem);
			$('#guiBcSgd_thcs').val(data.guiBcSgd);
			$('#hkTotK6').val(data.hkTotK6);
			$('#tlHkTotK6').val(data.tlHkTotK6);
			$('#hkKhaK6').val(data.hkKhaK6);
			$('#tlHkKhaK6').val(data.tlHkKhaK6);
			$('#hkTbK6').val(data.hkTbK6);
			$('#tlHkTbK6').val(data.tlHkTbK6);
			$('#hkYeuK6').val(data.hkYeuK6);
			$('#tlHkYeuK6').val(data.tlHkYeuK6);
			$('#hlGioiK6').val(data.hlGioiK6);
			$('#tlHlGioiK6').val(data.tlHlGioiK6);
			$('#hlKhaK6').val(data.hlKhaK6);
			$('#tlHlKhaK6').val(data.tlHlKhaK6);
			$('#hlTbK6').val(data.hlTbK6);
			$('#tlHlTbK6').val(data.tlHlTbK6);
			$('#hlYeuK6').val(data.hlYeuK6);
			$('#tlHlYeuK6').val(data.tlHlYeuK6);
			$('#hlKemK6').val(data.hlKemK6);
			$('#tlHlKemK6').val(data.tlHlKemK6);
			$('#hkTotK7').val(data.hkTotK7);
			$('#tlHkTotK7').val(data.tlHkTotK7);
			$('#hkKhaK7').val(data.hkKhaK7);
			$('#tlHkKhaK7').val(data.tlHkKhaK7);
			$('#hkTbK7').val(data.hkTbK7);
			$('#tlHkTbK7').val(data.tlHkTbK7);
			$('#hkYeuK7').val(data.hkYeuK7);
			$('#tlHkYeuK7').val(data.tlHkYeuK7);
			$('#hlGioiK7').val(data.hlGioiK7);
			$('#tlHlGioiK7').val(data.tlHlGioiK7);
			$('#hlKhaK7').val(data.hlKhaK7);
			$('#tlHlKhaK7').val(data.tlHlKhaK7);
			$('#hlTbK7').val(data.hlTbK7);
			$('#tlHlTbK7').val(data.tlHlTbK7);
			$('#hlYeuK7').val(data.hlYeuK7);
			$('#tlHlYeuK7').val(data.tlHlYeuK7);
			$('#hlKemK7').val(data.hlKemK7);
			$('#tlHlKemK7').val(data.tlHlKemK7);
			$('#hkTotK8').val(data.hkTotK8);
			$('#tlHkTotK8').val(data.tlHkTotK8);
			$('#hkKhaK8').val(data.hkKhaK8);
			$('#tlHkKhaK8').val(data.tlHkKhaK8);
			$('#hkTbK8').val(data.hkTbK8);
			$('#tlHkTbK8').val(data.tlHkTbK8);
			$('#hkYeuK8').val(data.hkYeuK8);
			$('#tlHkYeuK8').val(data.tlHkYeuK8);
			$('#hlGioiK8').val(data.hlGioiK8);
			$('#tlHlGioiK8').val(data.tlHlGioiK8);
			$('#hlKhaK8').val(data.hlKhaK8);
			$('#tlHlKhaK8').val(data.tlHlKhaK8);
			$('#hlTbK8').val(data.hlTbK8);
			$('#tlHlTbK8').val(data.tlHlTbK8);
			$('#hlYeuK8').val(data.hlYeuK8);
			$('#tlHlYeuK8').val(data.tlHlYeuK8);
			$('#hlKemK8').val(data.hlKemK8);
			$('#tlHlKemK8').val(data.tlHlKemK8);
			$('#hkTotK9').val(data.hkTotK9);
			$('#tlHkTotK9').val(data.tlHkTotK9);
			$('#hkKhaK9').val(data.hkKhaK9);
			$('#tlHkKhaK9').val(data.tlHkKhaK9);
			$('#hkTbK9').val(data.hkTbK9);
			$('#tlHkTbK9').val(data.tlHkTbK9);
			$('#hkYeuK9').val(data.hkYeuK9);
			$('#tlHkYeuK9').val(data.tlHkYeuK9);
			$('#hlGioiK9').val(data.hlGioiK9);
			$('#tlHlGioiK9').val(data.tlHlGioiK9);
			$('#hlKhaK9').val(data.hlKhaK9);
			$('#tlHlKhaK9').val(data.tlHlKhaK9);
			$('#hlTbK9').val(data.hlTbK9);
			$('#tlHlTbK9').val(data.tlHlTbK9);
			$('#hlYeuK9').val(data.hlYeuK9);
			$('#tlHlYeuK9').val(data.tlHlYeuK9);
			$('#hlKemK9').val(data.hlKemK9);
			$('#tlHlKemK9').val(data.tlHlKemK9);
			$('#hsK6_1_thcs').val(data.hsK6);
			$('#hsK7_1_thcs').val(data.hsK7);
			$('#hsK8_1_thcs').val(data.hsK8);
			$('#hsK9_1_thcs').val(data.hsK9);
			$('#tsHs_1_thcs').val(data.tsHs);
			if(data.hkTotK6 != null || data.hlGioiK6 != null){
				$('#checkc2').prop('checked', true);
				$('#checkc2').trigger("change");
			}
			else{
				$('#checkc2').prop('checked', false);
				$('#checkc2').trigger("change");
			}
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCao(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 999){
					toastInfo('Trả báo cáo số liệu cho Trường thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 999){
				showError('Thông báo','Trả báo cáo số liệu cho Trường không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function xoaThcs(id){
	$.ajax({
		url:pref_url+'/xoa-thcs',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCaoThcs(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao-thcs',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 999){
					toastInfo('Trả báo cáo số liệu cho Trường thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 999){
				showError('Thông báo','Trả báo cáo số liệu cho Trường không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}
