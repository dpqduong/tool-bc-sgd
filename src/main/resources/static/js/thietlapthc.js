var pref_url="/thiet-lap-thc";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'nam',title:'Năm học'},
			{name:'thangDauNam',title:'Tháng đầu năm'},
			{name:'thangCuoiNam',title:'Tháng cuối năm'},
		],
		extclass:'tbl-active-row',
		onRowSelected:function(obj){
			layChiTiet(obj.id);
		}
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	layDanhSach(0);
});

function check(){
	if($('#txt-id').val()===''){
		showError('Thông báo','Vui lòng chọn năm học để cập nhật!');
		return false;
	}
	if($('#txt-giaTri').val()===''){
		showError('Thông báo','Vui lòng nhập giá trị tham số!');
		return false;
	}
	return true;
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function huy(){
	$('#txt-idThamSo').val('');
	$('#txt-tenThamSo').val('');
	$('#txt-giaTri').val('');
	$('#grid-ds').data('jdgrid').clearSelectedRow();
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#txt-id').val(data.id);
			$('#txt-nam').val(data.nam);
			$('#cmb-thang-dau-nam').val(data.thangDauNam);
			$('#cmb-thang-cuoi-nam').val(data.thangCuoiNam);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}