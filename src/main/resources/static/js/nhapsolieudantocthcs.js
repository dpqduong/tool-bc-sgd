var pref_url="/nhap-so-lieu-dan-toc-thcs";
const pgd = [
    { id: '1', name: 'PGD&ĐT TP. Cà Mau' }, 
    { id: '2', name: 'PGD&ĐT Thới Bình' }, 
    { id: '3', name: 'PGD&ĐT U Minh' }, 
    { id: '4', name: 'PGD&ĐT Trần Văn Thời' }, 
    { id: '5', name: 'PGD&ĐT Cái Nước' }, 
    { id: '6', name: 'PGD&ĐT Phú Tân' },
    { id: '7', name: 'PGD&ĐT Đầm Dơi' },
    { id: '8', name: 'PGD&ĐT Năm Căn' },
    { id: '9', name: 'PGD&ĐT Ngọc Hiển' }
];

function getTenPgd(id){
	var tenPgd =  pgd.filter(function(item) {
		return item.id == id;
	});
	var result = tenPgd.map(({ name }) => name)[0];
	return result == undefined ?"":  result;
}

function getBtnThaoTac(idDonVi, tt, id){
	var btn = "";
	if(idDonVi == 10 || idDonVi == 11){
		var btn = tt === 1 ? "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-2' rid="+id+" title='Trả báo cáo Phòng GD'><i class='fa fa-reply-all'></i></a>" : "<td></td>";
	}else{
		var btn = tt === 1 ? "<td></td>" : "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-1' rid="+id+" title='Gửi báo cáo Sở GD'><i class='fa fa-reply-all'></i></a>&nbsp;&nbsp;<a href='#' class='row-edit-1' rid="+id+" title='Sửa' style=''><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='row-del-1 text-danger' rid="+id+" title='Xóa'><i class='fa fa-trash-o'></i></a></td>";
	}
	return btn;
}

function removeNull(val){
	return val == null ? "" : val; 
}

$(document).ready(function(){
	$('[data-toggle="push-menu"]').pushMenu('toggle');
	
	$('input').keyup(function(e)
            {
		if (/\D/g.test(this.value))
		{
		// Filter non-digits from input value.
		this.value = this.value.replace(/\D/g, '');
		}
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#cmb-nam-hoc-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#cmb-thang-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-ok').click(function(){
		if($('#checkc2').is(':checked')){
			if(check()){
				luu();
			}
		}else{
			luu();
		}
		
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#showBox').click(function(){
		$('#box-ds').boxWidget('toggle');
	});
	
	$('#showCt').click(function(){
		$('#box-ct').boxWidget('toggle');
	});
	
	//So lieu lop
	jQuery('#lopK6').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK7').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK8').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	jQuery('#lopK9').on('input', function() {
		var lopK6 = layGiaTriInput("lopK6");
	    var lopK7 = layGiaTriInput("lopK7");
	    var lopK8 = layGiaTriInput("lopK8");
	    var lopK9 = layGiaTriInput("lopK9");
	    var kq = lopK6 + lopK7 + lopK8 + lopK9;
	    $('#tsLop').val(kq);
	});
	
	//So lieu hoc sinh
	jQuery('#hsK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9;
	    $('#tsHs').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK6_1').val(hsK6);
	});
	
	jQuery('#hsK7').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9;
	    $('#tsHs').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK7_1').val(hsK7);
	});
	
	jQuery('#hsK8').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9 ;
	    $('#tsHs').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK8_1').val(hsK8);
	});
	
	jQuery('#hsK9').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
	    var hsK7 = layGiaTriInput("hsK7");
	    var hsK8 = layGiaTriInput("hsK8");
	    var hsK9 = layGiaTriInput("hsK9");
	    var kq = hsK6 + hsK7 + hsK8 + hsK9;
	    $('#tsHs').val(kq);
	    $('#tsHs_1').val(kq);
	    $('#hsK9_1').val(hsK9);
	});
	
	//Hanh Kiem'
	jQuery('#hkTotK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK6/hsK6)*100).toFixed(2);
	    $('#tlHkTotK6').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	    
	});
	
	jQuery('#hkKhaK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK6/hsK6)*100).toFixed(2);
	    $('#tlHkKhaK6').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK6/hsK6)*100).toFixed(2);
	    $('#tlHkTbK6').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK6/hsK6)*100).toFixed(2);
	    $('#tlHkYeuK6').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	jQuery('#hkTotK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK7/hsK7)*100).toFixed(2);
	    $('#tlHkTotK7').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	    
	});
	
	jQuery('#hkKhaK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK7/hsK7)*100).toFixed(2);
	    $('#tlHkKhaK7').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK7/hsK7)*100).toFixed(2);
	    $('#tlHkTbK7').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK7/hsK7)*100).toFixed(2);
	    $('#tlHkYeuK7').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	jQuery('#hkTotK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK8/hsK8)*100).toFixed(2);
	    $('#tlHkTotK8').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	});
	
	jQuery('#hkKhaK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK8/hsK8)*100).toFixed(2);
	    $('#tlHkKhaK8').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK8/hsK8)*100).toFixed(2);
	    $('#tlHkTbK8').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK8/hsK8)*100).toFixed(2);
	    $('#tlHkYeuK8').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	jQuery('#hkTotK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkTotK6 = layGiaTriInput("hkTotK6");
	    var hkTotK7 = layGiaTriInput("hkTotK7");
	    var hkTotK8 = layGiaTriInput("hkTotK8");
	    var hkTotK9 = layGiaTriInput("hkTotK9");
	    var kq = ((hkTotK9/hsK9)*100).toFixed(2);
	    $('#tlHkTotK9').val(kq);
	    var sum = hkTotK6+hkTotK7+hkTotK8+hkTotK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTot').val(sum);
	    $('#tlHkTot').val(kqt);
	});
	
	jQuery('#hkKhaK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkKhaK6 = layGiaTriInput("hkKhaK6");
	    var hkKhaK7 = layGiaTriInput("hkKhaK7");
	    var hkKhaK8 = layGiaTriInput("hkKhaK8");
	    var hkKhaK9 = layGiaTriInput("hkKhaK9");
	    var kq = ((hkKhaK9/hsK9)*100).toFixed(2);
	    $('#tlHkKhaK9').val(kq);
	    var sum = hkKhaK6+hkKhaK7+hkKhaK8+hkKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkKha').val(sum);
	    $('#tlHkKha').val(kqt);
	});
	
	jQuery('#hkTbK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkTbK6 = layGiaTriInput("hkTbK6");
	    var hkTbK7 = layGiaTriInput("hkTbK7");
	    var hkTbK8 = layGiaTriInput("hkTbK8");
	    var hkTbK9 = layGiaTriInput("hkTbK9");
	    var kq = ((hkTbK9/hsK9)*100).toFixed(2);
	    $('#tlHkTbK9').val(kq);
	    var sum = hkTbK6+hkTbK7+hkTbK8+hkTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkTb').val(sum);
	    $('#tlHkTb').val(kqt);
	});
	
	jQuery('#hkYeuK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hkYeuK6 = layGiaTriInput("hkYeuK6");
	    var hkYeuK7 = layGiaTriInput("hkYeuK7");
	    var hkYeuK8 = layGiaTriInput("hkYeuK8");
	    var hkYeuK9 = layGiaTriInput("hkYeuK9");
	    var kq = ((hkYeuK9/hsK9)*100).toFixed(2);
	    $('#tlHkYeuK9').val(kq);
	    var sum = hkYeuK6+hkYeuK7+hkYeuK8+hkYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hkYeu').val(sum);
	    $('#tlHkYeu').val(kqt);
	});
	
	//Hoc luc
	jQuery('#hlGioiK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK6/hsK6)*100).toFixed(2);
	    $('#tlHlGioiK6').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	    
	});
	
	jQuery('#hlKhaK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK6/hsK6)*100).toFixed(2);
	    $('#tlHlKhaK6').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK6/hsK6)*100).toFixed(2);
	    $('#tlHlTbK6').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK6/hsK6)*100).toFixed(2);
	    $('#tlHlYeuK6').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK6').on('input', function() {
		var hsK6 = layGiaTriInput("hsK6");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK6/hsK6)*100).toFixed(2);
	    $('#tlHlKemK6').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	
	jQuery('#hlGioiK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK7/hsK7)*100).toFixed(2);
	    $('#tlHlGioiK7').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	    
	});
	
	jQuery('#hlKhaK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK7/hsK7)*100).toFixed(2);
	    $('#tlHlKhaK7').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK7/hsK7)*100).toFixed(2);
	    $('#tlHlTbK7').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK7/hsK7)*100).toFixed(2);
	    $('#tlHlYeuK7').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK7').on('input', function() {
		var hsK7 = layGiaTriInput("hsK7");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK7/hsK7)*100).toFixed(2);
	    $('#tlHlKemK7').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	
	jQuery('#hlGioiK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK8/hsK8)*100).toFixed(2);
	    $('#tlHlGioiK8').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	});
	
	jQuery('#hlKhaK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK8/hsK8)*100).toFixed(2);
	    $('#tlHlKhaK8').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK8/hsK8)*100).toFixed(2);
	    $('#tlHlTbK8').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK8/hsK8)*100).toFixed(2);
	    $('#tlHlYeuK8').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK8').on('input', function() {
		var hsK8 = layGiaTriInput("hsK8");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK8/hsK8)*100).toFixed(2);
	    $('#tlHlKemK8').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	
	jQuery('#hlGioiK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlGioiK6 = layGiaTriInput("hlGioiK6");
	    var hlGioiK7 = layGiaTriInput("hlGioiK7");
	    var hlGioiK8 = layGiaTriInput("hlGioiK8");
	    var hlGioiK9 = layGiaTriInput("hlGioiK9");
	    var kq = ((hlGioiK9/hsK9)*100).toFixed(2);
	    $('#tlHlGioiK9').val(kq);
	    var sum = hlGioiK6+hlGioiK7+hlGioiK8+hlGioiK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlGioi').val(sum);
	    $('#tlHlGioi').val(kqt);
	});
	
	jQuery('#hlKhaK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlKhaK6 = layGiaTriInput("hlKhaK6");
	    var hlKhaK7 = layGiaTriInput("hlKhaK7");
	    var hlKhaK8 = layGiaTriInput("hlKhaK8");
	    var hlKhaK9 = layGiaTriInput("hlKhaK9");
	    var kq = ((hlKhaK9/hsK9)*100).toFixed(2);
	    $('#tlHlKhaK9').val(kq);
	    var sum = hlKhaK6+hlKhaK7+hlKhaK8+hlKhaK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKha').val(sum);
	    $('#tlHlKha').val(kqt);
	});
	
	jQuery('#hlTbK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlTbK6 = layGiaTriInput("hlTbK6");
	    var hlTbK7 = layGiaTriInput("hlTbK7");
	    var hlTbK8 = layGiaTriInput("hlTbK8");
	    var hlTbK9 = layGiaTriInput("hlTbK9");
	    var kq = ((hlTbK9/hsK9)*100).toFixed(2);
	    $('#tlHlTbK9').val(kq);
	    var sum = hlTbK6+hlTbK7+hlTbK8+hlTbK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlTb').val(sum);
	    $('#tlHlTb').val(kqt);
	});
	
	jQuery('#hlYeuK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlYeuK6 = layGiaTriInput("hlYeuK6");
	    var hlYeuK7 = layGiaTriInput("hlYeuK7");
	    var hlYeuK8 = layGiaTriInput("hlYeuK8");
	    var hlYeuK9 = layGiaTriInput("hlYeuK9");
	    var kq = ((hlYeuK9/hsK9)*100).toFixed(2);
	    $('#tlHlYeuK9').val(kq);
	    var sum = hlYeuK6+hlYeuK7+hlYeuK8+hlYeuK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlYeu').val(sum);
	    $('#tlHlYeu').val(kqt);
	});
	
	jQuery('#hlKemK9').on('input', function() {
		var hsK9 = layGiaTriInput("hsK9");
		var tsHs = layGiaTriInput("tsHs");
		var hlKemK6 = layGiaTriInput("hlKemK6");
	    var hlKemK7 = layGiaTriInput("hlKemK7");
	    var hlKemK8 = layGiaTriInput("hlKemK8");
	    var hlKemK9 = layGiaTriInput("hlKemK9");
	    var kq = ((hlKemK9/hsK9)*100).toFixed(2);
	    $('#tlHlKemK9').val(kq);
	    var sum = hlKemK6+hlKemK7+hlKemK8+hlKemK9 ;
	    var kqt = ((sum/tsHs)*100).toFixed(2);
	    $('#hlKem').val(sum);
	    $('#tlHlKem').val(kqt);
	});
	
	initForm();
	
	$('.kqhtc2').hide();
	
	$('#checkc2').change(function() {
        if(this.checked) {
        	$('.kqhtc2').show();
        }else{
        	$('.kqhtc2').hide();
        }
    });
});

function layGiaTriInput(tenInput){
	return $('#'+tenInput+'').val() != "" ? parseInt($('#'+tenInput+'').val()) : 0;
}

function check(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	
	var tongHsHkK6DaNhap = layGiaTriInput("hkTotK6")+layGiaTriInput("hkKhaK6")+layGiaTriInput("hkTbK6")+layGiaTriInput("hkYeuK6");
	if($('#hsK6').val() != tongHsHkK6DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 6 không khớp với tổng số học sinh Khối 6. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK7DaNhap = layGiaTriInput("hkTotK7")+layGiaTriInput("hkKhaK7")+layGiaTriInput("hkTbK7")+layGiaTriInput("hkYeuK7");
	if($('#hsK7').val() != tongHsHkK7DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 7 không khớp với tổng số học sinh Khối 7. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK8DaNhap = layGiaTriInput("hkTotK8")+layGiaTriInput("hkKhaK8")+layGiaTriInput("hkTbK8")+layGiaTriInput("hkYeuK8");
	if($('#hsK8').val() != tongHsHkK8DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 8 không khớp với tổng số học sinh Khối 8. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHkK9DaNhap = layGiaTriInput("hkTotK9")+layGiaTriInput("hkKhaK9")+layGiaTriInput("hkTbK9")+layGiaTriInput("hkYeuK9");
	if($('#hsK9').val() != tongHsHkK9DaNhap){
		showError('Thông báo','Số lượng hạnh kiểm Khối 9 không khớp với tổng số học sinh Khối 9. Vui lòng kiểm tra lại!');
		return false;
	}

	var tongHsHlK6DaNhap = layGiaTriInput("hlGioiK6")+layGiaTriInput("hlKhaK6")+layGiaTriInput("hlTbK6")+layGiaTriInput("hlYeuK6")+layGiaTriInput("hlKemK6");
	if($('#hsK6').val() != tongHsHlK6DaNhap){
		showError('Thông báo','Số lượng học lực Khối 6 không khớp với tổng số học sinh Khối 6. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK7DaNhap = layGiaTriInput("hlGioiK7")+layGiaTriInput("hlKhaK7")+layGiaTriInput("hlTbK7")+layGiaTriInput("hlYeuK7")+layGiaTriInput("hlKemK7");
	if($('#hsK7').val() != tongHsHlK7DaNhap){
		showError('Thông báo','Số lượng học lực Khối 7 không khớp với tổng số học sinh Khối 7. Vui lòng kiểm tra lại!');
		return false;
	}
	
	var tongHsHlK8DaNhap = layGiaTriInput("hlGioiK8")+layGiaTriInput("hlKhaK8")+layGiaTriInput("hlTbK8")+layGiaTriInput("hlYeuK8")+layGiaTriInput("hlKemK8");
	if($('#hsK8').val() != tongHsHlK8DaNhap){
		showError('Thông báo','Số lượng học lực Khối 8 không khớp với tổng số học sinh Khối 8. Vui lòng kiểm tra lại!');
		return false;
	}

	var tongHsHlK9DaNhap = layGiaTriInput("hlGioiK9")+layGiaTriInput("hlKhaK9")+layGiaTriInput("hlTbK9")+layGiaTriInput("hlYeuK9")+layGiaTriInput("hlKemK9");
	if($('#hsK9').val() != tongHsHlK9DaNhap){
		showError('Thông báo','Số lượng học lực Khối 9 không khớp với tổng số học sinh Khối 9. Vui lòng kiểm tra lại!');
		return false;
	}
	
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			
			// Return today's date and time
			var currentTime = new Date();

			// returns the month (from 0 to 11)
			var month = currentTime.getMonth() + 1;

			// returns the year (four digits)
			var year = currentTime.getFullYear();
			
			var namhoc=$.map(data.namhoc,function(obj){
				obj.id=obj.giaTri;
				obj.text=obj.giaTri;
				return obj;
			});
			
			
			$('#cmb-nam-hoc,#cmb-nam-hoc-filter').select2({
				data: namhoc
			});
			
			$('#cmb-nam-hoc').val(year);
		    $('#cmb-nam-hoc').select2().trigger('change');
		    
		    $('#cmb-thoi-gian').val(month);
		    $('#cmb-thang-filter').val(month);
		    
			if($('#cmb-nam-hoc-filter').val() !=null){
				layDanhSach(0);
			}
			
			if($('#cmb-thang-filter').val() !=null){
				layDanhSach(0);
			}
			
			$('#guiBcSgd').val(0);
			
			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
				$('#cmb-thang-filter').css("display","true");
			}
			else{
				$('#cmb-thang-filter').css("display","none");
			}
			},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$("#ds-1 > tbody > tr").remove();
			$("#ds-2 > tbody > tr").remove();
			$("#ds-3 > tbody > tr").remove();
			$("#ds-4 > tbody > tr").remove();
			$("#ds-5 > tbody > tr").remove();
			$("#ds-6 > tbody > tr").remove();
			$("#ds-7 > tbody > tr").remove();
			var tr1 = "";
			var tr2 = "";
			var tr3 = "";
			var tr4 = "";
			var tr5 = "";
			var tr6 = "";
			var tr7 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
				$.each(data.content, function(i,row){
						tr1 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.diemLe) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopK9) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsHs) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK6) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK7) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK8) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hsK9) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
				})
		    
			    $.each(data.content, function(i,row){
					tr2 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tsHs) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTot) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTot) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioi) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioi) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKha) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTb) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeu) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKem) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKem) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
							'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
					tr3 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK6) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKemK6) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
							'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
					tr4 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK7) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKemK7) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
							'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
					tr5 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK8) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKemK8) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
							'</tr>';
			    })
			    
			    $.each(data.content, function(i,row){
					tr6 += "<tr>" +
							"<td style='text-align:center'>" + (i+1) + "</td>" +
							"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hsK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTotK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTotK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hkYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHkYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlGioiK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlGioiK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKhaK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlTbK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlYeuK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.hlKemK9) + "</td>" +
							"<td style='text-align:right'>" + removeNull(row.tlHlKemK9) + "</td>" + "<td style='text-align:center'>" + removeNull(row.thang) + "</td>"  +getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
							'</tr>';
			    })
		    
			}
			else{
				tr1 = "<tr><td colspan='16' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr2 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr3 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr4 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr5 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr6 = "<tr><td colspan='23' style='text-align: center'>Không có dữ liệu</td></tr>";
			}
		    
			$('#ds-1 > tbody:last-child').append(tr1);
			$('#ds-2 > tbody:last-child').append(tr2);
			$('#ds-3 > tbody:last-child').append(tr3);
			$('#ds-4 > tbody:last-child').append(tr4);
			$('#ds-5 > tbody:last-child').append(tr5);
			$('#ds-6 > tbody:last-child').append(tr6);
			
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
				$('#box-ds').boxWidget('expand');
			});

			$('.row-edit-2').click(function(e){
				showError("Thông báo","Số liệu đã được gửi Sở giáo dục không được chỉnh sửa!");
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
			
			$('.row-reset-pwd-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCao('+$(this).attr('rid')+', 1)' );
				
			});
			
			$('.row-reset-pwd-2').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Phòng giáo dục?', 'baoCao('+$(this).attr('rid')+', 0)');
				
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				$('#box-ds').boxWidget('collapse');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
	$('#id').val('');
	$('#cmb-nam-hoc').val(year).trigger('change');
	$('#cmb-thoi-gian').val(month);
	$('#truong').val('');
	$('#lopK6').val('');
	$('#lopK7').val('');
	$('#lopK8').val('');
	$('#lopK9').val('');
	$('#tsLop').val('');
	$('#hsK6').val('');
	$('#hsK7').val('');
	$('#hsK8').val('');
	$('#hsK9').val('');
	$('#tsHs').val('');
	$('#guiBcSgd').val(0);
	$('#hkTot').val('');
	$('#tlHkTot').val('');
	$('#hkKha').val('');
	$('#tlHkKha').val('');
	$('#hkTb').val('');
	$('#tlHkTb').val('');
	$('#hkYeu').val('');
	$('#tlHkYeu').val('');
	$('#hlGioi').val('');
	$('#tlHlGioi').val('');
	$('#hlKha').val('');
	$('#tlHlKha').val('');
	$('#hlTb').val('');
	$('#tlHlTb').val('');
	$('#hlYeu').val('');
	$('#tlHlYeu').val('');
	$('#hlKem').val('');
	$('#tlHlKem').val('');
	$('#guiBcSgd').val('');
	$('#hkTotK6').val('');
	$('#tlHkTotK6').val('');
	$('#hkKhaK6').val('');
	$('#tlHkKhaK6').val('');
	$('#hkTbK6').val('');
	$('#tlHkTbK6').val('');
	$('#hkYeuK6').val('');
	$('#tlHkYeuK6').val('');
	$('#hlGioiK6').val('');
	$('#tlHlGioiK6').val('');
	$('#hlKhaK6').val('');
	$('#tlHlKhaK6').val('');
	$('#hlTbK6').val('');
	$('#tlHlTbK6').val('');
	$('#hlYeuK6').val('');
	$('#tlHlYeuK6').val('');
	$('#hlKemK6').val('');
	$('#tlHlKemK6').val('');
	$('#hkTotK7').val('');
	$('#tlHkTotK7').val('');
	$('#hkKhaK7').val('');
	$('#tlHkKhaK7').val('');
	$('#hkTbK7').val('');
	$('#tlHkTbK7').val('');
	$('#hkYeuK7').val('');
	$('#tlHkYeuK7').val('');
	$('#hlGioiK7').val('');
	$('#tlHlGioiK7').val('');
	$('#hlKhaK7').val('');
	$('#tlHlKhaK7').val('');
	$('#hlTbK7').val('');
	$('#tlHlTbK7').val('');
	$('#hlYeuK7').val('');
	$('#tlHlYeuK7').val('');
	$('#hlKemK7').val('');
	$('#tlHlKemK7').val('');
	$('#hkTotK8').val('');
	$('#tlHkTotK8').val('');
	$('#hkKhaK8').val('');
	$('#tlHkKhaK8').val('');
	$('#hkTbK8').val('');
	$('#tlHkTbK8').val('');
	$('#hkYeuK8').val('');
	$('#tlHkYeuK8').val('');
	$('#hlGioiK8').val('');
	$('#tlHlGioiK8').val('');
	$('#hlKhaK8').val('');
	$('#tlHlKhaK8').val('');
	$('#hlTbK8').val('');
	$('#tlHlTbK8').val('');
	$('#hlYeuK8').val('');
	$('#tlHlYeuK8').val('');
	$('#hlKemK8').val('');
	$('#tlHlKemK8').val('');
	$('#hkTotK9').val('');
	$('#tlHkTotK9').val('');
	$('#hkKhaK9').val('');
	$('#tlHkKhaK9').val('');
	$('#hkTbK9').val('');
	$('#tlHkTbK9').val('');
	$('#hkYeuK9').val('');
	$('#tlHkYeuK9').val('');
	$('#hlGioiK9').val('');
	$('#tlHlGioiK9').val('');
	$('#hlKhaK9').val('');
	$('#tlHlKhaK9').val('');
	$('#hlTbK9').val('');
	$('#tlHlTbK9').val('');
	$('#hlYeuK9').val('');
	$('#tlHlYeuK9').val('');
	$('#hlKemK9').val('');
	$('#tlHlKemK9').val('');
	$('#hsK6_1').val('');
	$('#hsK7_1').val('');
	$('#hsK8_1').val('');
	$('#hsK9_1').val('');
	$('#tsHs_1').val('');
	$('#diemLe').val('');
	$('#box-ds').boxWidget('collapse');
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id').val(data.id);
			$('#cmb-nam-hoc').val(data.nam).trigger('change');
			$('#cmb-thoi-gian').val(data.thang);
			$('#truong').val(data.truong);
			$('#lopK6').val(data.lopK6);
			$('#lopK7').val(data.lopK7);
			$('#lopK8').val(data.lopK8);
			$('#lopK9').val(data.lopK9);
			$('#tsLop').val(data.tsLop);
			$('#hsK6').val(data.hsK6);
			$('#hsK7').val(data.hsK7);
			$('#hsK8').val(data.hsK8);
			$('#hsK9').val(data.hsK9);
			$('#tsHs').val(data.tsHs);
			$('#guiBcSgd').val(data.guiBcSgd);
			$('#hkTot').val(data.hkTot);
			$('#tlHkTot').val(data.tlHkTot);
			$('#hkKha').val(data.hkKha);
			$('#tlHkKha').val(data.tlHkKha);
			$('#hkTb').val(data.hkTb);
			$('#tlHkTb').val(data.tlHkTb);
			$('#hkYeu').val(data.hkYeu);
			$('#tlHkYeu').val(data.tlHkYeu);
			$('#hlGioi').val(data.hlGioi);
			$('#tlHlGioi').val(data.tlHlGioi);
			$('#hlKha').val(data.hlKha);
			$('#tlHlKha').val(data.tlHlKha);
			$('#hlTb').val(data.hlTb);
			$('#tlHlTb').val(data.tlHlTb);
			$('#hlYeu').val(data.hlYeu);
			$('#tlHlYeu').val(data.tlHlYeu);
			$('#hlKem').val(data.hlKem);
			$('#tlHlKem').val(data.tlHlKem);
			$('#guiBcSgd').val(data.guiBcSgd);
			$('#hkTotK6').val(data.hkTotK6);
			$('#tlHkTotK6').val(data.tlHkTotK6);
			$('#hkKhaK6').val(data.hkKhaK6);
			$('#tlHkKhaK6').val(data.tlHkKhaK6);
			$('#hkTbK6').val(data.hkTbK6);
			$('#tlHkTbK6').val(data.tlHkTbK6);
			$('#hkYeuK6').val(data.hkYeuK6);
			$('#tlHkYeuK6').val(data.tlHkYeuK6);
			$('#hlGioiK6').val(data.hlGioiK6);
			$('#tlHlGioiK6').val(data.tlHlGioiK6);
			$('#hlKhaK6').val(data.hlKhaK6);
			$('#tlHlKhaK6').val(data.tlHlKhaK6);
			$('#hlTbK6').val(data.hlTbK6);
			$('#tlHlTbK6').val(data.tlHlTbK6);
			$('#hlYeuK6').val(data.hlYeuK6);
			$('#tlHlYeuK6').val(data.tlHlYeuK6);
			$('#hlKemK6').val(data.hlKemK6);
			$('#tlHlKemK6').val(data.tlHlKemK6);
			$('#hkTotK7').val(data.hkTotK7);
			$('#tlHkTotK7').val(data.tlHkTotK7);
			$('#hkKhaK7').val(data.hkKhaK7);
			$('#tlHkKhaK7').val(data.tlHkKhaK7);
			$('#hkTbK7').val(data.hkTbK7);
			$('#tlHkTbK7').val(data.tlHkTbK7);
			$('#hkYeuK7').val(data.hkYeuK7);
			$('#tlHkYeuK7').val(data.tlHkYeuK7);
			$('#hlGioiK7').val(data.hlGioiK7);
			$('#tlHlGioiK7').val(data.tlHlGioiK7);
			$('#hlKhaK7').val(data.hlKhaK7);
			$('#tlHlKhaK7').val(data.tlHlKhaK7);
			$('#hlTbK7').val(data.hlTbK7);
			$('#tlHlTbK7').val(data.tlHlTbK7);
			$('#hlYeuK7').val(data.hlYeuK7);
			$('#tlHlYeuK7').val(data.tlHlYeuK7);
			$('#hlKemK7').val(data.hlKemK7);
			$('#tlHlKemK7').val(data.tlHlKemK7);
			$('#hkTotK8').val(data.hkTotK8);
			$('#tlHkTotK8').val(data.tlHkTotK8);
			$('#hkKhaK8').val(data.hkKhaK8);
			$('#tlHkKhaK8').val(data.tlHkKhaK8);
			$('#hkTbK8').val(data.hkTbK8);
			$('#tlHkTbK8').val(data.tlHkTbK8);
			$('#hkYeuK8').val(data.hkYeuK8);
			$('#tlHkYeuK8').val(data.tlHkYeuK8);
			$('#hlGioiK8').val(data.hlGioiK8);
			$('#tlHlGioiK8').val(data.tlHlGioiK8);
			$('#hlKhaK8').val(data.hlKhaK8);
			$('#tlHlKhaK8').val(data.tlHlKhaK8);
			$('#hlTbK8').val(data.hlTbK8);
			$('#tlHlTbK8').val(data.tlHlTbK8);
			$('#hlYeuK8').val(data.hlYeuK8);
			$('#tlHlYeuK8').val(data.tlHlYeuK8);
			$('#hlKemK8').val(data.hlKemK8);
			$('#tlHlKemK8').val(data.tlHlKemK8);
			$('#hkTotK9').val(data.hkTotK9);
			$('#tlHkTotK9').val(data.tlHkTotK9);
			$('#hkKhaK9').val(data.hkKhaK9);
			$('#tlHkKhaK9').val(data.tlHkKhaK9);
			$('#hkTbK9').val(data.hkTbK9);
			$('#tlHkTbK9').val(data.tlHkTbK9);
			$('#hkYeuK9').val(data.hkYeuK9);
			$('#tlHkYeuK9').val(data.tlHkYeuK9);
			$('#hlGioiK9').val(data.hlGioiK9);
			$('#tlHlGioiK9').val(data.tlHlGioiK9);
			$('#hlKhaK9').val(data.hlKhaK9);
			$('#tlHlKhaK9').val(data.tlHlKhaK9);
			$('#hlTbK9').val(data.hlTbK9);
			$('#tlHlTbK9').val(data.tlHlTbK9);
			$('#hlYeuK9').val(data.hlYeuK9);
			$('#tlHlYeuK9').val(data.tlHlYeuK9);
			$('#hlKemK9').val(data.hlKemK9);
			$('#tlHlKemK9').val(data.tlHlKemK9);
			$('#hsK6_1').val(data.hsK6);
			$('#hsK7_1').val(data.hsK7);
			$('#hsK8_1').val(data.hsK8);
			$('#hsK9_1').val(data.hsK9);
			$('#tsHs_1').val(data.tsHs);
			$('#diemLe').val(data.diemLe);
			
			if(data.hkTotK6 != null || data.hlGioiK6 != null){
				$('#checkc2').prop('checked', true);
				$('#checkc2').trigger("change");
			}
			else{
				$('#checkc2').prop('checked', false);
				$('#checkc2').trigger("change");
			}
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCao(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}


