var pref_url="/nhap-so-lieu-mn";

const pgd = [
    { id: '1', name: 'PGD&ĐT TP. Cà Mau' }, 
    { id: '2', name: 'PGD&ĐT Thới Bình' }, 
    { id: '3', name: 'PGD&ĐT U Minh' }, 
    { id: '4', name: 'PGD&ĐT Trần Văn Thời' }, 
    { id: '5', name: 'PGD&ĐT Cái Nước' }, 
    { id: '6', name: 'PGD&ĐT Phú Tân' },
    { id: '7', name: 'PGD&ĐT Đầm Dơi' },
    { id: '8', name: 'PGD&ĐT Năm Căn' },
    { id: '9', name: 'PGD&ĐT Ngọc Hiển' }
];

function getTenPgd(id){
	var tenPgd =  pgd.filter(function(item) {
		return item.id == id;
	});
	var result = tenPgd.map(({ name }) => name)[0];
	return result == undefined ?"":  result;
}

function getBtnThaoTac(idDonVi, tt, id){
	var btn = "";
	if(idDonVi == 10 || idDonVi == 11){
		var btn = tt === 1 ? "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-2' rid="+id+" title='Trả báo cáo Phòng GD'><i class='fa fa-reply-all'></i></a>" : "<td></td>";
	}else{
		var btn = tt === 1 ? "<td></td>" : "<td style='text-align: center; width: auto;'><a href='#' class='row-reset-pwd-1' rid="+id+" title='Gửi báo cáo Sở GD'><i class='fa fa-reply-all'></i></a>&nbsp;&nbsp;<a href='#' class='row-edit-1' rid="+id+" title='Sửa' style=''><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='row-del-1 text-danger' rid="+id+" title='Xóa'><i class='fa fa-trash-o'></i></a></td>";
	}
	return btn;
}

function removeNull(val){
	return val == null ? "" : val; 
}

$(document).ready(function(){
	$('[data-toggle="push-menu"]').pushMenu('toggle');
	
	$('input').keyup(function(e)
            {
		if (/\D/g.test(this.value))
		{
		// Filter non-digits from input value.
		this.value = this.value.replace(/\D/g, '');
		}
	});
	
//	$("table").css({
//		"border": "1px solid #9d9d9d",
//		
//	});
//	$("th").css({
//		"border": "1px solid #9d9d9d",
//		
//	});
//	$("td").css({
//		"border": "1px solid #9d9d9d",
//		"padding-left":"2px",
//		"padding-right":"2px",
//	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#cmb-nam-hoc-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#cmb-thang-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#showBox').click(function(){
		$('#box-ds').boxWidget('toggle');
	});
	
	//So lieu truong
	jQuery('#truongClMn').on('input', function() {
		var truongClMn = layGiaTriInput("truongClMn");
	    var truongClMg = layGiaTriInput("truongClMg");
	    var truongTtMn = layGiaTriInput("truongTtMn");
	    var truongTtMg = layGiaTriInput("truongTtMg");
	    var kq = truongClMn + truongClMg;
	    $('#tsTruongCl').val(kq);
	    
	    var tsChungTruong =  layGiaTriInput("tsTruongCl") + layGiaTriInput("tsTruongTt");
	    $('#tsChungTruong').val(tsChungTruong);
	});
	
	
	jQuery('#truongClMg').on('input', function() {
		var truongClMn = layGiaTriInput("truongClMn");
	    var truongClMg = layGiaTriInput("truongClMg");
	    var truongTtMn = layGiaTriInput("truongTtMn");
	    var truongTtMg = layGiaTriInput("truongTtMg");
	    var kq = truongClMn + truongClMg;
	    $('#tsTruongCl').val(kq);
	    
	    var tsChungTruong =  layGiaTriInput("tsTruongCl") + layGiaTriInput("tsTruongTt");
	    $('#tsChungTruong').val(tsChungTruong);
	});
	
	jQuery('#truongTtMn').on('input', function() {
		var truongClMn = layGiaTriInput("truongClMn");
	    var truongClMg = layGiaTriInput("truongClMg");
		var truongTtMn = layGiaTriInput("truongTtMn");
	    var truongTtMg = layGiaTriInput("truongTtMg");
	    var kq = truongTtMn+ truongTtMg;
	    $('#tsTruongTt').val(kq);
	    
	    var tsChungTruong =  layGiaTriInput("tsTruongCl") + layGiaTriInput("tsTruongTt");
	    $('#tsChungTruong').val(tsChungTruong);
	});
	
	jQuery('#truongTtMg').on('input', function() {
		var truongClMn = layGiaTriInput("truongClMn");
	    var truongClMg = layGiaTriInput("truongClMg");
		var truongTtMn = layGiaTriInput("truongTtMn");
	    var truongTtMg = layGiaTriInput("truongTtMg");
	    var kq = truongTtMn+ truongTtMg;
	    $('#tsTruongTt').val(kq);
	    
	    var tsChungTruong =  layGiaTriInput("tsTruongCl") + layGiaTriInput("tsTruongTt");
	    $('#tsChungTruong').val(tsChungTruong);
	});
	
	//So lieu lop
	jQuery('#lopClNt').on('input', function() {
		var lopClNt = layGiaTriInput("lopClNt");
	    var lopClMg = layGiaTriInput("lopClMg");
	    var lopTtNt = layGiaTriInput("lopTtNt");
	    var lopTtMg = layGiaTriInput("lopTtMg");
	    var kq = lopClNt + lopClMg;
	    $('#tsLopCl').val(kq);
	    
	    var tsChungLop =  layGiaTriInput("tsLopCl") + layGiaTriInput("tsLopTt");
	    $('#tsChungLop').val(tsChungLop);
	});
	
	jQuery('#lopClMg').on('input', function() {
		var lopClNt = layGiaTriInput("lopClNt");
	    var lopClMg = layGiaTriInput("lopClMg");
	    var lopTtNt = layGiaTriInput("lopTtNt");
	    var lopTtMg = layGiaTriInput("lopTtMg");
	    var kq = lopClNt + lopClMg;
	    $('#tsLopCl').val(kq);
	    
	    var tsChungLop =  layGiaTriInput("tsLopCl") + layGiaTriInput("tsLopTt");
	    $('#tsChungLop').val(tsChungLop);
	});
	
	jQuery('#lopTtNt').on('input', function() {
		var lopClNt = layGiaTriInput("lopClNt");
	    var lopClMg = layGiaTriInput("lopClMg");
	    var lopTtNt = layGiaTriInput("lopTtNt");
	    var lopTtMg = layGiaTriInput("lopTtMg");
	    var kq = lopTtNt+ lopTtMg;
	    $('#tsLopTt').val(kq);
	    
	    var tsChungLop =  layGiaTriInput("tsLopCl") + layGiaTriInput("tsLopTt");
	    $('#tsChungLop').val(tsChungLop);
	});
	
	jQuery('#lopTtMg').on('input', function() {
		var lopClNt = layGiaTriInput("lopClNt");
	    var lopClMg = layGiaTriInput("lopClMg");
	    var lopTtNt = layGiaTriInput("lopTtNt");
	    var lopTtMg = layGiaTriInput("lopTtMg");
	    var kq = lopTtNt+ lopTtMg;
	    $('#tsLopTt').val(kq);
	    
	    var tsChungLop =  layGiaTriInput("tsLopCl") + layGiaTriInput("tsLopTt");
	    $('#tsChungLop').val(tsChungLop);
	});
	
	//So lieu tre
	jQuery('#treClNt').on('input', function() {
		var treClNt = layGiaTriInput("treClNt");
	    var treClMg = layGiaTriInput("treClMg");
	    var treTtNt = layGiaTriInput("treTtNt");
	    var treTtMg = layGiaTriInput("treTtMg");
	    var kq = treClNt + treClMg;
	    $('#tsTreCl').val(kq);
	    
	    var tsChungTre =  layGiaTriInput("tsTreCl") + layGiaTriInput("tsTreTt");
	    $('#tsChungTre').val(tsChungTre);
	});
	
	jQuery('#treClMg').on('input', function() {
		var treClNt = layGiaTriInput("treClNt");
	    var treClMg = layGiaTriInput("treClMg");
	    var treTtNt = layGiaTriInput("treTtNt");
	    var treTtMg = layGiaTriInput("treTtMg");
	    var kq = treClNt + treClMg;
	    $('#tsTreCl').val(kq);
	    
	    var tsChungTre =  layGiaTriInput("tsTreCl") + layGiaTriInput("tsTreTt");
	    $('#tsChungTre').val(tsChungTre);
	});
	
	jQuery('#treTtNt').on('input', function() {
		var treClNt = layGiaTriInput("treClNt");
	    var treClMg = layGiaTriInput("treClMg");
	    var treTtNt = layGiaTriInput("treTtNt");
	    var treTtMg = layGiaTriInput("treTtMg");
	    var kq = treTtNt+ treTtMg;
	    $('#tsTreTt').val(kq);
	    
	    var tsChungTre =  layGiaTriInput("tsTreCl") + layGiaTriInput("tsTreTt");
	    $('#tsChungTre').val(tsChungTre);
	});
	
	jQuery('#treTtMg').on('input', function() {
		var treClNt = layGiaTriInput("treClNt");
	    var treClMg = layGiaTriInput("treClMg");
	    var treTtNt = layGiaTriInput("treTtNt");
	    var treTtMg = layGiaTriInput("treTtMg");
	    var kq = treTtNt+ treTtMg;
	    $('#tsTreTt').val(kq);
	    
	    var tsChungTre =  layGiaTriInput("tsTreCl") + layGiaTriInput("tsTreTt");
	    $('#tsChungTre').val(tsChungTre);
	});
	
	//So lieu CBQL - GV - NV
	jQuery('#cbqlCl').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = cbqlCl+ cbqlTt;
	    $('#tsCbql').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#cbqlTt').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = cbqlCl+ cbqlTt;
	    $('#tsCbql').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#gvclNt').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = gvclNt+ gvclMg;
	    $('#tsGvcl').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#gvclMg').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = gvclNt+ gvclMg;
	    $('#tsGvcl').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#gvttNt').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = gvttNt+ gvttMg;
	    $('#tsGvtt').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#gvttMg').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = gvttNt+ gvttMg;
	    $('#tsGvtt').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#nvCl').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = nvCl+ nvTt;
	    $('#tsNv').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	jQuery('#nvTt').on('input', function() {
		var cbqlCl = layGiaTriInput("cbqlCl");
	    var cbqlTt = layGiaTriInput("cbqlTt");
	    
	    var gvclNt = layGiaTriInput("gvclNt");
	    var gvclMg = layGiaTriInput("gvclMg");
	    
	    var gvttNt = layGiaTriInput("gvttNt");
	    var gvttMg = layGiaTriInput("gvttMg");
	    
	    var nvCl = layGiaTriInput("nvCl");
	    var nvTt = layGiaTriInput("nvTt");
	    
	    var kq = nvCl+ nvTt;
	    $('#tsNv').val(kq);
	    
	    var tsChungCbGvNv =  layGiaTriInput("tsCbql") + layGiaTriInput("tsGvcl") + layGiaTriInput("tsGvtt") + layGiaTriInput("tsNv") ;
	    $('#tsChungCbGvNv').val(tsChungCbGvNv);
	});
	
	//CSVC
	jQuery('#phKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var phKienCoTt = layGiaTriInput("phKienCoTt");
	    var phBanKienCoTt = layGiaTriInput("phBanKienCoTt");
	    var phTamTt = layGiaTriInput("phTamTt");
	    
	    var kqcl = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongCl').val(kqcl);
	    
	    var kqtt = phKienCoTt+ phBanKienCoTt+phTamTt;
	    $('#tsPhongTt').val(kqtt);
	    
	    var tsPhongHoc =  layGiaTriInput("tsPhongCl")+ layGiaTriInput("tsPhongTt");
	    $('#tsPhongHoc').val(tsPhongHoc);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phBanKienCo').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var phKienCoTt = layGiaTriInput("phKienCoTt");
	    var phBanKienCoTt = layGiaTriInput("phBanKienCoTt");
	    var phTamTt = layGiaTriInput("phTamTt");
	    
	    var kqcl = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongCl').val(kqcl);
	    
	    var kqtt = phKienCoTt+ phBanKienCoTt+phTamTt;
	    $('#tsPhongTt').val(kqtt);
	    
	    var tsPhongHoc =  layGiaTriInput("tsPhongCl")+ layGiaTriInput("tsPhongTt");
	    $('#tsPhongHoc').val(tsPhongHoc);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phTam').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var phKienCoTt = layGiaTriInput("phKienCoTt");
	    var phBanKienCoTt = layGiaTriInput("phBanKienCoTt");
	    var phTamTt = layGiaTriInput("phTamTt");
	    
	    var kqcl = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongCl').val(kqcl);
	    
	    var kqtt = phKienCoTt+ phBanKienCoTt+phTamTt;
	    $('#tsPhongTt').val(kqtt);
	    
	    var tsPhongHoc =  layGiaTriInput("tsPhongCl")+ layGiaTriInput("tsPhongTt");
	    $('#tsPhongHoc').val(tsPhongHoc);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phKienCoTt').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var phKienCoTt = layGiaTriInput("phKienCoTt");
	    var phBanKienCoTt = layGiaTriInput("phBanKienCoTt");
	    var phTamTt = layGiaTriInput("phTamTt");
	    
	    var kqcl = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongCl').val(kqcl);
	    
	    var kqtt = phKienCoTt+ phBanKienCoTt+phTamTt;
	    $('#tsPhongTt').val(kqtt);
	    
	    var tsPhongHoc =  layGiaTriInput("tsPhongCl")+ layGiaTriInput("tsPhongTt");
	    $('#tsPhongHoc').val(tsPhongHoc);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phBanKienCoTt').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var phKienCoTt = layGiaTriInput("phKienCoTt");
	    var phBanKienCoTt = layGiaTriInput("phBanKienCoTt");
	    var phTamTt = layGiaTriInput("phTamTt");
	    
	    var kqcl = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongCl').val(kqcl);
	    
	    var kqtt = phKienCoTt+ phBanKienCoTt+phTamTt;
	    $('#tsPhongTt').val(kqtt);
	    
	    var tsPhongHoc =  layGiaTriInput("tsPhongCl")+ layGiaTriInput("tsPhongTt");
	    $('#tsPhongHoc').val(tsPhongHoc);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#phTamTt').on('input', function() {
		var phKienCo = layGiaTriInput("phKienCo");
	    var phBanKienCo = layGiaTriInput("phBanKienCo");
	    var phTam = layGiaTriInput("phTam");
	    
	    var phKienCoTt = layGiaTriInput("phKienCoTt");
	    var phBanKienCoTt = layGiaTriInput("phBanKienCoTt");
	    var phTamTt = layGiaTriInput("phTamTt");
	    
	    var kqcl = phKienCo+ phBanKienCo+phTam;
	    $('#tsPhongCl').val(kqcl);
	    
	    var kqtt = phKienCoTt+ phBanKienCoTt+phTamTt;
	    $('#tsPhongTt').val(kqtt);
	    
	    var tsPhongHoc =  layGiaTriInput("tsPhongCl")+ layGiaTriInput("tsPhongTt");
	    $('#tsPhongHoc').val(tsPhongHoc);
	    
	    var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
	    $('#tsPhong').val(tsPhong);
	});
	
	
	jQuery('#plvKienCo').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvKienCoTt = layGiaTriInput("plvKienCoTt");
		 var plvBanKienCoTt = layGiaTriInput("plvBanKienCoTt");
		 var plvTamTt = layGiaTriInput("plvTamTt");
		    
		 var kqcl = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViecCl').val(kqcl);
		 
		 var kqtt = plvKienCoTt + plvBanKienCoTt+plvTamTt;
		 $('#tsPhongLamViecTt').val(kqtt);
		 
		 var tsPhongLamViec = layGiaTriInput("tsPhongLamViecCl") + layGiaTriInput("tsPhongLamViecTt") ;
		 $('#tsPhongLamViec').val(tsPhongLamViec);
		 
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvBanKienCo').on('input', function() {
		var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvKienCoTt = layGiaTriInput("plvKienCoTt");
		 var plvBanKienCoTt = layGiaTriInput("plvBanKienCoTt");
		 var plvTamTt = layGiaTriInput("plvTamTt");
		    
		 var kqcl = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViecCl').val(kqcl);
		 
		 var kqtt = plvKienCoTt + plvBanKienCoTt+plvTamTt;
		 $('#tsPhongLamViecTt').val(kqtt);
		 
		 var tsPhongLamViec = layGiaTriInput("tsPhongLamViecCl") + layGiaTriInput("tsPhongLamViecTt") ;
		 $('#tsPhongLamViec').val(tsPhongLamViec);
		 
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvTam').on('input', function() {
		var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvKienCoTt = layGiaTriInput("plvKienCoTt");
		 var plvBanKienCoTt = layGiaTriInput("plvBanKienCoTt");
		 var plvTamTt = layGiaTriInput("plvTamTt");
		    
		 var kqcl = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViecCl').val(kqcl);
		 
		 var kqtt = plvKienCoTt + plvBanKienCoTt+plvTamTt;
		 $('#tsPhongLamViecTt').val(kqtt);
		 
		 var tsPhongLamViec = layGiaTriInput("tsPhongLamViecCl") + layGiaTriInput("tsPhongLamViecTt") ;
		 $('#tsPhongLamViec').val(tsPhongLamViec);
		 
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvKienCoTt').on('input', function() {
		 var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvKienCoTt = layGiaTriInput("plvKienCoTt");
		 var plvBanKienCoTt = layGiaTriInput("plvBanKienCoTt");
		 var plvTamTt = layGiaTriInput("plvTamTt");
		    
		 var kqcl = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViecCl').val(kqcl);
		 
		 var kqtt = plvKienCoTt + plvBanKienCoTt+plvTamTt;
		 $('#tsPhongLamViecTt').val(kqtt);
		 
		 var tsPhongLamViec = layGiaTriInput("tsPhongLamViecCl") + layGiaTriInput("tsPhongLamViecTt") ;
		 $('#tsPhongLamViec').val(tsPhongLamViec);
		 
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvBanKienCoTt').on('input', function() {
		var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvKienCoTt = layGiaTriInput("plvKienCoTt");
		 var plvBanKienCoTt = layGiaTriInput("plvBanKienCoTt");
		 var plvTamTt = layGiaTriInput("plvTamTt");
		    
		 var kqcl = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViecCl').val(kqcl);
		 
		 var kqtt = plvKienCoTt + plvBanKienCoTt+plvTamTt;
		 $('#tsPhongLamViecTt').val(kqtt);
		 
		 var tsPhongLamViec = layGiaTriInput("tsPhongLamViecCl") + layGiaTriInput("tsPhongLamViecTt") ;
		 $('#tsPhongLamViec').val(tsPhongLamViec);
		 
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	jQuery('#plvTamTt').on('input', function() {
		var plvKienCo = layGiaTriInput("plvKienCo");
		 var plvBanKienCo = layGiaTriInput("plvBanKienCo");
		 var plvTam = layGiaTriInput("plvTam");
		 var plvKienCoTt = layGiaTriInput("plvKienCoTt");
		 var plvBanKienCoTt = layGiaTriInput("plvBanKienCoTt");
		 var plvTamTt = layGiaTriInput("plvTamTt");
		    
		 var kqcl = plvKienCo+ plvBanKienCo+plvTam;
		 $('#tsPhongLamViecCl').val(kqcl);
		 
		 var kqtt = plvKienCoTt + plvBanKienCoTt+plvTamTt;
		 $('#tsPhongLamViecTt').val(kqtt);
		 
		 var tsPhongLamViec = layGiaTriInput("tsPhongLamViecCl") + layGiaTriInput("tsPhongLamViecTt") ;
		 $('#tsPhongLamViec').val(tsPhongLamViec);
		 
		 var tsPhong =  layGiaTriInput("tsPhongHoc") + layGiaTriInput("tsPhongLamViec") ;
		 $('#tsPhong').val(tsPhong);
	});
	
	//bổ sung ngày 07/10/2020
	
	jQuery('#lopNamTuoiCl').on('input', function() {
		 var lopNamTuoiCl = layGiaTriInput("lopNamTuoiCl");
		 var lopNamTuoiTt = layGiaTriInput("lopNamTuoiTt");
		    
		 var kq = lopNamTuoiCl+ lopNamTuoiTt;
		 $('#namTuoi').val(kq);
	});
	
	jQuery('#lopNamTuoiTt').on('input', function() {
		 var lopNamTuoiCl = layGiaTriInput("lopNamTuoiCl");
		 var lopNamTuoiTt = layGiaTriInput("lopNamTuoiTt");
		    
		 var kq = lopNamTuoiCl+ lopNamTuoiTt;
		 $('#namTuoi').val(kq);
	});
	
	jQuery('#treNamTuoiCl').on('input', function() {
		 var treNamTuoiCl = layGiaTriInput("treNamTuoiCl");
		 var treNamTuoiTt = layGiaTriInput("treNamTuoiTt");
		    
		 var kq = treNamTuoiCl+ treNamTuoiTt;
		 $('#treNamTuoi').val(kq);
	});
	
	jQuery('#treNamTuoiTt').on('input', function() {
		 var treNamTuoiCl = layGiaTriInput("treNamTuoiCl");
		 var treNamTuoiTt = layGiaTriInput("treNamTuoiTt");
		    
		 var kq = treNamTuoiCl+ treNamTuoiTt;
		 $('#treNamTuoi').val(kq);
	});
	
	initForm();
});

function layGiaTriInput(tenInput){
	return $('#'+tenInput+'').val() != "" ? parseInt($('#'+tenInput+'').val()) : 0;
}

function check(){
	if($('#cmb-nam-hoc').val()==''){
		showError('Thông báo','Vui lòng chọn năm học!');
		return false;
	}
	if($('#cmb-thoi-gian').val()==''){
		showError('Thông báo','Vui lòng chọn tháng!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			
			// Return today's date and time
			var currentTime = new Date();

			// returns the month (from 0 to 11)
			var month = currentTime.getMonth() + 1;

			// returns the year (four digits)
			var year = currentTime.getFullYear();
			
			var namhoc=$.map(data.namhoc,function(obj){
				obj.id=obj.giaTri;
				obj.text=obj.giaTri;
				return obj;
			});
			
			
			$('#cmb-nam-hoc,#cmb-nam-hoc-filter').select2({
				data: namhoc
			});
			
			$('#cmb-nam-hoc').val(year);
		    $('#cmb-nam-hoc').select2().trigger('change');
		    
		    $('#cmb-thoi-gian').val(month);
		    $('#cmb-thang-filter').val(month);
		    
			if($('#cmb-nam-hoc-filter').val() !=null){
				layDanhSach(0);
			}
			
			if($('#cmb-thang-filter').val() !=null){
				layDanhSach(0);
			}
			
			$('#guiBcSgd').val(0);
			
			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
				$('#cmb-thang-filter').css("display","true");
			}
			else{
				$('#cmb-thang-filter').css("display","none");
			}
			},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#idPhongGd').val(),nam:$('#cmb-nam-hoc-filter').val(), thang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){

			$("#ds-1 > tbody > tr").remove();
			$("#ds-2 > tbody > tr").remove();
			$("#ds-3 > tbody > tr").remove();
			var tr = "";
			var tr2 = "";
			var tr3 = "";
			var idDv = $('#idPhongGd').val();
			if(typeof data.content !== 'undefined' && data.content.length > 0){
				$.each(data.content, function(i,row){
						tr += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsTruongCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truongClMn) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truongClMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.diemLe) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.hocNho) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsTruongTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truongTtMn) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.truongTtMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.coSoHai) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.ttNhomLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsChungTruong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopClNt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopClMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLopCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopTtNt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopTtMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsLopTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsChungLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopNhomLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopNamTuoiCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.lopNamTuoiTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.namTuoi) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
		    })
		    
		    $.each(data.content, function(i,row){
				tr2 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsTreCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treClNt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treClMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsTreTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treTtNt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treTtMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsChungTre) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treNhomLop) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treNamTuoiCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treNamTuoiTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.treNamTuoi) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.cbqlCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.cbqlTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsCbql) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gvclNt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gvclMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsGvcl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gvttNt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gvttMg) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsGvtt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.nvCl) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.nvTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsNv) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsChungCbGvNv) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.gvNhomLop) + "</td>" + 
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
		    })
		    
		     $.each(data.content, function(i,row){
				tr3 += "<tr>" +
						"<td style='text-align:center'>" + (i+1) + "</td>" +
						"<td>" + getTenPgd(row.idPhongGd) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhong) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.tsPhongHoc) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCo+row.phBanKienCo+row.phTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCoTt+row.phBanKienCoTt+row.phTamTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phKienCoTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phBanKienCoTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.phTamTt) + "</td>" +
						
						"<td style='text-align:right'>" + 
						(row.thang < 11 && row.nam == 2020 ? removeNull(row.tsPhongLamViec-row.plvNhaCongVu) : removeNull(row.tsPhongLamViec))
						+ "</td>" +
						
						"<td style='text-align:right'>" + removeNull(row.plvKienCo+row.plvBanKienCo+row.plvTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvBanKienCo) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvTam) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvKienCoTt+row.plvBanKienCoTt+row.plvTamTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvKienCoTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvBanKienCoTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvTamTt) + "</td>" +
						"<td style='text-align:right'>" + removeNull(row.plvNhaCongVu) + "</td>" +
						"<td style='text-align:center'>" + removeNull(row.thang) + "</td>" +
						getBtnThaoTac(idDv,row.guiBcSgd, row.id) +
						'</tr>';
		    })
		    
			}
			else{
				tr = "<tr><td colspan='26' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr2 = "<tr><td colspan='29' style='text-align: center'>Không có dữ liệu</td></tr>";
				tr3 = "<tr><td colspan='24' style='text-align: center'>Không có dữ liệu</td></tr>";
			}
		    
			$('#ds-1 > tbody:last-child').append(tr);
			$('#ds-2 > tbody:last-child').append(tr2);
			$('#ds-3 > tbody:last-child').append(tr3);
			
			
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				$('#box-ds').boxWidget('expand');
				layChiTiet($(this).attr('rid'));
			});

			$('.row-edit-2').click(function(e){
				showError("Thông báo","Số liệu đã được gửi Sở giáo dục không được chỉnh sửa!");
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
			
			$('.row-reset-pwd-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý gửi số liệu báo cáo Sở giáo dục?', 'baoCao('+$(this).attr('rid')+', 1)' );
				
			});
			
			$('.row-reset-pwd-2').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn đồng ý trả số liệu báo cáo cho Phòng giáo dục?', 'baoCao('+$(this).attr('rid')+', 0)');
				
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
				$('#box-ds').boxWidget('collapse');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	// Return today's date and time
	var currentTime = new Date();

	// returns the month (from 0 to 11)
	var month = currentTime.getMonth() + 1;

	// returns the year (four digits)
	var year = currentTime.getFullYear();
	$('#id').val('');
	$('#cmb-nam-hoc').val(year).trigger('change');
	$('#cmb-thoi-gian').val(month);
	$('#truongClMn').val('');
	$('#truongClMg').val('');
	$('#tsTruongCl').val('');
	$('#truongTtMn').val('');
	$('#truongTtMg').val('');
	$('#tsTruongTt').val('');
	$('#tsChungTruong').val('');
	$('#lopClNt').val('');
	$('#lopClMg').val('');
	$('#tsLopCl').val('');
	$('#lopTtNt').val('');
	$('#lopTtMg').val('');
	$('#tsLopTt').val('');
	$('#tsChungLop').val('');
	$('#namTuoi').val('');
	$('#treClNt').val('');
	$('#treClMg').val('');
	$('#tsTreCl').val('');
	$('#treTtNt').val('');
	$('#treTtMg').val('');
	$('#tsTreTt').val('');
	$('#tsChungTre').val('');
	$('#treNamTuoi').val('');
	$('#cbqlCl').val('');
	$('#cbqlTt').val('');
	$('#tsCbql').val('');
	$('#gvclNt').val('');
	$('#gvclMg').val('');
	$('#tsGvcl').val('');
	$('#gvttNt').val('');
	$('#gvttMg').val('');
	$('#tsGvtt').val('');
	$('#nvCl').val('');
	$('#nvTt').val('');
	$('#tsNv').val('');
	$('#tsChungCbGvNv').val('');
	$('#guiBcSgd').val(0);
	$('#tsPhong').val('');
	$('#tsPhongHoc').val('');
	$('#phKienCo').val('');
	$('#phBanKienCo').val('');
	$('#phTam').val('');
	$('#tsPhongLamViec').val('');
	$('#plvKienCo').val('');
	$('#plvBanKienCo').val('');
	$('#plvTam').val('');
	$('#plvNhaCongVu').val('');
	$('#diemLe').val('');
	
	//Bổ sung ngày 07/10/2011
	$('#hocNho').val('');
	$('#coSoHai').val('');
	$('#ttNhomLop').val('');
	$('#lopNhomLop').val('');
	$('#lopNamTuoiCl').val('');
	$('#lopNamTuoiTt').val('');
	$('#treNhomLop').val('');
	$('#treNamTuoiCl').val('');
	$('#treNamTuoiTt').val('');
	$('#gvNhomLop').val('');
	$('#phKienCoTt').val('');
	$('#phBanKienCoTt').val('');
	$('#phTamTt').val('');
	$('#plvKienCoTt').val('');
	$('#plvBanKienCoTt').val('');
	$('#plvTamTt').val('');
	$('#tsPhongCl').val('');
	$('#tsPhongTt').val('');
	$('#tsPhongLamViecCl').val('');
	$('#tsPhongLamViecTt').val('');
	$('#box-ds').boxWidget('collapse');
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#id').val(data.id);
			$('#cmb-nam-hoc').val(data.nam).trigger('change');
			$('#cmb-thoi-gian').val(data.thang);
			$('#truongClMn').val(data.truongClMn);
			$('#truongClMg').val(data.truongClMg);
			$('#tsTruongCl').val(data.tsTruongCl);
			$('#truongTtMn').val(data.truongTtMn);
			$('#truongTtMg').val(data.truongTtMg);
			$('#tsTruongTt').val(data.tsTruongTt);
			$('#tsChungTruong').val(data.tsChungTruong);
			$('#lopClNt').val(data.lopClNt);
			$('#lopClMg').val(data.lopClMg);
			$('#tsLopCl').val(data.tsLopCl);
			$('#lopTtNt').val(data.lopTtNt);
			$('#lopTtMg').val(data.lopTtMg);
			$('#tsLopTt').val(data.tsLopTt);
			$('#tsChungLop').val(data.tsChungLop);
			$('#namTuoi').val(data.namTuoi);
			$('#treClNt').val(data.treClNt);
			$('#treClMg').val(data.treClMg);
			$('#tsTreCl').val(data.tsTreCl);
			$('#treTtNt').val(data.treTtNt);
			$('#treTtMg').val(data.treTtMg);
			$('#tsTreTt').val(data.tsTreTt);
			$('#tsChungTre').val(data.tsChungTre);
			$('#treNamTuoi').val(data.treNamTuoi);
			$('#cbqlCl').val(data.cbqlCl);
			$('#cbqlTt').val(data.cbqlTt);
			$('#tsCbql').val(data.tsCbql);
			$('#gvclNt').val(data.gvclNt);
			$('#gvclMg').val(data.gvclMg);
			$('#tsGvcl').val(data.tsGvcl);
			$('#gvttNt').val(data.gvttNt);
			$('#gvttMg').val(data.gvttMg);
			$('#tsGvtt').val(data.tsGvtt);
			$('#nvCl').val(data.nvCl);
			$('#nvTt').val(data.nvTt);
			$('#tsNv').val(data.tsNv);
			$('#tsChungCbGvNv').val(data.tsChungCbGvNv);
			$('#guiBcSgd').val(data.guiBcSgd);
			$('#tsPhong').val(data.tsPhong);
			$('#tsPhongHoc').val(data.tsPhongHoc);
			$('#phKienCo').val(data.phKienCo);
			$('#phBanKienCo').val(data.phBanKienCo);
			$('#phTam').val(data.phTam);
			$('#tsPhongLamViec').val(data.tsPhongLamViec);
			$('#plvKienCo').val(data.plvKienCo);
			$('#plvBanKienCo').val(data.plvBanKienCo);
			$('#plvTam').val(data.plvTam);
			$('#plvNhaCongVu').val(data.plvNhaCongVu);
			$('#diemLe').val(data.diemLe);
			$('#hocNho').val(data.hocNho);
			$('#coSoHai').val(data.coSoHai);
			$('#ttNhomLop').val(data.ttNhomLop);
			$('#lopNhomLop').val(data.lopNhomLop);
			$('#lopNamTuoiCl').val(data.lopNamTuoiCl);
			$('#lopNamTuoiTt').val(data.lopNamTuoiTt);
			$('#treNhomLop').val(data.treNhomLop);
			$('#treNamTuoiCl').val(data.treNamTuoiCl);
			$('#treNamTuoiTt').val(data.treNamTuoiTt);
			$('#gvNhomLop').val(data.gvNhomLop);
			$('#phKienCoTt').val(data.phKienCoTt);
			$('#phBanKienCoTt').val(data.phBanKienCoTt);
			$('#phTamTt').val(data.phTamTt);
			$('#plvKienCoTt').val(data.plvKienCoTt);
			$('#plvBanKienCoTt').val(data.plvBanKienCoTt);
			$('#plvTamTt').val(data.plvTamTt);
			
			var tsPhongCl = data.phKienCo+data.phKienCo+data.phTam;
			var tsPhongTt = data.phKienCoTt+data.phKienCoTt+data.phTamTt;
			var tsPhongLamViecCl = data.plvKienCo+data.plvKienCo+data.plvTam;
			var tsPhongLamViecTt = data.plvKienCoTt+data.plvKienCoTt+data.plvTamTt;
			$('#tsPhongCl').val(tsPhongCl);
			$('#tsPhongTt').val(tsPhongTt);
			$('#tsPhongLamViecCl').val(tsPhongLamViecCl);
			$('#tsPhongLamViecTt').val(tsPhongLamViecTt);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function baoCao(id, tt){
	$.ajax({
		url:pref_url+'/bao-cao',
		method:'post',
		dataType:'json',
		data:{id:id,tt:tt},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
					toastInfo('Trả báo cáo số liệu cho Phòng giáo dục thành công!');
				}else{
					toastInfo('Gửi báo cáo số liệu cho Sở giáo dục thành công!');
				}
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if($('#idPhongGd').val() == 10 || $('#idPhongGd').val() == 11){
				showError('Thông báo','Trả báo cáo số liệu cho Phòng giáo dục không thành công, vui lòng thử lại sau!');
			}
			else{
				showError('Thông báo','Gửi báo cáo số liệu cho Sở giáo dục không thành công, vui lòng thử lại sau!');
			}
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

