var url_pref='thong-tin-nguoi-dung';

$(document).ready(function(){
	
	layNguoiDung();
	
	$('#btn-submit').click(function(){
		if(kiemTra()){
			luuNguoiDung();
		}
	});
	
	$('#btnCapNhat').click(function(e){
    	if(kiemTra2()){
    		if(kiemTraMatKhau()){
    			doiMatKhau();
    		}else{
    			showError('Thông báo','Nhập lại mật khẩu mới không đúng!');
    		}
    	}
    });
    
    $('#btnHuy').click(function(e){
    	$('#inputoldPass').val('');
		$('#inputnewPass').val('');
		$('#inputreNewPass').val('');
    });
	
});

function layNguoiDung(){
	$.ajax({
		url:url_pref+'/lay-nguoi-dung',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('.box.box-primary').showLoading();
		},
		success:function(data){
			$('#inputidNguoiDung').val(data.idNguoiDung);
			$('#inputtenNguoiDung').val(data.tenNguoiDung);
			$("#inputtenNguoiDung").prop('readonly', true);
			$('#inputhoTen').val(data.hoTen);
		},
		error:function(){
			showError('Lỗi lấy người dùng', 'Đã có lỗi xãy ra, vui lòng thử lại sau');
		},
		complete:function(){
			$('.box.box-primary').hideLoading();
		}
	});
}

function luuNguoiDung(){
	var upt=$('#inputidNguoiDung').val()!='';
	$.ajax({
		url:url_pref+'/luu-nguoi-dung',
		data:new FormData($('#frm-nguoi-dung')[0]),
		method:'post',
		dataType:'json',
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('.box.box-primary').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layNguoiDung(0);
				toastInfo('Cập nhật người dùng thành công');				
			}else
				showError('Lỗi cập nhật người dùng','Cập nhật người dùng không thành công, vui lòng thử lại sau');
		},
		error:function(){
			showError('Lỗi cập nhật người dùng','Đã có lỗi xãy ra, vui lòng thử lại sau');
		},
		complete:function(){
			$('.box.box-primary').hideLoading();
		}
	});
}

function kiemTra(){
	if($('#inputhoTen').val()===''){
		showError('Thông báo','Vui lòng nhập họ tên!');
		return false;
	}
	return true;
}

function kiemTra2(){	
	if($('#inputoldPass').val()===''){
		showError('Thông báo','Vui lòng nhập mật khẩu cũ!');
		return false;
	}
	if($('#inputnewPass').val()===''){
		showError('Thông báo','Vui lòng nhập mật khẩu mới!');
		return false;
	}
	if($('#inputreNewPass').val()===''){
		showError('Thông báo','Vui lòng nhập lại mật khẩu mới!');
		return false;
	}
	return true;
}

function kiemTraMatKhau(){
	var err=false;
	
	if($('#inputnewPass').val() != $('#inputreNewPass').val()){
		err=true;
		$('#inputreNewPass').val('');
		$('#inputreNewPass').focus();		
	}		
	return !err;
}

function doiMatKhau(){
	var matKhauCu= $('#inputoldPass').val();
	var matKhauMoi = $('#inputnewPass').val();
	$.ajax({
		url: url_pref+'/doi-mat-khau',
		dataType: 'json',
		method:'post',
		data:{'matKhauCu': matKhauCu, 'matKhauMoi':matKhauMoi},
		beforeSend: function(jqXHR, settings){
			$('.box.box-success').showLoading();
		},
		success: function(data, textStatus, jqXHR ){
			if(data.resCode>0){
				toastInfo('Đổi mật khẩu thành công');
			}
			else{
				showError('Lỗi đổi mật khẩu','Đổi mật khẩu không thành công, vui lòng thử lại sau!');
			}
		},
		complete: function(jqXHR, textStatus){
			$('.box.box-success').hideLoading();
			$('#btnHuy').click();
		},
		error: function(jqXHR, textStatus, errorThrown){
			showError('Lỗi đổi mật khẩu','Đã có lỗi xãy ra, vui lòng thử lại sau!');
		}
	});
}